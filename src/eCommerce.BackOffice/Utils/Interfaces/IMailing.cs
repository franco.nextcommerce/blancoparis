﻿using eCommerce.Services.Model;
using System.Collections.Generic;

namespace eCommerce.Backoffice.Utils.Interfaces
{
    public interface IMailing
    {
        void SetTemplatesDirectory(string templatesDirectory);

        void SendCustomerConfirmedMail(string Email);

        void SendCustomerRejectedMail(string Email);

        void SendDeliveryOrderGuideMail(DeliveryOrder deliveryOrder);

        void SendOrderResumeMail(Order order);

        void SendOrderPushMail(List<OrderDetail> orderdetails);
    }
}
