﻿using eCommerce.BackOffice.ViewModels;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using eCommerce.Services.Services.Accounting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class OrderPaymentsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index(int Id)
        {
            return View("OrderPaymentList", this.OrderPaymentToShow(Id));
        }

        private Order OrderPaymentToShow(int OrderId)
        {
            var order = modelEntities.Orders.FirstOrDefault(x => x.OrderId == OrderId);
            order.Transactions = order.Transactions.Where(x => x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso && x.Deleted == null).OrderByDescending(x => x.Date).ToList();

            return order;
        }

        [HttpGet]
        public ActionResult OrderPaymentUpdate(int Id, int OrderId)
        {
            CreateOrderPaymentViewModel viewModel = new CreateOrderPaymentViewModel();

            var order = modelEntities.Orders.FirstOrDefault(x => x.OrderId == OrderId && x.Deleted == null);
            var accounts = modelEntities.Accountings.Where(x => x.Active);

            var banks = modelEntities.Banks.Where(x => x.Deleted == null).ToList();
            viewModel.Banks = new SelectList(banks, "BankId", "Name");

            viewModel.OrderId = OrderId;
            
            if (Id != -1)
            {
                viewModel.Payment = modelEntities.Transactions.FirstOrDefault(x => x.TransactionId == Id);
                viewModel.Accounts = new SelectList(accounts, "AccountId", "Name", viewModel.Payment.AccountId);
                viewModel.Payment.Checks = viewModel.Payment.Checks.Where(x => x.Deleted == null).ToList();

                return View(viewModel);
            }

            viewModel.Accounts = new SelectList(accounts, "AccountId", "Name");
            viewModel.Payment = new Transaction
            {
                TransactionId = Id,
                OrderId = OrderId,
                Order = order,
                CurrencyId = order.CurrenyId
            };
            
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult CancelDebt(int Id, int CustomerId)
        {
            CreateOrderPaymentViewModel viewModel = new CreateOrderPaymentViewModel();
            
            var accounts = modelEntities.Accountings.Where(x => x.Active);
            var banks = modelEntities.Banks.Where(x => x.Deleted == null).ToList();
            
            var orders = modelEntities.Orders.Include("TransactionDetails")
                .Where(x => x.Deleted == null && x.OrderStatusId == (int)OrderStatusEnum.Confirmado && x.CustomerId == CustomerId && x.PaymentStatusId != (int)PaymentStatusEnum.Cobrada).ToList();
            
            var checks = modelEntities.Checks.Where(x => x.ChargeTransactionId == Id && x.Deleted == null).ToList();
            viewModel.Checks = checks;
            viewModel.Accounts = new SelectList(accounts, "AccountId", "Name");
            viewModel.Banks = new SelectList(banks, "BankId", "Name");
            viewModel.CustomerId = CustomerId;

            if (Id != -1)
            {
                var transaction = modelEntities.Transactions.Find(Id);
                viewModel.Payment = transaction;
                viewModel.Payment.TransactionDetails = viewModel.Payment.TransactionDetails.Where(x => x.Deleted == null && x.Amount > 0).ToList();
                viewModel.Accounts = new SelectList(accounts, "AccountId", "Name", viewModel.Payment.AccountId);
                viewModel.Payment.Checks = viewModel.Payment.Checks.Where(x => x.Deleted == null).ToList();

                viewModel.OrderList = viewModel.Payment.TransactionDetails.Select(x => x.Order).Distinct().ToList()
                .Select(x => new TransactionDetailViewModel
                {
                    Order = x,
                    PendingAmountToPay = x.OrderTotalWithShippingCost - x.TransactionDetails.Where(s => s.Transaction.Deleted == null && s.Deleted == null).Sum(s => s.Amount),
                    TransactionDetail = x.TransactionDetails.FirstOrDefault(s => s.TransactionId == Id)
                })
                .ToList();
            }
            else
            {
                viewModel.Payment = new Transaction()
                {
                    CurrencyId = (int)CurrencyEnum.Peso,
                    Date = DateTime.Today,
                    Checks = new List<Check>(),
                    TransactionDetails = new List<TransactionDetail>(),
                    Order = new Order()
                };
                viewModel.OrderList = orders.ToList()
                .Select(x => new TransactionDetailViewModel
                {
                    Order = x,
                    PendingAmountToPay = x.OrderTotalWithShippingCost - x.TransactionDetails.Where(s => s.Transaction.Deleted == null && s.Deleted == null).Sum(s => s.Amount),
                    TransactionDetail = new TransactionDetail()
                })
                .ToList();
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CancelDebt(CreateOrderPaymentViewModel viewModel, FormCollection form)
        {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        Transaction transaction = new Transaction();

                        if (viewModel.Payment.TransactionId > 0)
                            transaction = modelEntities.Transactions.Find(viewModel.Payment.TransactionId);
                        
                        transaction.Date = viewModel.Payment.Date;
                        transaction.AccountId = viewModel.Payment.AccountId.Value;
                        transaction.CurrencyId = (int)CurrencyEnum.Peso;
                        transaction.Observations = viewModel.Payment.Observations;
                        transaction.CustomerId = viewModel.CustomerId;

                        if (!string.IsNullOrEmpty(Request["Payment.Amount"]))
                        {
                            transaction.Amount = decimal.Parse(Request["Payment.Amount"].Replace(".", ","));
                        }

                        if (transaction.TransactionId <= 0)
                        {
                            transaction.TransactionTypeId = (int)TransactionTypeEnum.Ingreso;
                            transaction.TransactionCategoryId = int.Parse(ConfigurationManager.AppSettings["ChargesCategoryId"]);
                            transaction.Created = CurrentDate.Now;
                            transaction.CreatedBy = User.Identity.Name;
                            modelEntities.Transactions.Add(transaction);
                        }

                        modelEntities.SaveChanges();

                        viewModel.Payment = transaction;

                        List<TransactionDetail> transactionDetails = GetTransactionDetails(viewModel, form);

                        UpdateTransactionDetails(viewModel, transactionDetails);

                        //ValidateTransactionDetailsAmount(order.OrderId);

                        AddChecks(viewModel, transaction);

                        UpdateAccountBalance(transaction.AccountId.Value);

                        UpdateCustomerAccountBalance(viewModel.CustomerId);

                        UpdateOrdersPaymentStatus(transactionDetails);

                        dbContextTransaction.Commit();
                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al realizar el cobro.");
                return RedirectToAction("OrderPaymentUpdate", new { id = viewModel.Payment.TransactionId, OrderId = viewModel.Payment.OrderId });
            }

            if (viewModel.Payment.OrderId.HasValue)
                return RedirectToAction("Index", new { Id = viewModel.Payment.OrderId });
            else
                return RedirectToAction("Index", "CustomerAccounting", new { Id = viewModel.Payment.CustomerId });
        }

        private List<TransactionDetail> GetTransactionDetails(CreateOrderPaymentViewModel viewModel, FormCollection form)
        {
            List<TransactionDetail> items = new List<TransactionDetail>();

            bool create_new_item = false;
            TransactionDetail item = new TransactionDetail();
            foreach (string key in Request.Form.AllKeys)
            {
                if (key.Contains("_"))
                {
                    if (create_new_item)
                    {
                        item = new TransactionDetail();
                        create_new_item = false;
                    }
                    if (key.Contains("OrderAmountToPay"))
                    {
                        if (Request.Form[key] != null && Request.Form[key] != string.Empty)
                        {
                            var OrderId = int.Parse(key.Split('_')[1]);
                            var Order = modelEntities.Orders.Find(OrderId);

                            item.Amount = Convert.ToDecimal(Request.Form[key]);
                            item.Date = viewModel.Payment.Date;
                            item.TransactionId = viewModel.Payment.TransactionId;
                            item.OrderId = OrderId;

                            items.Add(item);
                            create_new_item = true;
                        }
                    }
                }
            }

            return items;
        }

        private void UpdateTransactionDetails(CreateOrderPaymentViewModel viewModel, List<TransactionDetail> transactionDetails)
        {
            List<TransactionDetail> transactionDetailsToAdd = new List<TransactionDetail>();

            Order Order = new Order();
            foreach (var item in transactionDetails)
            {
                Order = new Order();
                Order = modelEntities.Orders.Find(item.OrderId);
                var transactionDetail = modelEntities.TransactionDetails.FirstOrDefault(x => x.OrderId == item.OrderId && x.TransactionId == item.TransactionId && x.Deleted == null);

                if (transactionDetail != null)
                {
                    transactionDetail.Amount = item.Amount;
                    transactionDetail.Date = item.Date;
                }
                else
                {
                    if (item.Amount > 0)
                    {
                        item.Created = DateTime.Now;
                        item.CreatedBy = User.Identity.Name;
                        item.TransactionId = viewModel.Payment.TransactionId;
                        transactionDetailsToAdd.Add(item);
                    }
                }
            }

            modelEntities.TransactionDetails.AddRange(transactionDetailsToAdd);
            modelEntities.SaveChanges();
        }

        private void UpdateOrdersPaymentStatus(List<TransactionDetail> transactionDetails)
        {
            //Obtengo los Ids de las ventas a actualizarle el estado de cobro.
            var OrdersId = transactionDetails.Select(x => x.OrderId).ToArray();
            var ordersId = modelEntities.Orders.Where(x => OrdersId.Contains(x.OrderId)).Select(x => x.OrderId).Distinct().ToList();

            foreach (var orderId in ordersId)
            {
                UpdateOrderPaymentStatusValue(orderId);
            }

            foreach (var detail in transactionDetails)
            {
                UpdateOrderPaymentStatusValue(detail.OrderId);
            }
        }

        private void UpdateOrderPaymentStatusValue(int orderId)
        {
            //DeliveryOrder PaymentStatus Update
            var Order = modelEntities.Orders.Find(orderId);
            var paidAmount = Order.TransactionDetails.Where(x => x.Deleted == null && x.Transaction.Deleted == null).Select(x => x.Amount).DefaultIfEmpty(0).Sum();
            if (Order.OrderTotal <= paidAmount)
            {
                Order.PaymentStatusId = (int)PaymentStatusEnum.Cobrada;
            }
            else
            {
                Order.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;
            }

            modelEntities.SaveChanges();
        }

        [HttpPost]
        public ActionResult OrderPaymentUpdate(CreateOrderPaymentViewModel viewModel)
        {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        Transaction transaction = new Transaction();

                        if (viewModel.Payment.TransactionId > 0)
                            transaction = modelEntities.Transactions.SingleOrDefault(x => x.TransactionId == viewModel.Payment.TransactionId && x.Deleted == null);

                        transaction.Date = viewModel.Payment.Date;
                        transaction.AccountId = viewModel.Payment.AccountId.Value;
                        transaction.CurrencyId = viewModel.Payment.CurrencyId;
                        transaction.Observations = viewModel.Payment.Observations;
                        transaction.OrderId = viewModel.Payment.OrderId;
                        if (transaction.OrderId.HasValue)
                        {
                            var order = modelEntities.Orders.Find(transaction.OrderId.Value);
                            transaction.CustomerId = order.CustomerId;
                        }


                        if (!string.IsNullOrEmpty(Request["Payment.Amount"]))
                        {
                            transaction.Amount = decimal.Parse(Request["Payment.Amount"].Replace(".", string.Empty));   
                        }

                        if (transaction.TransactionId <= 0)
                        {
                            transaction.TransactionTypeId = (int)TransactionTypeEnum.Ingreso;
                            transaction.TransactionCategoryId = int.Parse(ConfigurationManager.AppSettings["ChargesCategoryId"]);
                            transaction.Created = CurrentDate.Now;
                            transaction.CreatedBy = User.Identity.Name;
                            modelEntities.Transactions.Add(transaction);
                        }

                        modelEntities.SaveChanges();

                        AddChecks(viewModel, transaction);

                        UpdateAccountBalance(transaction.AccountId.Value);

                        UpdateOrderPaymentStatus(transaction.OrderId.Value);

                        UpdateCustomerAccountBalance(transaction.Order.CustomerId);

                        dbContextTransaction.Commit();
                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al realizar el cobro.");
            }

            return RedirectToAction("Index", new { Id = viewModel.Payment.OrderId });
        }

        [HttpGet]
        public virtual PartialViewResult OrderPaymentDelete(int Id)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var transaction = modelEntities.Transactions.Include("Order").FirstOrDefault(x => x.TransactionId == Id && x.Deleted == null);
                    
                    transaction.Deleted = CurrentDate.Now;
                    transaction.DeletedBy = User.Identity.Name;
                    modelEntities.SaveChanges();

                    DeleteChecks(transaction.TransactionId);

                    UpdateOrderPaymentStatus(transaction.OrderId.Value);

                    UpdateAccountBalance(transaction.AccountId.Value);

                    UpdateCustomerAccountBalance(transaction.Order.CustomerId);
                    

                    dbContextTransaction.Commit();

                    return PartialView("_OrderPaymentList", this.OrderPaymentToShow(transaction.OrderId.Value).Transactions);
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        private void DeleteChecks(int transactionId)
        {
            var checks = modelEntities.Checks.Where(x => x.ChargeTransactionId == transactionId && x.Deleted == null).ToList();
            foreach (var item in checks)
            {
                item.Deleted = CurrentDate.Now;
                item.DeletedBy = User.Identity.Name;
            }
            modelEntities.SaveChanges();
        }

        private void UpdateOrderPaymentStatus(int orderId)
        {
            var order = modelEntities.Orders.FirstOrDefault(x => x.OrderId == orderId);
            if (order.hasPayments)
            {
                if (order.IsTotallyPaid)
                    order.PaymentStatusId = (int)PaymentStatusEnum.Cobrada;
               
            } else
            {
                order.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;
            }
            modelEntities.SaveChanges();
        }

        private void UpdateAccountBalance(int accountId)
        {
            AccountingService.UpdateAccountBalance(accountId, ref modelEntities);
        }

        /*private void UpdateAccountBalance(int accountId)
        {
            try
            {
                var account = modelEntities.Accountings.SingleOrDefault(x => x.AccountId == accountId);
                var transactions = modelEntities.Transactions.Where(x => x.AccountId == account.AccountId && x.Deleted == null).ToList();

                decimal ingresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso && x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso).Sum(x => x.Amount);
                decimal egresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso).Sum(x => x.Amount);
                account.Balance = ingresos - egresos;

                ingresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar && x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso).Sum(x => x.Amount);
                egresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso).Sum(x => x.Amount);
                account.BalanceUsd = ingresos - egresos;

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        private void UpdateCustomerAccountBalance(int customerId)
        {
            AccountingService.UpdateCustomerBalance(customerId, ref modelEntities);
        }

        private void AddChecks(CreateOrderPaymentViewModel viewModel, Transaction transaction)
        {
            if (viewModel.Checks == null)
                return;

            foreach (var check in viewModel.Checks)
            {
                if (!check.Active)
                {
                    var checkToDelete = modelEntities.Checks.FirstOrDefault(x => x.CheckId == check.CheckId);
                    checkToDelete.Deleted = CurrentDate.Now;
                    checkToDelete.DeletedBy = User.Identity.Name;
                    //modelEntities.Checks.Attach(check);
                    modelEntities.Entry(checkToDelete).State = System.Data.Entity.EntityState.Modified;
                }

                if (check.CheckId == -1)
                {
                    check.ChargeTransactionId = transaction.TransactionId;
                    check.Created = CurrentDate.Now;
                    check.CreatedBy = User.Identity.Name;
                    check.Own = false;
                    check.Status = (int)CheckStatus.EnCartera;

                    modelEntities.Checks.Add(check);
                    //modelEntities.Checks.Attach(check);
                    //modelEntities.Entry(check).State = System.Data.Entity.EntityState.Modified;
                }
            }

            modelEntities.SaveChanges();
        }
    }
}