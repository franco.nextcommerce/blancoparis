﻿using eCommerce.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class MPAccountsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        // GET: MPAccounts
        public ActionResult Index()
        {
            List<MPAccount> MPAccounts = modelEntities.MPAccounts.Where(x => x.Deleted == null).ToList();
            return View("MPAccountsList", MPAccounts);
        }

        [HttpGet]
        public ActionResult MPAccountUpdate(int Id)
        {
            var MPAccounts = modelEntities.MPAccounts.ToList();

            if (Id != -1)
            {
                var MPAccount = modelEntities.MPAccounts.FirstOrDefault(x => x.MPAccountId == Id);
                return View(MPAccount);
            }

            return View(new MPAccount { MPAccountId = Id });
        }

        [HttpPost]
        public ActionResult MPAccountUpdate(FormCollection form)
        {
            MPAccount MPAccount = new MPAccount();

            if (form["MPAccountId"] != string.Empty && Convert.ToInt16(form["MPAccountId"]) > 0)
            {
                int MPAccountId = Convert.ToInt16(form["MPAccountId"]);
                MPAccount = modelEntities.MPAccounts.FirstOrDefault(x => x.MPAccountId == MPAccountId && x.Deleted == null && x.Active);
            }

            TryUpdateModel(MPAccount, form);

            if (MPAccount.MPAccountId <= 0)
            {
                MPAccount.Active = true;
                MPAccount.Created = CurrentDate.Now;
                MPAccount.CreatedBy = User.Identity.Name;
                modelEntities.MPAccounts.Add(MPAccount);
            }

            modelEntities.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult MPAccountDelete(int Id)
        {
            var MPAccount = modelEntities.MPAccounts.FirstOrDefault(x => x.MPAccountId == Id && x.Active);
            if (MPAccount != null)
            {
                MPAccount.Active = false;
                MPAccount.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.MPAccounts.Where(x => x.Active).ToList();
            return PartialView("_MPAccountsList", list);
        }

        [HttpGet]
        public ActionResult MPAccountRelationship()
        {
            var MPAccountRelationships = modelEntities.MPAccountRelationships.ToList();
            var MPAccounts = modelEntities.MPAccounts.Where(x => x.Active && x.Deleted == null);
            ViewData["MPAccounts"] = new SelectList(MPAccounts, "MPAccountId", "Description");
            return View(MPAccountRelationships);
        }

        [HttpPost]
        public JsonResult MPAccountUpdateRelationship(int UserType, int IvaCondition, string Description)
        {
            var MPAccountId = modelEntities.MPAccounts.Where(x => x.Active && x.Deleted == null && x.Description == Description).Select(x => x.MPAccountId).FirstOrDefault();
            MPAccountRelationship relationship = new MPAccountRelationship();
            relationship.IvaConditionId = IvaCondition;
            relationship.MPAccountId = MPAccountId;
            relationship.UserType = UserType;

            var existingRelationship = modelEntities.MPAccountRelationships.Where(x => x.UserType == relationship.UserType && x.IvaConditionId == IvaCondition).FirstOrDefault();
            if (existingRelationship != null)
            {
                existingRelationship.IvaConditionId = IvaCondition;
                existingRelationship.UserType = UserType;
                existingRelationship.MPAccountId = MPAccountId;
            }
            else
            {
                modelEntities.MPAccountRelationships.Add(relationship);
            }
            modelEntities.SaveChanges();


            return Json(new { Message = "Success" }, JsonRequestBehavior.AllowGet);
        }
    }
}