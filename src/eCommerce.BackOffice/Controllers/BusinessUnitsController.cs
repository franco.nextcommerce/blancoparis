﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    ////[AuthorizeUser(Roles = "Admin")]
    public class BusinessUnitsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            List<BusinessUnit> BusinessUnits = modelEntities.BusinessUnits.Where(x => x.Active == true).ToList();
            return View("BusinessUnitList", BusinessUnits);
        }

        [HttpGet]
        public ActionResult BusinessUnitUpdate(int Id)
        {
            if (Id != -1)
            {
                var BusinessUnit = modelEntities.BusinessUnits.SingleOrDefault(x => x.BusinessUnitId == Id);
                return View(BusinessUnit);
            }

            return View(new BusinessUnit { BusinessUnitId = Id });
        }

        [HttpPost]
        public ActionResult BusinessUnitUpdate(FormCollection form)
        {
            BusinessUnit BusinessUnit = new BusinessUnit();

            if (form["BusinessUnitId"] != string.Empty && Convert.ToInt16(form["BusinessUnitId"]) > 0)
            {
                int BusinessUnitId = Convert.ToInt16(form["BusinessUnitId"]);
                BusinessUnit = modelEntities.BusinessUnits.SingleOrDefault(x => x.BusinessUnitId == BusinessUnitId && x.Active);
            }

            TryUpdateModel(BusinessUnit, form);

            if (BusinessUnit.BusinessUnitId <= 0)
            {
                BusinessUnit.Active = true;
                BusinessUnit.Created = CurrentDate.Now;
                BusinessUnit.CreatedBy = User.Identity.Name;
                modelEntities.BusinessUnits.Add(BusinessUnit);
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult BusinessUnitDelete(int Id)
        {
            var BusinessUnit = modelEntities.BusinessUnits.SingleOrDefault(x => x.BusinessUnitId == Id && x.Active);
            if (BusinessUnit != null)
            {
                BusinessUnit.Active = false;
                BusinessUnit.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.BusinessUnits.Where(x => x.Active).ToList();
            return PartialView("_BusinessUnitList", list);
        }
    }
}