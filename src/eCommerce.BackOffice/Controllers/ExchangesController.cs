﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using eCommerce.Services;
using System.Configuration;
using eCommerce.Services.Services.Accounting;

namespace eCommerce.BackOffice.Controllers
{
    public class ExchangesController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            return View("ExchangesList", this.ExchangesToShow());
        }

        private List<Exchange> ExchangesToShow()
        {
            return modelEntities.Exchanges.Where(x => x.Active && x.Deleted == null).OrderByDescending(x => x.ExchangeId).ToList();
        }

        [HttpGet]
        public ActionResult ExchangeUpdate(int Id)
        {
            var customers = modelEntities.Customers.Where(x => x.Active).OrderBy(x => x.FullName).ToList();
            var users = modelEntities.USERS.Where(x => x.Active && x.Deleted == null).ToList();
            var deposits = modelEntities.Deposits.Where(x => x.Deleted == null).ToList();

            var user = users.FirstOrDefault(x => x.Username == User.Identity.Name && x.Active && x.Deleted == null);
            if (user.ROLES.FirstOrDefault().RoleName != "SuperAdmin")
            {
                customers = customers.Where(x => x.SalerId == user.User_Id).ToList();
                deposits = deposits.Where(x => x.SalerDeposits.Any(s => s.SalerId == user.User_Id)).ToList();
            }

            var types = modelEntities.TransactionTypes.Select(x => new { ExchangeDetailTypeId = x.TransactionTypeId, Text = x.Description }).ToList();
            TempData["ExchangeDetailTypes"] = new SelectList(types, "ExchangeDetailTypeId", "Text");

            if (Id != -1)
            {
                var exchange = modelEntities.Exchanges.Find(Id);
                exchange.ExchangeDetails = exchange.ExchangeDetails.Where(x => x.Active).OrderBy(x => x.ExchangeDetailId).ToList();

                var productsId = exchange.ExchangeDetails.Select(x => x.ProductId).ToArray();
                TempData["Stocks"] = modelEntities.Stocks.Where(x => productsId.Contains(x.ProductId)).ToList();

                var products = modelEntities.Products.Select(x => new { ProductId = x.ProductId, Text = x.Code + " - " + x.Title }).ToList();
                TempData["Products"] = new SelectList(products, "ProductId", "Text");

                var sizes = modelEntities.Sizes.Select(x => new { SizeId = x.SizeId, Text = x.Description }).ToList();
                TempData["Sizes"] = new SelectList(sizes, "SizeId", "Text");

                var colors = modelEntities.Colors.Select(x => new { ColorId = x.ColorId, Text = x.Description }).ToList();
                TempData["Colors"] = new SelectList(colors, "ColorId", "Text");
                

                TempData["Customers"] = new SelectList(customers, "CustomerId", "FullName", exchange.CustomerId);
                TempData["Deposits"] = new SelectList(deposits, "DepositId", "Name", exchange.DepositId);
                return View(exchange);
            }

            TempData["Customers"] = new SelectList(customers, "CustomerId", "FullName");
            TempData["Deposits"] = new SelectList(deposits, "DepositId", "Name");

            return View(new Exchange { ExchangeId = Id, SalerId = user.User_Id });
        }

        [HttpPost]
        public ActionResult ExchangeUpdate(FormCollection form)
         {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        Exchange exchange = new Exchange();

                        StockService stockService = new StockService();

                        if (form["ExchangeId"] != string.Empty && Convert.ToInt16(form["ExchangeId"]) > 0)
                        {
                            int ExchangeId = Convert.ToInt16(form["ExchangeId"]);
                            exchange = modelEntities.Exchanges.Find(ExchangeId);
                            exchange.ExchangeDetails = exchange.ExchangeDetails.Where(x => x.Active).ToList();
                        }

                        TryUpdateModel(exchange, form);
                        
                        if (exchange.ExchangeId <= 0)
                        {
                            exchange.Active = true;
                            exchange.Created = CurrentDate.Now;
                            exchange.CreatedBy = User.Identity.Name;
                            modelEntities.Exchanges.Add(exchange);
                            modelEntities.SaveChanges();
                        }

                        var listItems = GetExchangeDetails(form, exchange.ExchangeId);
                        UpdateExchangeDetails(exchange, listItems, stockService);
                        
                        UpdateCustomerAccount(exchange);
                        UpdateCustomerAccountBalance(exchange.CustomerId);

                        modelEntities.SaveChanges();
                        dbContextTransaction.Commit();
                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar guardar el cambio.");
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual ActionResult GetExchangeDetailTypes()
        {
            var types = modelEntities.TransactionTypes
                .Select(x => new { x.TransactionTypeId, x.Description}).ToList();
            return Json(types, JsonRequestBehavior.AllowGet);
        }

        private List<ExchangeDetail> GetExchangeDetails(FormCollection form, int exchangeId)
        {
            List<ExchangeDetail> items = new List<ExchangeDetail>();

            bool create_new_item = false;
            ExchangeDetail item = new ExchangeDetail();
            foreach (string key in Request.Form.AllKeys)
            {
                if (key.Contains("_"))
                {
                    if (create_new_item)
                    {
                        item = new ExchangeDetail();
                        create_new_item = false;
                    }

                    if (key.Contains("Quantity"))
                        item.Quantity = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("ProductId"))
                        item.ProductId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("SizeId"))
                        item.SizeId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("ColorId"))
                        item.ColorId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("ExchangeDetailTypeId"))
                        item.ExchangeDetailTypeId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("Price"))
                        item.Price = Convert.ToDecimal(Request.Form[key]);
                    else if (key.Contains("ExchangeDetailId"))
                    {
                        item.ExchangeDetailId = Convert.ToInt32(Request.Form[key]);
                        item.ExchangeId = exchangeId;
                        items.Add(item);
                        create_new_item = true;
                    }
                }
            }

            return items;
        }

        private void UpdateExchangeDetails(Exchange exchange, List<ExchangeDetail> details, StockService stockService)
        {
            List<ExchangeDetail> exchangeDetailsToDelete = new List<ExchangeDetail>();
            List<ExchangeDetail> exchangeDetailsToUpdate = new List<ExchangeDetail>();
            List<DeliveryOrderDetail> deliveryOrderDetails = new List<DeliveryOrderDetail>();

            List<DeliveryOrderDetail> existingDeliveryOrderDetails = modelEntities.DeliveryOrderDetails
                .Where(x => x.DeliveryOrder.Deleted == null && x.DeliveryOrder.ExchangeId.HasValue && x.DeliveryOrder.ExchangeId.Value == exchange.ExchangeId)
                .ToList();

            DeliveryOrder deliveryOrder = new DeliveryOrder();
            deliveryOrder = modelEntities.DeliveryOrders.FirstOrDefault(x => x.ExchangeId == exchange.ExchangeId && x.Deleted == null);
            if (deliveryOrder == null)
            {
                //Creación DeliveryOrder
                deliveryOrder = new DeliveryOrder()
                {
                    ExchangeId = exchange.ExchangeId,
                    Date = exchange.Created.HasValue ? exchange.Created.Value : DateTime.Now,
                    Created = DateTime.Now,
                    CreatedBy = User.Identity.Name,
                    PaymentStatusId = (int)PaymentStatusEnum.Pagada
                };
                modelEntities.DeliveryOrders.Add(deliveryOrder);
            }

            DeliveryOrderDetail deliveryOrderDetail = new DeliveryOrderDetail();
            foreach (var detail in exchange.ExchangeDetails)
            {
                var item = details.SingleOrDefault(x => x.ExchangeDetailId == detail.ExchangeDetailId);

                if (item != null)
                {
                    detail.Quantity = item.Quantity;
                    detail.Price = item.Price;

                    deliveryOrderDetail = existingDeliveryOrderDetails.FirstOrDefault(x => x.ExchangeDetailId == item.ExchangeDetailId);
                    deliveryOrderDetail.Quantity = item.Quantity;
                    if (item.ExchangeDetailTypeId == (int)TransactionTypeEnum.Egreso)
                    {
                        deliveryOrderDetail.Quantity = deliveryOrderDetail.Quantity * (-1);
                    }
                    //deliveryOrderDetail.Amount = getProductPriceByCustomer(item.ProductId, exchange.CustomerId) * deliveryOrderDetail.Quantity;
                    deliveryOrderDetail.Amount = item.Price * deliveryOrderDetail.Quantity;
                }
                else
                {
                    detail.Active = false;
                    detail.Deleted = CurrentDate.Now;
                    detail.DeletedBy = User.Identity.Name;
                    exchangeDetailsToDelete.Add(detail);
                }
            }

            foreach (var item in details)
            {
                if (item.ExchangeDetailId == -1)
                {
                    item.Exchange = exchange;
                    item.Active = true;
                    item.Created = CurrentDate.Now;
                    item.CreatedBy = User.Identity.Name;
                    modelEntities.ExchangeDetails.Add(item);
                    modelEntities.SaveChanges();
                    exchangeDetailsToUpdate.Add(item);
                    var multiplier = item.ExchangeDetailTypeId == (int)TransactionTypeEnum.Ingreso ? -1 : 1;
                    deliveryOrderDetail = new DeliveryOrderDetail()
                    {
                        DeliveryOrderId = deliveryOrder.DeliveryOrderId,
                        ProductId = item.ProductId,
                        Quantity = item.Quantity * multiplier,                        
                        Amount = item.Price * item.Quantity * multiplier,
                        ExchangeDetailId = item.ExchangeDetailId,
                        DepositId = exchange.DepositId
                    };
                    modelEntities.DeliveryOrderDetails.Add(deliveryOrderDetail);
                }
                else
                {
                    var exchangeDetail = modelEntities.ExchangeDetails.FirstOrDefault(x => x.ExchangeDetailId == item.ExchangeDetailId);
                    exchangeDetailsToUpdate.Add(exchangeDetail);
                }

            }

            stockService.UpdateExchangeMovements(exchangeDetailsToUpdate, ref modelEntities);
            stockService.DeleteExchangeMovements(exchangeDetailsToDelete, ref modelEntities);

            DeleteExchangeDetails(exchangeDetailsToDelete);
            SetOrderDeliveryTotalAmount(deliveryOrder, exchange);
        }

        private void DeleteExchangeDetails(List<ExchangeDetail> exchangeOrderDetails)
        {
            if (exchangeOrderDetails.Count > 0)
            {
                List<DeliveryOrderDetail> deliveryOrderDetailsToRemove = new List<DeliveryOrderDetail>();
                foreach (var item in exchangeOrderDetails)
                {
                    var deliveryOrderDetail = modelEntities.DeliveryOrderDetails.FirstOrDefault(x => x.ExchangeDetailId == item.ExchangeDetailId);
                    deliveryOrderDetailsToRemove.Add(deliveryOrderDetail);
                }
                modelEntities.DeliveryOrderDetails.RemoveRange(deliveryOrderDetailsToRemove);
                modelEntities.ExchangeDetails.RemoveRange(exchangeOrderDetails);
                modelEntities.SaveChanges();
            }
        }

        public void SetOrderDeliveryTotalAmount(DeliveryOrder deliveryOrder, Exchange exchange)
        {
            decimal cost = 0;
            decimal total = 0;
            decimal item_total = 0;
            foreach (var item in deliveryOrder.DeliveryOrderDetails)
            {
                item_total = 0;
                item_total = item.Amount.Value;

                item.Amount = item_total;
                total += item_total;
            }

            deliveryOrder.Amount = total;
            exchange.Amount = total;

            modelEntities.SaveChanges();
        }

        private void UpdateCustomerAccount(Exchange exchange)
        {
            int exchangeCategoryId = int.Parse(ConfigurationManager.AppSettings["ExchangeCategoryId"]);

            /*
            decimal exchange_total = 0;
            foreach (var item in exchange.ExchangeDetails)
            {
                decimal price = 0;
                if (item.Price.HasValue)
                    price = item.Price.Value * item.Quantity;
                else
                    price = getProductPriceByCustomer(item.ProductId, item.Exchange.CustomerId) * item.Quantity;

                if (item.ExchangeDetailTypeId == (int)ExchangeDetailTypeEnum.Ingreso)
                    exchange_total += price;
                else if (item.ExchangeDetailTypeId == (int)ExchangeDetailTypeEnum.Egreso)
                    exchange_total -= price;
            }*/

            if (exchange.Transactions.Any(x => x.Deleted == null))
            {
                var transaction = exchange.Transactions.FirstOrDefault(x => x.Deleted == null);

                transaction.Amount = exchange.Amount.Value * -1;
                transaction.Date = CurrentDate.Now;
            }
            else
            {
                Transaction transaction = new Transaction();

                transaction.ExchangeId = exchange.ExchangeId;
                transaction.Amount = exchange.Amount.Value * -1;
                transaction.CustomerId = exchange.CustomerId;
                transaction.CurrencyId = (int)CurrencyEnum.Peso;
                transaction.TransactionTypeId = (int)TransactionTypeEnum.Egreso;
                transaction.TransactionCategoryId = exchangeCategoryId;
                transaction.Date = DateTime.Today;
                transaction.Created = CurrentDate.Now;
                transaction.CreatedBy = User.Identity.Name;
                modelEntities.Transactions.Add(transaction);
            }

            modelEntities.SaveChanges();
        }

        private decimal getProductPriceByCustomer(int productId, int customerId)
        {
            var product = modelEntities.Products.Find(productId);
            var customer = modelEntities.Customers.Find(customerId);
            switch (customer.PriceList)
            {
                case (int)PriceListEnum.Minorista:
                    return product.RetailPrice.HasValue ? product.RetailPrice.Value : 0;

                case (int)PriceListEnum.Mayorista:
                    return product.WholesalePrice.HasValue ? product.WholesalePrice.Value : 0;
            }
            return 0;
        }

        public ActionResult ExchangeDelete(int id)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var exchange = modelEntities.Exchanges.SingleOrDefault(x => x.ExchangeId == id && x.Deleted == null);

                    exchange.Deleted = CurrentDate.Now;
                    exchange.DeletedBy = User.Identity.Name;

                    StockService stockService = new StockService();
                    stockService.DeleteExchangeMovements(exchange.ExchangeDetails, ref modelEntities);

                    modelEntities.SaveChanges();

                    DeleteExchangeTransaction(exchange.ExchangeId);

                    DeleteExchangeDeliveryOrder(exchange.ExchangeId);

                    UpdateCustomerAccountBalance(exchange.CustomerId);

                    modelEntities.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                }
            }

            return PartialView("_ExchangeList", this.ExchangesToShow());
        }

        private void DeleteExchangeTransaction(int exchangeId)
        {
            try
            {
                var transaction = modelEntities.Transactions.FirstOrDefault(x => x.ExchangeId == exchangeId && x.Deleted == null);
                transaction.Deleted = CurrentDate.Now;
                transaction.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeleteExchangeDeliveryOrder(int exchangeId)
        {
            try
            {
                var deliveryOrder = modelEntities.DeliveryOrders.FirstOrDefault(x => x.ExchangeId == exchangeId && x.Deleted == null);
                deliveryOrder.Deleted = CurrentDate.Now;
                deliveryOrder.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateCustomerAccountBalance(int customerId)
        {
            AccountingService.UpdateCustomerBalance(customerId, ref modelEntities);
        }

        #region Print

        [HttpGet]
        public virtual ActionResult ExchangePrint(int Id)
        {
            var exchange = modelEntities.Exchanges.Find(Id);
            return View(exchange);
        }

        [HttpGet]
        public virtual ActionResult ExchangePrintLayout(int Id)
        {
            var exchange = modelEntities.Exchanges.Find(Id);
            return View(exchange);
        }

        [HttpGet]
        [ChildActionOnly]
        public virtual PartialViewResult ShowPrintExchangeView(Exchange exchange)
        {
            exchange.ExchangeDetails = exchange.ExchangeDetails.OrderBy(x => x.ExchangeDetailTypeId).ToList();
            return PartialView("_ExchangePrint", exchange);
        }

        #endregion
    }
}