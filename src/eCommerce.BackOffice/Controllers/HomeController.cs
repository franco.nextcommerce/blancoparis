﻿using System.Web.Mvc;
using eCommerce.Services.Model;

namespace eCommerce.BackOffice.Controllers
{
    public class HomeController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        public ActionResult Index()
        {
            RBACUser requestingUser = new RBACUser(HttpContext.User.Identity.Name);
            if (requestingUser.HasRole("Ventas"))
            {
                return RedirectToAction("Index", "Orders");
            }

            return RedirectToAction("Index", "Reports");
            
           
        }
    }
}