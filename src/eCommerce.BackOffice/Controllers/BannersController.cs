﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System.IO;
using System.Threading;
using eCommerce.Services.Model.Enums;

namespace eCommerce.BackOffice.Controllers
{
    public class BannersController : Controller
    {

        private DbModelEntities modelEntities = new DbModelEntities();

        #region messages

        private object refreshPageMessage =
            new { messageType = "info", result = "Actualice la página y vuelva a intentarlo." };
        private object deleteErrorMessage =
            new { messageType = "error", result = "Error al generar la baja, intentelo nuevamente mas tarde" };
        private object deleteMessage =
            new { messageType = "success", result = "La baja se generó correctamente" };
        private object rehabilitateErrorMessage =
            new { messageType = "error", result = "Error al generar la rehabilitación, intentelo nuevamente mas tarde" };
        private object rehabilitateMessage =
            new { messageType = "success", result = "La rehabilitación se generó correctamente" };

        #endregion messages

        // GET: Banner
        public ActionResult Index()
        {

            return View("BannerList", this.BannerListToShow());

        }

        #region Banners


        [HttpGet]
        public ActionResult BannerUpdate(int Id)
        {
            if (Id != -1)
            {
                var banner = modelEntities.Banners.FirstOrDefault(x => x.BannerId == Id);
                return View(banner);
            }

            return View(new Banner { BannerId = Id });
        }

        [HttpPost]
        public ActionResult BannerUpdate(FormCollection form, HttpPostedFileBase image, HttpPostedFileBase imageMobile, bool flagFullImageDelete, bool flagImageMobileDelete)
        {
            try
            {
                Banner banner = new Banner();

                if (form["BannerId"] != string.Empty && Convert.ToInt16(form["BannerId"]) > 0)
                {
                    int bannerId = Convert.ToInt16(form["BannerId"]);
                    banner = modelEntities.Banners.SingleOrDefault(x => x.BannerId == bannerId);
                }

                TryUpdateModel(banner, form);

                banner.Active = (Request["Active"] != null && Request["Active"] == "on");

                #region Image Save

                if (image == null  && banner.Image == null)
                {
                    NotificationBar.ShowMessage(this, NotificationMessageType.Warning, "Debe agregarse al menos una de las dos imágenes");
                    return View(banner);
                }

                if (image != null)
                {

                    var path = Server.MapPath(banner.BannersFileUploadDirectory);

                    string fileName = Path.GetFileName(image.FileName);
                    string fileExtension = fileName.Substring(image.FileName.LastIndexOf("."));
                    string new_fileName = "image_" +
                        CurrentDate.Now.ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '_')
                        + CurrentDate.Now.Millisecond.ToString() + fileExtension;
                    string filePath = Path.Combine(path, new_fileName);

                    image.SaveAs(filePath);
                    banner.Image = new_fileName;

                    // se utiliza el sleep para asegurarse que el nombre de la imagen siempre vaya a ser unica
                    Thread.Sleep(1);

                }
                else
                {

                    banner.Image = flagFullImageDelete ? null : banner.Image;

                }

                #endregion Image Save

                #region ImageMobile Save

                if (imageMobile != null)
                {
                    var path = Server.MapPath(banner.BannersFileUploadDirectory);

                    string fileName = Path.GetFileName(imageMobile.FileName);
                    string fileExtension = fileName.Substring(imageMobile.FileName.LastIndexOf("."));
                    string new_fileName = "image-mobile_" +
                        CurrentDate.Now.ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '_')
                        + CurrentDate.Now.Millisecond.ToString() + fileExtension;
                    string filePath = Path.Combine(path, new_fileName);

                    imageMobile.SaveAs(filePath);
                    banner.ImageMobile = new_fileName;

                    // se utiliza el sleep para asegurarse que el nombre de la imagen siempre vaya a ser unica
                    Thread.Sleep(1);
                }
                else
                {
                    banner.ImageMobile = flagImageMobileDelete ? null : banner.ImageMobile;
                }

                #endregion Image Save

                if (banner.BannerId <= 0)
                {
                    banner.Created = CurrentDate.Now;
                    banner.CreatedBy = User.Identity.Name;
                    modelEntities.Banners.Add(banner);
                }

                modelEntities.SaveChanges();

                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                NotificationBar.ShowMessage(this, NotificationMessageType.Warning,
                    "Se generó un error al guardar los datos, inténtelo nuevamente mas tarde.");
                return RedirectToAction("Index");

            }

        }
        [HttpGet]
        public virtual PartialViewResult BannerDelete(int Id)
        {
            var banner = modelEntities.Banners.SingleOrDefault(x => x.BannerId == Id);
            if (banner != null)
            {
                banner.Active = false;
                banner.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            return PartialView("_BannerList", this.BannerListToShow());
        }

        [HttpPost]
        public JsonResult RehabilitateBanner(int id)
        {
            try
            {

                var bannerDeleted = modelEntities.Banners.SingleOrDefault(x => x.BannerId == id
                    && x.Deleted != null);

                if (bannerDeleted == null)
                {

                    return Json(refreshPageMessage);

                }

                bannerDeleted.Deleted = null;
                bannerDeleted.DeletedBy = null;
                modelEntities.SaveChanges();

            }
            catch (Exception ex)
            {

                return Json(rehabilitateErrorMessage);

            }

            return Json(rehabilitateMessage);

        }

        [HttpPost]
        public ActionResult BannersPartialRefresh()
        {

            return PartialView("_BannerList", this.BannerListToShow());

        }

        #region privates

        private IEnumerable<Banner> BannerListToShow()
        {

            var bannerList = modelEntities.Banners.Where(x => x.Deleted == null).OrderBy(x => x.Name);
            return bannerList;

        }

        #endregion privates

        #endregion Banners

    }
}