﻿using eCommerce.BackOffice.ViewModels;
using eCommerce.Services;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using eCommerce.Services.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace eCommerce.BackOffice.Controllers
{
    public class ProductsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        private const string ProductsFileUploadDirectory = "/Content/UploadDirectory/Products";
        private const string ProductsFileUploadDirectoryResized = "/Content/UploadDirectory/Products/Resized";

        public ActionResult Index()
        {
            return View("ProductList", this.ProductsToShow());
        }

        [HttpGet]
        public ActionResult DisplayOrder()
        {

            var categories = modelEntities.Categories
                .Where(cat => cat.Enabled && cat.Active && !cat.Deleted.HasValue).OrderBy(cat => cat.DisplayOrder)
                .Select(cat => new CategoryViewModel
                {
                    Id = cat.CategoryId,
                    Name = cat.CompleteName 
                }).OrderBy(x => x.Name).ToList();

            return View(categories);
        }

        [HttpPost]
        public JsonResult DisplayOrder(string productsJson)
        {
            bool success = true;
            try
            {
                var products = JsonConvert.DeserializeObject<List<ProductOrderViewModel>>(productsJson);
                var productIds = products.Select(x => x.Id).ToList();

                var productList = modelEntities.Products.Where(prod => productIds.Contains(prod.ProductId)).ToList();
                productList.ForEach(prod => prod.DisplayOrder = products.FirstOrDefault(x => x.Id == prod.ProductId).Order);
                modelEntities.SaveChanges();
            }
            catch (Exception e)
            {
                success = false;
            }

            return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult ProductsOrderList(int CategoryId, int? PublishedTypeId)
        {
            var products = GetProductsFor(CategoryId, PublishedTypeId);

            return View("_DisplayOrderList", products);
        }

        private List<ProductOrderViewModel> GetProductsFor(int CategoryId, int? PublishedTypeId)
        {
            var isSpecial = false;
            var category = modelEntities.Categories.FirstOrDefault(cat => cat.CategoryId == CategoryId);
            var pQuery = modelEntities.Products.Where(prod => !prod.Deleted.HasValue && prod.Active && prod.Published);

            if (category.CategoryId == Utility.AllProductsCategoryId)
            {
                isSpecial = true;
            }

            if (category.CategoryId == Utility.SaleCategoryId)
            {
                isSpecial = true;
                pQuery = pQuery.Where(x => x.OnSale);
            }

            if (category.CategoryId == Utility.NewCategoryId)
            {
                isSpecial = true;
                pQuery = pQuery.Where(x => x.IsNew).OrderByDescending(x => x.ProductId);
            }

            if (!isSpecial)
            {
                pQuery = pQuery.Where(x => x.CategoryId == category.CategoryId);
            }


            var products = pQuery.OrderByDescending(x => x.DisplayOrder.HasValue).ThenBy(x => x.DisplayOrder).ThenByDescending(x => x.Created)
                                .Select(prod => new ProductOrderViewModel
                                {
                                    Id = prod.ProductId,
                                    Name = prod.Title,
                                    ImageUrl = Utility.ImgUrl + "/Content/UploadDirectory/Products/" +
                                                (prod.Images.FirstOrDefault(img => img.Default) != null ?
                                                    "resized/" + prod.ProductId + "/" + prod.Images.FirstOrDefault(img => img.Default).ImageName :
                                                    prod.Images.Any() ?
                                                    "resized/" + prod.ProductId + "/" + prod.Images.FirstOrDefault().ImageName :
                                                    "products/DefaultProductImage.jpeg"
                                                ),
                                    Order = prod.DisplayOrder
                                }).ToList();

            return products;
        }

        [HttpGet]
        public ActionResult ProductUpdate(int Id)
        {
            var categories = modelEntities.Categories.Where(x => x.Active && !x.Deleted.HasValue).ToList();
            var sizes = modelEntities.Sizes.Where(x => x.Deleted == null).ToList();
            var colors = modelEntities.Colors.Where(x => x.Deleted == null).ToList();
            var deposits = modelEntities.Deposits.Where(x => x.Deleted == null).OrderBy(x => x.Name).ToList();
            var products = modelEntities.Products.Where(x => !x.Deleted.HasValue).ToList();
            var tags = modelEntities.Tags.Where(x => x.Deleted == null).OrderBy(x => x.TagName).ToList();


            var related = products.Select(x => new {
                Id = x.ProductId,
                Name = x.Code + " - " + x.Title ,
            });

            if (Id != -1)
            {
                var product = modelEntities.Products.FirstOrDefault(x => x.ProductId == Id);
                product.Stocks = this.StocksToShow(Id);

                //var relatedProducts = modelEntities.ProductRelationships
                //    .Where(x => x.ProductId == Id && !x.Product1.Deleted.HasValue)
                //    .Select(x => new { Id = x.RelatedProductId, Name = x.Product1.Title, ProductId = x.ProductId }).ToList();

                TempData["Tags"] = new SelectList(tags, "TagId", "TagName", product.TagId);
                TempData["RelatedProducts"] = new MultiSelectList(related, "Id", "Name", product.ProductRelationships.Select(x => x.RelatedProductId));
                TempData["PhotoProducts"] = new MultiSelectList(related, "Id", "Name", product.PhotoProducts.Select(x => x.RelatedProductId));
                TempData["Categories"] = new SelectList(categories, "CategoryId", "CompleteName", product.CategoryId);
                //TempData["Sizes"] = new SelectList(sizes, "SizeId", "Description");
                TempData["Sizes"] = sizes;

                TempData["StockColors"] = new MultiSelectList(colors, "ColorId", "Description", product.Stocks.Select(x => x.ColorId));

                TempData["Colors"] = new SelectList(colors, "ColorId", "Description");
                TempData["Deposits"] = new SelectList(deposits, "DepositId", "Name");

                #region Images

                var path = Path.Combine(ProductsFileUploadDirectory, Id.ToString());
                foreach (Services.Model.Image imagen in product.Images)
                {
                    imagen.URL = Path.Combine(path, imagen.ImageName);
                }

                #endregion

                return View(product);
            }

            TempData["Tags"] = new SelectList(tags, "TagId", "TagName");
            TempData["RelatedProducts"] = new MultiSelectList(related, "Id", "Name");
            TempData["PhotoProducts"] = new MultiSelectList(related, "Id", "Name");
            TempData["ParentCategories"] = new SelectList(categories.Where(x => x.ParentCategoryId == null), "CategoryId", "Name");
            TempData["Categories"] = new SelectList(categories, "CategoryId", "CompleteName");

            return View(new Product { ProductId = Id });
        }

        [HttpPost]
        public ActionResult ProductUpdate(FormCollection form, HttpPostedFileBase[] image, bool onlySave = false)
        {
            Product product = new Product();

            var productImages = image != null ? image.Where(x => x != null) : null;
            var defaultImage = (form["defaultImageName"] ?? string.Empty).ToString();

            if (form["ProductId"] != string.Empty && Convert.ToInt16(form["ProductId"]) > 0)
            {
                int productId = Convert.ToInt16(form["ProductId"]);
                product = modelEntities.Products.SingleOrDefault(j => j.ProductId == productId && j.Active);
            }

            

            TryUpdateModel(product, form);

            var publicationDate = form["PublicationDate"] != null ? DateTime.Parse(form["PublicationDate"]) : new DateTime();
            var publicationTime = form["PublicationDateTime"] != null ? DateTime.Parse(form["PublicationDateTime"]) : new DateTime();

            publicationDate = publicationDate.Add(publicationTime.TimeOfDay);

            product.PublicationDate = publicationDate;

            if (!ValidSeoUrl(product))
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "El Seo Url ya está usado por otro producto, intente con otro.");
                return RedirectToAction("ProductUpdate", new { Id = product.ProductId });
            }

            #region Set Flags

            if (Request["Published"] != null && Request["Published"] == "on")
                product.Published = true;
            else
                product.Published = false;

            if (Request["PublishedWholesaler"] != null && Request["PublishedWholesaler"] == "on")
                product.PublishedWholesaler = true;
            else
                product.PublishedWholesaler = false;

            if (Request["ShowInHome"] != null && Request["ShowInHome"] == "on")
                product.ShowInHome = true;
            else
                product.ShowInHome = false;

            if (Request["Outstanding"] != null && Request["Outstanding"] == "on")
                product.Outstanding = true;
            else
                product.Outstanding = false;

            if (Request["OnSale"] != null && Request["OnSale"] == "on")
                product.OnSale = true;
            else
                product.OnSale = false;

            if (Request["IsNew"] != null && Request["IsNew"] == "on")
                product.IsNew = true;
            else
                product.IsNew = false;

            if (Request["Denim"] != null && Request["Denim"] == "on")
                product.Denim = true;
            else
                product.Denim = false;

            if (Request["LastUnits"] != null && Request["LastUnits"] == "on")
                product.LastUnits = true;
            else
                product.LastUnits = false;

            if (Request["SoldOut"] != null && Request["SoldOut"] == "on")
                product.SoldOut = true;
            else
                product.SoldOut = false;

            if (Request["HasStockControl"] != null && Request["HasStockControl"] == "on")
                product.HasStockControl = true;
            else
                product.HasStockControl = false;

            #endregion

            /*if (!string.IsNullOrWhiteSpace(form["AverageCost"]))
                product.AverageCost = decimal.Parse(form["AverageCost"].Replace(".", ","));

            if (!string.IsNullOrWhiteSpace(form["AverageCostUsd"]))
                product.AverageCostUsd = decimal.Parse(form["AverageCostUsd"].Replace(".", ","));*/

            if (product.ProductId <= 0)
            {
                product.HasStockControl = true;
                product.Active = true;
                product.Created = CurrentDate.Now;
                product.CreatedBy = User.Identity.Name;
                modelEntities.Products.Add(product);
            }

            modelEntities.SaveChanges();

            #region Images

            try
            {
                string productId = product.ProductId.ToString();
                var path = Path.Combine(Server.MapPath(ProductsFileUploadDirectory), productId);
                var pathResized = Path.Combine(Server.MapPath(ProductsFileUploadDirectoryResized), productId);
                if (productImages != null)
                {
                    foreach (HttpPostedFileBase imageToSave in productImages)
                    {
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        if (!Directory.Exists(pathResized))
                            Directory.CreateDirectory(pathResized);

                        string fileName = Path.GetFileName(imageToSave.FileName);
                        string fileExtension = fileName.Substring(imageToSave.FileName.LastIndexOf("."));
                        string new_fileName = "image_" + Guid.NewGuid() + fileExtension;
                        //CurrentDate.Now.ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '_')
                        //+ CurrentDate.Now.Millisecond.ToString() + fileExtension;
                        string filePath = Path.Combine(path, new_fileName);
                        string filePathResized = Path.Combine(pathResized, new_fileName);

                        if (fileName == defaultImage)
                        {
                            defaultImage = new_fileName;
                        }
                        //var imageToScale = System.Drawing.Image.FromStream(imageToSave.InputStream, true, true);
                        //var newImage = ScaleImage(imageToScale, imageToScale.Width / 2, imageToScale.Height / 2);

                        //newImage.Save(filePathResized, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //imageToSave.SaveAs(filePath);


                        var imageToScale = System.Drawing.Image.FromStream(imageToSave.InputStream, true, true);
                        imageToScale = CorrectImageOrientation(imageToScale);
                        var percentage = 1;
                        if (imageToScale.Height > 6000)
                        {
                            percentage = 10;
                        }
                        if (imageToScale.Height < 6000 && imageToScale.Height > 3000)
                        {
                            percentage = 3;
                        }
                        else if (imageToScale.Height <= 3000 && imageToScale.Height > 1000)
                        {
                            percentage = 2;
                        }
                        var newImage = ScaleImage(imageToScale, imageToScale.Width / percentage, imageToScale.Height / percentage);
                        newImage.Save(filePathResized, System.Drawing.Imaging.ImageFormat.Jpeg);
                        imageToSave.SaveAs(filePath);
                        var imagenAux = new Services.Model.Image()
                        {
                            ProductId = product.ProductId,
                            ImageName = new_fileName
                        };
                        product.Images.Add(imagenAux);
                    }
                }

                if (!String.IsNullOrEmpty(form["erasedImages"]))
                {
                    var imagenesABorrar = form["erasedImages"].Split(';').ToList();
                    var list = modelEntities.Images.Where(x => imagenesABorrar.Any(y => x.ImageName.IndexOf(y) == 0))
                        .ToList();

                    list.ForEach(x => modelEntities.Images.Remove(x));

                    //borra las imágenes del servidor.
                    foreach (var item in list)
                    {
                        var imagePath = Path.Combine(path, item.ImageName);
                        if (System.IO.File.Exists(imagePath))
                            System.IO.File.Delete(imagePath);
                    }
                }

                var n = 1;
                foreach (var item in product.Images)
                {
                    if (form["ImageColorId_" + n] != string.Empty)
                    {
                        item.ColorId = int.Parse(form["ImageColorId_" + n]);
                    }

                    if (form["ImageOrientation_" + n] != string.Empty)
                    {
                        item.Orientation = int.Parse(form["ImageOrientation_" + n]);
                    }
                    n++;
                }

                modelEntities.SaveChanges();

                modelEntities.Images
                    .Where(x => x.ProductId == product.ProductId).ToList()
                    .ForEach(x => x.Default = defaultImage == x.ImageName);

                if(product.Images.Any() && !product.Images.Any(x => x.Default))
                {
                    var firstImage = modelEntities.Images.FirstOrDefault(x => x.ProductId == product.ProductId);
                    firstImage.Default = true;
                    modelEntities.SaveChanges();
                }

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error grabar las imágenes del producto.");
                return RedirectToAction("Index");
            }

            #endregion Images

            #region ProductRelationships

            if (!string.IsNullOrEmpty(form["RelatedProducts"]))
            {
                //Traigo los ids de los productos relacionados del form y los paso a int
                var relatedProductIds = form["RelatedProducts"].Split(',').ToList();
                List<int> relatedProdIds = relatedProductIds.Select(x => int.Parse(x)).ToList();


                //Borro todas las relaciones que tenga el producto al que le estoy haciendo update actualmente
                var currentProductRelationships = modelEntities.ProductRelationships.Where(x => x.ProductId == product.ProductId);

                if (currentProductRelationships.Count() > 0)
                {
                    modelEntities.ProductRelationships.RemoveRange(currentProductRelationships);
                    modelEntities.SaveChanges();
                }

                //Creo una relacion al producto por cada uno de los productos relacionados
                var products = modelEntities.Products.Where(x => relatedProdIds.Contains(x.ProductId)).ToList();
                foreach (var relatedProd in products)
                {
                    ProductRelationship relationship = new ProductRelationship();
                    relationship.ProductId = product.ProductId;
                    relationship.RelatedProductId = relatedProd.ProductId;
                    modelEntities.ProductRelationships.Add(relationship);
                }
                modelEntities.SaveChanges();


            }

            #endregion

            #region productPhotos
            if (!string.IsNullOrEmpty(form["ProductPhotos"]))
            {
                //Traigo los ids de los productos relacionados del form y los paso a int
                var photoProductIds = form["ProductPhotos"].Split(',').ToList();
                List<int> photoProdIds = photoProductIds.Select(x => int.Parse(x)).ToList();


                //Borro todas las relaciones que tenga el producto al que le estoy haciendo update actualmente
                var currentPhotoProducts = modelEntities.PhotoProducts.Where(x => x.ProductId == product.ProductId);

                if (currentPhotoProducts.Count() > 0)
                {
                    modelEntities.PhotoProducts.RemoveRange(currentPhotoProducts);
                    modelEntities.SaveChanges();
                }

                //Creo una relacion al producto por cada uno de los productos relacionados
                var products = modelEntities.Products.Where(x => photoProdIds.Contains(x.ProductId)).ToList();
                foreach (var relatedProd in products)
                {
                    PhotoProduct photoProduct = new PhotoProduct();
                    photoProduct.ProductId = product.ProductId;
                    photoProduct.RelatedProductId = relatedProd.ProductId;
                    modelEntities.PhotoProducts.Add(photoProduct);
                }
                modelEntities.SaveChanges();


            }
            #endregion

            #region Stocks

            StockService stockService = new StockService(new VirtualStock());
            Stock stock = new Stock();
            List<Stock> stocksList = new List<Stock>();
            var defaultDepositId = modelEntities.Deposits.FirstOrDefault(x => !x.Deleted.HasValue).DepositId;

            foreach (var item in form.AllKeys.Where(x => x.StartsWith("stock-")))
            {
                stock = new Stock();
                var sizeId = int.Parse(item.Split('-')[1]);
                var colorId = int.Parse(item.Split('-')[2]);
                var row = int.Parse(item.Split('-')[3]);


                var price = decimal.Parse(form["price-"+row]);

                var quantity = form[item] != null ? int.Parse(form[item]) : 1;

                var currentStock = product.Stocks.SingleOrDefault(x => x.SizeId == sizeId && x.ColorId == colorId);
                var currentQuantity = 0;
                currentQuantity = currentStock != null ?
                    currentStock.StockReal
                    : 0;

                
                var updatedQuantity = quantity - currentQuantity;
                if (updatedQuantity != 0 || currentStock == null || (currentStock.Price != price))
                    stockService.CreateStockMovement(defaultDepositId, StockMovementTypeEnum.Ingreso, product.ProductId, sizeId, colorId, updatedQuantity, "Carga desde edición", price);

                stock.ProductId = product.ProductId;
                stock.SizeId = sizeId;
                stock.ColorId = colorId;
                stock.Price = price;

                stocksList.Add(stock);
            }
            modelEntities.SaveChanges();

            List<Stock> stocksToDelete = new List<Stock>();

            foreach (var item in product.Stocks)
            {
                var existingStock = stocksList.SingleOrDefault(x => x.SizeId == item.SizeId && x.ColorId == item.ColorId);

                if (existingStock == null) stocksToDelete.Add(item);
            }

            stockService.DeleteStockMovements(stocksToDelete, ref modelEntities);
            //modelEntities.Stocks.RemoveRange(stocksToDelete);
            modelEntities.SaveChanges();

            #endregion

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            if (onlySave)
                return RedirectToAction("ProductUpdate", new { Id = product.ProductId });
            else
                return RedirectToAction("Index");
        }

        public bool ValidSeoUrl(Product product)
        {
            return !modelEntities.Products.Any(x => x.SeoUrl == product.SeoUrl && x.ProductId != product.ProductId && x.Deleted == null);

        }

        public static System.Drawing.Image ScaleImage(System.Drawing.Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);
            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);
            var newImage = new System.Drawing.Bitmap(newWidth, newHeight);

            using (var graphics = System.Drawing.Graphics.FromImage(newImage))
            {
                graphics.SmoothingMode = SmoothingMode.HighSpeed;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            return newImage;
        }

        [HttpGet]
        public virtual string ResizeAllImages()
        {
            try
            {
                var prodsIds = modelEntities.Products.Where(x => x.Deleted == null && x.Active).Select(x => x.ProductId).ToList();

                foreach (var productId in prodsIds)
                {
                    var path = Path.Combine(Server.MapPath(ProductsFileUploadDirectory), productId.ToString());

                    if (Directory.Exists(path))
                    {
                        var pathResized = Path.Combine(Server.MapPath(ProductsFileUploadDirectoryResized), productId.ToString());

                        string[] files = null;
                        int count = 0;
                        files = System.IO.Directory.GetFiles(path);

                        foreach (string file in files)
                        {
                            string fileName = Path.GetFileName(file);
                            FileStream stream = new FileStream(file, FileMode.Open);
                            string filePathResized = Path.Combine(pathResized, fileName);

                            var imageToScale = System.Drawing.Image.FromStream(stream, true, false);
                            var percentage = 1;
                            if(imageToScale.Height > 6000)
                            {
                                percentage = 10;
                            }
                            if(imageToScale.Height < 6000 && imageToScale.Height > 3000)
                            {
                                percentage = 3;
                            }
                            else if(imageToScale.Height <= 3000 && imageToScale.Height > 1000)
                            {
                                percentage = 2;
                            }

                            var newImage = ScaleImage(imageToScale, imageToScale.Width / percentage, imageToScale.Height / percentage);

                            newImage.Save(filePathResized, System.Drawing.Imaging.ImageFormat.Jpeg);

                            count++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "OK";
        }


        private System.Drawing.Image CorrectImageOrientation(System.Drawing.Image img)
        {
            foreach (var prop in img.PropertyItems)
            {
                if (prop.Id == 0x0112) //value of EXIF
                {
                    int orientationValue = img.GetPropertyItem(prop.Id).Value[0];
                    RotateFlipType rotateFlipType = GetOrientationToFlipType(orientationValue);
                    img.RotateFlip(rotateFlipType);
                    img.RemovePropertyItem(0x0112);
                    break;
                }
            }
            return img;
        }

        private static RotateFlipType GetOrientationToFlipType(int orientationValue)
        {
            RotateFlipType rotateFlipType = RotateFlipType.RotateNoneFlipNone;

            switch (orientationValue)
            {
                case 1:
                    rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                    break;
                case 2:
                    rotateFlipType = RotateFlipType.RotateNoneFlipX;
                    break;
                case 3:
                    rotateFlipType = RotateFlipType.Rotate180FlipNone;
                    break;
                case 4:
                    rotateFlipType = RotateFlipType.Rotate180FlipX;
                    break;
                case 5:
                    rotateFlipType = RotateFlipType.Rotate90FlipX;
                    break;
                case 6:
                    rotateFlipType = RotateFlipType.Rotate90FlipNone;
                    break;
                case 7:
                    rotateFlipType = RotateFlipType.Rotate270FlipX;
                    break;
                case 8:
                    rotateFlipType = RotateFlipType.Rotate270FlipNone;
                    break;
                default:
                    rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                    break;
            }

            return rotateFlipType;
        }

        //public static System.Drawing.Image ScaleImage(System.Drawing.Image image, int maxWidth, int maxHeight)
        //{
        //    var ratioX = (double)maxWidth / image.Width;
        //    var ratioY = (double)maxHeight / image.Height;
        //    var ratio = Math.Min(ratioX, ratioY);
        //    var newWidth = (int)(image.Width * ratio);
        //    var newHeight = (int)(image.Height * ratio);
        //    var newImage = new System.Drawing.Bitmap(newWidth, newHeight);

        //    using (var graphics = System.Drawing.Graphics.FromImage(newImage))
        //    {
        //        graphics.SmoothingMode = SmoothingMode.HighSpeed;
        //        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
        //        graphics.CompositingMode = CompositingMode.SourceCopy;
        //        graphics.DrawImage(image, 0, 0, newWidth, newHeight);
        //    }

        //    return newImage;
        //}

        [HttpGet]
        public virtual PartialViewResult ProductDelete(int Id)
        {
            var product = modelEntities.Products.SingleOrDefault(x => x.ProductId == Id && x.Active);
            if (product != null)
            {
                product.Active = false;
                product.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }
            
            return PartialView("_ProductList", this.ProductsToShow());
        }

        [HttpPost]
        public virtual bool CodeExists(string Code, string ProductId)
        {
            Code = Code.Trim().ToLower();
            var productId = int.Parse(ProductId);
            var existe = modelEntities.Products.Any(x => x.Code.Trim().ToLower() == Code && x.ProductId != productId && x.Active && x.Deleted == null);
            return existe;
        }

        [HttpPost]
        public virtual ActionResult AddStock(int ProductId, int SizeId, int ColorId, int DepositId, int Quantity)
        {
            StockService stockService = new StockService(new VirtualStock());
            if (DepositId == -1)
            {
                var deposit = modelEntities.Deposits.FirstOrDefault(x => !x.Deleted.HasValue);
            }
            stockService.CreateStockMovement(DepositId, StockMovementTypeEnum.Ingreso, ProductId, SizeId, ColorId, Quantity, "Carga desde edición");

            return PartialView("_ProductStockList", this.StocksToShow(ProductId));
        }

        private List<Stock> StocksToShow(int productId)
        {
            return modelEntities.Stocks.Where(x => x.ProductId == productId && x.StockReal > 0)
                .OrderByDescending(x => x.Size.Description).ThenBy(x => x.Color.Description).ThenBy(x => x.DepositId).ToList();
        }

        [HttpGet]
        public virtual ActionResult Stocks()
        {
            var products = modelEntities.Stocks.Where(x => x.Product.Active && x.Product.Deleted == null && x.StockReal > 0)
                .OrderBy(x => x.Product.Code).ThenByDescending(x => x.Size.Description).ThenBy(x => x.Color.Description).ThenBy(x => x.Deposit.Name)
                .Select(x => new StockListViewModel
                {
                    ProductId = x.ProductId,
                    SizeId = x.SizeId,
                    ColorId = x.ColorId,
                    Code = x.Product.Code,
                    Size = x.Size.Description,
                    Color = x.Color.Description,
                    StockVirtual = x.StockVirtual,
                    StockReal = x.StockReal,
                    Title = x.Product.Title 
                }).ToList();

            var groupedList = products.GroupBy(g => new { g.Code, g.Title, g.Size, g.Color, g.StockVirtual, g.ProductId, g.SizeId, g.ColorId})
                .Select(n => new StockListViewModel
                {
                    Code = n.Key.Code,
                    Size = n.Key.Size,
                    Color = n.Key.Color,
                    StockVirtual = n.Key.StockVirtual,
                    StockReal = n.Sum(s => s.StockReal),
                    ProductId = n.Key.ProductId,
                    SizeId = n.Key.SizeId,
                    ColorId = n.Key.ColorId,
                    Title = n.Key.Title
                })
                .ToList();

            return View("StockList", groupedList);
        }

        [HttpGet]
        public virtual ActionResult PriceList()
        {
            var products = modelEntities.Stocks.Where(x => x.Product.Active && x.Product.Deleted == null && x.StockReal > 0)
                .OrderBy(x => x.Product.Code).ThenByDescending(x => x.Size.Description).ThenBy(x => x.Color.Description).ThenBy(x => x.Deposit.Name)
                .Select(x => new StockListViewModel
                {
                    ProductId = x.ProductId,
                    SizeId = x.SizeId,
                    ColorId = x.ColorId,
                    Code = x.Product.Code,
                    Size = x.Size.Description,
                    Color = x.Color.Description,
                    StockVirtual = x.StockVirtual,
                    StockReal = x.StockReal,
                    Title = x.Product.Title,
                    CategoryName = x.Product.Category.CompleteName,
                    RetailPrice = x.Product.RetailPrice,
                    WholesalePrice = x.Product.WholesalePrice,
                    DistributorPrice = x.Product.DistributorPrice,
                }).ToList();

            var groupedList = products.GroupBy(g => new { g.Code, g.Title, g.Size, g.Color, g.StockVirtual, g.ProductId, g.SizeId, g.ColorId, g.CategoryName, g.RetailPrice, g.WholesalePrice, g.DistributorPrice })
                .Select(n => new StockListViewModel
                {
                    Code = n.Key.Code,
                    Size = n.Key.Size,
                    Color = n.Key.Color,
                    StockVirtual = n.Key.StockVirtual,
                    StockReal = n.Sum(s => s.StockReal),
                    ProductId = n.Key.ProductId,
                    SizeId = n.Key.SizeId,
                    ColorId = n.Key.ColorId,
                    Title = n.Key.Title,
                    CategoryName = n.Key.CategoryName,
                    RetailPrice = n.Key.RetailPrice,
                    WholesalePrice = n.Key.WholesalePrice,
                    DistributorPrice = n.Key.DistributorPrice
                })
                .ToList();

            return View("PriceList", groupedList);
        }

        [HttpGet]
        public virtual ActionResult StocksByDeposit()
        {
            var products = modelEntities.Stocks.Where(x => x.Product.Active && x.Product.Deleted == null && x.StockReal > 0)
                .OrderBy(x => x.Product.Code).ThenByDescending(x => x.Size.Description).ThenBy(x => x.Color.Description).ThenBy(x => x.Deposit.Name)
                .Select(x => new StockListViewModel
                {
                    Code = x.Product.Code,
                    Title = x.Product.Title,
                    Size = x.Size.Description,
                    Color = x.Color.Description,
                    DepositName = x.Deposit.Name,
                    StockReal = x.StockReal
                }).ToList();

            return View("StockListByDeposit", products);
        }

        [HttpPost]
        public string StockUpdate(int ProductId, string NewStock, int sizeId, int colorId)
        {
            try
            {
                int newValue = int.Parse(NewStock);
                var stock = modelEntities.Stocks.FirstOrDefault(x => x.ProductId == ProductId && x.SizeId == sizeId && x.ColorId == colorId);

                var currentQuantity = stock != null ? stock.StockVirtual : 0;

                var updatedQuantity = int.Parse(NewStock) - currentQuantity;

                var nuevoStockReal = stock.StockReal + updatedQuantity;

                if (nuevoStockReal < 0)
                {
                    return "stockVirtualError/" + currentQuantity;
                }

                StockService stockService = new StockService(new VirtualStock());
                var defaultDepositId = modelEntities.Deposits.FirstOrDefault(x => !x.Deleted.HasValue).DepositId;

                StockMovementTypeEnum movementType = updatedQuantity < 0 ? 
                    StockMovementTypeEnum.Egreso : StockMovementTypeEnum.Ingreso; 

                stockService.CreateStockMovement(defaultDepositId, StockMovementTypeEnum.Ingreso, ProductId, sizeId, colorId, updatedQuantity, 
                    "Carga desde edición rápida");

                return nuevoStockReal.ToString();
            }
            catch (Exception ex)
            {
                return "error";
            }
        }

        [HttpGet]
        [RBAC]
        public virtual ActionResult StockMovements()
        {
            StockMovementListViewModel model = new StockMovementListViewModel();
            //model.StockMovements = this.StockMovementsToShow();

            StockExchangeViewModel stockExchange = new StockExchangeViewModel();
            var deposits = modelEntities.Deposits.Where(x => x.Deleted == null).OrderBy(x => x.Name).ToList();
            stockExchange.Deposits = new SelectList(deposits, "DepositId", "Name");

            var products = modelEntities.Products.Where(x => x.Deleted == null).OrderBy(x => x.Code).
                Select(s => new SelectListItem { Value = s.ProductId.ToString(), Text = s.Code + " - " + s.Title}).ToList();
            stockExchange.Products = new SelectList(products, "Value", "Text");

            var sizes = modelEntities.Sizes.Where(x => x.Deleted == null).OrderByDescending(x => x.Description).ToList();
            stockExchange.Sizes = new SelectList(sizes, "SizeId", "Description");

            var colors = modelEntities.Colors.Where(x => x.Deleted == null).OrderBy(x => x.Description).ToList();
            stockExchange.Colors = new SelectList(colors, "ColorId", "Description");

            model.StockExchange = stockExchange;

            var fromDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            var toDate = DateTime.Today;

            model.FromDate = fromDate.ToShortDateString();
            model.ToDate = toDate.ToShortDateString();

            return View("StockMovementList", model);
        }

        [HttpGet]
        public PartialViewResult GetStockMovements(string sFrom, string sTo)
        {
            return PartialView("_StockMovementList", this.StockMovementsToShow(sFrom, sTo));
        }

        private List<StockMovementViewModel> StockMovementsToShow(string sFrom = "", string sTo = "")
        {
            DateTime fromDate = string.IsNullOrWhiteSpace(sFrom) ? new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1) : DateTime.Parse(sFrom);
            DateTime toDate = string.IsNullOrWhiteSpace(sTo) ? DateTime.Today : DateTime.Parse(sTo);

            var result = modelEntities.StockMovements.Where(x => x.Quantity != 0 && x.Created >= fromDate && x.Created <= toDate).AsQueryable();

            return result.OrderByDescending(x => x.StockMovementId)
                .Select(x => new StockMovementViewModel
                {
                    StockMovementId = x.StockMovementId,
                    ProductCode = x.Stock.Product.Code,
                    SizeName = x.Stock.Size.Description,
                    ColorName = x.Stock.Color.Description,
                    DepositName = x.Stock.Deposit.Name,
                    MovementType = x.MovementType,
                    MovementTypeName = ((StockMovementTypeEnum)x.MovementType).ToString(),
                    Concept = x.Concept,
                    Quantity = x.Quantity,
                    Date = x.Created
                }).ToList();
        }

        [HttpGet]
        [RBAC]
        public virtual ActionResult FastEdit()
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            products = modelEntities.Products.Where(x => x.Active).OrderBy(x => x.Title).Select(
            x => new ProductViewModel
            {
                ProductId = x.ProductId,
                Code = x.Code,
                Title = x.Title,
                CategoryCompleteName = x.Category.CompleteName,
                WholesalePrice = x.WholesalePrice,
                OldRetailPrice = x.OldRetailPrice,
                OldWholesalePrice = x.OldWholesalePrice,
                RetailPrice = x.RetailPrice,
                Published = x.Published,
                IsOnSale = x.OnSale
            }).ToList();

            return View("FastEditList", products);
        }

        [HttpPost]
        public string FastEdit(int ProductId, string Atributo, string NewValue)
        {
            try
            {
                var product = modelEntities.Products.SingleOrDefault(x => x.ProductId == ProductId && x.Active);

                switch (Atributo)
                {
                    case "Code":
                        var prod = modelEntities.Products.FirstOrDefault(x => x.Code == NewValue);
                        if (prod == null)
                        {
                            product.Code = NewValue;
                        } else
                        {
                            return "CodigoRepetido/" + product.Code;
                        }
                        break;
                    case "Titulo":
                        product.Title = NewValue;
                        break;
                    case "RetailPrice":
                        NewValue = NewValue.Replace("$", "").Replace(".", ",");
                        if (!string.IsNullOrEmpty(NewValue))
                        {
                            product.RetailPrice = decimal.Parse(NewValue);
                            NewValue = product.RetailPrice.Value.ToString("c2");
                        } else
                        {
                            product.RetailPrice = null;
                        }
                        break;
                    case "OldRetailPrice":
                        NewValue = NewValue.Replace("$", "").Replace(".", ",");
                        if (!string.IsNullOrEmpty(NewValue))
                        {
                            product.OldRetailPrice = decimal.Parse(NewValue);
                            NewValue = product.OldRetailPrice.Value.ToString("c2");
                        }
                        else
                        {
                            product.OldRetailPrice = null;
                        }
                        break;
                    case "WholesalePrice":
                        NewValue = NewValue.Replace("$", "").Replace(".", ",");
                        if (!string.IsNullOrEmpty(NewValue))
                        {
                            product.WholesalePrice = decimal.Parse(NewValue);
                            NewValue = product.WholesalePrice.Value.ToString("c2");
                        } else
                        {
                            product.WholesalePrice = null;
                        }
                        break;
                    case "OldWholesalePrice":
                        NewValue = NewValue.Replace("$", "").Replace(".", ",");
                        if (!string.IsNullOrEmpty(NewValue))
                        {
                            product.OldWholesalePrice = decimal.Parse(NewValue);
                            NewValue = product.OldWholesalePrice.Value.ToString("c2");
                        }
                        else
                        {
                            product.OldWholesalePrice = null;
                        }
                        break;

                    default:
                        break;
                }
                modelEntities.SaveChanges();
                return NewValue;
            }
            catch (Exception)
            {
                return "error";
            }
        }

        [HttpPost]
        public string StockEdit(int ProductId, string Atributo, string NewValue)
        {
            try
            {
                //var product = modelEntities.Products.SingleOrDefault(x => x.ProductId == ProductId && x.Active);
                var product = modelEntities.Stocks.FirstOrDefault(x => x.Product.ProductId == ProductId && x.Product.Active && !x.Product.Deleted.HasValue);

                switch (Atributo)
                {
                    case "stock":
                        var value = int.Parse(NewValue);
                        //var prod = modelEntities.Stocks.FirstOrDefault(x => x.StockReal == value);

                        if (!string.IsNullOrEmpty(NewValue))
                        {
                            //product.Stocks.FirstOrDefault().StockReal = int.Parse(NewValue);
                            product.StockReal = int.Parse(NewValue);
                        }
                        break;
                    default:
                        break;
                }
                modelEntities.SaveChanges();
                return NewValue;
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }

        [HttpGet]
        public virtual PartialViewResult CategoryUpdateModal(int productId)
        {
            var categories = modelEntities.Categories.Where(x => x.Active).ToList();
            var product = modelEntities.Products.FirstOrDefault(x => x.ProductId == productId && x.Active);
            TempData["Categories"] = new SelectList(categories, "CategoryId", "CompleteName", product.CategoryId);
            return PartialView("_CategoryUpdate", productId);
        }

        [HttpGet]
        public virtual string CategoryUpdate(int productId, int categoryId)
        {
            var product = modelEntities.Products.FirstOrDefault(x => x.ProductId == productId && x.Active);
            if (product != null)
            {
                product.CategoryId = categoryId;
                modelEntities.SaveChanges();
                return product.Category.CompleteName;
            }
            return "error";
        }

        [HttpPost]
        public string PublishStatusUpdate(int ProductId)
        {
            try
            {
                var product = modelEntities.Products.SingleOrDefault(x => x.ProductId == ProductId && x.Active);
                product.Published = !product.Published;

                if (product.Published)
                {
                    product.PublicationDate = CurrentDate.Now;
                }

                modelEntities.SaveChanges();
                return product.Published ? "published" : "unpublished";
            }
            catch (Exception)
            {
                return "error";
            }
        }

        [HttpPost]
        public string OnSaleStatusUpdate(int ProductId)
        {
            try
            {
                var product = modelEntities.Products.SingleOrDefault(x => x.ProductId == ProductId && x.Active);
                product.OnSale = !product.OnSale;

                modelEntities.SaveChanges();
                return product.OnSale ? "true" : "false";
            }
            catch (Exception)
            {
                return "error";
            }
        }

        [HttpGet]
        [RBAC]
        public virtual ActionResult LimitDiscount()
        {
            List<Product> products = new List<Product>();
            products = modelEntities.Products.Where(x => x.Active).OrderBy(x => x.Title).ToList();
            return View("LimitDiscountList", products);
        }

        [HttpPost]
        public string LimitDiscountUpdate(int ProductId, string LimitDiscount)
        {
            try
            {
                var product = modelEntities.Products.SingleOrDefault(x => x.ProductId == ProductId && x.Active);
                decimal maxDiscount;

                if (product != null && decimal.TryParse(LimitDiscount.Replace(".", ","), out maxDiscount))
                {
                    product.MaxDiscount = maxDiscount;
                    modelEntities.SaveChanges();

                    return maxDiscount.ToString("N");
                }
                else if (string.IsNullOrWhiteSpace(LimitDiscount))
                {
                    product.MaxDiscount = null;
                    modelEntities.SaveChanges();

                    return string.Empty;
                }
                else
                    return "error";
            }
            catch (Exception)
            {
                return "error";
            }
        }

        #region Private Methods

        private void SetSuppliers(FormCollection form, Product product)
        {
            product.SupplierProducts.ToList().ForEach(x => modelEntities.SupplierProducts.Remove(x));
            if (!string.IsNullOrEmpty(form["Suppliers"]))
            {
                string[] selectedItems = form["Suppliers"].Split(',');
                foreach (string item in selectedItems)
                {
                    product.SupplierProducts.Add(new SupplierProduct { SupplierId = int.Parse(item), ProductId = product.ProductId });
                }
            }
        }

        private string[] GetSelectedSuppliers(List<SupplierProduct> selectedItems)
        {
            string[] values = new string[selectedItems.Count];
            for (int i = 0; i < selectedItems.Count; i++)
            {
                values[i] = selectedItems[i].SupplierId.ToString();
            }

            return values;
        }

        #endregion

        [HttpGet]
        public virtual ActionResult GetProducts()
        {
            var products = modelEntities.Products.Where(x => x.Active == true).Select(x => new { x.ProductId, x.Code, x.Title }).ToList();
            return Json(products, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetProductsToSale()
        {
            var products = modelEntities.Stocks.Where(x => x.Product.Active)
                .Select(x => new { x.ProductId, x.Product.Code, x.Product.Title }).Distinct().OrderBy(x => x.Code).ToList();
            return Json(products, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetProductSizesToSale(int ProductId)
        {
            var sizes = modelEntities.Stocks.Where(x => x.ProductId == ProductId)
                .Select(x => new { x.SizeId, x.Size.Description }).Distinct().ToList();

            return Json(sizes, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetProductSizesAndPricesToSale(int ProductId, int UserType, int PriceList)
        {
            //if (ProductId != null)
            //{
                decimal price = 0;
                decimal oldprice = 0;
                var sizes = modelEntities.Stocks.Where(x => x.ProductId == ProductId)
                .Select(x => new { x.SizeId, x.Size.Description }).Distinct().ToList();
                var product = modelEntities.Products.Find(ProductId);
            if ((int)UserTypeEnum.Mayorista == UserType)
                {
                    price = product.WholesalePrice.HasValue ? product.WholesalePrice.Value : 0;
                    oldprice = product.OldWholesalePrice.HasValue ? product.OldWholesalePrice.Value : 0;    
                }
                if ((int)UserTypeEnum.Minorista == UserType)
                {
                    price = product.RetailPrice.HasValue ? product.RetailPrice.Value : 0;
                    oldprice = product.OldRetailPrice.HasValue ? product.OldRetailPrice.Value : 0;
                }
                return Json(new { Sizes = sizes, Price = price, OldPrice = oldprice }, JsonRequestBehavior.AllowGet);
            //} else
            //{
            //    var sizes = modelEntities.Stocks.Select(x => new { x.SizeId, x.Size.Description }).Distinct().ToList();
            //    return Json(sizes, JsonRequestBehavior.AllowGet);
            //}
            
        }
        [HttpGet]
        public virtual ActionResult GetUserType(int customerId)
        {
            var customer = modelEntities.Customers.Find(customerId);
            var userType = customer.UserType;
            var priceList = ((PriceListEnum)customer.PriceList).ToString();
            return Json( new { UserType = userType, PriceList = priceList }, JsonRequestBehavior.AllowGet);
        }

        
        [HttpGet]
        public virtual ActionResult GetProductColorsToSale(int? ProductId, int? SizeId)
        {
            if (ProductId != null && SizeId != null)
            {
                var colors = modelEntities.Stocks.Where(x => x.ProductId == ProductId && x.SizeId == SizeId)
                .Select(x => new { x.ColorId, x.Color.Description, x.StockVirtual }).Distinct().ToList();
                return Json(colors, JsonRequestBehavior.AllowGet);
            } else
            {
                var colors = modelEntities.Stocks.Select(x => new { x.ColorId, x.Color.Description, x.StockReal }).Distinct().ToList();
                return Json(colors, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpGet]
        public virtual ActionResult GetProductInfoToSale(int ProductId, int SizeId, int ColorId, int PriceList)
        {
            decimal price = 0;
            decimal oldPrice = 0;

            var product = modelEntities.Products.Find(ProductId);

            switch ((PriceListEnum)PriceList)
            {
                default:
                case PriceListEnum.Minorista:
                    price = product.CustomerPrice > 0 ? product.CustomerPrice : 0;
                    oldPrice = product.OldRetailPrice.HasValue ? product.OldRetailPrice.Value : 0;
                    break;

                case PriceListEnum.Mayorista:
                    price = product.WholesalePrice.HasValue ? product.WholesalePrice.Value : 0;
                    oldPrice = product.OldWholesalePrice.HasValue ? product.OldWholesalePrice.Value : 0;
                    break;
            }

            var stock = modelEntities.Stocks.FirstOrDefault(x => x.ProductId == ProductId && x.SizeId == SizeId && x.ColorId == ColorId);
            var stockVirtual = stock != null ? stock.StockVirtual : 0;

            return Json(new { CustomerPrice = price, CustomerOldPrice = oldPrice, StockVirtual = stockVirtual, Code = product.Code, MaxDiscount = product.MaxDiscount }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult ProductUpload()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult ProductUpload(HttpPostedFileBase productsFile)
        {
            try
            {
                string filePath;

                //Upload excel file with products.
                filePath = SaveFile(productsFile);

                //Fill excel into Datatable
                DataTable dtProducts = LoadExcelToDataTable(filePath);

                //Bind prouducts into Database
                InsertProducts(dtProducts);

                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "El archivo fue procesado correctamente!" );

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, ex.Message);
                return RedirectToAction("Index");
            }
        }

        private string SaveFile(HttpPostedFileBase productsFile)
        {
            if (productsFile != null && productsFile.ContentLength > 0)
            {
                var path = Server.MapPath("/Content/UploadDirectory/ProductUpload/");
                var fullPath = path + "Listado.Productos - " + CurrentDate.Now.Date.ToShortDateString().Replace("/", "-") + ".xlsx";

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                productsFile.SaveAs(fullPath);
                return fullPath;
            }

            return string.Empty;
        }

        private DataTable LoadExcelToDataTable(string filePath)
        {
            return ExcelUtils.ReadXlsFile(filePath, "Productos");
        }

        private void InsertProducts(DataTable dtProducts)
        {
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(modelEntities.Database.Connection.ConnectionString, SqlBulkCopyOptions.Default))
            {
                try
                {
                    //Truncate table sabana
                    modelEntities.Database.ExecuteSqlCommand("Truncate table Sabana");

                    //Remove 1st row of titles
                    dtProducts.Rows[0].Delete();

                    // Write from the source to the destination.
                    bulkCopy.DestinationTableName = "dbo.Sabana";
                    bulkCopy.WriteToServer(dtProducts);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    //Insert Products from Sabana
                    modelEntities.Database.ExecuteSqlCommand("InsertProductsFromSabana");
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        private Category GetCategory(IQueryable<Category> categories, string categoryName)
        {
            string[] items = categoryName.Split('>');
            string lastItem = items[items.Length - 1].Trim();

            var results = categories.Where(x => x.Name.Trim().ToUpper() == lastItem.Trim().ToUpper());

            foreach (Category cat in results)
            {
                if (cat.CompleteName.ToUpper() == categoryName.ToUpper())
                    return cat;
            }

            return null;
        }

        [HttpGet]
        public JsonResult GetCategories()
        {
            var categories = modelEntities.Categories.Where(x => x.Active && x.Deleted == null && x.Enabled).Select(y => new { CategoryId = y.CategoryId, Name = y.CompleteName });

            return Json(new { Categories = categories }, JsonRequestBehavior.AllowGet);
        }


        private List<ProductViewModel> ProductsToShow()
        {
            var prods = modelEntities.Products.Where(x => x.Active && x.Deleted == null).OrderByDescending(x => x.ProductId)
                .Select(
                x => new ProductViewModel
                {
                    ProductId = x.ProductId,
                    Code = x.Code,
                    Title = x.Title,
                    CategoryCompleteName = x.Category.CompleteName,
                    WholesalePrice = x.WholesalePrice,
                    OldWholesalePrice = x.OldWholesalePrice,
                    RetailPrice = x.RetailPrice,
                    OldRetailPrice = x.OldRetailPrice,
                    Published = x.Published,
                    ShowInHome = x.ShowInHome,
                    IsOnSale= x.OnSale,
                    PublishedWholesaler = x.PublishedWholesaler,
                    Created = x.Created.Value,
                    Cloth = x.Cloth
                }).ToList();

            return prods;
        }

        [HttpPost]
        public PartialViewResult StockExchange(StockExchangeViewModel model)
        {
            try
            {
                StockService stockService = new StockService();
                stockService.StockExchange(model.OriginDepositId, model.DestinyDepositId, model.ProductId, model.SizeId, model.ColorId, model.Quantity);

                return PartialView("_StockMovementList", this.StockMovementsToShow());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public PartialViewResult CreateStockMovement(StockExchangeViewModel model)
        {
            try
            {
                StockService stockService = new StockService();
                stockService.CreateStockMovement(model.OriginDepositId, (StockMovementTypeEnum)model.MovementType, model.ProductId, model.SizeId, model.ColorId, model.Quantity,model.Concept);

                return PartialView("_StockMovementList", this.StockMovementsToShow());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public virtual PartialViewResult GetStockDetail(int productId, int sizeId, int colorId)
        {
            List<Stock> stockDetails = new List<Stock>();
            
            stockDetails = modelEntities.Stocks.Where(x => x.ProductId == productId && x.SizeId == sizeId && x.ColorId == colorId && x.StockReal > 0)
                    .OrderBy(x => x.Product.Code).ThenByDescending(x => x.Size.Description).ThenBy(x => x.Color.Description).ThenBy(x => x.Deposit.Name).ToList();
            
            return PartialView("_StockDetail", stockDetails);
        }

        [HttpGet]
        public virtual ActionResult GetDepositsAndStock(int productId, int sizeId, int colorId)
        {
            var user = modelEntities.USERS.FirstOrDefault(x => x.Username == User.Identity.Name && x.Active && x.Deleted == null);

            var stocks = modelEntities.Stocks.Where(x => x.ProductId == productId && x.SizeId == sizeId && x.ColorId == colorId && x.StockReal > 0)
                .OrderBy(x => x.Deposit.Name)
                .Select(x => new
                {
                    DepositId = x.DepositId,
                    DepositName = x.Deposit.Name,
                    Stock = x.StockReal,
                }).ToList();

            return Json(stocks, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MassivePriceUpdate(int CategoryId, string Value, string OperationType,  string UpdateRetailPrice, string UpdateWholesalePrice)
        {
            try
            {
                List<Product> products = modelEntities.Products.Where(x => x.Active && x.Deleted == null && (x.CategoryId == CategoryId || x.Category.ParentCategoryId == CategoryId)).ToList();
                if (CategoryId == -1)
                {
                    products = modelEntities.Products.Where(x => x.Active && x.Deleted == null).ToList();
                }

                if (OperationType.Equals("multiply"))
                {
                    decimal Amount = decimal.Parse(Value, CultureInfo.InvariantCulture);
                    if (Amount == 0)
                        Amount = 1;

                    products.ForEach(x =>
                    {
                        if (UpdateRetailPrice == "true")
                        {
                            if (Amount < 1)
                            {
                                x.OldRetailPrice = x.RetailPrice;
                            }
                            x.RetailPrice = (x.RetailPrice * Amount);
                        }
                        if (UpdateWholesalePrice == "true")
                        {
                            if (Amount < 1)
                            {
                                x.OldWholesalePrice = x.WholesalePrice;
                            }
                            x.WholesalePrice = (x.WholesalePrice * Amount);
                        }
                    });
                }
                else
                {
                    products.ForEach(x =>
                    {
                        if (UpdateRetailPrice == "true" && x.OldRetailPrice.HasValue)
                        {
                            x.RetailPrice = x.OldRetailPrice.Value;
                            x.OldRetailPrice = null;
                        }

                        if (UpdateWholesalePrice == "true" && x.OldWholesalePrice.HasValue)
                        {
                            x.WholesalePrice = x.OldWholesalePrice.Value;
                            x.OldWholesalePrice = null;
                        }
                    });
                }

                modelEntities.SaveChanges();

                return Json(new { Message = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Message = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }


        #region Export to Excel

        [HttpGet]
        public ActionResult ExportStocksToExcel()
        {
            //var products = modelEntities.Stocks.Where(x => x.Product.Active && x.Product.Deleted == null && x.StockReal > 0)
            //    .OrderBy(x => x.Product.Code).ThenByDescending(x => x.Size.Description).ThenBy(x => x.Color.Description).ThenBy(x => x.Deposit.Name)
            //    .Select(x => new StockListViewModel
            //    {
            //        Code = x.Product.Code,
            //        Title = x.Product.Title,
            //        Size = x.Size.Description,
            //        Color = x.Color.Description,
            //        StockVirtual = x.StockVirtual,
            //        StockReal = x.StockReal
            //    }).ToList();

            //var groupedList = products.GroupBy(g => new { g.Code, g.Title,g.Size, g.Color, g.StockVirtual })
            //    .Select(n => new 
            //    {
            //        Código = n.Key.Code,
            //        Título = n.Key.Title,
            //        Talle = n.Key.Size,
            //        Color = n.Key.Color,
            //        Stock_Disponible = n.Key.StockVirtual,
            //        Stock_Físico = n.Sum(s => s.StockReal)
            //    })
            //    .ToList();

            var products = modelEntities.Stocks.Where(x => x.Product.Active && x.Product.Deleted == null && x.StockReal > 0)
                .OrderBy(x => x.Product.Code).ThenByDescending(x => x.Size.Description).ThenBy(x => x.Color.Description).ThenBy(x => x.Deposit.Name)
                .ToList()
                .Select(x => new
                 {
                     Codigo = x.Product.Code,
                     Titulo = x.Product.Title,
                     Color = x.Color.Description,
                     Talle = x.Size.Description,
                     Stock_Disponible = x.StockVirtual,
                     Stock_Fisico = x.StockReal,
                     Categoria = (x.Product.Category.ParentCategory != null ? x.Product.Category.ParentCategory.Name : x.Product.Category.Name),
                     Minorista = x.Product.RetailPrice,
                     Mayorista = x.Product.WholesalePrice,
                     Minorista_Tachado = x.Product.OldRetailPrice,
                     Mayorista_Tachado = x.Product.OldWholesalePrice,
                     Mostrar_En_Home = x.Product.ShowInHome ? "Si" : "No",
                     Sale = x.Product.OnSale ? "Si" : "No",
                     New = x.Product.IsNew ? "Si" : "No",
                     Denim = x.Product.Denim ? "Si" : "No",
                     Agotado = x.Product.SoldOut ? "Si" : "No",
                     Publicado = x.Product.Published ? "Si" : "No",
                     PublicadoMayorista = x.Product.PublishedWholesaler ? "Si" : "No",
                     Control_De_Stock = x.Product.HasStockControl ? "Si" : "No",
                     Foto = x.Product.Images.Any() ? "Si" : "No"

                    //Deposito = x.Deposit.Name,

                    //Subcategoria = (x.Product.Category.ParentCategory != null ? x.Product.Category.Name : string.Empty),

                    //P_Distribuidor = x.Product.DistributorPrice,
                    //Descripcion = x.Product.Description,
                    //Meta_Title = x.Product.MetaTitle,
                    //Meta_Description = x.Product.MetaDescription,
                    //Keywords = x.Product.Tags
                });

            var grid = new System.Web.UI.WebControls.GridView();
            grid.DataSource = products.ToList();
            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=BlancoParis-Stock.xls");
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }

        [HttpGet]
        public ActionResult ExportPriceListToExcel()
        {
            var products = modelEntities.Stocks.Where(x => x.Product.Active && x.Product.Deleted == null && x.StockReal > 0)
                .OrderBy(x => x.Product.Code).ThenByDescending(x => x.Size.Description).ThenBy(x => x.Color.Description).ThenBy(x => x.Deposit.Name)
                .Select(x => new StockListViewModel
                {
                    ProductId = x.ProductId,
                    SizeId = x.SizeId,
                    ColorId = x.ColorId,
                    Code = x.Product.Code,
                    Size = x.Size.Description,
                    Color = x.Color.Description,
                    CategoryName = x.Product.Category.CompleteName,
                    RetailPrice = x.Product.RetailPrice,
                    WholesalePrice = x.Product.WholesalePrice,
                    DistributorPrice = x.Product.DistributorPrice,
                }).ToList();

            var groupedList = products.GroupBy(g => new { g.Code, g.Size, g.Color, g.CategoryName, g.RetailPrice, g.WholesalePrice, g.DistributorPrice })
                .Select(n => new 
                {
                    Codigo = n.Key.Code,
                    Talle = n.Key.Size,
                    Color = n.Key.Color,
                    Categoria = n.Key.CategoryName,
                    Minorista = string.Format("{0:c2}", n.Key.RetailPrice),
                    Mayorista = string.Format("{0:c2}", n.Key.WholesalePrice),
                    DistributorPrice = string.Format("{0:c2}", n.Key.DistributorPrice)
                })
                .ToList();

            var grid = new System.Web.UI.WebControls.GridView();
            grid.DataSource = groupedList.ToList();
            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=BlancoParis-Lista-De-Precios.xls");
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }

        [HttpGet]
        public ActionResult ExportStocksByDepositToExcel()
        {
            var products = modelEntities.Stocks.Where(x => x.Product.Active && x.Product.Deleted == null && x.StockReal > 0)
                .OrderBy(x => x.Product.Code).ThenByDescending(x => x.Size.Description).ThenBy(x => x.Color.Description).ThenBy(x => x.Deposit.Name)
                .Select(x => new
                {
                    Código = x.Product.Code,
                    Título = x.Product.Title,
                    Talle = x.Size.Description,
                    Color = x.Color.Description,
                    Deposito = x.Deposit.Name,
                    Stock_Físico = x.StockReal
                }).ToList();

            var grid = new System.Web.UI.WebControls.GridView();
            grid.DataSource = products.ToList();
            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=BlancoParis-Stock_por_deposito.xls");
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }

        #endregion

    }
}
 