﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using eCommerce.BackOffice.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using eCommerce.Services;
using eCommerce.Services.Services.Accounting;
using System.Web;
using System.IO;

namespace eCommerce.BackOffice.Controllers
{
    public class TagsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        private const string TagsFileUploadDirectory = "/Content/UploadDirectory/Tags";

        [HttpGet]
        public ActionResult Index()
        {
            //var tags = modelEntities.Tags.Where(x => x.Deleted == null).ToList();
            return View("TagList", this.TagsToList());
        }

        private List<Tag> TagsToList()
        {
            return modelEntities.Tags.Where(x => x.Deleted == null).OrderBy(x => x.TagName).ToList();
        }

        [HttpGet]
        public ActionResult TagUpdate(int Id)
        {

            if (Id != -1)
            {

                var path = Path.Combine(TagsFileUploadDirectory, Id.ToString());
                var tag = modelEntities.Tags.Find(Id);

                if (!string.IsNullOrEmpty(tag.Image))
                    ViewData["image"] = Path.Combine(path, tag.Image);

                return View(tag);
            }

            return View(new Tag { TagId = Id });
        }

        [HttpPost]
        public ActionResult TagUpdate(FormCollection form, HttpPostedFileBase image)
        {
            Tag tag = new Tag();

            if (form["TagId"] != string.Empty && Convert.ToInt16(form["TagId"]) > 0)
            {
                int tagId = Convert.ToInt16(form["TagId"]);
                tag = modelEntities.Tags.FirstOrDefault(x => x.TagId == tagId && x.Deleted == null);
            }

            TryUpdateModel(tag, form);

            if (tag.TagId <= 0)
            {
                modelEntities.Tags.Add(tag);
            }

            modelEntities.SaveChanges();

            if (image != null && image.ContentLength > 0)
            {
                string itemId = tag.TagId.ToString();
                var path = Path.Combine(Server.MapPath(TagsFileUploadDirectory), itemId);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string fileName = Path.GetFileName(image.FileName);
                string fileExtension = fileName.Substring(image.FileName.LastIndexOf("."));
                string new_fileName = "image_" + itemId + fileExtension;
                string filePath = Path.Combine(path, new_fileName);

                image.SaveAs(filePath);
                tag.Image = new_fileName;
                modelEntities.SaveChanges();
            }

            //Product tags
            //if (!string.IsNullOrEmpty(form["Products"]))
            //{
            //    var productIds = form["Products"].Split(',').ToList();

            //    //Borro los que estaban antes y creo los nuevos
            //    if (modelEntities.ProductTags.Any(x => x.TagId == tag.TagId))
            //    {
            //        var productsInCurrentTag = tag.ProductTags.FirstOrDefault().Tag.ProductTags.ToList();
            //        modelEntities.ProductTags.RemoveRange(productsInCurrentTag);
            //    }
            //    modelEntities.SaveChanges();

            //    foreach (var prodId in productIds)
            //    {
            //        ProductTag prodTag = new ProductTag();
            //        prodTag.ProductId = int.Parse(prodId);
            //        prodTag.TagId = tag.TagId;
            //        modelEntities.ProductTags.Add(prodTag);
            //    }

            //    modelEntities.SaveChanges();
            //} 


            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult TagDelete(int Id)
        {
            var tag = modelEntities.Tags.FirstOrDefault(x => x.TagId == Id);
            if (tag != null)
            {
                tag.Deleted = CurrentDate.Now;
                tag.DeletedBy = "Borrado por backoffice";
                modelEntities.SaveChanges();
            }

            return PartialView("_TagList", this.TagsToList());
        }

    }
}