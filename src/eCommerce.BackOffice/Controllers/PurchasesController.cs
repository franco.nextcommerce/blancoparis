﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using eCommerce.Services.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class PurchasesController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            return View("PurchaseList", this.PuerchasesToShow());
        }

        public ActionResult SuggestedPurchases()
        {
            List<Product> products = modelEntities.Products.Where(x => x.Active == true).ToList();
            return View("SuggestedList", products);
        }

        [HttpGet]
        public ActionResult PurchaseUpdate(int Id)
        {
            var suppliers = modelEntities.Suppliers.Where(x => x.Active).ToList();

            if (Id != -1)
            {
                var purchase = modelEntities.Purchases.Include("PurchaseDetails").SingleOrDefault(x => x.PurchaseId == Id);
                purchase.PurchaseDetails = purchase.PurchaseDetails.Where(x => x.Active && x.Deleted == null).ToList();
                    
                TempData["Suppliers"] = new SelectList(suppliers, "SupplierId", "Name", purchase.SupplierId);

                var products = modelEntities.Products.Where(x => x.Active).Select(x => new { ProductId = x.ProductId, Text = x.Code + " - " + x.Title.Substring(0, 50) }).ToList();
                TempData["Products"] = new SelectList(products, "ProductId", "Text");

                return View(purchase);
            }

            TempData["Suppliers"] = new SelectList(suppliers, "SupplierId", "Name");

            return View(new Purchase { PurchaseId = Id });
        }

        [HttpPost]
        public ActionResult PurchaseUpdate(FormCollection form)
        {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        Purchase purchase = new Purchase();

                        if (form["PurchaseId"] != string.Empty && Convert.ToInt16(form["PurchaseId"]) > 0)
                        {
                            int PurchaseId = Convert.ToInt16(form["PurchaseId"]);
                            purchase = modelEntities.Purchases.SingleOrDefault(x => x.PurchaseId == PurchaseId && x.Active);
                        }

                        TryUpdateModel(purchase, form);

                        if (!string.IsNullOrEmpty(Request["IIBB_Percent"]))
                            purchase.IIBB_Percent = decimal.Parse(Request["IIBB_Percent"].Replace(".", ",")); 

                        if (purchase.CurrencyId == (int)CurrencyEnum.Peso)
                            purchase.ChangeRate = null;
                        else
                        {
                            decimal changeRate;  
                            if (!string.IsNullOrWhiteSpace(Request["ChangeRate"]) && decimal.TryParse(Request["ChangeRate"].Replace(".", ","),out changeRate))
                                purchase.ChangeRate = changeRate;
                            else
                                purchase.ChangeRate = null;
                        }    

                        if (purchase.PurchaseId <= 0)
                        {
                            purchase.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;
                            purchase.DeliveryStatusId = (int)DeliveryStatusEnum.Pendiente;

                            purchase.Active = true;
                            purchase.Created = CurrentDate.Now;
                            purchase.CreatedBy = User.Identity.Name;
                            modelEntities.Purchases.Add(purchase);

                            modelEntities.SaveChanges();
                        }

                        //Set PurchaseItems
                        var listItems = GetPurchaseDetails(form, purchase.PurchaseId);
                        UpdatePurchaseDetails(purchase, listItems);

                        purchase.Total = decimal.Parse(Request["Total"].Replace(".", ","));

                        UpdateSupplierAccount(purchase);

                        UpdateSupplierAccountBalance(purchase.SupplierId);

                        //UpdateProductsCostProcess(purchase);

                        modelEntities.SaveChanges();
                        dbContextTransaction.Commit();

                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");                        
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar guardar los datos.");
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult PurchaseDelete(int Id)
        {
            var purchase = modelEntities.Purchases.SingleOrDefault(x => x.PurchaseId == Id && x.Active);
            if (purchase != null)
            {
                if (purchase.hasPayments || purchase.hasDeliveries)
                    throw new Exception();

                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        purchase.Active = false;
                        purchase.Deleted = CurrentDate.Now;
                        purchase.DeletedBy = User.Identity.Name;
                        modelEntities.SaveChanges();

                        RemoveSupplierPurchaseTransaction(Id);

                        UpdateSupplierAccountBalance(purchase.SupplierId);

                        dbContextTransaction.Commit();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            
            return PartialView("_PurchaseList", this.PuerchasesToShow());
        }

        #region PURCHASE SKUS

        /*
        [HttpGet]
        public ActionResult PurchaseSkus(int purchaseId)
        {
            ViewData["puchaseId"] = purchaseId;
             
            var list = modelEntities.ProductSkus.Where(x => x.PurchaseId == purchaseId).ToList();
            int count = list.Count;
            
            //Si ya existen no los vuelvo a generar - revisar.
            if (count > 0)
            {
                ViewData["ItemsCount"] = count;
                return View("SkuList", list);
            }

            //Get Purchase Items
            var purchaseItems = modelEntities.PurchaseDetails.Where(x => x.PurchaseId == purchaseId).ToList();

            SKUManager manager = new SKUManager();
            foreach (PurchaseDetail item in purchaseItems)
            {
                manager.GenerateProductSKUs(item.ProductId, item.PurchaseId, item.Quantity, item.UnitsPerBox,ProductSKU_Origin.Purchase);
            }

            list = modelEntities.ProductSkus.Where(x => x.PurchaseId == purchaseId).ToList();
            ViewData["ItemsCount"] = list.Count;

            return View("SkuList", list);
        }

        [HttpGet]
        public ActionResult RecreatePurchaseSkus(int purchaseId)
        {
            //Delete existen items associated to PurchaseId.
            modelEntities.ProductSkus.RemoveRange(modelEntities.ProductSkus.Where(x => x.PurchaseId == purchaseId));
            modelEntities.SaveChanges();

            return RedirectToAction("PurchaseSkus", new { purchaseId = purchaseId });
        }
        */
        #endregion

        #region Private Methods

        private List<Purchase> PuerchasesToShow()
        {
            return modelEntities.Purchases.Where(x => x.Active).OrderByDescending(x => x.PurchaseId).ToList();
        }

        private List<PurchaseDetail> GetPurchaseDetails(FormCollection form, int purchaseId)
        {
            List<PurchaseDetail> items = new List<PurchaseDetail>();

            bool create_new_item = false;
            PurchaseDetail item = new PurchaseDetail();
            foreach (string key in Request.Form.AllKeys)
            {
                if (key.Contains("_"))
                {
                    if (create_new_item)
                    {
                        item = new PurchaseDetail();
                        create_new_item = false;
                    }

                    if (key.Contains("Quantity"))
                        item.Quantity = Convert.ToInt32(Request.Form[key]);
                    /*if (key.Contains("UnitsPerBox") && !string.IsNullOrEmpty(Request.Form[key]))
                        item.UnitsPerBox = Convert.ToInt32(Request.Form[key]);*/
                    else if (key.Contains("IVA_Percent") && !string.IsNullOrWhiteSpace(Request.Form[key]))
                        item.Iva_Percent = decimal.Parse(Request.Form[key].Replace(".", ","));
                    else if (key.Contains("Internal_Tax") && !string.IsNullOrWhiteSpace(Request.Form[key]))
                        item.Internal_Tax = decimal.Parse(Request.Form[key].Replace(".", ","));
                    else if (key.Contains("ProductId"))
                        item.ProductId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("PurchaseDetailId"))
                        item.PurchaseDetailId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("Price"))
                        item.Price = decimal.Parse(Request.Form[key].Replace(".", ","));
                    else if (key.Contains("Subtotal") && key != "Subtotal_Pesos")
                    {
                        item.Subtotal = decimal.Parse(Request.Form[key].Replace(".", ","));
                        item.PurchaseId = purchaseId;
                        items.Add(item);
                        create_new_item = true;
                    }
                }
            }

            return items;
        }

        private void UpdatePurchaseDetails(Purchase purchase, List<PurchaseDetail> details)
        {
            foreach (var detail in purchase.PurchaseDetails)
            {
                var item = details.SingleOrDefault(x => x.PurchaseDetailId == detail.PurchaseDetailId);

                if (item != null)
                {
                    detail.Modified = CurrentDate.Now;
                    detail.ModifiedBy = User.Identity.Name;
                    detail.Quantity = item.Quantity;
                    detail.Price = item.Price;
                    detail.Iva_Percent = item.Iva_Percent;
                    detail.Internal_Tax = item.Internal_Tax;
                    detail.Subtotal = item.Subtotal;
                }
                else
                {
                    detail.Active = false;
                    detail.Deleted = CurrentDate.Now;
                    detail.DeletedBy = User.Identity.Name;
                }
            }

            foreach (var item in details.Where(x => x.PurchaseDetailId == -1))
            {
                item.Purchase = purchase;
                item.Active = true;
                item.Created = CurrentDate.Now;
                item.CreatedBy = User.Identity.Name;
                modelEntities.PurchaseDetails.Add(item);
            }
        }

        //private void UpdateProductsCostProcess(Purchase purchase)
        //{
        //    //Formula de Costo Promedio:
        //    //1- [Costo promedio actual * Stock Actual] + [costo nuevo unitario * Cantidad comprada].
        //    //2- [Resultado 1] / [Existencia Total](Stock actual + StockCromprado) = Nuevo Costo Promedio

        //    CurrencyEnum operationCurrency = CurrencyEnum.Peso;
        //    if (purchase.CurrencyId == (int)CurrencyEnum.Dolar && !purchase.ChangeRate.HasValue)
        //        operationCurrency = CurrencyEnum.Dolar;

        //    Currency currencyExchange = modelEntities.Currencies.First(x => x.CurrencyId == (int)CurrencyEnum.Dolar);

        //    var purchaseDetails = purchase.PurchaseDetails.Where(x => x.Active && x.Deleted == null);
        //    int[] productIds = purchaseDetails.Select(s => s.ProductId).ToArray();
        //    var products = modelEntities.Products.Where(x => productIds.Any(a => a == x.ProductId) && x.Active).ToList();

        //    foreach (var item in purchaseDetails)
        //    {
        //        var product = products.FirstOrDefault(x => x.ProductId == item.ProductId);

        //        decimal averageCost;
        //        CurrencyEnum currentCurrency;
        //        CurrencyEnum destinationCurrency;

        //        if (product.AverageCostUsd.HasValue)
        //        {
        //            currentCurrency = CurrencyEnum.Dolar;
        //            averageCost = product.AverageCostUsd.HasValue ? product.AverageCostUsd.Value : 0;
        //        }
        //        else
        //        {
        //            currentCurrency = CurrencyEnum.Peso;
        //            averageCost = product.AverageCost.HasValue ? product.AverageCost.Value : 0;
        //        }

        //        //Restar la cantidad recibida en entregas parciales de esta compra a la cantidad existente.?????
        //        /*if (purchase.hasDeliveries)
        //        {
        //            var a = modelEntities.DeliverySuppliersDetails
        //                .Where(x => x.ProductId == product.ProductId && x.DeliverySupplier.PurchaseId == purchase.PurchaseId)
        //                .Sum(x => x.Quantity);
        //        }*/
                    
        //        //if existent stock > new stock then keeps product cost currency.
        //        /*if (product.StockReal > item.Quantity) 
        //            destinationCurrency = currentCurrency;
        //        else
        //            destinationCurrency = operationCurrency;*/

        //        decimal average_cost = 0;
        //        if (currentCurrency != operationCurrency)
        //        {
        //            if (currentCurrency == CurrencyEnum.Dolar)
        //                averageCost = averageCost * currencyExchange.ExchangeRate.Value;
        //            else
        //                averageCost = averageCost / currencyExchange.ExchangeRate.Value;
        //        }

        //        //average_cost = ((averageCost * product.StockReal.Value) + (item.Quantity * item.Price)) / (product.StockReal.Value + item.Quantity);
                
        //        if (operationCurrency != destinationCurrency)
        //        {
        //            if(destinationCurrency == CurrencyEnum.Dolar)
        //            {
        //                product.AverageCostUsd = average_cost / currencyExchange.ExchangeRate.Value;
        //                product.AverageCost = null;
        //            }
        //            else
        //            {
        //                product.AverageCostUsd = average_cost * currencyExchange.ExchangeRate.Value;
        //                product.AverageCost = null;
        //            }
        //        }
        //        else
        //        {
        //            if (destinationCurrency == CurrencyEnum.Dolar)
        //            {
        //                product.AverageCostUsd = average_cost;
        //                product.AverageCost = null;
        //            }
        //            else
        //            {
        //                product.AverageCost = average_cost;
        //                product.AverageCostUsd = null;
                        
        //            }
        //        }
        //    }

        //    modelEntities.SaveChanges();
        //}

        private void UpdateSupplierAccount(Purchase purchase)
        {
            try
            {
                if (purchase.Transactions.Any(x => x.PurchaseId == purchase.PurchaseId && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso && x.Deleted == null))
                {
                    var transaction = purchase.Transactions.FirstOrDefault();
                    transaction.Amount = purchase.Total * -1;
                    transaction.CurrencyId = purchase.ChangeRate.HasValue && purchase.ChangeRate.Value > 0 ? (int)CurrencyEnum.Peso : purchase.CurrencyId;
                    transaction.Date = CurrentDate.Now;
                }
                else
                {
                    Transaction transaction = new Transaction();

                    transaction.PurchaseId = purchase.PurchaseId;
                    transaction.Amount = purchase.Total * -1;
                    transaction.CurrencyId = purchase.ChangeRate.HasValue && purchase.ChangeRate.Value > 0 ? (int)CurrencyEnum.Peso : purchase.CurrencyId;
                    transaction.TransactionTypeId = (int)TransactionTypeEnum.Egreso;
                    transaction.TransactionCategoryId = int.Parse(ConfigurationManager.AppSettings["PurchaseCategoryId"]);
                    transaction.Date = CurrentDate.Now;
                    transaction.Created = CurrentDate.Now;
                    transaction.CreatedBy = User.Identity.Name;
                    modelEntities.Transactions.Add(transaction);
                }

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateSupplierAccountBalance(int supplierId)
        {
            try
            {
                var supplier = modelEntities.Suppliers.SingleOrDefault(x => x.SupplierId == supplierId);
                var transactions = modelEntities.Transactions.Where(x => x.Purchase.SupplierId == supplierId && x.Deleted == null).ToList();
                
                supplier.Balance = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso).Sum(x => x.Amount);
                supplier.BalanceUsd = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar).Sum(x => x.Amount);

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RemoveSupplierPurchaseTransaction(int purchaseId)
        {
            try
            {
                var transaction = modelEntities.Transactions
                    .FirstOrDefault(x => x.PurchaseId == purchaseId && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso && x.Deleted == null);

                if (transaction != null)
                {
                    transaction.Deleted = CurrentDate.Now;
                    transaction.DeletedBy = User.Identity.Name;
                    modelEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

    }
}