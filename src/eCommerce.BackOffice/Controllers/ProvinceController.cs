﻿using eCommerce.BackOffice.ViewModels;
using eCommerce.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class ProvinceController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        // GET: Province
        //public ActionResult Index()
        //{
        //    return View("FastEditList");
        //}

        [HttpGet]
        [RBAC]
        public virtual ActionResult Index()
        {
            List<ProvinceViewModel> province = new List<ProvinceViewModel>();
            province = modelEntities.Provinces.OrderBy(x => x.ProvinceId).Select(
            x => new ProvinceViewModel
            {
               ProvinceId = x.ProvinceId,
               Name = x.Name,
               Cost = x.Cost
            }).ToList();

            return View("FastEditList", province);
        }
        
        [HttpPost]
        public string FastEdit(int ProvinceId, string Atributo, decimal? NewValue)
        {
            try
            {
                var Province = modelEntities.Provinces.SingleOrDefault(x => x.ProvinceId == ProvinceId);

                switch (Atributo)
                {
                    case "Cost":
                            Province.Cost = NewValue;
                        break;
                    default:
                        break;
                }
                modelEntities.SaveChanges();
                return NewValue.ToString();
            }
            catch (Exception)
            {
                return "error";
            }
        }

    }
}