﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Web;
using eCommerce.Services.Security;
using System.Text;
using eCommerce.BackOffice.ViewModels;
using System.IO;

namespace eCommerce.BackOffice.Controllers
{
    public class ConfigurationController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        private const string LogoFileUploadDirectory = "/Content/UploadDirectory/Logo";

        [HttpGet]
        public ActionResult PromotionalText()
        {
            var list = modelEntities.PromotionalTexts.ToList();
            return View(list);
        }

        [HttpPost]
        public ActionResult PromotionalText(FormCollection form)
        {
            var list = modelEntities.PromotionalTexts.ToList();
            try
            {

                foreach (var item in form.AllKeys.Where(x => x.Contains("hidHtml_")))
                {
                    var promId = int.Parse(item.Split('_')[1]);
                    var text = HttpUtility.UrlDecode(form[item]);
                    var active = Request["active_" + promId] != null && Request["active_" + promId] == "on"; ;
                    var promText = list.FirstOrDefault(promtext => promtext.PromotionalTextId == promId);

                    promText.Text = text;
                    promText.Active = active;

                }

                foreach (var item in form.AllKeys.Where(x => x.Contains("hidHtmlColor_")))
                {
                    var promId = int.Parse(item.Split('_')[1]);
                    var text = form[item];
                    //var active = Request["active_" + promId] != null && Request["active_" + promId] == "on"; ;
                    var promText = list.FirstOrDefault(promtext => promtext.PromotionalTextId == promId);

                    promText.BackgroundColor = text;
                    //promText.Active = active;

                }

                modelEntities.SaveChanges();
                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar actualizar el minimo de compra.");
            }

            return View(list);
        }

        [HttpGet]
        public ActionResult DolarExchangeRate()
        {
            Currency currency = modelEntities.Currencies.FirstOrDefault(x => x.CurrencyId == (int)CurrencyEnum.Dolar);

            return View("DolarExchangeRate", currency);
        }

        [HttpPost]
        public ActionResult DolarExchangeRate(FormCollection form)
        {
            Currency currency = modelEntities.Currencies.FirstOrDefault(x => x.CurrencyId == (int)CurrencyEnum.Dolar);

            try
            {
                decimal exchangeRate = decimal.Parse(form["ExchangeRate"]);
                currency.ExchangeRate = exchangeRate;

                modelEntities.SaveChanges();
                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar actualizar la Cotización del Dolar.");
            }

            return View("DolarExchangeRate", currency);
        }

        [HttpGet]
        public ActionResult ShipmentCost()
        {
            Parameter parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "ShipmentCost");
            return View(parameter);
        }

        [HttpPost]
        public ActionResult ShipmentCost(FormCollection form)
        {
            Parameter parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "ShipmentCost");

            try
            {
                decimal costo = decimal.Parse(form["Value"]);
                parameter.Value = costo;
                modelEntities.SaveChanges();
                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar actualizar el Costo de envio para Transporte.");
            }

            return View(parameter);
        }

        [HttpGet]
        public ActionResult FreeShipping()
        {
            List<Parameter> list = new List<Parameter>();
            Parameter parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "FreeShippingRetail");
            list.Add(parameter);
            parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "FreeShippingWS");
            list.Add(parameter);
            parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "FreeShippingD");
            list.Add(parameter);

            return View(list);
        }

        [HttpPost]
        public ActionResult FreeShipping(FormCollection form)
        {
            List<Parameter> list = new List<Parameter>();
            try
            {
                Parameter parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "FreeShippingRetail");
                list.Add(parameter);
                parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "FreeShippingWS");
                list.Add(parameter);
                parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "FreeShippingD");
                list.Add(parameter);

                foreach (var item in list)
                {
                    decimal costo = decimal.Parse(form[item.Name]);
                    item.Value = costo;
                }

                modelEntities.SaveChanges();
                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                return View(list);
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar actualizar el costo de envío gratis.");
                return View(list);
            }
        }

        [HttpGet]
        public ActionResult Discounts()
        {
            var config = modelEntities.Configurations.FirstOrDefault();
            return View(config);
        }

        [HttpPost]
        public ActionResult Discounts(FormCollection form)
        {
            var config = modelEntities.Configurations.FirstOrDefault();

            try
            {
                config.FirstOrderDiscountEnabled = Request["FirstOrderDiscountEnabled"] != null && Request["FirstOrderDiscountEnabled"] == "on";

                if (config.FirstOrderDiscountEnabled)
                {
                    config.FirstOrderDiscountDurationPeriodDays = int.Parse(form["FirstOrderDiscountDurationPeriodDays"]);
                    config.FirstOrderDiscountPercent = int.Parse(form["FirstOrderDiscountPercent"]);
                }

                config.FirstOrderDiscountEnabledWS = Request["FirstOrderDiscountEnabledWS"] != null && Request["FirstOrderDiscountEnabledWS"] == "on";

                if (config.FirstOrderDiscountEnabledWS)
                {
                    config.FirstOrderDiscountDurationPeriodDaysWS = int.Parse(form["FirstOrderDiscountDurationPeriodDaysWS"]);
                    config.FirstOrderDiscountPercentWS = int.Parse(form["FirstOrderDiscountPercentWS"]);
                }

                modelEntities.SaveChanges();

                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            }
            catch (Exception)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar actualizar la configuración.");
            }

            return View(config);
        }

        [HttpGet]
        public ActionResult MinimumOrder()
        {
            List<Parameter> list = new List<Parameter>();
            Parameter parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "MinimumOrderRetail");
            list.Add(parameter);
            parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "MinimumOrderWS");
            list.Add(parameter);
            parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "MinimumOrderD");
            list.Add(parameter);

            return View(list);
        }

        [HttpPost]
        public ActionResult MinimumOrder(FormCollection form)
        {
            List<Parameter> list = new List<Parameter>();
            try
            {
                Parameter parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "MinimumOrderRetail");
                list.Add(parameter);
                parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "MinimumOrderWS");
                list.Add(parameter);
                parameter = modelEntities.Parameters.SingleOrDefault(x => x.Name == "MinimumOrderD");
                list.Add(parameter);

                foreach (var item in list)
                {
                    decimal costo = decimal.Parse(form[item.Name]);
                    item.Value = costo;
                }

                modelEntities.SaveChanges();
                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                return View(list);
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar actualizar el minimo de compra.");
                return View(list);
            }
        }

        [HttpGet]
        public ActionResult Parameters()
        {
            var parameters = modelEntities.Parameters.Where(x => x.Visible != null && x.Visible == true).ToList();

            var logo = parameters.FirstOrDefault(y => y.Name == "Logo");
            var path = Path.Combine(LogoFileUploadDirectory, "1");
            if (!string.IsNullOrEmpty(logo.Text))
                ViewData["image"] = Path.Combine(path, logo.Text);

            if (parameters != null)
            {
                return View(parameters);
            }

            return View(new Parameter());
        }

        [HttpPost]
        public ActionResult Parameters(FormCollection form, HttpPostedFileBase image)
        {
            var parameters = modelEntities.Parameters.Where(x => x.Visible == true).ToList();
            string new_fileName = "";

            if (image != null && image.ContentLength > 0)
            {
                var param = parameters.FirstOrDefault(x => x.Name == "Logo");

                string itemId = "1";
                var path = Path.Combine(Server.MapPath(LogoFileUploadDirectory), itemId);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string fileName = Path.GetFileName(image.FileName);
                string fileExtension = fileName.Substring(image.FileName.LastIndexOf("."));
                new_fileName = "logo_" + itemId + fileExtension;
                string filePath = Path.Combine(path, new_fileName);

                image.SaveAs(filePath);
                param.Text = new_fileName;
            }

            try
            {

                foreach (var item in form.AllKeys.Where(x => x.Contains("param-")))
                {
                    var paramName = item.Split('-')[1];
                    var param = parameters.FirstOrDefault(x => x.Name == paramName);

                    if (param != null)
                    {

                        if (paramName == ConfigurationParametersEnum.MinimumOrderRetail.ToString() || paramName == ConfigurationParametersEnum.FreeShippingRetail.ToString())
                        {
                            if (!string.IsNullOrEmpty(form[item]))
                                param.Value = decimal.Parse(form[item]);
                            else
                                param.Value = null;
                        }else
                        {
                            param.Text = form[item];
                        }

                        //switch ("decimal")
                        //{
                        //    case "decimal":
                        //    case "integer":
                        //        if (!string.IsNullOrEmpty(form[item]))
                        //            param.Value = decimal.Parse(form[item]);
                        //        else
                        //            param.Value = null;
                        //        break;

                        //    case "string":
                        //        param.Text = form[item];
                        //        break;
                        //}
                    }
                }

                modelEntities.SaveChanges();
                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar actualizar el minimo de compra.");
            }

            parameters = modelEntities.Parameters.Where(x => x.Visible != null && x.Visible == true).ToList();
            var logo = parameters.FirstOrDefault(y => y.Name == "Logo");
            var pathImage = Path.Combine(LogoFileUploadDirectory, "1");
            if (!string.IsNullOrEmpty(logo.Text))
                ViewData["image"] = Path.Combine(pathImage, logo.Text);

            return View(parameters);
        }

        [HttpGet]
        public ActionResult Configurations()
        {
            var parameters = modelEntities.Parameters.Where(x => x.Visible != null && x.Visible == true).ToList();

            if (parameters != null)
            {
                return View(parameters);
            }
            return View(new Parameter());
        }


        [HttpPost]
        public ActionResult Configurations(FormCollection form)
        {
            var parameters = modelEntities.Parameters.Where(x => x.Visible != null && x.Visible == true).ToList();
            try
            {
                foreach (var item in form.AllKeys.Where(x => x.Contains("_")))
                {
                    var paramId = int.Parse(item.Split('_')[1]);
                    var matchingParameter = modelEntities.Parameters.FirstOrDefault(x => x.ParameterId == paramId);
                    if (matchingParameter.Name == "PromotionalBanner")
                    {
                        matchingParameter.Text = HttpUtility.UrlDecode(form[item]);
                    }
                    else
                    {
                        matchingParameter.Value = decimal.Parse(form[item]);
                    }
                }

                modelEntities.SaveChanges();
                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar actualizar el minimo de compra.");
            }

            return View(parameters);
        }

        public string UpdateSeoUrls()
        {
            var categories = modelEntities.Categories.Where(x => x.Active);
            var products = modelEntities.Products.Where(x => x.Active && x.Deleted == null && string.IsNullOrEmpty(x.SeoUrl));
            try
            {
                foreach (var cat in categories)
                {
                    var cat_url = Regex.Replace(cat.Name, "[^0-9A-Za-z ]", "");
                    cat_url = cat_url.Replace(" ", "-").ToLower();
                    if (cat.ParentCategory != null)
                    {
                        var seo_url = cat.ParentCategory.SeoUrl + "-" + cat_url;
                        var valid = !modelEntities.Categories.Any(x => x.SeoUrl == seo_url && x.CategoryId != cat.CategoryId);
                        if (!valid)
                        {
                            return "Error al actualizar urls de SEO, la categoria '" + cat.Name + "' genera un nombre de SEO repetido.";
                        }
                        else
                        {
                            cat.SeoUrl = seo_url;
                            cat.SeoUrlSelf = cat_url;
                        }
                    }
                    else
                    {
                        var valid = !modelEntities.Categories.Any(x => x.SeoUrl == cat_url && x.CategoryId != cat.CategoryId);
                        if (!valid)
                        {
                            return "Error al actualizar urls de SEO, la categoria '" + cat.Name + "' genera un nombre de SEO repetido.";
                        }
                        else
                        {
                            cat.SeoUrl = cat_url;
                            cat.SeoUrlSelf = cat_url;
                        }
                    }
                }

                foreach (var prod in products)
                {
                    var prod_url = Regex.Replace(prod.Title, "[^0-9A-Za-z ]", "");
                    prod_url = prod_url.Replace(" ", "-").ToLower();

                    var valid = !modelEntities.Products.Any(x => x.SeoUrl == prod_url && x.ProductId != prod.ProductId && x.Active && x.Deleted == null);
                    if (!valid)
                    {
                        return "Error al actualizar urls de SEO, el producto '" + prod.Title + "' genera un nombre de SEO repetido.";
                    }
                    else
                    {
                        prod.SeoUrl = prod_url;
                    }
                }
                modelEntities.SaveChanges();
                return "Actualización completada correctamente.";
            }
            catch (Exception)
            {
                return "Error inesperado";
                throw;
            }

        }

        //public string UpdateCustomerPasswords()
        //{
        //    try
        //    {
        //        String KeyString = "Gv7L3V15jCdb9P5XGKiPnhHZ7JlKcmU=";
        //        var customers = modelEntities.Customers.Where(x => x.Password.Equals("Mayoristagms17!")).ToList();
        //        foreach (var customer in customers)
        //        {
        //            customer.Password = EncryptionService.Encrypt(customer.Password, KeyString);
        //        }

        //        modelEntities.SaveChanges();
        //        return "Success";
        //    }
        //    catch (Exception ex)
        //    {
        //        return $"Error: {ex.Message}";
        //    }
        //}

        [HttpGet]
        public ActionResult PaymentMethods()
        {
            var PaymentMethods = modelEntities.PaymentMethods.Where(x => x.Active).ToList();
            var paymentMethodsSelect = modelEntities.PaymentMethods.Where(x => x.Active);
            TempData["PaymentMethods"] = new SelectList(paymentMethodsSelect, "PaymentMethodId", "Name");

            if (PaymentMethods != null)
            {
                return View(PaymentMethods);
            }
            return View(new PaymentMethod());
        }

        [HttpPost]
        public ActionResult PaymentMethods(FormCollection form)
        {

            foreach (var key in form.AllKeys.Where(x => x.StartsWith("PM_Id")))
            {
                PaymentMethod pm = new PaymentMethod();
                var id = form[key] != null ? int.Parse(form[key]) : null as int?;
                var n = key.Split('-')[1];

                TryUpdateModel(pm, form);

                if (id > 0)
                {
                    var paymentMethod = modelEntities.PaymentMethods.FirstOrDefault(x => x.PaymentMethodId == id);

                    var deleted = form[$"PM_Deleted-{n}"] != null ? bool.Parse(form[$"PM_Deleted-{n}"]) : false;

                    if (deleted)
                    {
                        if (paymentMethod.PaymentMethodId > 0)
                            paymentMethod.Active = false;
                        modelEntities.SaveChanges();
                        continue;
                    }

                    paymentMethod.Name = !string.IsNullOrEmpty(form[$"PM_Name-{n}"]) ? form[$"PM_Name-{n}"] : null;
                    paymentMethod.Description = !string.IsNullOrEmpty(form[$"PM_Description-{n}"]) ? form[$"PM_Description-{n}"] : null;
                    paymentMethod.RequiresData = !string.IsNullOrEmpty(form[$"PM_RequiresData-{n}"]) && form[$"PM_RequiresData-{n}"] == "on";
                    paymentMethod.Enabled = !string.IsNullOrEmpty(form[$"PM_Enabled-{n}"]) && form[$"PM_Enabled-{n}"] == "on"; ;
                    paymentMethod.MailText = HttpUtility.UrlDecode(Request[$"PM_MailText-{n}"]);

                    modelEntities.SaveChanges();
                }
                else
                {
                    pm.Name = !string.IsNullOrEmpty(form[$"PM_Name-{n}"]) ? form[$"PM_Name-{n}"] : null;
                    pm.Description = !string.IsNullOrEmpty(form[$"PM_Description-{n}"]) ? form[$"PM_Description-{n}"] : null;
                    pm.RequiresData = !string.IsNullOrEmpty(form[$"PM_RequiresData-{n}"]) && form[$"PM_RequiresData-{n}"] == "on";
                    pm.MailText = HttpUtility.UrlDecode(Request[$"PM_MailText-{n}"]);
                    pm.Active = true;
                    modelEntities.PaymentMethods.Add(pm);
                    modelEntities.SaveChanges();
                }
            }

            var PaymentMethods = modelEntities.PaymentMethods.Where(x => x.Active).ToList();
            var paymentMethodsSelect = modelEntities.PaymentMethods.Where(x => x.Active);
            TempData["PaymentMethods"] = new SelectList(paymentMethodsSelect, "PaymentMethodId", "Name");

            return View(PaymentMethods);
        }

        [HttpGet]
        public ActionResult DeliveryMethods()
        {
            var DeliveryMethods = modelEntities.DeliveryMethods.Where(x => x.Active).ToList();
            var deliveryMethodsSelect = modelEntities.DeliveryMethods.Where(x => x.Active);
            var PaymentMethods = modelEntities.PaymentMethods.Where(x => x.Active);
            TempData["DeliveryMethods"] = new SelectList(deliveryMethodsSelect, "DeliveryMethodId", "Name");
            TempData["ExcludedPaymentMethod"] = new MultiSelectList(PaymentMethods, "PaymentMethodId", "Name");

            List<DeliveryMethodListViewModel> dm = new List<DeliveryMethodListViewModel>();

            foreach (var item in DeliveryMethods)
            {
                DeliveryMethodListViewModel model = new DeliveryMethodListViewModel();


                var excludedPaymentValue = item.ExcludedPaymetnMethods != null ? item.ExcludedPaymetnMethods.Split(',') : null ; 

                model.DeliveryMethodId = item.DeliveryMethodId;
                model.Name = item.Name;
                model.Description = item.Description;
                model.Cost = item.Cost;
                model.Enabled = item.Enabled;
                model.IsLocal = item.IsLocal;
                model.IsProvince = item.IsProvince;
                model.IsTransport = item.IsTransport;
                model.RequiresData = item.RequiresData;
                model.ShowLocales = item.ShowLocales;
                model.Form = new SelectList(DeliveryMethods, "DeliveryMethodId", "Name", model.DeliveryMethodId);
                model.ExcludedPaymentMethods = new MultiSelectList(PaymentMethods, "PaymentMethodId", "Name", excludedPaymentValue);

                dm.Add(model);
            }

            if (dm != null)
            {
                return View(dm);
            }
            return View(new DeliveryMethod());
        }

        [HttpPost]
        public ActionResult DeliveryMethods(FormCollection form)
        {
            foreach (var key in form.AllKeys.Where(x => x.StartsWith("DM_Id")))
            {
                DeliveryMethod dm = new DeliveryMethod();
                var id = form[key] != null ? int.Parse(form[key]) : null as int?;
                var n = key.Split('-')[1];

                TryUpdateModel(dm, form);

                if (id > 0)
                {
                    var deliveryMethod = modelEntities.DeliveryMethods.FirstOrDefault(x => x.DeliveryMethodId == id);

                    var deleted = form[$"DM_Deleted-{n}"] != null ? bool.Parse(form[$"DM_Deleted-{n}"]) : false;

                    if (deleted)
                    {
                        if (deliveryMethod.DeliveryMethodId > 0)
                            deliveryMethod.Active = false;
                        modelEntities.SaveChanges();
                        continue;
                    }

                    StringBuilder builder = new StringBuilder();
                    if (form[$"DM_PaymentMethodExcluded-{n}"] != null)
                    {
                        foreach (var value in form[$"DM_PaymentMethodExcluded-{n}"])
                        {
                            if (value == ',')
                            {

                            }
                            else
                            {
                                if (form[$"DM_PaymentMethodExcluded-{n}"].Count() > 1)
                                {
                                    builder.Append(value);
                                    builder.Append(",");
                                }
                                else
                                {
                                    builder.Append(value);
                                }
                            }
                        }
                    }

                    deliveryMethod.Name = !string.IsNullOrEmpty(form[$"DM_Name-{n}"]) ? form[$"DM_Name-{n}"] : null;
                    deliveryMethod.Description = !string.IsNullOrEmpty(form[$"DM_Description-{n}"]) ? form[$"DM_Description-{n}"] : null;
                    deliveryMethod.Cost = !string.IsNullOrEmpty(form[$"DM_Cost-{n}"]) ? int.Parse(form[$"DM_Cost-{n}"]) : 0;
                    deliveryMethod.RequiresData = !string.IsNullOrEmpty(form[$"DM_RequiresData-{n}"]) && form[$"DM_RequiresData-{n}"] == "on";
                    deliveryMethod.IsTransport = !string.IsNullOrEmpty(form[$"DM_IsTransport-{n}"]) && form[$"DM_IsTransport-{n}"] == "on";
                    deliveryMethod.IsProvince = !string.IsNullOrEmpty(form[$"DM_IsProvince-{n}"]) && form[$"DM_IsProvince-{n}"] == "on";
                    deliveryMethod.IsLocal = !string.IsNullOrEmpty(form[$"DM_IsLocal-{n}"]) && form[$"DM_IsLocal-{n}"] == "on";
                    deliveryMethod.ShowLocales = !string.IsNullOrEmpty(form[$"DM_ShowLocales-{n}"]) && form[$"DM_ShowLocales-{n}"] == "on";
                    deliveryMethod.ExcludedPaymetnMethods = !string.IsNullOrEmpty(builder.ToString()) ? builder.ToString() : "";
                    deliveryMethod.Enabled = !string.IsNullOrEmpty(form[$"DM_Enabled-{n}"]) && form[$"DM_Enabled-{n}"] == "on";

                    modelEntities.SaveChanges();
                }
                else
                {

                    dm.Name = !string.IsNullOrEmpty(form[$"DM_Name-{n}"]) ? form[$"DM_Name-{n}"] : null;
                    dm.Description = !string.IsNullOrEmpty(form[$"DM_Description-{n}"]) ? form[$"DM_Description-{n}"] : null;
                    dm.RequiresData = !string.IsNullOrEmpty(form[$"DM_RequiresData-{n}"]) && form[$"DM_RequiresData-{n}"] == "on";
                    dm.Active = true;
                    dm.IsTransport = !string.IsNullOrEmpty(form[$"DM_IsTransport-{n}"]) && form[$"DM_IsTransport-{n}"] == "on";
                    dm.IsProvince = !string.IsNullOrEmpty(form[$"DM_IsProvince-{n}"]) && form[$"DM_IsProvince-{n}"] == "on";
                    dm.IsLocal = !string.IsNullOrEmpty(form[$"DM_IsLocal-{n}"]) && form[$"DM_IsLocal-{n}"] == "on";
                    dm.ShowLocales = !string.IsNullOrEmpty(form[$"DM_ShowLocales-{n}"]) && form[$"DM_ShowLocales-{n}"] == "on";
                    //dm.ExcludedPaymetnMethods = !string.IsNullOrEmpty(builder.ToString()) ? builder.ToString() : "";
                    dm.Enabled = !string.IsNullOrEmpty(form[$"DM_Enabled-{n}"]) && form[$"DM_Enabled-{n}"] == "on";

                    modelEntities.DeliveryMethods.Add(dm);

                    try
                    {
                        modelEntities.SaveChanges();

                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                }
            }

            return RedirectToAction("DeliveryMethods");
        }

        private List<PaymentMethod> PaymentMethodToShow(int productId)
        {
            return modelEntities.PaymentMethods.Where(x => x.PaymentMethodId == productId && x.Active)
                .OrderByDescending(x => x.Name).ToList();
        }
    }
}