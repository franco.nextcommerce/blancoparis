﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using eCommerce.BackOffice.ViewModels;

namespace eCommerce.BackOffice.Controllers
{
    public class CityController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        // GET: City

        public ActionResult Index()
        {
            return View("CityList", this.CitiesToShow());
        }

        [HttpGet]
        public JsonResult GetCities()
        {
            return Json(new { data = this.CitiesToShow() }, JsonRequestBehavior.AllowGet);
        }

        private List<CityListViewModel> CitiesToShow()
        {
            return modelEntities.Cities.Where(x => x.Deleted == null)
                .Select(x => new CityListViewModel()
                {
                    CityId = x.CityId,
                    CityName = x.Name,
                    ProvinceName = x.Province.Name,
                    IsExclusive = x.Exclusive
                }).OrderByDescending(x => x.CityId).ToList();
        }

        [HttpGet]
        public ActionResult CityUpdate(int Id)
        {
            var provinces = modelEntities.Provinces.ToList();

            if (Id != -1)
            {
                var city = modelEntities.Cities.Find(Id);
                TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name", city.ProvinceId);
                return View(city);
            }

            TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name");
            return View(new City { CityId = Id });
        }

        [HttpPost]
        public ActionResult CityUpdate(FormCollection form)
        {
            City city = new City();

            if (form["CityId"] != string.Empty && Convert.ToInt16(form["CityId"]) > 0)
            {
                int cityId = Convert.ToInt16(form["CityId"]);
                city = modelEntities.Cities.Find(cityId);
            }

            TryUpdateModel(city, form);

            if (Request["Exclusive"] != null && Request["Exclusive"] == "on")
                city.Exclusive = true;
            else
                city.Exclusive = false;

            if (city.CityId <= 0)
            {
                modelEntities.Cities.Add(city);
            }

            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }


        [HttpGet]
        public virtual PartialViewResult CityDelete(int Id)
        {
            var City = modelEntities.Cities.FirstOrDefault(x => x.CityId == Id && x.Active);
            if (City != null)
            {
                City.Active = false;
                City.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.Cities.Where(x => x.Active && !x.Deleted.HasValue).ToList();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "La ciudad ha sido borrada correctamente!");
            return PartialView("_CityList", list);
        }
    }
}