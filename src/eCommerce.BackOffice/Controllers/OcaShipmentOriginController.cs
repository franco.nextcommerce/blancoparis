﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System.Configuration;

namespace eCommerce.BackOffice.Controllers
{
    public class OcaShipmentOriginController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        // GET: OcaShipmentOrigin
        public ActionResult Index()
        {
            return View("OriginList", this.OriginsToShow());
        }

        [HttpGet]
        [OutputCache(Duration = 0)]
        public ActionResult OriginUpdate(int Id)
        {
            var provinces = modelEntities.Provinces.ToList();

            int zipCode = int.Parse(ConfigurationManager.AppSettings["CodigoPostalOrigen"]);

            OcaEPAK.Oep_TrackingSoapClient ocaShippingService = new OcaEPAK.Oep_TrackingSoapClient();
            System.Xml.XmlNode node = ocaShippingService.GetCentrosImposicionConServiciosByCP(zipCode);

            List<SelectListItem> items = new List<SelectListItem>();
            if (node.ChildNodes.Count > 0)
            {
                foreach (System.Xml.XmlNode child in node.ChildNodes)
                {
                    var item = new SelectListItem();
                    item.Text = child.ChildNodes[3].InnerText.Trim() + " " + child.ChildNodes[4].InnerText.Trim() + " - Tel: " + child.ChildNodes[11].InnerText.Trim();
                    item.Value = child.ChildNodes[0].InnerText.ToString();
                    items.Add(item);
                }

            }

            if (Id != -1)
            {
                var origin = modelEntities.OcaShipmentOrigins.Find(Id);
                TempData["CentrosDeImposicion"] = new SelectList(items, "Value", "Text", origin.OcaCenterId);
                TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name", origin.ProvinceId);
                return View(origin);
            }

            TempData["CentrosDeImposicion"] = new SelectList(items, "Value", "Text");
            TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name");
            return View(new OcaShipmentOrigin { OcaShipmentOriginId = Id });
        }

        [HttpPost]
        public ActionResult OriginUpdate(FormCollection form)
        {
            OcaShipmentOrigin origin = new OcaShipmentOrigin();

            if (form["OcaShipmentOriginId"] != string.Empty && Convert.ToInt16(form["OcaShipmentOriginId"]) > 0)
            {
                int originId = Convert.ToInt16(form["OcaShipmentOriginId"]);
                origin = modelEntities.OcaShipmentOrigins.Find(originId);
            }

            TryUpdateModel(origin, form);

            if (origin.OcaShipmentOriginId <= 0)
            {
                origin.Created = CurrentDate.Now;
                origin.CreatedBy = User.Identity.Name;
                origin.Active = true;
                modelEntities.OcaShipmentOrigins.Add(origin);
            }

            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult OriginDelete(int Id)
        {
            var origin = modelEntities.OcaShipmentOrigins.Find(Id);
            if (origin != null)
            {
                origin.DeletedBy = User.Identity.Name;
                origin.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            return PartialView("_OriginList", this.OriginsToShow());
        }

        private List<OcaShipmentOrigin> OriginsToShow()
        {
            return modelEntities.OcaShipmentOrigins.Where(x => x.Deleted == null).OrderByDescending(x => x.OcaShipmentOriginId).ToList();
        }
    }
}