﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using eCommerce.BackOffice.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using eCommerce.Services;
using eCommerce.Services.Services.Accounting;
using System.Web;
using System.IO;

namespace eCommerce.BackOffice.Controllers
{
    public class LookbookController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        private const string LookbooksFileUploadDirectory = "/Content/UploadDirectory/Lookbooks";

        [HttpGet]
        public ActionResult Index()
        {
            //var Lookbooks = modelEntities.Lookbooks.Where(x => x.Deleted == null).ToList();
            return View("LookbookList", this.LookbooksToList());
        }

        private List<Lookbook> LookbooksToList()
        {
            return modelEntities.Lookbooks.Where(x => !x.Deleted.HasValue).OrderBy(x => x.Name).ToList();
        }

        [HttpGet]
        public ActionResult LookbookUpdate(int Id)
        {
            var products = modelEntities.Products.Where(x => !x.Deleted.HasValue).OrderBy(p => p.Title).ToList();
            var lookbookGroups = modelEntities.LookbookGroups.Where(x => !x.Deleted.HasValue).OrderBy(p => p.Name).ToList();

            var detailedProducts = products.Select(x => new {
                Id = x.ProductId,
                Name = x.Code + " - " + x.Title,
            });

            if (Id != -1)
            {

                var path = Path.Combine(LookbooksFileUploadDirectory, Id.ToString());
                var Lookbook = modelEntities.Lookbooks.Find(Id);

                TempData["LookbookGroups"] = new SelectList(lookbookGroups, "LookbookGroupId", "Name", Lookbook.LookbookGroupId);
                TempData["Products"] = new MultiSelectList(detailedProducts, "Id", "Name", Lookbook.LookbookProducts.Select(x => x.ProductId));
                if (!string.IsNullOrEmpty(Lookbook.Image))
                    ViewData["image"] = Path.Combine(path, Lookbook.Image);

                return View(Lookbook);
            }

            TempData["LookbookGroups"] = new SelectList(lookbookGroups, "LookbookGroupId", "Name");
            TempData["Products"] = new MultiSelectList(detailedProducts, "Id", "Name");
            return View(new Lookbook { LookbookId = Id });
        }

        [HttpPost]
        public ActionResult LookbookUpdate(FormCollection form, HttpPostedFileBase image)
        {
            Lookbook Lookbook = new Lookbook();

            if (form["LookbookId"] != string.Empty && Convert.ToInt16(form["LookbookId"]) > 0)
            {
                int LookbookId = Convert.ToInt16(form["LookbookId"]);
                Lookbook = modelEntities.Lookbooks.FirstOrDefault(x => x.LookbookId == LookbookId && !x.Deleted.HasValue);
            }

            TryUpdateModel(Lookbook, form);

            if (Lookbook.LookbookId <= 0)
            {
                modelEntities.Lookbooks.Add(Lookbook);
            }

            modelEntities.SaveChanges();

            if (image != null && image.ContentLength > 0)
            {
                string itemId = Lookbook.LookbookId.ToString();
                var path = Path.Combine(Server.MapPath(LookbooksFileUploadDirectory), itemId);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string fileName = Path.GetFileName(image.FileName);
                string fileExtension = fileName.Substring(image.FileName.LastIndexOf("."));
                string new_fileName = "image_" + itemId + fileExtension;
                string filePath = Path.Combine(path, new_fileName);

                image.SaveAs(filePath);
                Lookbook.Image = new_fileName;
                modelEntities.SaveChanges();
            }

            //Product Lookbooks
            if (!string.IsNullOrEmpty(form["Products"]))
            {
                var productIds = form["Products"].Split(',').ToList();

                //Borro los que estaban antes y creo los nuevos
                if (modelEntities.LookbookProducts.Any(x => x.LookbookId == Lookbook.LookbookId))
                {
                    var productsInCurrentLookbook = Lookbook.LookbookProducts;
                    modelEntities.LookbookProducts.RemoveRange(productsInCurrentLookbook);
                }
                modelEntities.SaveChanges();

                foreach (var prodId in productIds)
                {
                    LookbookProduct prodLookbook = new LookbookProduct();
                    prodLookbook.ProductId = int.Parse(prodId);
                    prodLookbook.LookbookId = Lookbook.LookbookId;
                    modelEntities.LookbookProducts.Add(prodLookbook);
                }

                modelEntities.SaveChanges();
            }


            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult LookbookDelete(int Id)
        {
            var Lookbook = modelEntities.Lookbooks.FirstOrDefault(x => x.LookbookId == Id);
            if (Lookbook != null)
            {
                Lookbook.Deleted = CurrentDate.Now;
                Lookbook.DeletedBy = "Borrado por backoffice";
                modelEntities.SaveChanges();
            }

            return PartialView("_LookbookList", this.LookbooksToList());
        }

    }
}