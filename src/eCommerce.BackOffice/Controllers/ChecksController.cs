﻿using eCommerce.BackOffice.Models;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eCommerce.BackOffice.Controllers
{
    public class ChecksController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult GetLastCheckIndex()
        {
            var check = modelEntities.Checks
                           .OrderByDescending(p => p.Created)
                           .FirstOrDefault();

            return Json(new { CheckId = check != null ? check.CheckId : 1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index(ChecksFilterViewModel filter)
        {
            if (filter.Status == 0)
                filter.Status = (int)CheckStatus.EnCartera;

            var checksViewModel = new CheckListViewModel();
            checksViewModel.Filter = filter;

            var checks = modelEntities.Checks.Where(x => x.Status == (int)CheckStatus.EnCartera && x.Deleted == null).OrderByDescending(x => x.Created).ToList();

            checks = FilterChecks(filter, checks);

            checksViewModel.Checks = this.ChecksToList();
            checksViewModel.Suppliers = new SelectList(modelEntities.Suppliers.Where(w => w.Active).ToList(), "SupplierId", "CoroporateName", filter.SupplierId);

            return View("CheckList", checksViewModel);
        }

        private List<Check> ChecksToList()
        {
            return modelEntities.Checks.Where(x => x.Deleted == null).OrderByDescending(x => x.Created).ToList();
        }

        private List<eCommerce.Services.Model.Check> FilterChecks(ChecksFilterViewModel filter, List<eCommerce.Services.Model.Check> checks)
        {
            //checks = FilterByFilterViewType(filter, checks);
            checks = FilterByStatus(filter, checks);
            checks = FilterByDates(filter, checks);
            checks = FilterByDays(filter, checks);
            return checks;
        }

        private List<Check> FilterByDays(ChecksFilterViewModel filter, List<Check> checks)
        {
            if (filter.DaysToDue > 0)
                checks = checks.Where(c => c.DaysToDueDate <= filter.DaysToDue).ToList();

            return checks;
        }

        private List<Check> FilterByDates(BasicFilterViewModel filter, List<Check> checks)
        {
            if (filter.ParsedStartingDate != null && filter.ParsedEndingDate != null)
                checks = checks.Where(p => p.DueDate >= filter.ParsedStartingDate && p.DueDate <= filter.ParsedEndingDate).ToList();

            return checks;
        }

        /*private List<Check> FilterByFilterViewType(BasicFilterViewModel filter, List<Check> checks)
        {
            if (filter.FilterView != 0)
            {
                var filterView = (FilterViewType)filter.FilterView;
                var thisWeek = CurrentDate.Now.StartOfWeek(DayOfWeek.Monday);
                var nextWeek = thisWeek.AddDays(7);

                if (filterView == FilterViewType.ThisWeek)
                {
                    checks = checks.Where(p => p.DueDate <= thisWeek && p.DueDate >= thisWeek.AddDays(7)).ToList();
                }
                else if (filterView == FilterViewType.NextWeek)
                {
                    checks = checks.Where(p => p.DueDate >= nextWeek && p.DueDate <= nextWeek.AddDays(7)).ToList();
                }
                else if (filterView == FilterViewType.ThisMonth)
                {
                    checks = checks.Where(p => p.DueDate.Month == CurrentDate.Now.Month && p.DueDate.Year == CurrentDate.Now.Year).ToList();
                }
            }

            return checks;
        }*/

        private List<Check> FilterByStatus(BasicFilterViewModel filter, List<Check> checks)
        {
            if (filter.Status == (int)CheckStatus.Entregado)
                checks = checks.Where(p => p.Status == (int)CheckStatus.Entregado).ToList();
            else if(filter.Status == (int)CheckStatus.EnCartera)
                checks = checks.Where(p => p.Status == (int)CheckStatus.EnCartera).ToList();

            return checks;
        }

        [HttpGet]
        public ActionResult CheckUpdate(int Id)
        {
            var customers = modelEntities.Customers.Where(x => x.Active).ToList();
            var banks = modelEntities.Banks.Where(x => x.Deleted == null).OrderBy(x=> x.Name).ToList();

            if (Id != -1)
            {
                var check = modelEntities.Checks.SingleOrDefault(x => x.CheckId == Id);

                TempData["Customers"] = new SelectList(customers, "CustomerId", "Name");
                TempData["Banks"] = new SelectList(banks, "BankId", "Name", check.BankId);

                return View(check);
            }

            TempData["Customers"] = new SelectList(customers, "CustomerId", "Name");
            TempData["Banks"] = new SelectList(banks, "BankId", "Name");

            return View(new Check { CheckId = Id });
        }

        [HttpPost]
        public ActionResult CheckUpdate(FormCollection form)
        {
            Check check = new Check();

            if (form["CheckId"] != string.Empty && Convert.ToInt16(form["CheckId"]) > 0)
            {
                int CheckId = Convert.ToInt16(form["CheckId"]);
                check = modelEntities.Checks.SingleOrDefault(x => x.CheckId == CheckId && x.Deleted == null);
            }

            TryUpdateModel(check, form);

            if (check.CheckId <= 0)
            {
                check.Created = CurrentDate.Now;
                check.CreatedBy = User.Identity.Name;
                modelEntities.Checks.Add(check);
            }

            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        public ActionResult History(int checkId)
        {
            var check = modelEntities.Checks.FirstOrDefault(c => c.Deleted == null && c.CheckId == checkId);

            return View(check);
        }

        [HttpGet]
        public virtual PartialViewResult CheckDelete(int Id)
        {
            var check = modelEntities.Checks.SingleOrDefault(x => x.CheckId == Id && x.Deleted == null);
            if (check != null)
            {
                check.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            return PartialView("_CheckList", this.ChecksToList());
        }


        public ActionResult ExportToExcel()
        {
            var checks = modelEntities.Checks.Where(c => c.Deleted == null).OrderByDescending(c => c.RecievedDate).ToList();

            var data = checks.Select(c =>
            {
                return new
                {
                    NumeroInterno = c.CheckId,
                    Recibido = c.RecievedDate.ToShortDateString(),
                    NumeroCheque = c.Number,
                    Vencimiento = c.DueDate.ToShortDateString(),
                    Banco = c.Bank.Name,
                    Monto = c.Amount.ToString("C2"),
                    Plazo = c.DaysToDueDate.Value,
                    EntregadoA = c.EntregadoA,
                    Dueño = c.Dueño,
                    RecibidoDe = c.RecibidoDe,
                };
            }).ToList();

            ExportToExcel(string.Format("Cheques-{0}", CurrentDate.Now.ToString("dd-MM-yyyy")), data);

            return null;
        }

        private void ExportToExcel<T>(string fileName, IList<T> data)
        {
            var grid = new GridView();

            grid.DataSource = data;
            grid.DataBind();

            SetColumnNames(grid);

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}.xls", fileName));
            Response.ContentType = "application/ms-excel;charset=utf-8";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        private void SetColumnNames(GridView grid)
        {
            grid.HeaderRow.Cells[0].Text = "N° Interno";
            grid.HeaderRow.Cells[1].Text = "F. Recibo";
            grid.HeaderRow.Cells[2].Text = "N° Cheque";
            grid.HeaderRow.Cells[3].Text = "F. Vencimiento";
            grid.HeaderRow.Cells[7].Text = "Entregado A";
            grid.HeaderRow.Cells[8].Text = "Dueño";
            grid.HeaderRow.Cells[9].Text = "Recibido De";
        }
    }
}