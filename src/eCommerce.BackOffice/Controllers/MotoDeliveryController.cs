﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    //[AuthorizeUser(Roles = "Admin")]
    public class MotoDeliveryController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            List<int> provinciasIds = new List<int>();
            provinciasIds.Add(int.Parse(ConfigurationManager.AppSettings["BuenosAiresCityId"]));
            provinciasIds.Add(int.Parse(ConfigurationManager.AppSettings["CapitalCityId"]));
            var Cities = modelEntities.Cities.Where(x => provinciasIds.Contains(x.ProvinceId) && x.Active).ToList();
            return View("MotoDeliveryList", Cities);
        }

        [HttpPost]
        public string MotoDeliveryUpdate(int CityId, string NewName, string NewShippingCost)
        {
            try
            {
                var City = modelEntities.Cities.SingleOrDefault(x => x.CityId == CityId && x.Active);
                var nuevoCosto = decimal.Parse(NewShippingCost.Replace(".", ","));

                if (City != null)
                {
                    City.Name = NewName;
                    City.ShippingCost = nuevoCosto;
                    modelEntities.SaveChanges();
                }

                return nuevoCosto.ToString();
            }
            catch (Exception)
            {
                return "error";
            }
        }
        
        [HttpGet]
        public virtual PartialViewResult MotoDeliveryDelete(int Id)
        {
            var City = modelEntities.Cities.SingleOrDefault(x => x.CityId == Id && x.Active);
            if (City != null)
            {
                City.Active = false;
                City.Deleted = CurrentDate.Now;
                City.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            List<int> provinciasIds = new List<int>();
            provinciasIds.Add(int.Parse(ConfigurationManager.AppSettings["BuenosAiresCityId"]));
            provinciasIds.Add(int.Parse(ConfigurationManager.AppSettings["CapitalCityId"]));
            var list = modelEntities.Cities.Where(x => provinciasIds.Contains(x.ProvinceId) && x.Active).ToList();
            return PartialView("_MotoDeliveryList", list);
        }

        [HttpGet]
        [OutputCache(Duration = 0)]
        public ActionResult MotoDeliveryCreate()
        {
            var provincias = modelEntities.Provinces.OrderBy(x => x.Name).ToList();
            TempData["Provincias"] = new SelectList(provincias, "ProvinceId", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult MotoDeliveryCreate(FormCollection form)
        {
            City city = new City();

            TryUpdateModel(city, form);

            city.ShippingCost = decimal.Parse(form["Cost"].Replace(".", ",")); 

            if (city.CityId <= 0)
            {
                city.Active = true;
                city.Created = CurrentDate.Now;
                city.CreatedBy = User.Identity.Name;
                modelEntities.Cities.Add(city);
            }

            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }
    }
}