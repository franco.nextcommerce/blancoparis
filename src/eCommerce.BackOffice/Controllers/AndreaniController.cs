﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using eCommerce.Services.Services.Andreani;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class AndreaniController : Controller
    {
        private DbModelEntities _modelEntities;
        private AndreaniService _andreani;

        public AndreaniController()
        {
            this._andreani = new AndreaniService();
            this._modelEntities = new DbModelEntities();
        }

        public ActionResult CreateShipping(int Id)
        {
            var deliveryOrderId = Id;
            var deliveryOrder = _modelEntities.DeliveryOrders.Find(deliveryOrderId);
            var customer = deliveryOrder.Order.Customer;

            var sucursales = _andreani.GetSucursales().Select(x => new
            {
                Text = $"{ x.Descripcion } ({ x.Numero }) - { x.Direccion }",
                Value = x.Sucursal
            }).OrderBy(x => x.Text).ToList();

            var sucursalesSL = new SelectList(sucursales, "Value", "Text", deliveryOrder.Order.BranchAndreani);

            TempData["Height"] = ConfigurationManager.AppSettings["AndreaniHeight"];
            TempData["Width"] = ConfigurationManager.AppSettings["AndreaniWidth"];
            TempData["Length"] = ConfigurationManager.AppSettings["AndreaniLength"];

            var model = new AndreaniShippingViewModel()
            {
                Sucursales = sucursalesSL,
                DeliveryOrder = deliveryOrder,
                Customer = customer,
                ValorDeclarado = 500
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult CreateShipping(AndreaniEnvioViewModel model)
        {
            var deliveryOrder = _modelEntities.DeliveryOrders.Find(model.DeliveryOrderId);

            //model.ValorDeclarado = Math.Round(Convert.ToDouble(deliveryOrder.Amount), 0, MidpointRounding.AwayFromZero).ToString();
            if (!string.IsNullOrEmpty(Request["ValorDeclarado"]))
                model.ValorDeclarado = Math.Round(Convert.ToDouble(Request["ValorDeclarado"].Replace(".", ",")), 0, MidpointRounding.AwayFromZero).ToString();

            decimal total = deliveryOrder.Amount.Value;
            model.Total = total;
            var cotizacion = _andreani.CotizarEnvio(model.TipoEnvio, model.CP, model.Alto, model.Ancho, model.Largo, model.ValorDeclarado, model.SucursalDestino, model.Total);
            var envio = _andreani.GenerarEnvio(model, cotizacion);

            if (envio.Estado == AndreaniStatus.Ok)
            {
                deliveryOrder.AndreaniNumero = envio.NumeroAndreani;
                deliveryOrder.AndreaniTarifa = cotizacion.Tarifa;
                _modelEntities.SaveChanges();
                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Se creó el envío de Andreani.");
            }
            else
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar crear el envío de Andreani.");
            }

            return RedirectToAction("DeliveryOrderList", "DeliveryOrders", new { Id = deliveryOrder.Order.OrderId });
        }

        public ActionResult Etiqueta(int Id)
        {
            var deliveryOrder = _modelEntities.DeliveryOrders.Find(Id);
            var result = _andreani.GetImprimirEtiquetaLink(deliveryOrder.AndreaniNumero);

            if (result.Estado == AndreaniStatus.Ok)
            {
                return Redirect(result.PdfLink);
            }
            else
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Warning, "Se produjo un error al intentar imprimir la etiqueta.");
                return RedirectToAction("DeliveryOrderList", "DeliveryOrders", new { Id = deliveryOrder.Order.OrderId });
            }
        }

        public JsonResult AnularEnvio(string numero)
        {
            try
            {
                var result = _andreani.AnularEnvio(numero);

                var deliveryOrder = _modelEntities.DeliveryOrders.FirstOrDefault(x => x.AndreaniNumero == numero);
                deliveryOrder.AndreaniNumero = string.Empty;
                deliveryOrder.AndreaniTarifa = string.Empty;
                _modelEntities.SaveChanges();

                if (result.CodigoTransaccion.Contains("inexistente"))
                    return Json(new { Status = (int)AndreaniStatus.Error }, JsonRequestBehavior.AllowGet);

                return Json(new { Status = (int)AndreaniStatus.Ok }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Status = (int)AndreaniStatus.Error }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSucuralesJSON(string cp, string localidad, string provincia)
        {
            var sucursales = _andreani.GetSucursales(cp, localidad, provincia);
            return Json(sucursales, JsonRequestBehavior.AllowGet);
        }
    }
}