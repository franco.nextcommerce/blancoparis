﻿using System.Web.Mvc;

namespace RBACDemo.Areas.Steria.Controllers
{
    public class UnauthorisedController : Controller
    {
        // GET: Unauthorised
        public ActionResult Index()
        {
            return View();
        }
    }
}