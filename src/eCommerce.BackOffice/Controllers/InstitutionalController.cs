﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class InstitutionalController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        [HttpGet]
        public ActionResult CompanyUpdate()
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            return View(institutional);
        }

        [HttpPost]
        public ActionResult CompanyUpdate(FormCollection form)
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            if (institutional != null)
            {
                institutional.Company = HttpUtility.UrlDecode(Request["hidHtml"]) ;
                modelEntities.SaveChanges();
            }

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("CompanyUpdate");
        }

        [HttpGet]
        public ActionResult ShippingInfo()
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            return View(institutional);
        }

        [HttpPost]
        public ActionResult ShippingInfo(FormCollection form)
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            if (institutional != null)
            {
                institutional.ShippingInfo = HttpUtility.UrlDecode(Request["hidHtml"]);
                modelEntities.SaveChanges();
            }

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("ShippingInfo");
        }

        [HttpGet]
        public ActionResult Shipping()
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            return View(institutional);
        }

        [HttpPost]
        public ActionResult Shipping(FormCollection form)
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            if (institutional != null)
            {
                institutional.Shipping = HttpUtility.UrlDecode(Request["hidHtml"]);
                modelEntities.SaveChanges();
            }

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Shipping");
        }

        [HttpGet]
        public ActionResult PrivacyPolicyUpdate()
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            return View(institutional);
        }

        [HttpPost]
        public ActionResult PrivacyPolicyUpdate(FormCollection form)
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            if (institutional != null)
            {
                institutional.PrivacyPolicy = HttpUtility.UrlDecode(Request["hidHtml"]);
                modelEntities.SaveChanges();
            }

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("PrivacyPolicyUpdate");
        }

        [HttpGet]
        public ActionResult TermsAndConditionsUpdate()
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            return View(institutional);
        }

        [HttpPost]
        public ActionResult TermsAndConditionsUpdate(FormCollection form)
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            if (institutional != null)
            {
                institutional.TermsAndConditions = HttpUtility.UrlDecode(Request["hidHtml"]);
                modelEntities.SaveChanges();
            }

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("TermsAndConditionsUpdate");
        }


        [HttpGet]
        public ActionResult ChangesUpdate()
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            return View(institutional);
        }

        [HttpPost]
        public ActionResult ChangesUpdate(FormCollection form)
        {
            var institutional = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1);
            if (institutional != null)
            {
                institutional.Changes = HttpUtility.UrlDecode(Request["hidHtml"]);
                modelEntities.SaveChanges();
            }

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("ChangesUpdate");
        }

        [HttpGet]
        public ActionResult Faqs()
        {
            List<Faq> faqs = modelEntities.Faqs.Where(x => x.Active).ToList();
            return View("FaqList",faqs);
        }

        [HttpGet]
        public ActionResult FaqUpdate(int Id)
        {
            if (Id != -1)
            {
                var faq = modelEntities.Faqs.SingleOrDefault(x => x.FaqId == Id);
                return View(faq);
            }

            return View(new Faq { FaqId = Id });
        }

        [HttpPost]
        public ActionResult FaqUpdate(FormCollection form)
        {
            Faq question = new Faq();

            if (form["FaqId"] != string.Empty && Convert.ToInt16(form["FaqId"]) > 0)
            {
                int faqId = Convert.ToInt16(form["FaqId"]);
                question = modelEntities.Faqs.Single(x => x.FaqId == faqId && x.Active);
            }

            TryUpdateModel(question, form);

            if (question.FaqId <= 0)
            {
                question.Active = true;
                question.Created = CurrentDate.Now;
                question.CreatedBy = User.Identity.Name;
                modelEntities.Faqs.Add(question);
            }

            question.Answer = HttpUtility.UrlDecode(Request["hidHtml"]);

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Faqs");
        }

        [HttpGet]
        public virtual PartialViewResult FaqDelete(int Id)
        {
            var faq = modelEntities.Faqs.SingleOrDefault(x => x.FaqId == Id && x.Active);
            if (faq != null)
            {
                faq.Active = false;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.Faqs.Where(x => x.Active).ToList();
            return PartialView("_FaqList", list);
        }
    }
}