﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Utils;
using eCommerce.Services.Model.Enums;
using System.Globalization;

namespace eCommerce.BackOffice.Controllers
{
    public class PromotionsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            List<Promotion> promotions = modelEntities.Promotions.Where(x => !x.Deleted.HasValue).OrderByDescending(x => x.PromotionId).ToList();
            return View("PromotionList", promotions);
        }

        [HttpGet]
        public ActionResult PromotionUpdate(int Id)
        {
            var categories = modelEntities.Categories.Where(x => !x.Deleted.HasValue && x.Active).ToList();
            var products = modelEntities.Products.Where(x => !x.Deleted.HasValue && x.Active).ToList();
            var paymentMethods = modelEntities.PaymentMethods.ToList();

            var detailedProducts = products.Select(x => new {
                Id = x.ProductId,
                Name = x.Code + " - " + x.Title,
            });

            if (Id != -1)
            {
                var promotion = modelEntities.Promotions.FirstOrDefault(x => x.PromotionId == Id);
                TempData["Categories"] = new MultiSelectList(categories, "CategoryId", "CompleteName", promotion.CategoryPromotions.Select(x => x.CategoryId));
                TempData["Products"] = new MultiSelectList(detailedProducts, "Id", "Name", promotion.ProductPromotions.Select(x => x.ProductId));
                TempData["ProductsExcluded"] = new MultiSelectList(detailedProducts, "Id", "Name", promotion.ExcludedProducts.Select(x => x.ProductId));
                TempData["PaymentMethods"] = new MultiSelectList(paymentMethods, "PaymentMethodId", "Description", promotion.Promotion_PaymentMethods.Select(x => x.PaymentMethodId));

                return View(promotion);
            }

            TempData["Categories"] = new MultiSelectList(categories, "CategoryId", "CompleteName");
            TempData["Products"] = new MultiSelectList(detailedProducts, "Id", "Name");
            TempData["ProductsExcluded"] = new MultiSelectList(detailedProducts, "Id", "Name");
            TempData["PaymentMethods"] = new MultiSelectList(paymentMethods, "PaymentMethodId", "Description");

            return View(new Promotion { PromotionId = Id, StartDate = new DateTime(CurrentDate.Now.Year, CurrentDate.Now.Month, 1), EndDate = CurrentDate.Now });
        }

        [HttpPost]
        public ActionResult PromotionUpdate(FormCollection form)
        {
            Promotion promotion = new Promotion();

            if (form["PromotionId"] != string.Empty && Convert.ToInt16(form["PromotionId"]) > 0)
            {
                int promotionId = Convert.ToInt16(form["PromotionId"]);
                promotion = modelEntities.Promotions.FirstOrDefault(x => x.PromotionId == promotionId);
            }

            TryUpdateModel(promotion, form);

            if (promotion.PromotionId <= 0)
            {
                modelEntities.Promotions.Add(promotion);
            }

            #region DateTime

            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();

            FromDate = Convert.ToDateTime(form["StartDate"] + " " + form["StartTime"].Insert(form["StartTime"].Length - 2, ":00 "), CultureInfo.CurrentCulture);
            ToDate = Convert.ToDateTime(form["EndDate"] + " " + form["EndTime"].Insert(form["EndTime"].Length - 2, ":00 "), CultureInfo.CurrentCulture);

            promotion.StartDate = FromDate;
            promotion.EndDate = ToDate;

            modelEntities.SaveChanges();

            #endregion

            //MEDIOS DE PAGO
            var paymentMethods = form["PaymentMethodIds"];

            var currentPaymentMethods = promotion.Promotion_PaymentMethods;
            modelEntities.Promotion_PaymentMethods.RemoveRange(currentPaymentMethods);
            if (!string.IsNullOrEmpty(paymentMethods))
            {
                var paymentMethodIds = paymentMethods.Split(',').ToList();
                foreach (var paymentMethodId in paymentMethodIds)
                {
                    Promotion_PaymentMethods method = new Promotion_PaymentMethods();
                    method.PaymentMethodId = int.Parse(paymentMethodId);
                    method.PromotionId = promotion.PromotionId;
                    modelEntities.Promotion_PaymentMethods.Add(method);
                }
            }

            //CATEGORIAS
            var categories = form["CategoryIds"];

            var currentCategoryPromotions = modelEntities.CategoryPromotions.Where(x => x.PromotionId == promotion.PromotionId);
            modelEntities.CategoryPromotions.RemoveRange(currentCategoryPromotions);
            if (!string.IsNullOrEmpty(categories))
            {
                var categoriesIds = categories.Split(',').ToList();
                foreach (var categoryId in categoriesIds)
                {
                    CategoryPromotion categoryPromotion = new CategoryPromotion();
                    categoryPromotion.CategoryId = int.Parse(categoryId);
                    categoryPromotion.PromotionId = promotion.PromotionId;
                    modelEntities.CategoryPromotions.Add(categoryPromotion);
                }
            }

            //PRODUCTOS
            var products = form["ProductIds"];

            var currentProductPromotions = modelEntities.ProductPromotions.Where(x => x.PromotionId == promotion.PromotionId);
            modelEntities.ProductPromotions.RemoveRange(currentProductPromotions);
            if (!string.IsNullOrEmpty(products))
            {
                var productsIds = products.Split(',').ToList();
                foreach (var productId in productsIds)
                {
                    ProductPromotion productPromotion = new ProductPromotion();
                    productPromotion.ProductId = int.Parse(productId);
                    productPromotion.PromotionId = promotion.PromotionId;
                    modelEntities.ProductPromotions.Add(productPromotion);
                }
            }

            //PRODUCTOS EXCLUIDOS

            var productsExcluded = form["ProductExcludedIds"];

            var currentProductExcluded = modelEntities.ExcludedProducts.Where(x => x.PromotionId == promotion.PromotionId);
            modelEntities.ExcludedProducts.RemoveRange(currentProductExcluded);
            if (!string.IsNullOrEmpty(productsExcluded))
            {
                var productsIds = productsExcluded.Split(',').ToList();
                foreach (var productId in productsIds)
                {
                    ExcludedProduct product = new ExcludedProduct();
                    product.ProductId = int.Parse(productId);
                    product.PromotionId = promotion.PromotionId;
                    modelEntities.ExcludedProducts.Add(product);
                }
            }
            
            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult PromotionDelete(int Id)
        {
            var promotion = modelEntities.Promotions.Find(Id);
            if (promotion != null)
            {
                promotion.Deleted = CurrentDate.Now;
                promotion.DeletedBy = "Delete in list";
                modelEntities.SaveChanges();
            }

            var list = modelEntities.Promotions.Where(x => !x.Deleted.HasValue).ToList();
            return PartialView("_PromotionList", list);
        }
    }
}