﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using eCommerce.Backoffice.Utils.Interfaces;
using eCommerce.Backoffice.Utils;
using eCommerce.BackOffice.ViewModels;
using eCommerce.Services.Security;

namespace eCommerce.BackOffice.Controllers
{
    public class CustomersController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        String KeyString = "Gv7L3V15jCdb9P5XGKiPnhHZ7JlKcmU=";

        public ActionResult Index()
        {
            RBACUser requestingUser = new RBACUser(HttpContext.User.Identity.Name);
            if (requestingUser.HasRole("Mayor"))
            {
                return RedirectToAction("Wholesaler");
            }
            return View("CustomerList2");
            //return View("CustomerList", this.CustomersToShow());
        }
        public ActionResult Index2()
        {
            return View("CustomerList2");
        }

        public ActionResult Wholesaler()
        {
            TempData["Wholesaler"] = true;
            return View("CustomerList2");
        }

        [HttpGet]
        public JsonResult GetCustomers()
        {
            var jsonResult = Json(new { data = this.CustomersToShow() }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        [HttpGet]
        public JsonResult GetCustomersForSelect(string q)
        {
            var active_customers = modelEntities.Customers.Where(x => x.Active && !x.Deleted.HasValue && x.FullName.Contains(q))
            .Select(x => new { id = x.CustomerId, text = x.FullName }).ToList();

            var jsonResult = Json(new { results = active_customers }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }


        private List<CustomerViewModel> CustomersToShow()
        {
            var customers = modelEntities.Customers.Where(x => x.Active && x.Deleted == null);

            return customers.OrderByDescending(x => x.Created)
                .Select(x => new CustomerViewModel
                {
                    CustomerId = x.CustomerId,
                    FullName = x.FullName,
                    Email = x.Email,
                    Tipo = ((UserTypeEnum)x.UserType).ToString(),
                    CUIT = x.CUIT,
                    Province = x.Province.Name,
                    City = x.City1 != null ? x.City1.Name : x.City,
                    Enabled = x.Enabled,
                    RegisterDate = x.Created,
                    UserName = x.Username
                }).ToList();
        }

        [HttpGet]
        public ActionResult CustomerUpdate(int Id)
        {
            var provinces = modelEntities.Provinces.ToList();
            var priceLists = GetPriceLists();
            var userTypes = GetUserTypes();
            var users = modelEntities.USERS.Where(x => x.Active && x.Deleted == null).ToList();

            if (Id != -1)
            {
                var customer = modelEntities.Customers.SingleOrDefault(x => x.CustomerId == Id && x.Active);

                if (customer.ProvinceId.HasValue)
                {
                    TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name", customer.ProvinceId.Value);
                }
                else
                {
                    TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name");
                }

                TempData["UserTypes"] = new SelectList(userTypes, "Value", "Text", customer.UserType);
                TempData["PriceLists"] = new SelectList(priceLists, "Value", "Text", customer.PriceList);
                TempData["USERS"] = new SelectList(users, "User_Id", "Username", customer.SalerId);

                if (!string.IsNullOrWhiteSpace(customer.Password))
                    customer.Password = EncryptionService.Decrypt(customer.Password, KeyString);

                return View(customer);
            }

            var user = users.FirstOrDefault(x => x.Username == User.Identity.Name);
            TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name");
            TempData["UserTypes"] = new SelectList(userTypes, "Value", "Text");
            TempData["PriceLists"] = new SelectList(priceLists, "Value", "Text");

            return View(new Customer { CustomerId = Id, Enabled = true, SalerId = user.User_Id });
        }

        [HttpPost]
        public ActionResult CustomerUpdate(FormCollection form)
        {
            Customer customer = new Customer();

            if (form["CustomerId"] != string.Empty && Convert.ToInt16(form["CustomerId"]) > 0)
            {
                int customerId = Convert.ToInt16(form["CustomerId"]);
                customer = modelEntities.Customers.SingleOrDefault(x => x.CustomerId == customerId && x.Active);
            }
            
            TryUpdateModel(customer, form);

            if (Request["Enabled"] != null && Request["Enabled"] == "on")
                customer.Enabled = true;
            else
                customer.Enabled = false;
            

            if (!string.IsNullOrWhiteSpace(customer.Password))
                customer.Password = EncryptionService.Encrypt(customer.Password, KeyString);

            if (customer.CustomerId <= 0)
            {
                customer.Active = true;
                customer.Created = CurrentDate.Now;
                customer.Confirmed = true;
                customer.IsSiteUser = false;

                modelEntities.Customers.Add(customer);
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CustomerConfirm(int Id)
        {
            var customer = modelEntities.Customers.SingleOrDefault(x => x.CustomerId == Id && x.Active);
            if (customer != null)
            {
                customer.Confirmed = true;
                modelEntities.SaveChanges();

                //envio mail de usuario confirmado
                #region MailSend
                IMailing mailUtil = Mailing.Instance;
                mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                mailUtil.SendCustomerConfirmedMail(customer.Email);
                #endregion MailSend

                //TODO: Send Notification.
            }

            return PartialView("_CustomerList", this.CustomersToShow());
        }

        [HttpPost]
        public ActionResult CustomerReject(int Id)
        {
            var customer = modelEntities.Customers.SingleOrDefault(x => x.CustomerId == Id && x.Active);
            if (customer != null)
            {
                customer.Rejected = true;
                modelEntities.SaveChanges();

                //envio mail de usuario confirmado
                #region MailSend
                IMailing mailUtil = Mailing.Instance;
                mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                mailUtil.SendCustomerRejectedMail(customer.Email);
                #endregion MailSend

                //TODO: Send Notification.
            }

            return PartialView("_CustomerList", this.CustomersToShow());
        }

        public ActionResult Confirm(int Id)
        {
            var customer = modelEntities.Customers.SingleOrDefault(x => x.CustomerId == Id && x.Active);
            if (customer != null)
            {
                customer.Confirmed = true;
                modelEntities.SaveChanges();

                //envio mail de usuario confirmado
                #region MailSend
                IMailing mailUtil = Mailing.Instance;
                mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                mailUtil.SendCustomerConfirmedMail(customer.Email);
                #endregion MailSend

                //TODO: Send Notification.
            }

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Usuario confirmado!");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public virtual JsonResult ResetCustomersShowTopBanner()
        {
            try
            {
                modelEntities.Database.ExecuteSqlCommand("EXEC ResetCustomersTopBanner");
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public string CustomerDelete(int Id)
        {
            try
            {
                var customer = modelEntities.Customers.SingleOrDefault(x => x.CustomerId == Id && x.Active);
                if (customer != null)
                {
                    customer.Active = false;
                    customer.Deleted = CurrentDate.Now;
                    if (customer.IsFacebookUser)
                    {
                        var external = modelEntities.CustomerExternalLogins.FirstOrDefault(x => x.CustomerId == customer.CustomerId);
                        modelEntities.CustomerExternalLogins.Remove(external);
                    }
                    modelEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }

            return string.Empty;
            //return PartialView("_CustomerList", this.CustomersToShow());
        }

        [HttpGet]
        public PartialViewResult ModalCreateCustomer()
        {
            Customer new_customer = new Customer();
            new_customer.CustomerId = -1;

            var provinces = modelEntities.Provinces.OrderBy(x => x.Name).ToList();
            var priceLists = GetPriceLists();
            var userTypes = GetUserTypes();

            TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name");
            TempData["UserTypes"] = new SelectList(userTypes, "Value", "Text");
            TempData["PriceLists"] = new SelectList(priceLists, "Value", "Text");

            return PartialView("_CustomerCreate", new_customer);
        }

        [HttpPost]
        public JsonResult ModalCreateCustomer(Customer model)
        {
            try
            {
                var customer = new Customer();

                customer.FullName = model.FullName;
                customer.Email = model.Email;
                customer.PriceList = model.UserType;
                customer.UserType = model.UserType;
                customer.Cellphone = model.Cellphone;

                customer.Enabled = true;
                customer.Active = true;
                customer.Created = CurrentDate.Now;
                customer.Password = string.Empty;
                customer.Confirmed = true;
                customer.IsSiteUser = false;

                modelEntities.Customers.Add(customer);
                modelEntities.SaveChanges();

                return Json(new { tipoMensaje = "success", resultado = "Cliente creado exitosamente.", CustomerId = customer.CustomerId, FullName = customer.FullName, PriceList = customer.PriceList });
            }
            catch (Exception ex)
            {
                return Json(new { tipoMensaje = "error", resultado = "Se ha generado un error al intentar crear el cliente." });
            }
        }

        public ActionResult AsyncCities(string ProvinceId)
        {
            var provinceId = Convert.ToInt32(ProvinceId);
            var cities = modelEntities.Cities.Where(x => x.ProvinceId == provinceId).ToList().OrderBy(x => x.Name).Select(a => new SelectListItem()
            {
                Text = a.Name,
                Value = a.CityId.ToString(),
            });

            return Json(cities);
        }

        public List<SelectListItem> GetPriceLists()
        {
            Array values = Enum.GetValues(typeof(PriceListEnum));
            List<SelectListItem> items = new List<SelectListItem>(values.Length);

            foreach (var i in values)
            {
                items.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(PriceListEnum), i),
                    Value = ((int)i).ToString()
                });
            }

            return items;
        }

        public List<SelectListItem> GetUserTypes()
        {
            Array values = Enum.GetValues(typeof(UserTypeEnum));
            List<SelectListItem> items = new List<SelectListItem>(values.Length);

            foreach (var i in values)
            {
                items.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(UserTypeEnum), i),
                    Value = ((int)i).ToString()
                });
            }

            return items;
        }

        [HttpGet]
        public ActionResult OrderHistory(int Id)
        {
            CustomerOrderHistoryViewModel model = new CustomerOrderHistoryViewModel();
            model.Customer = modelEntities.Customers.FirstOrDefault(x => x.CustomerId == Id && x.Deleted == null);

            List<OrderViewModel> list = new List<OrderViewModel>();
            list = modelEntities.Orders.Where(x => x.CustomerId == Id && x.Deleted == null).OrderByDescending(x => x.OrderId)
            .Select(x => new OrderViewModel
            {
                OrderId = x.OrderId,
                CreatedDate = x.Created,
                CustomerName = x.Customer.FullName,
                Channel = x.Channel,
                DeliveryMethodId = x.DeliveryMethodId,
                PaymentMethodId = x.PaymentMethodId,
                Total = x.Total,
                PaymentStatusId = x.PaymentStatusId,
                DeliveryStatusId = x.DeliveryStatusId,
                OrderStatusId = x.OrderStatusId
            })
            .ToList();
            model.Orders = list;

            return View("CustomerOrderHistory", model);
        }

        #region Export to Excel

        [HttpGet]
        public ActionResult ExportToExcel()
        {
            var grid = new System.Web.UI.WebControls.GridView();


            var customers = modelEntities.Customers.Where(x => x.Active && !x.Deleted.HasValue).ToList()
                .Select(c => new
                {
                    Id = c.CustomerId,
                    Nombre_Completo = c.FullName,
                    Direccion = c.Address + " " + c.AddressNumber,
                    Piso = c.Floor,
                    Departamento = c.Department,
                    Ciudad = c.CityId.HasValue ? c.City1.Name : c.City,
                    Provincia = c.ProvinceId.HasValue ? c.Province.Name : string.Empty,
                    Codigo_Postal = c.ZipCode,
                    Celular = c.Cellphone,
                    Numero_Particular = c.PhoneNumber,
                    Tipo_Usuario = ((UserTypeEnum)c.UserType).ToString(),
                    Confirmado = c.Confirmed ? "Si" : "No",
                    Activo = c.Enabled ? "Si" : "No",
                    Email = c.Email,
                    DNI = c.DNI,
                    CUIT = c.CUIT,
                    RazonSocial = c.CorporateName,
                    Condicion_IVA = c.IvaConditionId.HasValue ? ((IvaConditionEnum)c.IvaConditionId).ToString() : string.Empty,
                    Direccion_De_Facturacion = c.BillingAddress,
                    Marcas_Que_Trabaja = c.WorkingBrands,
                    Donde_Vende = c.SellingLocationId.HasValue ? ((SellingLocationEnum)c.SellingLocationId).ToString() :string.Empty,
                    Fecha_Contacto = c.ContactDate.HasValue ? c.ContactDate.Value.ToShortDateString() : string.Empty,
                    Observaciones = c.Observations,
                    Fecha_Respuesta_Contacto = c.ContactAnswerDate.HasValue ? c.ContactAnswerDate.Value.ToShortDateString() :string.Empty,
                    Respuesta_Contacto = c.CustomerAnswer,
                });
            
            grid.DataSource = customers.ToList();
            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Clientes.xls");
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }

        #endregion

        [HttpGet]
        public string EncryptCustomersPasswords()
        {
            var result = "Todo ok";

            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var customers = modelEntities.Customers.Where(x => !x.Deleted.HasValue && !x.Password.Contains("==")).ToList();

                    foreach (var customer in customers)
                    {
                        customer.Password = EncryptionService.Encrypt(customer.Password, KeyString);
                    }

                    modelEntities.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                }
            }

            return result; 
        }

    }
}