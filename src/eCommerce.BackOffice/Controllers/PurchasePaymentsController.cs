﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class PurchasePaymentsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index(int Id)
        {
            return View("PurchasePaymentList", this.PurchasePaymentSuppliersToShow(Id));
        }

        private Purchase PurchasePaymentSuppliersToShow(int purchaseId)
        {
            var purchase = modelEntities.Purchases.FirstOrDefault(x => x.PurchaseId == purchaseId);
            purchase.Transactions = purchase.Transactions.Where(x => x.TransactionTypeId == (int)TransactionTypeEnum.Egreso && x.AccountId.HasValue && x.Deleted == null).OrderByDescending(x => x.Date).ToList();

            return purchase;
        }

        [HttpGet]
        public ActionResult PurchasePaymentUpdate(int Id, int purchaseId)
        {
            var purchase = modelEntities.Purchases.FirstOrDefault(x => x.PurchaseId == purchaseId && x.Active);
            var accounts = modelEntities.Accountings.Where(x => x.Active);

            if (Id != -1)
            {
                var transaction = modelEntities.Transactions.SingleOrDefault(x => x.TransactionId == Id);
                ViewData["Accounts"] = new SelectList(accounts, "AccountId", "Name", transaction.AccountId);

                return View(transaction);
            }

            ViewData["Accounts"] = new SelectList(accounts, "AccountId", "Name");

            return View(new Transaction { TransactionId = Id, PurchaseId = purchaseId, Purchase = purchase });
        }

        [HttpPost]
        public ActionResult PurchasePaymentUpdate(FormCollection form)
        {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        Transaction transaction = new Transaction();

                        if (form["TransactionId"] != string.Empty && Convert.ToInt16(form["TransactionId"]) > 0)
                        {
                            int transactionId = Convert.ToInt16(form["TransactionId"]);
                            transaction = modelEntities.Transactions.SingleOrDefault(x => x.TransactionId == transactionId && x.Deleted == null);
                        }

                        if (!string.IsNullOrEmpty(Request["Amount"]))
                        {
                            transaction.Amount = decimal.Parse(Request["Amount"].Replace(".", string.Empty));
                        }

                        TryUpdateModel(transaction, form);

                        if (transaction.TransactionId <= 0)
                        {
                            transaction.TransactionTypeId = (int)TransactionTypeEnum.Egreso;
                            transaction.TransactionCategoryId = int.Parse(ConfigurationManager.AppSettings["PaymentsCategoryId"]);
                            transaction.Created = CurrentDate.Now;
                            transaction.CreatedBy = User.Identity.Name;
                            modelEntities.Transactions.Add(transaction);
                        }
                        else
                        {
                            //remover relaciones del transaction con los cheques.
                            transaction.PaymentChecks.Clear();
                        }

                        modelEntities.SaveChanges();

                        AddChecks(form, transaction);

                        UpdateAccountBalance(transaction.AccountId.Value);

                        UpdatePurchasePaymentStatus(transaction.PurchaseId.Value);

                        UpdateSupplierAccountBalance(transaction.Purchase.SupplierId);
                        
                        dbContextTransaction.Commit();
                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al realizar la transferencia.");
            }
            
            return RedirectToAction("Index", new { Id = form["PurchaseId"] });
        }

        private void AddChecks(FormCollection form, Transaction transaction)
        {
            if (string.IsNullOrEmpty(form["ChecksIds"]))
                return;

            var ids = form["ChecksIds"].Split(',');
            var checksIds = Array.ConvertAll(ids, int.Parse);
            var checks = modelEntities.Checks.Where(x => checksIds.Contains(x.CheckId) && x.Deleted == null).ToList();


            foreach (var check in checks)
            {
                check.PaymentTransactionId = transaction.TransactionId;
                check.Status = (int)CheckStatus.Entregado;
            }

            modelEntities.SaveChanges();
        }

        [HttpGet]
        public virtual PartialViewResult PurchasePaymentDelete(int Id)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var transaction = modelEntities.Transactions.Include("Purchase").SingleOrDefault(x => x.TransactionId == Id && x.Deleted ==  null);

                    transaction.Deleted = CurrentDate.Now;
                    transaction.DeletedBy = User.Identity.Name;

                    DeleteChecks(Id);

                    UpdatePurchasePaymentStatus(transaction.PurchaseId.Value);

                    UpdateAccountBalance(transaction.AccountId.Value);

                    UpdateSupplierAccountBalance(transaction.Purchase.SupplierId);

                    modelEntities.SaveChanges();

                    dbContextTransaction.Commit();

                    return PartialView("_PurchasePaymentList", this.PurchasePaymentSuppliersToShow(transaction.PurchaseId.Value).Transactions);
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        private void DeleteChecks(int id)
        {
            var checks = modelEntities.Checks.Where(x => x.PaymentTransactionId == id && x.Deleted == null).ToList();
            foreach (var check in checks)
            {
                check.Deleted = CurrentDate.Now;
                check.DeletedBy = User.Identity.Name;
            }
            
        }

        private void UpdatePurchasePaymentStatus(int purchaseId)
        {
            var purchase = modelEntities.Purchases.FirstOrDefault(x => x.PurchaseId == purchaseId);
            

            if (purchase.IsTotallyPaid)
                purchase.PaymentStatusId = (int)PaymentStatusEnum.Pagada;
            else
                purchase.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;

            modelEntities.SaveChanges();
        }

        private void UpdateAccountBalance(int accountId)
        {
            try
            {
                var account = modelEntities.Accountings.SingleOrDefault(x => x.AccountId == accountId);
                var transactions = modelEntities.Transactions.Where(x => x.AccountId == account.AccountId && x.Deleted == null).ToList();

                decimal ingresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso && x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso).Sum(x => x.Amount);
                decimal egresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso).Sum(x => x.Amount);
                account.Balance = ingresos - egresos;

                ingresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar && x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso).Sum(x => x.Amount);
                egresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso).Sum(x => x.Amount);
                account.BalanceUsd = ingresos - egresos;

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateSupplierAccountBalance(int supplierId)
        {
            try
            {
                var supplier = modelEntities.Suppliers.SingleOrDefault(x => x.SupplierId == supplierId);
                var transactions = modelEntities.Transactions.Where(x => x.Purchase.SupplierId == supplierId && x.Deleted == null).ToList();

                supplier.Balance = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso).Sum(x => x.Amount);
                supplier.BalanceUsd = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar).Sum(x => x.Amount);

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public virtual PartialViewResult CheckListPartial()
        {
            var checks = modelEntities.Checks.Where(x => x.Status == (int)CheckStatus.EnCartera && x.Deleted == null).ToList();
            return PartialView("~/Views/Checks/_CheckList.cshtml", checks);
        }
    }
}