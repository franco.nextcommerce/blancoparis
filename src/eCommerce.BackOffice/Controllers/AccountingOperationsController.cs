﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    ////[AuthorizeUser(Roles = "Admin")]
    public class AccountingOperationsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index(int AccountId = -1)
        {
            List<AccountingOperation> operations = modelEntities.AccountingOperations.Where(x => (x.AccountId == AccountId || AccountId == -1) && x.Active == true)
                .OrderByDescending(x => x.AccountingOperationId).ToList();
            
            var account = modelEntities.Accountings.FirstOrDefault(x => x.AccountId == AccountId);
            if (account != null)
                ViewData["AccountName"] = account.Name;
            else
                ViewData["AccountName"] = "Todos";

            ViewData["AccountId"] = AccountId.ToString();

            return View("AccountingOperationList", operations);
        }
        
        [HttpGet]
        public ActionResult AccountingOperationUpdate(int Id, int AccountId = -1)
        {
            var accounts = modelEntities.Accountings.Where(x => x.Active).OrderBy(x => x.Name);
            var concepts = modelEntities.AccountingCategories.Where(x => x.Active).OrderBy(x => x.Name);

            if (Id != -1)
            {
                var operation = modelEntities.AccountingOperations.SingleOrDefault(x => x.AccountingOperationId == Id);

                TempData["Accounts"] = new SelectList(accounts,"AccountId","Name", operation.AccountId);
                TempData["Concepts"] = new SelectList(concepts, "AccountingCategoryId", "Name", operation.AccountingCategoryId);

                return View(operation);
            }

            if(AccountId != -1)
                TempData["Accounts"] = new SelectList(accounts, "AccountId", "Name", AccountId);
            else
                TempData["Accounts"] = new SelectList(accounts, "AccountId", "Name");

            TempData["Concepts"] = new SelectList(concepts, "AccountingCategoryId", "Name");

            return View(new AccountingOperation { AccountId = AccountId });
        }

        [HttpPost]
        public ActionResult AccountingOperationUpdate(FormCollection form)
        {
            AccountingOperation operation = new AccountingOperation();

            if (form["AccountingOperationId"] != string.Empty && Convert.ToInt16(form["AccountingOperationId"]) > 0)
            {
                int AccountOperationId = Convert.ToInt16(form["AccountingOperationId"]);
                operation = modelEntities.AccountingOperations.SingleOrDefault(x => x.AccountingOperationId == AccountOperationId && x.Active);
            }

            TryUpdateModel(operation, form);

            var accountCategory = modelEntities.AccountingCategories.SingleOrDefault(x => x.AccountingCategoryId == operation.AccountingCategoryId);
            if ((AccountingOperationTypeEnum)accountCategory.AccountingType == AccountingOperationTypeEnum.Egreso && operation.Value > 0)
                operation.Value = operation.Value * -1;

            if ((AccountingOperationTypeEnum)accountCategory.AccountingType == AccountingOperationTypeEnum.Ingreso && operation.Value < 0)
                operation.Value = operation.Value * -1;

            if (operation.AccountingOperationId <= 0)
            {
                operation.Active = true;
                operation.Created = DateTime.Now;
                operation.CreatedBy = User.Identity.Name;
                modelEntities.AccountingOperations.Add(operation);
            }

            //update Balance
            var account = modelEntities.Accountings.Include("AccountingOperations").SingleOrDefault(x => x.AccountId == operation.AccountId);
            if (account.AccountingOperations.Any())
            {
                var balances = account.AccountingOperations.Where(x => x.Active)
                    .GroupBy(g => new { g.AccountingCategory.Currency })
                    .Select(n => new
                    {
                        Currency = n.Key.Currency,
                        Balance = n.Sum(x => x.Value)
                    }).ToList();

                foreach (var balance in balances)
                {
                    if(balance.Currency == (int)CurrencyEnum.Peso)
                        account.Balance = balance.Balance;
                    else
                        account.BalanceUsd = balance.Balance;
                }
            }
            else
            {
                if (accountCategory.Currency == (int)CurrencyEnum.Peso)
                    account.Balance = operation.Value;
                else
                    account.BalanceUsd = operation.Value;
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index", new { AccountId = account.AccountId });
        }

        [HttpGet]
        public virtual PartialViewResult AccountingDelete(int Id)
        {
            var account = modelEntities.Accountings.SingleOrDefault(x => x.AccountId == Id && x.Active);
            if (account != null)
            {
                account.Active = false;
                account.Deleted = DateTime.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.Accountings.Where(x => x.Active).ToList();
            return PartialView("_AccountingList", list);
        }


        /************************* ACCOUNTS MOVEMENTS *************************/

        [HttpGet]
        public ActionResult AccountingMovementUpdate(int Id)
        {
            var accounts = modelEntities.Accountings.Where(x => x.Active).OrderBy(x => x.Name);
            var concepts = modelEntities.AccountingCategories.Where(x => x.Active).OrderBy(x => x.Name);

            if (Id != -1)
            {
                var operation = modelEntities.AccountingOperations.SingleOrDefault(x => x.AccountingOperationId == Id);

                TempData["OriginAccounts"] = new SelectList(accounts, "AccountId", "Name", operation.AccountId);
                TempData["DestinationAccounts"] = new SelectList(accounts, "AccountId", "Name", operation.AccountId);

                TempData["Concepts"] = new SelectList(concepts, "AccountingCategoryId", "Name", operation.AccountingCategoryId);

                return View(operation);
            }

            TempData["OriginAccounts"] = new SelectList(accounts, "AccountId", "Name");
            TempData["DestinationAccounts"] = new SelectList(accounts, "AccountId", "Name");
            TempData["Concepts"] = new SelectList(concepts, "AccountingCategoryId", "Name");

            return View(new AccountingOperation { AccountId = Id });
        }

        [HttpPost]
        public ActionResult AccountMovementUpdate(FormCollection form)
        {
            AccountingOperation operation = new AccountingOperation();

            if (form["AccountingOperationId"] != string.Empty && Convert.ToInt16(form["AccountingOperationId"]) > 0)
            {
                int AccountOperationId = Convert.ToInt16(form["AccountingOperationId"]);
                operation = modelEntities.AccountingOperations.SingleOrDefault(x => x.AccountingOperationId == AccountOperationId && x.Active);
            }

            TryUpdateModel(operation, form);

            if (operation.AccountingOperationId <= 0)
            {
                operation.Active = true;
                operation.Created = DateTime.Now;
                operation.CreatedBy = User.Identity.Name;
                modelEntities.AccountingOperations.Add(operation);
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }
    }
}
 
 