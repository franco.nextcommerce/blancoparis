﻿using eCommerce.BackOffice.ViewModels;
using eCommerce.Services.Model;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class SupplierAccountingController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index(int Id)
        {
            SupplierAccountingViewModel model = new SupplierAccountingViewModel();

            model.Supplier = modelEntities.Suppliers.FirstOrDefault(x => x.SupplierId == Id);
            model.Transactions = this.SupplierTransactionsToShow(Id);
            
            return View("SupplierAccountingList", model);
        }

        private List<Transaction> SupplierTransactionsToShow(int Id)
        {
            return modelEntities.Transactions.Where(x => x.Purchase.SupplierId == Id && x.Deleted == null).OrderByDescending(x=> x.Created).ToList();
        }
    }
}