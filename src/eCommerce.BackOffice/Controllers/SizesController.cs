﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class SizesController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            return View("SizeList", this.SizesToList());
        }

        private List<Size> SizesToList()
        {
            return modelEntities.Sizes.Where(x => x.Deleted == null).OrderByDescending(x => x.SizeId).ToList();
        }

        [HttpGet]
        public ActionResult SizeUpdate(int Id)
        {
            if (Id != -1)
            {
                var size = modelEntities.Sizes.SingleOrDefault(x => x.SizeId== Id && x.Deleted == null);
                return View(size);
            }            
            return View(new Size { SizeId = Id });
        }

        [HttpPost]
        public ActionResult SizeUpdate(FormCollection form)
        {
            Size size= new Size();

            if (form["SizeId"] != string.Empty && Convert.ToInt16(form["SizeId"]) > 0)
            {
                int sizeId = Convert.ToInt16(form["SizeId"]);
                size = modelEntities.Sizes.SingleOrDefault(x => x.SizeId == sizeId && x.Deleted == null);
            }

            TryUpdateModel(size, form);

            if (size.SizeId <= 0)
            {
                modelEntities.Sizes.Add(size);
            }

            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult SizeDelete(int Id)
        {
            var size = modelEntities.Sizes.SingleOrDefault(x => x.SizeId == Id && x.Deleted == null);
            if (size != null)
            {
                size.Deleted = CurrentDate.Now;
                size.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            return PartialView("_SizeList", this.SizesToList());
        }

        [HttpPost]
        public JsonResult ModalCreateSize(string Descripcion)
        {
            try
            {
                var size = new Size();

                size.Description = Descripcion;
                modelEntities.Sizes.Add(size);
                modelEntities.SaveChanges();

                return Json(new { tipoMensaje = "success", resultado = "Talle creado exitosamente.", SizeId = size.SizeId, Description = size.Description });
            }
            catch (Exception ex)
            {
                return Json(new { tipoMensaje = "error", resultado = "Se ha generado un error al intentar crear el talle." });
            }
        }
    }
}