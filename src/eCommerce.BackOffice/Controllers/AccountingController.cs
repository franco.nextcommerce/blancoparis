﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    ////[AuthorizeUser(Roles = "Admin")]
    public class AccountingController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            List<eCommerce.Services.Model.Accounting> accounts = modelEntities.Accountings.Where(x => x.Active == true).ToList();
            return View("AccountingList", accounts);
        }

        [HttpGet]
        public ActionResult AccountingUpdate(int Id)
        {
            if (Id != -1)
            {
                var account = modelEntities.Accountings.SingleOrDefault(x => x.AccountId == Id);
                return View(account);
            }

            return View(new eCommerce.Services.Model.Accounting { AccountId = Id });
        }

        [HttpPost]
        public ActionResult AccountingUpdate(FormCollection form)
        {
            eCommerce.Services.Model.Accounting account = new Services.Model.Accounting();

            if (form["AccountId"] != string.Empty && Convert.ToInt16(form["AccountId"]) > 0)
            {
                int AccountId = Convert.ToInt16(form["AccountId"]);
                account = modelEntities.Accountings.SingleOrDefault(x => x.AccountId == AccountId && x.Active);
            }

            TryUpdateModel(account, form);

            if (account.AccountId <= 0)
            {
                account.Active = true;
                account.Created = CurrentDate.Now;
                account.CreatedBy = User.Identity.Name;
                modelEntities.Accountings.Add(account);
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult AccountingDelete(int Id)
        {
            var account = modelEntities.Accountings.SingleOrDefault(x => x.AccountId == Id && x.Active);
            if (account != null)
            {
                account.Active = false;
                account.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.Accountings.Where(x => x.Active).ToList();
            return PartialView("_AccountingList", list);
        }

    }
}
 