﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Model;
using System.Configuration;
using System.Xml.Linq;
using System.Data;
using System.Globalization;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using System.Xml;
using eCommerce.BackOffice.ViewModels;

namespace eCommerce.BackOffice.Controllers
{
    public class OcaController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        // GET: Oca
        public ActionResult Index()
        {
            return View("ShippingList", this.ShippingsToShow());
        }

        private List<OcaShipment> ShippingsToShow()
        {
            return modelEntities.OcaShipments.Where(x => x.Deleted == null).OrderByDescending(x => x.Date).ToList();
        }

        public PartialViewResult RefreshList()
        {
            return PartialView("_ShippingList", this.ShippingsToShow());
        }

        public ActionResult PrintLabel(int Id)
        {
            OcaShipment shipment = modelEntities.OcaShipments.Find(Id);

            var idOrdenRetiro = shipment.OrdenRetiro;
            string NroEnvio = shipment.NumeroEnvio;
            OcaOEP.Oep_TrackingSoapClient ocaShippingService = new OcaOEP.Oep_TrackingSoapClient();
            try
            {
                string html = ocaShippingService.GetHtmlDeEtiquetasPorOrdenOrNumeroEnvio(idOrdenRetiro, NroEnvio);
                ViewBag.HtmlLabel = html;
            }
            catch (Exception)
            {
                ViewBag.HtmlLabel = "Envío invalido";
            }

            return View();            
        }

        [HttpGet]
        public JsonResult CancelShipment(int Id)
        {
            OcaShipment shipment = modelEntities.OcaShipments.Find(Id);

            string Username = ConfigurationManager.AppSettings["OcaUsername"];
            string Password = ConfigurationManager.AppSettings["OcaPassword"];
            int idOrdenRetiro = int.Parse(shipment.OrdenRetiro);
            OcaOEP.Oep_TrackingSoapClient ocaShippingService = new OcaOEP.Oep_TrackingSoapClient();
            DataSet ds = ocaShippingService.AnularOrdenGenerada(Username, Password, idOrdenRetiro);
            int status = int.Parse(ds.Tables[0].Rows[0][0].ToString());
            switch (status)
            {
                case (int)CancelShipmentStatusEnum.Success:
                    //Solamente le pongo active = false, que significa que está anulada. Si esta deleted != null significa que esta BORRADA.
                    shipment.Active = false;
                    modelEntities.SaveChanges();
                    break;
                default:
                    break;
            }

            return Json(new { Status = status }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteShipment(int Id)
        {
            OcaShipment shipment = modelEntities.OcaShipments.Find(Id);
            int status = 0;

            try
            {
                if (!shipment.Active)
                {
                    if (shipment.Deleted == null)
                    {
                        shipment.Deleted = DateTime.Now;
                        shipment.DeletedBy = User.Identity.Name;
                        status = (int)DeleteShipmentStatusEnum.Success;
                        modelEntities.SaveChanges();
                    }
                    else
                    {
                        status = (int)DeleteShipmentStatusEnum.AlreadyDeleted;
                    }
                }
                else
                {
                    status = (int)DeleteShipmentStatusEnum.NotCanceled;
                }
            }
            catch (Exception)
            {
                status = (int)DeleteShipmentStatusEnum.Unknown;
            }
            

            return Json(new { Status = status }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateOcaShipping(int Id)
        {
            DeliveryOrder deliveryOrder = new DeliveryOrder();
            try
            {
                deliveryOrder = modelEntities.DeliveryOrders.Find(Id);
                int zipCode = int.Parse(ConfigurationManager.AppSettings["CodigoPostalOrigen"]);

                OcaEPAK.Oep_TrackingSoapClient ocaShippingService = new OcaEPAK.Oep_TrackingSoapClient();
                System.Xml.XmlNode node = ocaShippingService.GetCentrosImposicionConServiciosByCP(zipCode);

                if (node.ChildNodes.Count > 0)
                {
                    List<OcaCentroDeImposicion> centros = GetCentrosDeOca(node, OcaServiceType.AdmisionDePaquetes);
                    List<SelectListItem> items = new List<SelectListItem>();

                    foreach (var centro in centros)
                    {
                        var item = new SelectListItem();
                        item.Text = centro.Calle + " " + centro.Numero + " (CP: " + centro.CodigoPostal + ") - Tel: " + centro.Telefono;
                        item.Value = centro.CentroId;
                        items.Add(item);
                    }

                    TempData["CentrosDeImposicion"] = new SelectList(items, "Value", "Text");
                }

                var origins = modelEntities.OcaShipmentOrigins.Where(x => x.Deleted == null).ToList().Select(x => new
                {
                    Value = x.OcaShipmentOriginId,
                    Name = x.Name + " (" + x.Street + " " + x.StreetNumber + ")"
                });
                TempData["OcaOrigins"] = new SelectList(origins, "Value", "Name");
            }
            catch (Exception)
            {
                throw;
            }
            return View(deliveryOrder);
        }

        private List<OcaCentroDeImposicion> GetCentrosDeOca(XmlNode xmlDoc, OcaServiceType serviceType)
        {
            List<OcaCentroDeImposicion> centros = new List<OcaCentroDeImposicion>();
            OcaCentroDeImposicion centro;

            foreach (XmlNode node in xmlDoc.ChildNodes)
            {
                centro = new OcaCentroDeImposicion();
                centro.Calle = node["Calle"].InnerText.Trim();
                centro.Numero = node["Numero"].InnerText.Trim();
                centro.CodigoPostal = node["CodigoPostal"].InnerText.Trim();
                centro.Provincia = node["Provincia"].InnerText.Trim();
                centro.Localidad = node["Localidad"].InnerText.Trim();
                centro.CentroId = node["IdCentroImposicion"].InnerText.Trim();
                centro.Telefono = node["Telefono"].InnerText.Trim();

                foreach (XmlNode service in node["Servicios"].SelectNodes("Servicio"))
                {
                    if (int.Parse(service["IdTipoServicio"].InnerText) == (int)serviceType)
                    {
                        centros.Add(centro);
                        break;
                    }
                }
            }
            return centros.OrderBy(x => x.Calle).ToList();
        }


        [HttpPost]
        public ActionResult CreateOcaShipping(FormCollection form)
        {
            var deliveryOrderId = int.Parse(form["DeliveryOrderId"]);
            try
            {
                DeliveryOrder deliveryOrder = modelEntities.DeliveryOrders.Find(deliveryOrderId);
                Order order = deliveryOrder.Order;
                string Username = ConfigurationManager.AppSettings["OcaUsername"];
                string Password = ConfigurationManager.AppSettings["OcaPassword"];

                string xml = GenerateOcaXML(order, form);
                OcaEPAK.Oep_TrackingSoapClient ocaShippingService = new OcaEPAK.Oep_TrackingSoapClient();
                System.Data.DataSet ds = ocaShippingService.IngresoORMultiplesRetiros(Username, Password, xml, true, string.Empty, string.Empty);

                OcaShipment shipment = new OcaShipment();

                if (ds.Tables[0].TableName == "Error")
                {
                    throw new Exception(ds.Tables[0].Rows[0][0].ToString());
                }
                else
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string OrdenRetiro = row.Table.DataSet.Tables[1].Rows[0]["OrdenRetiro"].ToString();
                        string NumeroEnvio = row.Table.DataSet.Tables[1].Rows[0]["NumeroEnvio"].ToString();

                        shipment.DeliveryOrderId = deliveryOrderId;
                        shipment.OriginTypeId = int.Parse(form["ShipmentType"]);
                        shipment.DestionationTypeId = order.DeliveryMethodId == (int)DeliveryMethodEnum.OcaPaP ?
                            (int)DestinationShipmentTypeEnum.Domicilio :
                            (int)DestinationShipmentTypeEnum.Sucursal;
                        shipment.OrdenRetiro = OrdenRetiro;
                        shipment.NumeroEnvio = NumeroEnvio;
                        shipment.Date = DateTime.Now;
                        shipment.Active = true;

                        modelEntities.OcaShipments.Add(shipment);
                    }
                }

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Warning, ex.Message);
                return RedirectToAction("CreateOcaShipping", deliveryOrderId);
            }

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "¡Solicitud de envío creada correctamente!");
            return RedirectToAction("Index");
        }
        
        private string GenerateOcaXML(Order order, FormCollection form)
        {
            string Fecha = DateTime.Today.ToString("yyyyMMdd", CultureInfo.InvariantCulture);

            int NroRemito = order.OrderId;
            int IdOperativa = GetOperativaId(order);

            var ocaShipmentOriginId = int.Parse(form["OcaShipmentOriginId"]);
            var centroDeImposicionId = form["OcaCenterId"];
            var shipmentType = int.Parse(form["ShipmentType"]); // 1 = retiro / 2 = admisión
            var solicitante = form["Solicitante"];
            var observaciones = form["Observaciones"];

            var OcaOrigin = modelEntities.OcaShipmentOrigins.Find(ocaShipmentOriginId);

            //Datos del solicitante
            string CalleOrigen, CalleNumeroOrigen, PisoOrigen, DeptoOrigen, CPOrigen, LocalidadOrigen, ProvinciaOrigen, ContactoOrigen, EmailOrigen,
                SolicitanteOrigen, ObservacionesOrigen, CentroCostoOrigen, IdFranjaHorariaOrigen, IdCentroImposicionOrigen;
            CalleOrigen = CalleNumeroOrigen = PisoOrigen = DeptoOrigen = CPOrigen = LocalidadOrigen = ProvinciaOrigen = ContactoOrigen = EmailOrigen =
                SolicitanteOrigen = ObservacionesOrigen = CentroCostoOrigen = IdFranjaHorariaOrigen = IdCentroImposicionOrigen = string.Empty;

            if (shipmentType == 2)
            {
                CentroCostoOrigen = "0";
                IdCentroImposicionOrigen = centroDeImposicionId;

                CalleOrigen = OcaOrigin.Street;
                CalleNumeroOrigen = OcaOrigin.StreetNumber;
                PisoOrigen = OcaOrigin.Floor;
                DeptoOrigen = OcaOrigin.Department;
                CPOrigen = OcaOrigin.ZipCode;
                LocalidadOrigen = OcaOrigin.City;
                ProvinciaOrigen = OcaOrigin.Province.Name;
                ContactoOrigen = OcaOrigin.ContactName;
                EmailOrigen = OcaOrigin.Email;
                SolicitanteOrigen = solicitante;
                ObservacionesOrigen = observaciones;
                CentroCostoOrigen = "0";
                IdFranjaHorariaOrigen = "1";
            }
            else
            {
                IdCentroImposicionOrigen = OcaOrigin.OcaCenterId.ToString();
                CalleOrigen = OcaOrigin.Street;
                CalleNumeroOrigen = OcaOrigin.StreetNumber;
                PisoOrigen = OcaOrigin.Floor;
                DeptoOrigen = OcaOrigin.Department;
                CPOrigen = OcaOrigin.ZipCode;
                LocalidadOrigen = OcaOrigin.City;
                ProvinciaOrigen = OcaOrigin.Province.Name;
                ContactoOrigen = OcaOrigin.ContactName;
                EmailOrigen = OcaOrigin.Email;
                SolicitanteOrigen = solicitante;
                ObservacionesOrigen = observaciones;
                CentroCostoOrigen = "0";
                IdFranjaHorariaOrigen = "1";
            }

            //Datos del usuario
            string ApellidoDestino, NombreDestino, CalleDestino, CalleNumeroDestino, PisoDestino, DeptoDestino, LocalidadDestino, ProvinciaDestino, CPDestino,
                TelefonoDestino, EmailDestino, IDCIDestino, IdReservaDestino, CelularDestino, ObservacionesDestino;
            ApellidoDestino = NombreDestino = CalleDestino = CalleNumeroDestino = PisoDestino = DeptoDestino = LocalidadDestino = ProvinciaDestino = CPDestino =
                TelefonoDestino = EmailDestino = IDCIDestino = IdReservaDestino = CelularDestino = ObservacionesDestino = string.Empty;

            var name = order.Customer.FullName.Split(' ')[0];
            var lastname = order.Customer.FullName.Split(' ').Length > 1 ? order.Customer.FullName.Split(' ')[1] : order.Customer.FullName.Split(' ')[0];

            if (order.DeliveryMethodId == (int)DeliveryMethodEnum.OcaPaS)
            {
                IDCIDestino = order.OcaCenterId.ToString();
                ApellidoDestino = lastname;
                NombreDestino = name;
                CPDestino = order.Customer.ZipCode;
                TelefonoDestino = !string.IsNullOrEmpty(order.Customer.PhoneNumber) ? order.Customer.PhoneNumber : string.Empty;
                EmailDestino = order.Customer.Email;
                CelularDestino = !string.IsNullOrEmpty(order.Customer.Cellphone) ? order.Customer.Cellphone : string.Empty;
                ObservacionesDestino = order.Observations != null ? order.Observations : string.Empty;
            }
            else if (order.DeliveryMethodId == (int)DeliveryMethodEnum.OcaPaP)
            {
                IDCIDestino = "0";
                ApellidoDestino = lastname;
                NombreDestino = name;
                CalleDestino = order.Customer.Address;
                CalleNumeroDestino = order.Customer.AddressNumber;
                PisoDestino = order.Customer.Floor != null ? order.Customer.Floor : string.Empty;
                DeptoDestino = order.Customer.Department != null ? order.Customer.Department : string.Empty;
                LocalidadDestino = order.Customer.City;
                ProvinciaDestino = order.Customer.Province.Name;
                CPDestino = order.Customer.ZipCode;
                TelefonoDestino = !string.IsNullOrEmpty(order.Customer.PhoneNumber) ? order.Customer.PhoneNumber : string.Empty;
                EmailDestino = order.Customer.Email;
                CelularDestino = !string.IsNullOrEmpty(order.Customer.Cellphone) ? order.Customer.Cellphone : string.Empty;
                ObservacionesDestino = order.Observations != null ? order.Observations : string.Empty;
            }

            //Datos de volumenes de paquete
            string Alto, Ancho, Largo, Peso, Valor, Cantidad;
            Alto = ConfigurationManager.AppSettings["defaultHigh"];
            Ancho = ConfigurationManager.AppSettings["defaultWidth"];
            Largo = ConfigurationManager.AppSettings["defaultLong"];
            Peso = ConfigurationManager.AppSettings["defaultWeight"];
            Valor = "0";
            Cantidad = "1";

            //Datos de OCA
            string NroCuenta = ConfigurationManager.AppSettings["OcaNroCta"];

            //Genero el XML
            XDocument xml = new XDocument(
                new XDeclaration("1.0", "iso-8859-1", "yes"),
                new XElement("ROWS",
                    new XElement("cabecera",
                        new XAttribute("ver", "2.0"),
                        new XAttribute("nrocuenta", NroCuenta)
                    ),
                    new XElement("origenes",
                        new XElement("origen",
                            new XAttribute("calle", CalleOrigen),
                            new XAttribute("nro", CalleNumeroOrigen),
                            new XAttribute("piso", PisoOrigen),
                            new XAttribute("depto", DeptoOrigen),
                            new XAttribute("cp", CPOrigen),
                            new XAttribute("localidad", LocalidadOrigen),
                            new XAttribute("provincia", ProvinciaOrigen),
                            new XAttribute("contacto", ContactoOrigen),
                            new XAttribute("email", EmailOrigen),
                            new XAttribute("solicitante", SolicitanteOrigen),
                            new XAttribute("observaciones", ObservacionesOrigen),
                            new XAttribute("centrocosto", CentroCostoOrigen),
                            new XAttribute("idfranjahoraria", IdFranjaHorariaOrigen),
                            new XAttribute("idcentroimposicionorigen", IdCentroImposicionOrigen),
                            new XAttribute("fecha", Fecha),
                            new XElement("envios",
                                new XElement("envio",
                                    new XAttribute("idoperativa", IdOperativa),
                                    new XAttribute("nroremito", NroRemito),
                                    new XElement("destinatario",
                                        new XAttribute("apellido", ApellidoDestino),
                                        new XAttribute("nombre", NombreDestino),
                                        new XAttribute("calle", CalleDestino),
                                        new XAttribute("nro", CalleNumeroDestino),
                                        new XAttribute("piso", PisoDestino),
                                        new XAttribute("depto", DeptoDestino),
                                        new XAttribute("localidad", LocalidadDestino),
                                        new XAttribute("provincia", ProvinciaDestino),
                                        new XAttribute("cp", CPDestino),
                                        new XAttribute("telefono", TelefonoDestino),
                                        new XAttribute("email", EmailDestino),
                                        new XAttribute("idci", IDCIDestino),
                                        //new XAttribute("idreserva", IdReservaDestino),
                                        new XAttribute("celular", CelularDestino),
                                        new XAttribute("observaciones", ObservacionesDestino)
                                        ),
                                    new XElement("paquetes",
                                        new XElement("paquete",
                                            new XAttribute("alto", Alto),
                                            new XAttribute("ancho", Ancho),
                                            new XAttribute("largo", Largo),
                                            new XAttribute("peso", Peso),
                                            new XAttribute("valor", Valor),
                                            new XAttribute("cant", Cantidad)
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                );

            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        private int GetOperativaId(Order order)
        {
            int operativa = int.Parse(ConfigurationManager.AppSettings["OperativaSaS"]);

            if (order.DeliveryMethodId == (int)DeliveryMethodEnum.OcaPaP)
                operativa = int.Parse(ConfigurationManager.AppSettings["OperativaSaP"]);

            return operativa;
        }

    }
}