﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class StoresController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            return View("StoreList", this.StoresToList());
        }

        private List<Store> StoresToList()
        {
            return modelEntities.Stores.Where(x => x.Deleted == null).OrderBy(x => x.Name).ToList();
        }

        [HttpGet]
        public ActionResult StoreUpdate(int Id)
        {
            var provinces = modelEntities.Provinces.ToList();

            if (Id != -1)
            {
                var store = modelEntities.Stores.SingleOrDefault(x => x.StoreId == Id);
                if (store.ProvinceId.HasValue)
                {
                    TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name", store.ProvinceId);
                }
                else
                {
                    TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name");
                }
                return View(store);
            }

            TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name");
            return View(new Store { StoreId = Id });
        }

        [HttpPost]
        public ActionResult StoreUpdate(FormCollection form)
        {
            Store store = new Store();

            if (form["StoreId"] != string.Empty && Convert.ToInt16(form["StoreId"]) > 0)
            {
                int storeId = Convert.ToInt16(form["StoreId"]);
                store = modelEntities.Stores.SingleOrDefault(x => x.StoreId == storeId && x.Deleted == null);
            }

            TryUpdateModel(store, form);

            if (Request["RetailDelivery"] != null && Request["RetailDelivery"] == "on")
                store.RetailDelivery = true;
            else
                store.RetailDelivery = false;
            if (Request["WholesalerDelivery"] != null && Request["WholesalerDelivery"] == "on")
                store.WholesalerDelivery = true;
            else
                store.WholesalerDelivery = false;

            if (store.StoreId<= 0)
            {
                modelEntities.Stores.Add(store);
            }

            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult StoreDelete(int Id)
        {
            var store = modelEntities.Stores.SingleOrDefault(x => x.StoreId == Id && x.Deleted == null);
            if (store != null)
            {
                store.Deleted = CurrentDate.Now;
                store.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            return PartialView("_StoreList", this.StoresToList());
        }        
    }
}