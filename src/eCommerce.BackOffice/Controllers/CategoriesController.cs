﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Utils;
using eCommerce.Services.Model.Enums;

namespace eCommerce.BackOffice.Controllers
{
    public class CategoriesController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        private const string CategoriesFileUploadDirectory = "/Content/UploadDirectory/Categories";

        public ActionResult Index()
        {
            List<Category> categories = modelEntities.Categories.Where(x => x.Active == true).ToList();
            return View("CategoryList", categories);
        }

        private List<SelectListItem> GetUserSizeTableTypes()
        {
            Array values = Enum.GetValues(typeof(SizeTableTypeEnum));
            List<SelectListItem> items = new List<SelectListItem>(values.Length);

            foreach (var i in values)
            {
                items.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(SizeTableTypeEnum), i),
                    Value = ((int)i).ToString()
                });
            }

            return items;
        }

        [HttpGet]
        [OutputCache(Duration = 0)]
        public ActionResult CategoryUpdate(int Id)
        {
            var categories = modelEntities.Categories.Where(x => x.Active).OrderBy(x => x.Name).ToList();
            ViewData["Image"] = string.Empty;

            var sizeTableTypes = GetUserSizeTableTypes();
            TempData["SizeTableTypes"] = new SelectList(sizeTableTypes, "Value", "Text");
            if (Id != -1)
            {
                var category = modelEntities.Categories.SingleOrDefault(x => x.CategoryId == Id);
                var path = Path.Combine(CategoriesFileUploadDirectory, Id.ToString());

                TempData["Categories"] = new SelectList(categories, "CategoryId", "CompleteName", category.ParentCategoryId);

                if (!string.IsNullOrEmpty(category.Image))
                    ViewData["image"] = Path.Combine(path, category.Image);

                return View(category);
                TempData["SizeTableTypes"] = new SelectList(sizeTableTypes, "Value", "Text", category.SizeTableTypeId);
            }

            TempData["Categories"] = new SelectList(categories, "CategoryId", "CompleteName");

            return View(new Category { CategoryId = Id });
        }

        [HttpPost]
        public ActionResult CategoryUpdate(FormCollection form, HttpPostedFileBase image)
        {
            Category category = new Category();

            if (form["CategoryId"] != string.Empty && Convert.ToInt16(form["CategoryId"]) > 0)
            {
                int categoryId = Convert.ToInt16(form["CategoryId"]);
                category = modelEntities.Categories.SingleOrDefault(x => x.CategoryId == categoryId && x.Active);
            }


            TryUpdateModel(category, form);

            category.Name = category.Name.Trim();

            if (category.ParentCategoryId.HasValue && category.ParentCategoryId > 0)
            {
                var parentCategory = modelEntities.Categories.Find(category.ParentCategoryId.Value);
                category.SeoUrl = parentCategory.SeoUrl + "-" + category.SeoUrlSelf;
            } else
            {
                category.SeoUrl = category.SeoUrlSelf;
            }

            if (!ValidSeoUrl(category))
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "El Seo Url ya está usado por otra categoría, intente con otro.");
                return RedirectToAction("CategoryUpdate", new { Id = category.CategoryId });
            }

            UpdateChildCategoriesSeoUrl(category);


            //foreach (var item in category.ChildCategories)
            //{
            //    item.SeoUrl = category.SeoUrl + "-" + item.SeoUrlEditable;
            //    if (item.ChildCategories.Any())
            //    {
            //        foreach (var subItem in item.ChildCategories)
            //        {
            //            subItem.SeoUrl = item.SeoUrl + "-" + subItem.SeoUrl;
            //            if (subItem.ChildCategories.Any())
            //            {
            //                foreach (var subSubItem in subItem.ChildCategories)
            //                {
            //                    subSubItem.SeoUrl = subItem.SeoUrl + "-" + subSubItem.SeoUrl;
            //                }
            //            }
            //        }
            //    }
            //}

            if (Request["flagDelete"] != null && Request["flagDelete"] != string.Empty && bool.Parse(Request["flagDelete"]) == true)
                category.Image = null;

            if (Request["ShowInHome"] != null && Request["ShowInHome"] == "on")
                category.ShowInHome = true;
            else
                category.ShowInHome = false;

            if (Request["ShowInHeader"] != null && Request["ShowInHeader"] == "on")
                category.ShowInHeader = true;
            else
                category.ShowInHeader = false;

            if (Request["Enabled"] != null && Request["Enabled"] == "on")
                category.Enabled = true;
            else
                category.Enabled = false;

            if (category.CategoryId <= 0)
            {
                category.Active = true;
                category.Created = CurrentDate.Now;
                category.CreatedBy = User.Identity.Name;
                modelEntities.Categories.Add(category);
            }

            modelEntities.SaveChanges();

            category = modelEntities.Categories.Include("Category2").FirstOrDefault(x => x.CategoryId == category.CategoryId);
            category.CompleteName = category.GetCompleteName;
            modelEntities.SaveChanges();
            if (image != null && image.ContentLength > 0)
            {
                string itemId = category.CategoryId.ToString();
                var path = Path.Combine(Server.MapPath(CategoriesFileUploadDirectory), itemId);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string fileName = Path.GetFileName(image.FileName);
                string fileExtension = fileName.Substring(image.FileName.LastIndexOf("."));
                string new_fileName = "image_" + itemId + fileExtension;
                string filePath = Path.Combine(path, new_fileName);

                image.SaveAs(filePath);
                category.Image = new_fileName;
                modelEntities.SaveChanges();
            }
            
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult CategoryDelete(int Id)
        {
            var Category = modelEntities.Categories.SingleOrDefault(x => x.CategoryId == Id && x.Active);
            if (Category != null)
            {
                Category.Active = false;
                Category.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.Categories.Where(x => x.Active).ToList();
            return PartialView("_CategoryList", list);
        }

        [HttpGet]
        public virtual JsonResult GetCategorySeoUrl(int CategoryId)
        {
            try
            {
                var category = modelEntities.Categories.Find(CategoryId);
                return Json(new { status = "ok", data = "/" + Utility.CataloguePrefix + "/" + category.SeoUrl }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = "error" }, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

        private string GetSeoUrl(Category category)
        {
            if (category.ParentCategory == null)
            {
                return category.SeoUrl;
            }
            else
            {
                return GetSeoUrl(category.ParentCategory) +
                    "-" + category.SeoUrl;
            }
        }

        public bool ValidSeoUrl(Category category)
        {
            return !modelEntities.Categories.Any(x => x.SeoUrl == category.SeoUrl && x.CategoryId != category.CategoryId && x.Deleted == null);
        }
        
        private void UpdateChildCategoriesSeoUrl(Category category)
        {
            foreach (var item in category.ChildCategories)
            {
                item.SeoUrl = category.SeoUrl + "-" + item.SeoUrlSelf;
                if (item.ChildCategories.Any())
                {
                    UpdateChildCategoriesSeoUrl(item);
                }
            }
        }
    }
}