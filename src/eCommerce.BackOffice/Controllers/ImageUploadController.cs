﻿using System.Web.Mvc;

namespace eCommerce.Controllers.Backoffice
{
    public partial class BackOfficeController : Controller
    {
        public const string UploadDirectory = "~/Content/UploadControl/UploadFolder/Products/";
        public const string UploadDirectory_Headings = "~/Content/UploadFolder/Headings/";

        public const string UploadCategoryDirectory = "~/Content/UploadControl/UploadFolder/Categories/";
        public const string UploadPressDirectory = "~/Content/UploadControl/UploadFolder/Press/";
        public const string UploadColorDirectory = "~/Content/UploadControl/UploadFolder/Colors/";
        public const string UploadHomeDirectory = "~/Content/UploadControl/UploadFolder/HomeDraft/";
        public const string UploadLookbookDirectory = "~/Content/UploadControl/UploadFolder/Lookbook/";


                
        /*


        public ActionResult CallbacksLookbookImage()
        {
            string filePrefix = "image_";

            Lookbook new_item = new Lookbook();
            new_item.Active = true;
            new_item.Enabled = true;
            
            modelEntities.Lookbooks.AddObject(new_item);
            modelEntities.SaveChanges();

            TempData["FileName"] = filePrefix + new_item.LookbookId.ToString();
            TempData["ItemId"] = new_item.LookbookId.ToString();

            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ucCallbacksLookbookImage", PhotoUtils.ValidationSettings, ImageLookbookUploadComplete);
            return null;
        }

        public ActionResult CallbacksImageOne(int Id)
        {
            var prod = modelEntities.Products.Single(x => x.ProductId == Id);

            TempData["ProductId"] = prod.ProductId;
            TempData["FilePrefix"] = "image1_";
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ucCallbacksImageOne", PhotoUtils.ValidationSettings, ImageUploadCompleteResizer);
            return null;
        }

        public ActionResult CallbacksImage2(int Id)
        {
            var prod = modelEntities.Products.Single(x => x.ProductId == Id);

            TempData["ProductId"] = prod.ProductId;
            TempData["FilePrefix"] = "image2_";
            UploadControlExtension.GetUploadedFiles("ucCallbacksImage2", PhotoUtils.ValidationSettings, ImageUploadCompleteResizer);
            return null;
        }

        public ActionResult CallbacksImage3(int Id)
        {
            var prod = modelEntities.Products.Single(x => x.ProductId == Id);

            TempData["ProductId"] = prod.ProductId;
            TempData["FilePrefix"] = "image3_";
            UploadControlExtension.GetUploadedFiles("ucCallbacksImage3", PhotoUtils.ValidationSettings, ImageUploadCompleteResizer);
            return null;
        }

        public ActionResult CallbacksImage4(int Id)
        {
            var prod = modelEntities.Products.Single(x => x.ProductId == Id);

            TempData["ProductId"] = prod.ProductId;
            TempData["FilePrefix"] = "image4_";
            UploadControlExtension.GetUploadedFiles("ucCallbacksImage4", PhotoUtils.ValidationSettings, ImageUploadCompleteResizer);
            return null;
        }

        public ActionResult CallbacksImage5(int Id)
        {
            var prod = modelEntities.Products.Single(x => x.ProductId == Id);

            TempData["ProductId"] = prod.ProductId;
            TempData["FilePrefix"] = "image5_";
            UploadControlExtension.GetUploadedFiles("ucCallbacksImage5", PhotoUtils.ValidationSettings, ImageUploadCompleteResizer);
            return null;
        }

        public ActionResult CallbacksImage6(int Id)
        {
            var prod = modelEntities.Products.Single(x => x.ProductId == Id);

            TempData["ProductId"] = prod.ProductId;
            TempData["FilePrefix"] = "image6_";
            UploadControlExtension.GetUploadedFiles("ucCallbacksImage6", PhotoUtils.ValidationSettings, ImageUploadCompleteResizer);
            return null;
        }

        public ActionResult CallbacksImage7(int Id)
        {
            var prod = modelEntities.Products.Single(x => x.ProductId == Id);

            TempData["ProductId"] = prod.ProductId;
            TempData["FilePrefix"] = "image7_";
            UploadControlExtension.GetUploadedFiles("ucCallbacksImage7", PhotoUtils.ValidationSettings, ImageUploadCompleteResizer);
            return null;
        }

        public ActionResult CallbacksColorThumb(int Id)
        {
            var color = modelEntities.Colors.Single(x => x.ColorId == Id && x.Active);

            TempData["ColorId"] = color.ColorId;
            TempData["FilePrefix"] = "color_" + color.ColorId.ToString();
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ucCallbacksColorThumb", PhotoUtils.ValidationSettings, ImageUploadCompleteColor);
            return null;
        }

        public ActionResult CallbacksCategoryThumb(int Id)
        {
            TempData["CategoryId"] = Id;
            TempData["FilePrefix"] = "category_";
            UploadControlExtension.GetUploadedFiles("ucCallbacksCategoryThumb", PhotoUtils.ValidationSettings, CategoryImageUploadComplete);
            return null;
        }

        public ActionResult CallbacksPressThumb(int Id)
        {
            TempData["PressId"] = Id;
            TempData["FilePrefix"] = "press_";
            UploadControlExtension.GetUploadedFiles("ucCallbacksPressThumb", PhotoUtils.ValidationSettings, PressImageUploadComplete);
            return null;
        }

        protected void ImageUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string fullDir = Request.MapPath(UploadDirectory + TempData["ProductId"]);

                if (!Directory.Exists(fullDir))
                    Directory.CreateDirectory(fullDir);

                string fileExtension = e.UploadedFile.FileName.Substring(e.UploadedFile.FileName.LastIndexOf("."));
                string fileName = TempData["FilePrefix"].ToString() + fileExtension;
                string filePath = UploadDirectory + TempData["ProductId"] + "/" + fileName ;

                using (Image original = Image.FromStream(e.UploadedFile.FileContent))
                {
                    PhotoUtils.SaveToJpeg(original, Request.MapPath(filePath));
                }

                e.CallbackData = fileName + "|" + Url.Content(filePath) + "?refresh=" + Guid.NewGuid();
            }
        }

        protected void ImageUploadCompleteColor(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string fullDir = Request.MapPath(UploadColorDirectory + TempData["ColorId"]);

                if (!Directory.Exists(fullDir))
                    Directory.CreateDirectory(fullDir);

                string fileExtension = e.UploadedFile.FileName.Substring(e.UploadedFile.FileName.LastIndexOf("."));
                string fileName = TempData["FilePrefix"].ToString() + fileExtension;
                string filePath = UploadColorDirectory + TempData["ColorId"] + "/" + fileName;

                using (Image original = Image.FromStream(e.UploadedFile.FileContent))
                {
                    PhotoUtils.SaveToJpeg(original, Request.MapPath(filePath));
                }

                e.CallbackData = fileName + "|" + Url.Content(filePath) + "?refresh=" + Guid.NewGuid();
            }
        }

        protected void CategoryImageUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string fullDir = Request.MapPath(UploadCategoryDirectory + TempData["CategoryId"]);

                if (!Directory.Exists(fullDir))
                    Directory.CreateDirectory(fullDir);

                string filePath = UploadCategoryDirectory + TempData["CategoryId"] + "/" + TempData["FilePrefix"] + e.UploadedFile.FileName;

                using (Image original = Image.FromStream(e.UploadedFile.FileContent))
                {
                    PhotoUtils.SaveToJpeg(original, Request.MapPath(filePath));
                }

                e.CallbackData = TempData["FilePrefix"] + e.UploadedFile.FileName + "|" + Url.Content(filePath) + "?refresh=" + Guid.NewGuid();
            }
        }

        protected void PressImageUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string fullDir = Request.MapPath(UploadPressDirectory + TempData["PressId"]);

                if (!Directory.Exists(fullDir))
                    Directory.CreateDirectory(fullDir);

                string filePath = UploadPressDirectory + TempData["PressId"] + "/" + TempData["FilePrefix"] + e.UploadedFile.FileName;

                using (Image original = Image.FromStream(e.UploadedFile.FileContent))
                {
                    PhotoUtils.SaveToJpeg(original, Request.MapPath(filePath));
                }

                e.CallbackData = TempData["FilePrefix"] + e.UploadedFile.FileName + "|" + Url.Content(filePath) + "?refresh=" + Guid.NewGuid();
            }
        }

        protected void ImageUploadCompleteResizer(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string fullDir = Request.MapPath(UploadDirectory + TempData["ProductId"]);

                if (!Directory.Exists(fullDir))
                    Directory.CreateDirectory(fullDir);

                string fileExtension = e.UploadedFile.FileName.Substring(e.UploadedFile.FileName.LastIndexOf("."));
                string fileName = TempData["FilePrefix"].ToString() + fileExtension;
                string filePathDetail = UploadDirectory + TempData["ProductId"] + "/detail_" + fileName;
                string filePathList = UploadDirectory + TempData["ProductId"] + "/list_" + fileName;

                using (Image original = Image.FromStream(e.UploadedFile.FileContent))
                {
                    PhotoUtils.SaveToJpeg(original, Request.MapPath(filePathDetail));
                    PhotoUtils.SaveToJpeg(PhotoUtils.Inscribe(original,235,329), Request.MapPath(filePathList));
                    //PhotoUtils.SaveToJpeg(original, Request.MapPath(filePathList));
                    //PhotoUtils.SaveToJpeg(PhotoUtils.Inscribe(original, 800, 1236), Request.MapPath(filePathDetail));
                    
                }

                e.CallbackData = fileName + "|" + Url.Content(filePathList) + "?refresh=" + Guid.NewGuid();

                int productId = int.Parse(TempData["ProductId"].ToString());
                var product = modelEntities.Products.FirstOrDefault(x => x.ProductId == productId);

                switch (TempData["FilePrefix"].ToString())
                {
                    case "image1_":
                        product.Image1 = fileName;
                        break;
                    case "image2_":
                        product.Image2 = fileName;
                        break;
                    case "image3_":
                        product.Image3 = fileName;
                        break;
                    case "image4_":
                        product.Image4 = fileName;
                        break;
                    case "image5_":
                        product.Image5 = fileName;
                        break;
                    case "image6_":
                        product.Image6 = fileName;
                        break;
                    case "image7_":
                        product.Image7 = fileName;
                        break;
                }

                modelEntities.SaveChanges();
            }
        }

        #region Images Home

        public ActionResult CallbacksHome1(int Id)
        {
            TempData["HomeImageName"] = "ImageHome1.jpg";
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ucCallbacksHome1", PhotoUtils.ValidationSettings, ImageHomeUploadComplete);
            return null;
        }

        public ActionResult CallbacksHome2(int Id)
        {
            TempData["HomeImageName"] = "ImageHome2.jpg";
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ucCallbacksHome2", PhotoUtils.ValidationSettings, ImageHomeUploadComplete);
            return null;
        }

        public ActionResult CallbacksHome3(int Id)
        {
            TempData["HomeImageName"] = "ImageHome3.jpg";
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ucCallbacksHome3", PhotoUtils.ValidationSettings, ImageHomeUploadComplete);
            return null;
        }

        protected void ImageHomeUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string filePath = UploadHomeDirectory + TempData["HomeImageName"];

                using (Image original = Image.FromStream(e.UploadedFile.FileContent))
                {
                    PhotoUtils.SaveToJpeg(original, Request.MapPath(filePath));
                }

                e.CallbackData = TempData["HomeImageName"] + "|" + Url.Content(filePath) + "?refresh=" + Guid.NewGuid();
            }
        }

        protected void ImageLookbookUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string file_extension = Path.GetExtension(e.UploadedFile.FileName);
                string filePath = UploadLookbookDirectory + TempData["FileName"] + file_extension;

                using (Image original = Image.FromStream(e.UploadedFile.FileContent))
                {
                    PhotoUtils.SaveToJpeg(original, Request.MapPath(filePath));
                }

                e.CallbackData = TempData["ItemId"].ToString()  + "|" + Url.Content(filePath) + "?refresh=" + Guid.NewGuid();
            }
        }

        #endregion*/
    }
}
