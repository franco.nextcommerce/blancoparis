﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Model.Enums;
using static eCommerce.Services.Utils.Utility;
using System.ComponentModel.DataAnnotations;

namespace eCommerce.BackOffice.Controllers
{
    public class BankInterestController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        //private const string BankPromotionsFileUploadDirectory = "/Content/UploadDirectory/BankPromotions";

        public ActionResult Index()
        {
            return View("BankInterestList", this.BankInterestsToList());
        }

        private List<BankInterest> BankInterestsToList()
        {
            return modelEntities.BankInterests.Where(x => !x.Deleted.HasValue).OrderBy(x => x.BankId).ThenBy(x => x.Name).ToList();
        }

        [HttpGet]
        public ActionResult BankInterestUpdate(int Id)
        {
            var banks = modelEntities.Banks.Where(x => !x.Deleted.HasValue).ToList();
            var StoreTypeList = from StoreTypeEnum publishedType in EnumUtil.GetValues<StoreTypeEnum>()
                                    select new
                                    {
                                        Id = (int)publishedType,
                                        Name = publishedType.GetAttribute<DisplayAttribute>().Name
                                    };

            if (Id != -1)
            {
                var bankInterest = modelEntities.BankInterests.FirstOrDefault(x => x.BankInterestId == Id);

                TempData["Banks"] = new SelectList(banks, "BankId", "Name", bankInterest.BankId);
                TempData["StoreType"] = new SelectList(StoreTypeList, "Id", "Name", bankInterest.StoreType);
                return View(bankInterest);
            }

            TempData["Banks"] = new SelectList(banks, "BankId", "Name");
            TempData["StoreType"] = new SelectList(StoreTypeList, "Id", "Name");
            return View(new BankInterest { BankInterestId = Id });
        }

        [HttpPost]
        public ActionResult BankInterestUpdate(FormCollection form)
        {
            BankInterest bankInterest = new BankInterest();

            if (form["BankInterestId"] != string.Empty && Convert.ToInt16(form["BankInterestId"]) > 0)
            {
                int bankInterestId = Convert.ToInt16(form["BankInterestId"]);
                bankInterest = modelEntities.BankInterests.FirstOrDefault(x => x.BankInterestId == bankInterestId && x.Deleted == null);
            }

            TryUpdateModel(bankInterest, form);

            if (bankInterest.Installments == 0)
            {
                bankInterest.Installments = 1;
            }


            if (Request["Active"] != null && Request["Active"] == "on")
                bankInterest.Active = true;
            else
                bankInterest.Active = false;

            if (bankInterest.BankInterestId <= 0)
            {
                modelEntities.BankInterests.Add(bankInterest);
            }

            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult BankInterestDelete(int Id)
        {
            var bankInterest = modelEntities.BankInterests.FirstOrDefault(x => x.BankInterestId == Id && !x.Deleted.HasValue);
            if (bankInterest != null)
            {
                bankInterest.Deleted = CurrentDate.Now;
                bankInterest.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            return PartialView("_BankInterestList", this.BankInterestsToList());
        }        
    }
}