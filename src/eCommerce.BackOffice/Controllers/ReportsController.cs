﻿using eCommerce.BackOffice.ViewModels;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace eCommerce.BackOffice.Controllers
{
    public class ReportsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        // GET: Reports
        public ActionResult Index()
        {
            OrderReportsViewModel model = new OrderReportsViewModel()
            {
                FromDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToShortDateString(),
                ToDate = DateTime.Today.ToShortDateString()
            };
            return View("ReportView", model);
        }


        [HttpGet]
        public ActionResult getReports(string from, string to, string userType)
        {
            DateTime fromDate = string.IsNullOrWhiteSpace(from) ? new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1) : DateTime.Parse(from);
            DateTime toDate = string.IsNullOrWhiteSpace(to) ? DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Parse(to).AddHours(23).AddMinutes(59).AddSeconds(59);
            int UserType = !string.IsNullOrEmpty(userType) ? int.Parse(userType) : -1;

            ReportsViewModel model = new ReportsViewModel();
            List<Order> orders = modelEntities.Orders.Include("Customer")
                    .Where(x => x.Active && x.Created >= fromDate && x.Created <= toDate && (x.StoreType == UserType || UserType == -1))
                    .OrderByDescending(x => x.OrderId).ToList();

            List<MostSoldProductViewModel> soldProductsRanking = modelEntities.OrderDetails.Where(x => x.Active && x.Created >= fromDate && x.Created <= toDate && x.Deleted == null && x.OrderId.HasValue && ( x.StoreType == UserType || UserType == -1) && x.Order.OrderStatusId == (int)OrderStatusEnum.Confirmado)
                .GroupBy(x => x.Product.Title)
                .Select(x => new MostSoldProductViewModel
                {
                    Title = x.Key,
                    Count = x.Sum(od => od.Quantity)
                }).OrderByDescending(x => x.Count).ToList();

            List<MostSoldUserViewModel> soldUserRanking = orders.Where(x => x.Active && x.Created >= fromDate && x.Created <= toDate && x.Deleted == null && (x.StoreType == UserType || UserType == -1) && x.OrderStatusId == (int)OrderStatusEnum.Confirmado)
                .GroupBy(x => x.Customer.FullName)
                .Select(x => new MostSoldUserViewModel
                {
                    Title = x.Key,
                    Amount = x.Sum(o => o.Total)
                }).OrderByDescending(x => x.Amount).ToList();

           
            var closedOrders = orders.Where(x => x.OrderStatusId == (int)OrderStatusEnum.Confirmado).ToList();
            model.ClosedOrders = closedOrders.Count();
            model.ProcessOrders = orders.Where(x => x.OrderStatusId == (int)OrderStatusEnum.EnProceso).Count();
            model.CanceledOrders = orders.Where(x => x.OrderStatusId == (int)OrderStatusEnum.Cancelada).Count();

            var orderIds = closedOrders.Select(x => x.OrderId).ToList();
            var orderDetails = modelEntities.OrderDetails.Where(x => x.OrderId.HasValue && orderIds.Contains(x.OrderId.Value) && x.Active
                && x.Order.OrderStatusId == (int)OrderStatusEnum.Confirmado && (x.Order.StoreType == UserType || UserType == -1)).ToList();

            var orderMobile = closedOrders.Where(x=>x.Channel == (int)OrderChannelEnum.Telefono);
            var orderDesktop = closedOrders.Where(x => x.Channel == (int)OrderChannelEnum.SitioWeb);
            var orderSoldMobile = closedOrders.Where(x => x.OrderStatusId == (int)OrderStatusEnum.Confirmado && x.Channel == (int)OrderChannelEnum.Telefono);
            var orderSoldDesktop = closedOrders.Where(x => x.OrderStatusId == (int)OrderStatusEnum.Confirmado && x.Channel == (int)OrderChannelEnum.SitioWeb);


            model.ClosedOrdersMobile = orderSoldMobile.Count();
            model.ClosedOrdersDesktop = orderSoldDesktop.Count();
            model.OrderDesktopTotalSales = orderDesktop.Sum(x => x.OrderTotal);
            model.OrderMobileTotalSales = orderMobile.Sum(x => x.OrderTotal);

            model.ClosedOrdersUnitsSold = orderDetails.Sum(x => x.Quantity);
            model.ClosedOrdersTotalSales = closedOrders.Sum(x => x.OrderTotal);
            model.MostSoldProducts = soldProductsRanking;
            model.mostSoldUser = soldUserRanking;

            try
            {
                model.AverageOrderAmount = model.ClosedOrdersTotalSales / model.ClosedOrders;
            }
            catch (DivideByZeroException)
            {
                model.AverageOrderAmount = 0;
            }

            var customers = modelEntities.Customers.Where(x => x.Created >= fromDate && x.Created <= toDate && x.Active && (x.UserType == UserType || UserType == -1));
            model.RegisteredUsers = customers.Count();

            try
            {
                model.ClosedOrdersByRegistersPercentage = decimal.Round(model.ClosedOrders / model.RegisteredUsers * 100, 2);
                model.OrdersByRegistersPercentage = decimal.Round(orders.Count() / model.RegisteredUsers * 100, 2);
            }
            catch (DivideByZeroException)
            {
                model.ClosedOrdersByRegistersPercentage = 0;
                model.OrdersByRegistersPercentage = 0;
            }

            try
            {
                model.CheckOutAbandonRate = decimal.Round((model.ProcessOrders + model.CanceledOrders) / orders.Count() * 100, 2);
            }
            catch (DivideByZeroException)
            {
                model.CheckOutAbandonRate = 0;
            }

            model.RegionIndicators = closedOrders.Where(x => x.Customer.ProvinceId.HasValue)
                .GroupBy(x => new { ProvinceId = x.Customer.ProvinceId.Value, ProvinceName = x.Customer.Province.Name }).ToList()
                .Select(x => new eCommerce.BackOffice.ViewModels.ReportsViewModel.RegionIndicator
                {
                    ProvinceId = x.Key.ProvinceId,
                    ProvinceName = x.Key.ProvinceName,
                    ClosedOrders = x.Count(),
                    ClosedOrdersTotal = x.Sum(s => s.OrderTotal)
                }).ToList();

            model.RegionCustomers = customers.Where(x => x.ProvinceId.HasValue)
                .GroupBy(x => new { ProvinceId = x.ProvinceId.Value, ProvinceName = x.Province.Name })
                .Select(x => new eCommerce.BackOffice.ViewModels.ReportsViewModel.RegionCustomer
                {
                    ProvinceId = x.Key.ProvinceId,
                    ProvinceName = x.Key.ProvinceName,
                    Count = x.Count()
                }).ToList();

            return PartialView("_Reports", model);
        }

        [HttpGet]
        public ActionResult Excels()
        {
            ExcelReportsViewModel model = new ExcelReportsViewModel()
            {
                FromDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToShortDateString(),
                ToDate = DateTime.Today.ToShortDateString()
            };
            return View("Excels", model);
        }

        [HttpPost]
        //public ActionResult GenerateReport(string from, string to, string reportType)
        public ActionResult GenerateReport(FormCollection form)
        {
            DateTime fromDate = DateTime.Parse(form["fromDate"]);
            DateTime toDate = DateTime.Parse(form["toDate"]).AddHours(23).AddMinutes(59).AddSeconds(59);

            var grid = new System.Web.UI.WebControls.GridView();
            string fileName = string.Empty;
            switch ((ExcelReportEnum)int.Parse(form["ReportType"]))
            {
                case ExcelReportEnum.Sales:
                    grid.DataSource = ExportOrdersToExcel(fromDate, toDate);
                    fileName = "Ventas.xls";
                    break;

                case ExcelReportEnum.Charges:
                    grid.DataSource = ExportPaymentsToExcel(fromDate, toDate);
                    fileName = "Cobranzas.xls";
                    break;

                case ExcelReportEnum.Exchanges:
                    grid.DataSource = ExportExchangesToExcel(fromDate, toDate);
                    fileName = "Cambios.xls";
                    break;

                case ExcelReportEnum.Refunds:
                    grid.DataSource = ExportRefundsToExcel(fromDate, toDate);
                    fileName = "Devoluciones.xls";
                    break;
            }

            if(grid.DataSource == null)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "No se encontraron datos para las fechas ingresadas.");
                ExcelReportsViewModel model = new ExcelReportsViewModel()
                {
                    FromDate = fromDate.ToShortDateString(),
                    ToDate = toDate.ToShortDateString()
                };
                return RedirectToAction("Excels", model);
            }

            grid.DataBind();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }


        #region Excels

        #endregion

        private dynamic ExportOrdersToExcel(DateTime fromDate, DateTime toDate)
        {
            var result = modelEntities.OrderDetails.Where(x => x.Active && x.Deleted == null && x.Order.Active && x.Order.Deleted == null && x.Order.Created >= fromDate && x.Order.Created <= toDate)
                .OrderByDescending(x => x.OrderId)
                .ToList();

            if (result != null)
            {
                return  result.Select(x => new
                {
                    Tipo_Operación = "Venta",
                    Fecha = x.Order.Created.ToShortDateString(),
                    Nro_Pedido = x.OrderId,
                    Cliente = x.Order.Customer.FullName,
                    Email = x.Order.Customer.Email,
                    
                    Tipo_Usuario = ((UserTypeEnum)x.Order.Customer.UserType).ToString(),
                    Estado = ((OrderStatusEnum)x.Order.OrderStatusId).ToString(),
                    Forma_Pago = ((PaymentMethodEnum)x.Order.PaymentMethodId).ToString(),
                    Condicion_Iva = x.Order.Customer.IvaConditionId != null ? ((IvaConditionEnum)x.Order.Customer.IvaConditionId).ToString() : string.Empty,
                    Razon_Social = x.Order.Customer.CorporateName != null ? x.Order.Customer.CorporateName : string.Empty,
                    Provincia = x.Order.Customer.Province != null ? x.Order.Customer.Province.Name : string.Empty,
                    Codigo = x.Product.Code,
                    Nombre = x.Product.Title,
                    Talle = x.Size.Description,
                    Color = x.Color.Description,
                    Cantidad = x.Quantity,
                    Precio = x.Price.ToString("C2"),
                    Desc_Unit = (x.Discount.HasValue ? x.Discount.Value + "%" : string.Empty),
                    Desc_Unit_Monto = (x.Discount.HasValue ? (x.Price * (x.Discount.Value / 100)).ToString("C2") : string.Empty),
                    Desc_Pedido = (x.Order.Discount_Percent.HasValue ? x.Order.Discount_Percent.Value + "%" : string.Empty),
                    Desc_Pedido_Monto = (x.Order.Discount.HasValue ? (x.Price_with_discount * (x.Order.Discount_Percent.Value / 100)).ToString("C2") : string.Empty),
                    Recargo = (x.Order.Surcharge.HasValue ? x.Order.Surcharge.Value + "%" : string.Empty),
                    Recargo_valor = (x.Order.Surcharge.HasValue ? (x.Price_with_order_discount * (x.Order.Surcharge.Value / 100)).ToString("C2") : string.Empty),
                    Unitario_Final = x.Precio_Unitario_Final.ToString("C2"),
                    Total = x.Total_with_concepts.ToString("C2")
                });
            }

            return null;
        }

        private dynamic ExportRefundsToExcel(DateTime fromDate, DateTime toDate)
        {
            return modelEntities.RefundDetails.Where(x => x.Active && x.Deleted == null && x.Refund.Active && x.Refund.Deleted == null && x.Refund.Created >= fromDate && x.Refund.Created <= toDate)
                .OrderByDescending(x => x.RefundId)
                .ToList()
                .Select(x => new
                {
                    Tipo_Operación = "Devolución",
                    Fecha = x.Refund.Created.Value.ToShortDateString(),
                    Nro_Devolucion = x.RefundId,
                    Cliente = x.Refund.Customer.FullName,
                    Email = x.Refund.Customer.Email,
                    Tipo_Usuario = ((UserTypeEnum)x.Refund.Customer.UserType).ToString(),
                    Estado = string.Empty,
                    Forma_Pago = string.Empty,
                    //Condicion_Iva = x.Refund.Customer.IvaConditionId != null ? ((IvaConditionEnum)x.Refund.Customer.IvaConditionId).ToString() : string.Empty,
                    //Razon_Social = x.Refund.Customer.CorporateName != null ? x.Refund.Customer.CorporateName : string.Empty,
                    //Provincia = x.Refund.Customer.Province.Name,
                    Codigo = x.Product.Code,
                    Nombre = x.Product.Title,
                    Talle = x.Size.Description,
                    Color = x.Color.Description,
                    Cantidad = x.Quantity * -1,
                    Precio = x.Price.HasValue ? x.Price.Value.ToString("C2") : string.Empty,
                    Desc_unit = (x.Discount.HasValue ? x.Discount.Value + "%" : string.Empty),
                    Desc_Unit_Monto = (x.Discount.HasValue ? (x.Price.Value * (x.Discount.Value / 100)).ToString("C2") : string.Empty),
                    Desc_Pedido = (x.Refund.Discount_Percent.HasValue ? x.Refund.Discount_Percent.Value + "%" : string.Empty),
                    Desc_Pedido_Monto = (x.Refund.Discount.HasValue ? (x.Price_with_discount * (x.Refund.Discount_Percent.Value / 100)).ToString("C2") : string.Empty),
                    Recargo = (x.Refund.Surcharge.HasValue ? x.Refund.Surcharge.Value + "%" : string.Empty),
                    Recargo_valor = (x.Refund.Surcharge.HasValue ? (x.Price_with_order_discount * (x.Refund.Surcharge.Value / 100)).ToString("C2") : string.Empty),
                    Unitario_Final = x.Precio_Unitario_Final.ToString("C2"),
                    Total = (x.Refund.Amount.Value * -1).ToString("C2") 
                });
        }

        private dynamic ExportExchangesToExcel(DateTime fromDate, DateTime toDate)
        {
            return modelEntities.ExchangeDetails.Where(x => x.Active && x.Deleted == null && x.Exchange.Active && x.Exchange.Deleted == null && x.Exchange.Created >= fromDate && x.Exchange.Created <= toDate)
               .OrderByDescending(x => x.ExchangeId)
               .ToList()
               .Select(x => new
               {
                   Tipo_Operación = "Cambio",
                   Fecha = x.Exchange.Created.Value.ToShortDateString(),
                   Nro_Cambio = x.ExchangeId,
                   Cliente = x.Exchange.Customer.FullName,
                   Email = x.Exchange.Customer.Email,
                   Tipo_Usuario = ((UserTypeEnum)x.Exchange.Customer.UserType).ToString(),
                   Estado = string.Empty,
                   Forma_de_Pago = string.Empty,
                   //Condicion_Iva = x.Exchange.Customer.IvaConditionId != null ? ((IvaConditionEnum)x.Exchange.Customer.IvaConditionId).ToString() : string.Empty,
                   //Razon_Social = x.Exchange.Customer.CorporateName != null ? x.Exchange.Customer.CorporateName : string.Empty,
                   //Provincia = x.Exchange.Customer.Province.Name,
                   Codigo = x.Product.Code,
                   Nombre = x.Product.Title,
                   Talle = x.Size.Description,
                   Color = x.Color.Description,
                   Cantidad = (ExchangeDetailTypeEnum)x.ExchangeDetailTypeId == ExchangeDetailTypeEnum.Ingreso ? x.Quantity * -1 : x.Quantity,
                   Precio = x.Price.Value.ToString("C2"),
                   Desc_unit = string.Empty,
                   Desc_Unit_Monto = string.Empty,
                   Desc_Pedido = string.Empty,
                   Desc_Pedido_Monto = string.Empty,
                   Recargo = string.Empty,
                   Recargo_valor = string.Empty,
                   Unitario_Final = x.Price.Value.ToString("C2"),
                   Total = (x.Price.Value * x.Quantity * ((ExchangeDetailTypeEnum)x.ExchangeDetailTypeId == ExchangeDetailTypeEnum.Ingreso ? -1 : 1)).ToString("C2")
               });
        }

        private dynamic ExportPaymentsToExcel(DateTime fromDate, DateTime toDate)
        {
            var checks = modelEntities.Checks.Where(x => x.Deleted == null).ToList();

            var result = modelEntities.TransactionDetails
                .Where(x => x.Deleted == null && x.Date >= fromDate && x.Date <= toDate && x.Transaction.Deleted == null && x.Created >= fromDate && x.Created <= toDate)
                .OrderByDescending(x => x.Date)
                .ToList();

            if (result != null)
            {
                return result.Select(x => new
                {
                    Fecha = x.Created.ToShortDateString(),
                    Cliente = x.Order.Customer.FullName,
                    Email = x.Order.Customer.Email,
                    DNI = x.Order.Customer.DNI,
                    CUIT = x.Order.Customer.CUIT,
                    Tipo_Usuario = ((UserTypeEnum)x.Order.Customer.UserType).ToString(),
                    Tiene_Cheques = (checks.Any(s => s.ChargeTransactionId == x.TransactionId || s.TransactionId == x.TransactionId) ? "Si" : "No"),
                    Nro_Pedido = x.OrderId,
                    Cuenta_de_Pago = x.Transaction.Accounting.Name,
                    Importe = x.Amount,
                    Observaciones = x.Transaction.Observations,
                    Numero_de_operacion = x.TransactionId,
                    Condicion_Iva = x.Order.Customer.IvaConditionId != null ? ((IvaConditionEnum)x.Order.Customer.IvaConditionId).ToString() : string.Empty,
                    Razon_Social = x.Order.Customer.CorporateName != null ? x.Order.Customer.CorporateName : string.Empty,
                    Provincia = x.Order.Customer.Province.Name,
                    Forma_de_Facturacion = x.Order.Customer.BillingForm != null ? x.Order.Customer.BillingForm : string.Empty
                });
            }

            return null;
        }

    }
}
 