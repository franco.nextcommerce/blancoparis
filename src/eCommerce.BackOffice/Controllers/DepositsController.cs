﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class DepositsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            return View("DepositList", this.DepositToShow());
        }

        [HttpGet]
        [OutputCache(Duration = 0)]
        public ActionResult DepositUpdate(int Id)
        {
            if (Id != -1)
            {
                var deposit = modelEntities.Deposits.SingleOrDefault(x => x.DepositId == Id);
                return View(deposit);
            }

            return View(new Deposit { DepositId = Id });
        }

        [HttpPost]
        public ActionResult DepositUpdate(FormCollection form, HttpPostedFileBase image)
        {
            Deposit deposit = new Deposit();

            if (form["DepositId"] != string.Empty && Convert.ToInt16(form["DepositId"]) > 0)
            {
                int DepositId = Convert.ToInt16(form["DepositId"]);
                deposit = modelEntities.Deposits.SingleOrDefault(x => x.DepositId == DepositId && x.Deleted == null);
            }

            TryUpdateModel(deposit, form);

            if (deposit.DepositId <= 0)
            {
                deposit.Created = CurrentDate.Now;
                deposit.CreatedBy = User.Identity.Name;
                modelEntities.Deposits.Add(deposit);
            }

            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult DepositDelete(int Id)
        {
            var Deposit = modelEntities.Deposits.SingleOrDefault(x => x.DepositId == Id && x.Deleted == null);
            if (Deposit != null)
            {
                Deposit.DeletedBy = User.Identity.Name;
                Deposit.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            return PartialView("_DepositList", this.DepositToShow());
        }

        private List<Deposit> DepositToShow()
        {
            return modelEntities.Deposits.Where(x => x.Deleted == null).OrderBy(x => x.Name).ToList();
        }
    }
}