﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class DeliverySuppliersController : Controller
    {

        private DbModelEntities modelEntities = new DbModelEntities();

        #region messages

        private object refreshPageMessage =
            new { messageType = "info", result = "Actualice la página y vuelva a intentarlo." };
        private object deleteErrorMessage =
            new { messageType = "error", result = "Error al generar la baja, intentelo nuevamente mas tarde" };
        private object deleteMessage =
            new { messageType = "success", result = "La baja se generó correctamente" };
        private object NotEnoughtStockMessage =
            new { messageType = "warning", result = "No hay suficiente stock como para anular la recepción." };

        #endregion messages

        public ActionResult DeliverySuppliersList(int id)
        {
            return View(this.PurchaseDeliverySuppliersToShow(id));
        }

        [HttpGet]
        public ActionResult DeliverySuppliersUpdate(int Id)
        {
            var purchase = modelEntities.Purchases.FirstOrDefault(x => x.PurchaseId == Id);

            return View(new DeliverySupplier { Purchase = purchase });
        }

        [HttpPost]
        public ActionResult DeliverySuppliersUpdate(FormCollection form,int[] purchaseDetailIdHidden, bool[] enabledStock, int[] stock)
        {
            var PurchaseId = int.Parse(form["PurchaseId"].ToString());

            try
            {
                DeliverySupplier deliverySupplier = new DeliverySupplier();

                TryUpdateModel(deliverySupplier, form);
                deliverySupplier.Created = CurrentDate.Now;
                deliverySupplier.CreatedBy = User.Identity.Name;

                #region DeliverySuppliersDetails

                var count = 0;
                var countStock = 0;
                var purchase = modelEntities.Purchases.FirstOrDefault(x => x.PurchaseId == PurchaseId);

                foreach (int purchaseDetailId in purchaseDetailIdHidden)
                {
                    var purchaseDetail = purchase.PurchaseDetails
                        .FirstOrDefault(x => x.PurchaseDetailId == purchaseDetailId);
                    
                    if (enabledStock[count])
                    {
                        var stockToModify = modelEntities.Products.FirstOrDefault(x => x.ProductId == purchaseDetail.ProductId);

                        var stockToAdd = stock[countStock];

                        deliverySupplier.DeliverySuppliersDetails.Add(
                            new DeliverySuppliersDetail()
                            {
                                DeliverySuppliersId = deliverySupplier.DeliverySuppliersId,
                                ProductId = purchaseDetail.ProductId,
                                Quantity = stockToAdd,
                                PurchaseDetailId = purchaseDetail.PurchaseDetailId,
                                PurchaseDetail = purchaseDetail
                            }
                        );

                        //stockToModify.StockReal += stockToAdd;
                        //stockToModify.StockVirtual += stockToAdd;
                        countStock++;
                    }

                    count++;

                }

                #endregion DeliverySuppliersDetails

                if (!DeliverySupplierMandatoryItemsCompleted(deliverySupplier))
                {
                    GenerateStockDraftTempData(purchaseDetailIdHidden, enabledStock, stock, deliverySupplier);

                    return RedirectToAction("DeliverySuppliersUpdate/" + PurchaseId.ToString());
                }


                if (!ValidDeliverySupplier(deliverySupplier))
                {
                    GenerateStockDraftTempData(purchaseDetailIdHidden, enabledStock, stock, deliverySupplier);

                    return RedirectToAction("DeliverySuppliersUpdate/" + PurchaseId.ToString());
                }

                modelEntities.DeliverySuppliers.Add(deliverySupplier);
                modelEntities.SaveChanges();

                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente");
                return RedirectToAction("DeliverySuppliersList/" + PurchaseId.ToString());

            }
            catch (Exception ex)
            {

                NotificationBar.ShowMessage(this, NotificationMessageType.Warning,
                    "Se generó un error al guardar los datos, inténtelo nuevamente mas tarde.");
                return RedirectToAction("DeliverySuppliersList/" + PurchaseId.ToString());

            }

        }

        [HttpPost]
        public JsonResult DeleteDelivery(int id)
        {
            try
            {
                var deliveryDeleted = modelEntities.DeliverySuppliers.SingleOrDefault(x => x.DeliverySuppliersId == id && x.Deleted == null);

                if (deliveryDeleted == null)
                {
                    return Json(refreshPageMessage);
                }

                /*if (deliveryDeleted.DeliverySuppliersDetails.Any(x => x.Quantity > x.Product.StockReal))
                {
                    return Json(NotEnoughtStockMessage);
                }*/
                
                deliveryDeleted.Deleted = CurrentDate.Now;
                /*foreach (DeliverySuppliersDetail deliverySuppliersDetail in deliveryDeleted.DeliverySuppliersDetails)
                {
                    deliverySuppliersDetail.Product.StockReal -= deliverySuppliersDetail.Quantity;
                    deliverySuppliersDetail.Product.StockVirtual -= deliverySuppliersDetail.Quantity;
                }*/

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {

                return Json(deleteErrorMessage);

            }

            return Json(deleteMessage);

        }

        [HttpPost]
        public PartialViewResult DeliveryPartialRefresh(int purchaseId)
        {

            return PartialView("_DeliverySuppliersRefreshPartial",
                this.PurchaseDeliverySuppliersToShow(purchaseId));

        }

        [HttpPost]
        public PartialViewResult GetDeliveryDetail(int deliverySuppliersId)
        {

            var deliverySuppliersDetails = modelEntities.DeliverySuppliers
                .FirstOrDefault(deliverySupplier => deliverySupplier.DeliverySuppliersId == deliverySuppliersId)
                .DeliverySuppliersDetails;

            return PartialView("_DeliveryDetailModal", deliverySuppliersDetails);

        }

        #region privates

        private Purchase PurchaseDeliverySuppliersToShow(int purchaseId)
        {

            var purchase = modelEntities.Purchases.FirstOrDefault(x => x.PurchaseId == purchaseId);
            purchase.DeliverySuppliers = purchase.DeliverySuppliers.Where(x => x.Deleted == null).ToList();

            return purchase;

        }

        private bool DeliverySupplierMandatoryItemsCompleted(DeliverySupplier deliverySupplier)
        {

            if (deliverySupplier.Date == null || deliverySupplier.DeliverySuppliersDetails.All(x => x.Quantity == 0))
            {

                NotificationBar.ShowMessage(this, NotificationMessageType.Warning,
                    "Hay datos obligatorios que no fueron cargados");
                return false;

            }

            return true;

        }

        private bool ValidDeliverySupplier(DeliverySupplier deliverySupplier)
        {
            if (deliverySupplier.DeliverySuppliersDetails.Any(x => x.Quantity > x.PurchaseDetail.PendingDeliveries))
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Warning,
                    "Una de las recepciones es mayor a su pendiente");
                return false;
            }

            return true;
        }

        private void GenerateStocks(int PurchaseId)
        {
            /*
            var purchaseDetails = modelEntities.PurchaseDetails.Where(x => x.PurchaseId == PurchaseId);
            foreach (PurchaseDetail purchaseDetail in purchaseDetails)
            {
                var stock = modelEntities.Products.FirstOrDefault(x => x.ProductId == purchaseDetail.ProductId);

                if (stock == null)
                {
                    modelEntities.Stocks.Add(
                        new Stock()
                        {
                            ProductId = purchaseDetail.ProductId,
                            RealStock = 0
                        }
                    );
                }
            }

            modelEntities.SaveChanges();*/
        }

        private void GenerateStockDraftTempData(int[] purchaseDetailIdHidden, bool[] enabledStock, int[] stock, DeliverySupplier deliverySupplier)
        {
            var count = 0;
            var countStock = 0;
            var StockDraft = new Dictionary<int, int>();

            foreach (int purchaseDetailId in purchaseDetailIdHidden)
            {

                if (enabledStock[count])
                {

                    StockDraft.Add(purchaseDetailId, stock[countStock]);
                    countStock++;
                }
                else
                {
                    StockDraft.Add(purchaseDetailId, 0);
                }

                count++;
            }

            TempData["StockDraft"] = StockDraft;
            TempData["Date"] = deliverySupplier.Date.ToShortDateString();
            TempData["ReferNumber"] = deliverySupplier.ReferNumber;
            TempData["Comment"] = deliverySupplier.Comment;
        }

        #endregion privates

    }
}