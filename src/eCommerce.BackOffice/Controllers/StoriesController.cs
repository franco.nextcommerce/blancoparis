﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using eCommerce.Services.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    ////[AuthorizeUser(Roles = "Admin")]
    public class StoriesController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        //private const string StoriesFileUploadDirectory = "/Content/UploadDirectory/Stories";

        public ActionResult Index()
        {
            var stories = modelEntities.Stories.Where(x => x.Deleted == null).ToList();
            return View("StoryList", stories);
        }

        [HttpGet]
        public ActionResult StoryUpdate(int Id)
        {
            if (Id != -1)
            {
                var story = modelEntities.Stories.SingleOrDefault(x => x.Id == Id);
                return View(story);
            }

            return View(new Story { Id = Id });
        }

        [HttpPost]
        public ActionResult StoryUpdate(FormCollection form)
        {
            var story = new Story();

            if (form["Id"] != string.Empty && Convert.ToInt16(form["Id"]) > 0)
            {
                int storyId = Convert.ToInt16(form["Id"]);
                story = modelEntities.Stories.SingleOrDefault(x => x.Id == storyId && x.Active);
            }

            if (Request["IsNew"] != null && Request["IsNew"] == "on")
                story.IsNew = true;
            else
                story.IsNew = false;

            TryUpdateModel(story, form);

            story.SeoUrl = Utility.GenerateSeoString(story.Name);

            if (story.Id <= 0)
            {
                story.Active = true;
                story.Created = CurrentDate.Now;
                story.CreatedBy = User.Identity.Name;
                modelEntities.Stories.Add(story);
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }

        public ActionResult Images(int Id)
        {
            var story = modelEntities.Stories.SingleOrDefault(x => x.Id == Id);

            var path = Path.Combine(Utility.StoriesFileUploadDirectory, Id.ToString());
            foreach (var imagen in story.StoryImages)
            {
                imagen.URL = Path.Combine(path, imagen.ImageName);
            }

            return View(story);
        }
        
        [HttpPost]
        public ActionResult Images(FormCollection form, HttpPostedFileBase[] images)
        {
            var storyId = Convert.ToInt16(form["Id"]);
            var story = modelEntities.Stories.SingleOrDefault(x => x.Id == storyId && x.Active);

            var id = storyId.ToString();
            var path = Path.Combine(Server.MapPath(Utility.StoriesFileUploadDirectory), id);
            foreach (HttpPostedFileBase imageToSave in images.Where(x => x != null))
            {
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string fileName = Path.GetFileName(imageToSave.FileName);
                string fileExtension = fileName.Substring(imageToSave.FileName.LastIndexOf("."));
                string new_fileName = "story_" + Guid.NewGuid() + fileExtension;
                string filePath = Path.Combine(path, new_fileName);
                
                imageToSave.SaveAs(filePath);
                var storyImage = new StoryImage()
                {
                    StoryId = storyId,
                    ImageName = new_fileName,
                    DisplayOrder = 1
                };

                story.StoryImages.Add(storyImage);
            }

            if (!String.IsNullOrEmpty(form["erasedImages"]))
            {
                var imagenesABorrar = form["erasedImages"].Split(';').ToList();
                var list = modelEntities.StoryImages.Where(x => imagenesABorrar.Any(y => x.ImageName.IndexOf(y) == 0))
                    .ToList();

                list.ForEach(x => modelEntities.StoryImages.Remove(x));

                //borra las imágenes del servidor.
                foreach (var item in list)
                {
                    var imagePath = Path.Combine(path, item.ImageName);
                    if (System.IO.File.Exists(imagePath))
                        System.IO.File.Delete(imagePath);
                }
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }
        [HttpGet]
        public virtual PartialViewResult StoryDelete(int Id)
        {
            var story = modelEntities.Stories.SingleOrDefault(x => x.Id == Id);
            if (story != null)
            {
                story.Active = false;
                story.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.Stories.Where(x => x.Deleted == null).ToList();
            return PartialView("_StoryList", list);
        }
    }
}