﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class BankPromotionsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        private const string BankPromotionsFileUploadDirectory = "/Content/UploadDirectory/BankPromotions";

        public ActionResult Index()
        {
            return View("BankPromotionList", this.BankPromotionsToList());
        }

        private List<BankPromotion> BankPromotionsToList()
        {
            return modelEntities.BankPromotions.Where(x => x.Deleted == null).OrderBy(x => x.Title).ToList();
        }

        [HttpGet]
        public ActionResult BankPromotionUpdate(int Id)
        {
            if (Id != -1)
            {
                var bankPromotion = modelEntities.BankPromotions.FirstOrDefault(x => x.BankPromotionId == Id);
                var path = Path.Combine(BankPromotionsFileUploadDirectory, Id.ToString());

                if (!string.IsNullOrEmpty(bankPromotion.Image))
                    ViewData["image"] = Path.Combine(path, bankPromotion.Image);

                return View(bankPromotion);
            }

            return View(new BankPromotion { BankPromotionId = Id });
        }

        [HttpPost]
        public ActionResult BankPromotionUpdate(FormCollection form, HttpPostedFileBase image)
        {
            BankPromotion bankPromotion = new BankPromotion();

            if (form["BankPromotionId"] != string.Empty && Convert.ToInt16(form["BankPromotionId"]) > 0)
            {
                int bankPromotionId = Convert.ToInt16(form["BankPromotionId"]);
                bankPromotion = modelEntities.BankPromotions.FirstOrDefault(x => x.BankPromotionId == bankPromotionId && x.Deleted == null);
            }
            TryUpdateModel(bankPromotion, form);

            bankPromotion.Content = HttpUtility.UrlDecode(Request["hidHtml"]);

            if (bankPromotion.BankPromotionId <= 0)
            {
                modelEntities.BankPromotions.Add(bankPromotion);
            }

            modelEntities.SaveChanges();

            if (image != null && image.ContentLength > 0)
            {
                string itemId = bankPromotion.BankPromotionId.ToString();
                var path = Path.Combine(Server.MapPath(BankPromotionsFileUploadDirectory), itemId);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string fileName = Path.GetFileName(image.FileName);
                string fileExtension = fileName.Substring(image.FileName.LastIndexOf("."));
                string new_fileName = "image_" + itemId + fileExtension;
                string filePath = Path.Combine(path, new_fileName);

                image.SaveAs(filePath);
                bankPromotion.Image = new_fileName;
                modelEntities.SaveChanges();
            }

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult BankPromotionDelete(int Id)
        {
            var bankPromotion = modelEntities.BankPromotions.FirstOrDefault(x => x.BankPromotionId == Id && x.Deleted == null);
            if (bankPromotion != null)
            {
                bankPromotion.Deleted = CurrentDate.Now;
                bankPromotion.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            return PartialView("_BankPromotionList", this.BankPromotionsToList());
        }        
    }
}