﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using eCommerce.BackOffice.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using eCommerce.Services;
using eCommerce.Services.Services.Accounting;

namespace eCommerce.BackOffice.Controllers
{
    public class RefundsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        #region messages

        private object refreshPageMessage =
            new { messageType = "info", result = "Actualice la página y vuelva a intentarlo." };
        private object deleteErrorMessage =
            new { messageType = "error", result = "Error al generar la baja, intentelo nuevamente mas tarde." };
        private object deleteMessage =
            new { messageType = "success", result = "La devolución ha sido borrada correctamente y su stock devuelto." };

        #endregion messages

        // GET: Refund
        public ActionResult Index()
        {
            return View("RefundList", this.RefundsToShow());
        }

        private List<Refund> RefundsToShow()
        {
            return modelEntities.Refunds.Where(x => x.Active && x.Deleted == null).OrderByDescending(x => x.RefundId).ToList();
        }

        [HttpGet]
        public ActionResult RefundUpdate(int Id)
        {
            var customers = modelEntities.Customers.Where(x => x.Active).OrderBy(x => x.FullName).ToList();
            var users = modelEntities.USERS.Where(x => x.Active && x.Deleted == null).ToList();
            var deposits = modelEntities.Deposits.Where(x => x.Deleted == null).ToList();

            var user = users.FirstOrDefault(x => x.Username == User.Identity.Name && x.Active && x.Deleted == null);
            if (user.ROLES.FirstOrDefault().RoleName != "SuperAdmin")
            {
                customers = customers.Where(x => x.SalerId == user.User_Id).ToList();
                deposits = deposits.Where(x => x.SalerDeposits.Any(s => s.SalerId == user.User_Id)).ToList();
            }

            if (Id != -1)
            {
                var refund = modelEntities.Refunds.Include("RefundDetails").SingleOrDefault(x => x.RefundId == Id);
                refund.RefundDetails = refund.RefundDetails.Where(x => x.Active).OrderBy(x => x.RefundDetailId).ToList();

                var productsId = refund.RefundDetails.Select(x => x.ProductId).ToArray();
                TempData["Stocks"] = modelEntities.Stocks.Where(x => productsId.Contains(x.ProductId)).ToList();

                var products = modelEntities.Products.Select(x => new { ProductId = x.ProductId, Text = x.Code + " - " + x.Title }).ToList();
                TempData["Products"] = new SelectList(products, "ProductId", "Text");

                var sizes = modelEntities.Sizes.Select(x => new { SizeId = x.SizeId, Text = x.Description }).ToList();
                TempData["Sizes"] = new SelectList(sizes, "SizeId", "Text");

                var colors = modelEntities.Colors.Select(x => new { ColorId = x.ColorId, Text = x.Description }).ToList();
                TempData["Colors"] = new SelectList(colors, "ColorId", "Text");

                TempData["Customers"] = new SelectList(customers, "CustomerId", "FullName", refund.CustomerId);
                TempData["Deposits"] = new SelectList(deposits, "DepositId", "Name", refund.DepositId);
                return View(refund);
            }

            TempData["Customers"] = new SelectList(customers, "CustomerId", "FullName");
            TempData["Deposits"] = new SelectList(deposits, "DepositId", "Name");

            return View(new Refund { RefundId = Id, SalerId = user.User_Id });
        }

        [HttpPost]
        public ActionResult RefundUpdate(FormCollection form)
        {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        Refund refund = new Refund();

                        StockService stockService = new StockService();

                        if (form["RefundId"] != string.Empty && Convert.ToInt16(form["RefundId"]) > 0)
                        {
                            int RefundId = Convert.ToInt16(form["RefundId"]);
                            refund = modelEntities.Refunds.SingleOrDefault(x => x.RefundId== RefundId && x.Active);
                            refund.RefundDetails= refund.RefundDetails.Where(x => x.Active).ToList();
                        }

                        TryUpdateModel(refund, form);

                        refund.Amount = decimal.Parse(form["Amount"].Replace(".", ","));
                        
                        if (!string.IsNullOrEmpty(form["Discount"]))
                        {
                            refund.Discount = decimal.Parse(form["Discount"].Replace(".", ","));
                        }

                        if (!string.IsNullOrEmpty(form["SurchargeAmount"]))
                        {
                            refund.SurchargeAmount = decimal.Parse(form["SurchargeAmount"].Replace(".", ","));
                        }

                        if (refund.RefundId <= 0)
                        {
                            refund.Active = true;
                            refund.Created = CurrentDate.Now;
                            refund.CreatedBy = User.Identity.Name;
                            modelEntities.Refunds.Add(refund);
                            modelEntities.SaveChanges();
                        }

                        var listItems = GetRefundDetails(form, refund.RefundId);
                        UpdateRefundDetails(refund, listItems, stockService);

                        UpdateCustomerAccount(refund);
                        UpdateCustomerAccountBalance(refund.CustomerId);

                        modelEntities.SaveChanges();

                        //UpdateProductsStock(order);

                        //UpdateCustomerAccount(deliveryOrder);

                        //UpdateCustomerAccountBalance(refund.CustomerId, refund.RefundId);
                        
                        dbContextTransaction.Commit();
                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            //catch (NoStockException ex)
            //{
            //    NotificationBar.ShowMessage(this, NotificationMessageType.Error, ex.Message);
            //}
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar guardar la devolución.");
            }

            return RedirectToAction("Index");
        }

        private void UpdateRefundDetails(Refund refund, List<RefundDetail> details, StockService stockService)
        {
            List<RefundDetail> refundDetailsToDelete = new List<RefundDetail>();
            List<RefundDetail> refundDetailsToUpdate = new List<RefundDetail>();
            List<DeliveryOrderDetail> deliveryOrderDetails = new List<DeliveryOrderDetail>();

            List<DeliveryOrderDetail> existingDeliveryOrderDetails = modelEntities.DeliveryOrderDetails
                .Where(x => x.DeliveryOrder.Deleted == null && x.DeliveryOrder.RefundId.HasValue && x.DeliveryOrder.RefundId.Value == refund.RefundId)
                .ToList();

            DeliveryOrder deliveryOrder = new DeliveryOrder();
            deliveryOrder = modelEntities.DeliveryOrders.FirstOrDefault(x => x.RefundId == refund.RefundId && x.Deleted == null);
            if (deliveryOrder == null)
            {
                //Creación DeliveryOrder
                deliveryOrder = new DeliveryOrder()
                {
                    RefundId = refund.RefundId,
                    Date = refund.Created.HasValue ? refund.Created.Value : DateTime.Now,
                    Created = DateTime.Now,
                    CreatedBy = User.Identity.Name,
                    PaymentStatusId = (int)PaymentStatusEnum.Pagada
                };
                modelEntities.DeliveryOrders.Add(deliveryOrder);
            }
            
            DeliveryOrderDetail deliveryOrderDetail = new DeliveryOrderDetail();
            foreach (var detail in refund.RefundDetails)
            {
                var item = details.SingleOrDefault(x => x.RefundDetailId == detail.RefundDetailId);

                if (item != null)
                {
                    detail.Discount = item.Discount;
                    detail.Quantity = item.Quantity;
                    detail.Price = item.Price;
                    deliveryOrderDetail = existingDeliveryOrderDetails.FirstOrDefault(x => x.RefundDetailId == item.RefundDetailId);
                    deliveryOrderDetail.Quantity = item.Quantity * (-1);
                    //deliveryOrderDetail.Amount = getProductPriceByCustomer(item.ProductId, refund.CustomerId) * deliveryOrderDetail.Quantity;
                    deliveryOrderDetail.Amount = item.Price * deliveryOrderDetail.Quantity;
                }
                else
                {
                    detail.Active = false;
                    detail.Deleted = CurrentDate.Now;
                    detail.DeletedBy = User.Identity.Name;
                    refundDetailsToDelete.Add(detail);
                }
            }

            foreach (var item in details/*.Where(x => x.RefundDetailId == -1)*/)
            {
                if (item.RefundDetailId == -1)
                {
                    item.Refund = refund;
                    item.Active = true;
                    item.Created = CurrentDate.Now;
                    item.CreatedBy = User.Identity.Name;
                    modelEntities.RefundDetails.Add(item);
                    modelEntities.SaveChanges();
                    refundDetailsToUpdate.Add(item);
                    deliveryOrderDetail = new DeliveryOrderDetail()
                    {
                        DeliveryOrderId = deliveryOrder.DeliveryOrderId,
                        ProductId = item.ProductId,
                        Quantity = item.Quantity * (-1),
                        Amount = item.Price * item.Quantity * (-1),
                        //Amount = getProductPriceByCustomer(item.ProductId, item.Refund.CustomerId) * item.Quantity * (-1),
                        RefundDetailId = item.RefundDetailId,
                        DepositId = refund.DepositId                        
                    };
                    modelEntities.DeliveryOrderDetails.Add(deliveryOrderDetail);
                } else
                {
                    var refundDetail = modelEntities.RefundDetails.FirstOrDefault(x => x.RefundDetailId == item.RefundDetailId);
                    refundDetailsToUpdate.Add(refundDetail);
                }

            }
            
            stockService.UpdateRefundMovements(refundDetailsToUpdate, ref modelEntities);
            stockService.DeleteRefundMovements(refundDetailsToDelete, ref modelEntities);
            DeleteRefundDetails(refundDetailsToDelete);
            SetOrderDeliveryTotalAmount(deliveryOrder, refund);
        }

        private void DeleteRefundDetails(List<RefundDetail> refundOrderDetails)
        {
            if (refundOrderDetails.Count > 0)
            {
                List<DeliveryOrderDetail> deliveryOrderDetailsToRemove = new List<DeliveryOrderDetail>();
                foreach (var item in refundOrderDetails)
                {
                    var deliveryOrderDetail = modelEntities.DeliveryOrderDetails.FirstOrDefault(x => x.RefundDetailId == item.RefundDetailId);
                    deliveryOrderDetailsToRemove.Add(deliveryOrderDetail);
                }
                modelEntities.DeliveryOrderDetails.RemoveRange(deliveryOrderDetailsToRemove);
                modelEntities.RefundDetails.RemoveRange(refundOrderDetails);
                modelEntities.SaveChanges();
            }
        }

        public void SetOrderDeliveryTotalAmount(DeliveryOrder deliveryOrder, Refund refund)
        {
            decimal cost = 0;
            decimal total = 0;
            decimal item_total = 0;
            foreach (var item in deliveryOrder.DeliveryOrderDetails)
            {
                item_total = 0;
                item_total = item.Amount.Value;

                item.Amount = item_total;
                total += item_total;
            }
            
            deliveryOrder.Amount = total;
            //deliveryOrder.Cost = cost;

            modelEntities.SaveChanges();
        }

        private List<RefundDetail> GetRefundDetails(FormCollection form, int refundId)
        {
            List<RefundDetail> items = new List<RefundDetail>();

            bool create_new_item = false;
            RefundDetail item = new RefundDetail();
            foreach (string key in Request.Form.AllKeys)
            {
                if (key.Contains("_"))
                {
                    if (create_new_item)
                    {
                        item = new RefundDetail();
                        create_new_item = false;
                    }

                    if (key.Contains("Quantity"))
                        item.Quantity = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("ProductId"))
                        item.ProductId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("SizeId"))
                        item.SizeId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("ColorId"))
                        item.ColorId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("RefundDetailId"))
                        item.RefundDetailId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("Price"))
                        item.Price = decimal.Parse(Request.Form[key].Replace(".", ","));
                    else if (key.Contains("RefundDetailDiscount"))
                    {
                        if (!string.IsNullOrEmpty(Request.Form[key]))
                        {
                            item.Discount = decimal.Parse(Request.Form[key].Replace(".", ","));
                        }
                        item.RefundId = refundId;
                        items.Add(item);
                        create_new_item = true;
                    }
                }
            }

            return items;
        }

        //private decimal getProductPriceByCustomer(int productId, int customerId)
        //{
        //    var product = modelEntities.Products.Find(productId);
        //    var customer = modelEntities.Customers.Find(customerId);
        //    switch (customer.PriceList)
        //    {
        //        case (int)PriceListEnum.Minorista:
        //            return product.RetailPrice.HasValue ? product.RetailPrice.Value : 0;

        //        case (int)PriceListEnum.Mayorista:
        //            return product.WholesalePrice.HasValue ? product.WholesalePrice.Value : 0;

        //        case (int)PriceListEnum.Distribuidor:
        //            return product.DistributorPrice.HasValue ? product.DistributorPrice.Value : 0;
        //    }
        //    return 0;
        //}

        private void UpdateCustomerAccount(Refund refund)
        {
            int refundCategoryId = int.Parse(ConfigurationManager.AppSettings["RefundCategoryId"]);
            
            //foreach (var item in refund.RefundDetails)
            //{
            //    //var last_delivery = modelEntities.DeliveryOrderDetails
            //    //    .Where(x =>
            //    //        x.DeliveryOrder.Order.CustomerId == refund.CustomerId &&
            //    //        x.ProductId == item.ProductId &&
            //    //        x.DeliveryOrder.Deleted == null
            //    //    ).OrderByDescending(x => x.DeliveryOrder.Date).FirstOrDefault();

            //    //if (last_delivery != null)
            //    //{
            //    //    if (last_delivery.OrderDetail.Order.Discount_Percent.HasValue)
            //    //        refund_total += (last_delivery.OrderDetail.Price * item.Quantity) * (100 - last_delivery.OrderDetail.Order.Discount_Percent.Value) / 100;
            //    //    else
            //    //        refund_total += last_delivery.OrderDetail.Price * item.Quantity;
            //    //}
            //    //else
            //    //{
            //    //    refund_total += Convert.ToDecimal(item.Price * item.Quantity);
            //    //}
            //    if(item.Discount.HasValue)
            //        refund_total += (item.Price.Value * item.Quantity) - (item.Price.Value * item.Quantity) * item.Discount.Value / 100;
            //    else
            //        refund_total += item.Price.Value * item.Quantity;

            //}

            if (refund.Transactions.Any(x => x.Deleted == null))
            {
                var transaction = refund.Transactions.FirstOrDefault(x => x.Deleted == null);

                transaction.Amount = refund.Amount.Value;
                transaction.Date = CurrentDate.Now;
            }
            else
            {
                Transaction transaction = new Transaction();

                transaction.RefundId = refund.RefundId;
                transaction.Amount = refund.Amount.Value;
                transaction.CustomerId = refund.CustomerId;
                transaction.CurrencyId = (int)CurrencyEnum.Peso;
                transaction.TransactionTypeId = (int)TransactionTypeEnum.Ingreso;
                transaction.TransactionCategoryId = refundCategoryId;
                transaction.Date = DateTime.Today;
                transaction.Created = CurrentDate.Now;
                transaction.CreatedBy = User.Identity.Name;

                modelEntities.Transactions.Add(transaction);
            }

            //refund.Amount = refund.RefundDetails.Where(x => x.Active && x.Deleted == null).Sum(x => x.Discount.HasValue ? x.Price - (x.Price * x.Discount / 100) : x.Price);
            //if (refund.Discount.HasValue)
            //    refund.Amount -= refund.Discount.Value;
            //if (refund.SurchargeAmount.HasValue)
            //    refund.Amount += refund.SurchargeAmount.Value;
            

            modelEntities.SaveChanges();
        }

        private void UpdateCustomerAccountBalance(int customerId)
        {
            AccountingService.UpdateCustomerBalance(customerId, ref modelEntities);
        }
        
        public ActionResult RefundDelete(int id)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var refund = modelEntities.Refunds.SingleOrDefault(x => x.RefundId == id && x.Deleted == null);

                    refund.Deleted = CurrentDate.Now;
                    refund.DeletedBy = User.Identity.Name;

                    StockService stockService = new StockService(new VirtualStock());
                    stockService.DeleteRefundMovements(refund.RefundDetails, ref modelEntities);

                    modelEntities.SaveChanges();

                    DeleteRefundTransaction(refund.RefundId);

                    DeleteRefundDeliveryOrder(refund.RefundId);

                    UpdateCustomerAccountBalance(refund.CustomerId);

                    modelEntities.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                }
            }

            return PartialView("_RefundList", this.RefundsToShow());
        }

        private void DeleteRefundTransaction(int refundId)
        {
            try
            {
                var transaction = modelEntities.Transactions.FirstOrDefault(x => x.RefundId == refundId && x.Deleted == null);
                transaction.Deleted = CurrentDate.Now;
                transaction.DeletedBy = User.Identity.Name;

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeleteRefundDeliveryOrder(int refundId)
        {
            try
            {
                var deliveryOrder = modelEntities.DeliveryOrders.FirstOrDefault(x => x.RefundId == refundId && x.Deleted == null);
                deliveryOrder.Deleted = CurrentDate.Now;
                deliveryOrder.DeletedBy = User.Identity.Name;

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Print

        [HttpGet]
        public virtual ActionResult RefundPrint(int Id)
        {
            var refund = modelEntities.Refunds.Find(Id);
            return View(refund);
        }

        [HttpGet]
        public virtual ActionResult RefundPrintLayout(int Id)
        {
            var refund = modelEntities.Refunds.Find(Id);
            return View(refund);
        }

        [HttpGet]
        [ChildActionOnly]
        public virtual PartialViewResult ShowPrintRefundView(Refund refund)
        {
            refund.RefundDetails = refund.RefundDetails.OrderBy(x => x.Product.Title).ToList();
            return PartialView("_RefundPrint", refund);
        }

        #endregion
    }
}