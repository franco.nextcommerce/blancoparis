﻿using eCommerce.BackOffice.Models;
using eCommerce.Services.Security;
using System;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class AccountController : Controller
    {
        public IFormsAuthenticationService FormsService
        {
            get;
            private set;
        }

        public IMembershipService MembershipService
        {
            get;
            private set;
        }


        public AccountController()
            : this(null, null)
        {
        }

        public AccountController(IFormsAuthenticationService formsService, IMembershipService membershipService)
        {
            FormsService = formsService ?? new FormsAuthenticationService();
            MembershipService = membershipService ?? new MembershipService();
        }

        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult Login(LoginViewModel model, string returnUrl)
        {
            string username = Request["Username"];
            string password = Request["Password"];
            bool rememberMe = Request["RememberMe"] != null ? true : false;
            string action = string.Empty;
            bool isValid = false;

            isValid = MembershipService.Validate(username, password);

            if (isValid)
            {
                FormsService.SignIn(username, rememberMe);

                if (!String.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return Redirect("/");
                }
            }
            else
            {
                ViewData["ResultMessage"] = "El Nombre de Usuario o Contraseña es incorrecto.";
            }

            return View();
        }


        [HttpGet]
        public virtual ActionResult LogOut()
        {
            FormsService.SignOut();
            return RedirectToAction("Login", "Account");
        }
    }
}