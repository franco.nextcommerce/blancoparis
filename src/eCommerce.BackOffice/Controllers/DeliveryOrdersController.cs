﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using eCommerce.BackOffice.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using eCommerce.Services.Utils;
using eCommerce.Services.Model.Enums;
using System.Web;
using System.IO;
using eCommerce.Backoffice.Utils.Interfaces;
using eCommerce.Backoffice.Utils;
using eCommerce.Services;
using System.Web.UI;
using System.Configuration;

namespace eCommerce.BackOffice.Controllers
{
    public class DeliveryOrdersController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        private const string GuideFileUploadDirectory = "/Content/UploadDirectory/DeliveryOrderGuides";

        #region messages

        private object refreshPageMessage =
            new { messageType = "info", result = "Actualice la página y vuelva a intentarlo." };
        private object deleteErrorMessage =
            new { messageType = "error", result = "Error al generar la baja, intentelo nuevamente mas tarde." };
        private object deleteMessage =
            new { messageType = "success", result = "La entrega ha sido borrada correctamente y su stock devuelto." };
        
        #endregion messages

        public ActionResult DeliveryOrderList(int id)
        {
            return View(this.OrderDeliveryOrderToShow(id));
        }

        [HttpGet]
        public ActionResult DeliveryOrderUpdate(int OrderId, int DeliveryOrderId)
        {
            DeliveryOrder deliveryOrder = new DeliveryOrder();
            var order = modelEntities.Orders.FirstOrDefault(x => x.OrderId == OrderId);
            ViewData["Image"] = string.Empty;

            var user = modelEntities.USERS.FirstOrDefault(x => x.Username == User.Identity.Name && x.Active && x.Deleted == null);
            List<int> deposits = new List<int>();
            if (user.ROLES.FirstOrDefault().RoleName != "SuperAdmin")
                deposits = user.SalerDeposits.Select(x => x.Deposit).Select(x => x.DepositId).ToList();
            else
                deposits = modelEntities.Deposits.Where(x => x.Deleted == null).Select(x => x.DepositId).ToList();

            TempData["SalerDepositIds"] = deposits;

            if (DeliveryOrderId != -1)
            {
                deliveryOrder = modelEntities.DeliveryOrders.SingleOrDefault(a => a.OrderId == OrderId && a.DeliveryOrderId == DeliveryOrderId);

                var path = Path.Combine(GuideFileUploadDirectory, DeliveryOrderId.ToString());
                if (!string.IsNullOrEmpty(deliveryOrder.Image))
                    ViewData["image"] = Path.Combine(path, deliveryOrder.Image);

                var deliveryOrderDetails = modelEntities.DeliveryOrderDetails.Where(x => x.DeliveryOrderId == deliveryOrder.DeliveryOrderId).ToList();
                deliveryOrder.DeliveryOrderDetails = deliveryOrderDetails;
                deliveryOrder.Order = order;

                return View(deliveryOrder);
            }
            else
            {
                order.OrderDetails = order.OrderDetails.Where(x => x.Active && x.Deleted == null).ToList();

                return View(new DeliveryOrder { Order = order });
            }

        }

        [HttpPost]
        public ActionResult DeliveryOrderUpdate(FormCollection form, int[] DeliveryOrderDetailIdHidden, int[] OrderDetailIdHidden, bool[] enabledStock,int[] depositId, int[] stock, HttpPostedFileBase image)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                var OrderId = int.Parse(form["OrderId"].ToString());
                var DeliveryOrderId = int.Parse(form["DeliveryOrderId"].ToString());

                DeliveryOrder deliveryOrder;
                StockService stockService = new StockService();

                try
                {
                    var order = modelEntities.Orders.Include("OrderDetails").FirstOrDefault(x => x.OrderId == OrderId);
                    order.OrderDetails = order.OrderDetails.Where(x => x.Active).ToList();

                    var count = 0;
                    var countStock = 0;

                    if (DeliveryOrderId == 0)
                    {
                        deliveryOrder = new DeliveryOrder();
                        deliveryOrder.Created = CurrentDate.Now;
                        deliveryOrder.CreatedBy = User.Identity.Name;
                        deliveryOrder.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;
                        TryUpdateModel(deliveryOrder, form);

                        #region DeliveryOrderDetails

                        foreach (int OrderDetailId in OrderDetailIdHidden)
                        {
                            var orderDetail = order.OrderDetails.FirstOrDefault(x => x.OrderDetailId == OrderDetailId);
                            var product = modelEntities.Products.FirstOrDefault(x => x.ProductId == orderDetail.ProductId);

                            if (enabledStock[count])
                            {
                                var stockToAdd = stock[countStock];
                                var deposit = depositId[countStock];

                                deliveryOrder.DeliveryOrderDetails.Add(
                                    new DeliveryOrderDetail()
                                    {
                                        DeliveryOrderId = deliveryOrder.DeliveryOrderId,
                                        ProductId = orderDetail.ProductId,
                                        Quantity = stockToAdd,
                                        OrderDetailId = orderDetail.OrderDetailId,
                                        OrderDetail = orderDetail,
                                        DepositId = deposit
                                    }
                                );
                                //product.StockReal -= stockToAdd;
                                countStock++;
                            }
                            count++;
                        }
                        #endregion

                        modelEntities.DeliveryOrders.Add(deliveryOrder);
                        modelEntities.SaveChanges();

                        stockService.CreateDeliveryMovements(deliveryOrder, ref modelEntities);
                    }
                    else
                    {
                        deliveryOrder = modelEntities.DeliveryOrders.FirstOrDefault(x => x.DeliveryOrderId == DeliveryOrderId && x.Deleted == null);

                        TryUpdateModel(deliveryOrder, form);

                        #region DeliveryOrderDetails

                        /*var deliveryOrderDetails = modelEntities.DeliveryOrderDetails.Include("Product")
                            .Where(x => DeliveryOrderDetailIdHidden.Contains(x.DeliveryOrderDetailId));*/

                        var deliveryOrderDetails = modelEntities.DeliveryOrderDetails.Where(x => DeliveryOrderDetailIdHidden.Contains(x.DeliveryOrderDetailId));

                        List<DeliveryOrderDetail> deliveryOrderDetailsToDelete = new List<DeliveryOrderDetail>();

                        foreach (var deliveryOrderDetail in deliveryOrderDetails)
                        {
                            var product = deliveryOrderDetail.Product;

                            if (enabledStock[count] && stock[countStock] > 0)
                            {
                                var stockToAdd = stock[countStock];
                                countStock++;

                                deliveryOrderDetail.Quantity = stockToAdd;

                                if(deliveryOrderDetail.StockMovement != null)
                                    deliveryOrderDetail.StockMovement.Quantity = stockToAdd;
                                else
                                    stockService.CreateDeliveryItemMovement(deliveryOrderDetail, ref modelEntities); //corrige entregas sin control de stock que ahora se agregaron.

                                modelEntities.SaveChanges();

                                stockService.UpdateStockReal(deliveryOrderDetail.StockMovement.Stock, ref modelEntities);
                            }
                            else
                            {
                                deliveryOrderDetailsToDelete.Add(deliveryOrderDetail);
                            }
                            count++;
                        }

                        stockService.DeleteDeliveryMovements(deliveryOrderDetailsToDelete, ref modelEntities);
                        DeleteDeliveryOrderDetails(deliveryOrderDetailsToDelete);

                        #endregion
                    }

                    if (!ValidDeliveryOrder(deliveryOrder))
                    {
                        GenerateStockDraftTempData(OrderDetailIdHidden, enabledStock, stock, deliveryOrder);
                        dbContextTransaction.Rollback();
                        return RedirectToAction("DeliveryOrderUpdate", new { OrderId = OrderId.ToString(), DeliveryOrderId = deliveryOrder.OrderId > 0 ? deliveryOrder.OrderId : -1 });
                    }

                    modelEntities.SaveChanges();

                    //var total = deliveryOrder.DeliveryOrderDetails.Sum(x => x.OrderDetail.Price * x.Quantity);
                    decimal total = 0;
                    decimal item_total = 0;
                    foreach (var item in deliveryOrder.DeliveryOrderDetails)
                    {
                        item_total = 0;
                        if (item.OrderDetail.Discount.HasValue)
                        {
                            item_total = (item.OrderDetail.Price * item.Quantity) - (item.OrderDetail.Price * item.Quantity) * item.OrderDetail.Discount.Value / 100;
                            total += (item.OrderDetail.Price * item.Quantity) - (item.OrderDetail.Price * item.Quantity) * item.OrderDetail.Discount.Value / 100;
                        }
                        else
                        {
                            item_total = (item.OrderDetail.Price * item.Quantity);
                            total += (item.OrderDetail.Price * item.Quantity);
                        }
                        item.Amount = item_total;
                    }
                    modelEntities.SaveChanges();

                    if (deliveryOrder.Order.Discount.HasValue && deliveryOrder.Order.Discount.Value > 0)
                    {
                        total -= deliveryOrder.Order.Discount.Value;
                    }

                    SetOrderDeliveryTotalAmount(deliveryOrder);

                    UpdateOrderDeliveryStatus(deliveryOrder);

                    //UpdateCustomerAccount(deliveryOrder);

                    //UpdateCustomerAccountBalance(deliveryOrder.Order.CustomerId);

                    SaveDeliveryGuide(image, deliveryOrder);

                    dbContextTransaction.Commit();

                    NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente");
                    return RedirectToAction("DeliveryOrderList/" + OrderId.ToString());

                }
                catch (Exception ex)
                {
                    NotificationBar.ShowMessage(this, NotificationMessageType.Warning, "Se generó un error al guardar los datos, inténtelo nuevamente mas tarde.");
                    dbContextTransaction.Rollback();
                    return RedirectToAction("DeliveryOrderList/" + OrderId.ToString());
                }
            }
        }

        private void SetOrderDeliveryTotalAmount(DeliveryOrder deliveryOrder)
        {
            decimal total = 0;
            decimal item_total = 0;
            foreach (var item in deliveryOrder.DeliveryOrderDetails)
            {
                item_total = 0;
                if (item.OrderDetail.Discount.HasValue)
                    item_total = (item.OrderDetail.Price * item.Quantity) - (item.OrderDetail.Price * item.Quantity) * item.OrderDetail.Discount.Value / 100;
                else
                    item_total = (item.OrderDetail.Price * item.Quantity);

                item.Amount = item_total;
                total += item_total;
            }
            modelEntities.SaveChanges();

            if (deliveryOrder.Order.Discount_Percent.HasValue && deliveryOrder.Order.Discount_Percent.Value > 0)
            {
                total -= total * (deliveryOrder.Order.Discount_Percent.Value / 100);
            }

            deliveryOrder.Amount = total;
        }

        private void UpdateCustomerAccount(DeliveryOrder deliveryOrder)
        {
            try
            {
                int salesCategoryId = int.Parse(ConfigurationManager.AppSettings["SalesCategoryId"]);
                //int shippingCostCategoryId = int.Parse(ConfigurationManager.AppSettings["SalesCategoryId"]);

                if (deliveryOrder.Transactions.Any(x => x.DeliveryOrderId == deliveryOrder.DeliveryOrderId && x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso && x.Deleted == null))
                {
                    var transaction = deliveryOrder.Transactions.FirstOrDefault();
                    //transaction.Amount = deliveryOrder.Amount.Value * -1;
                    transaction.Amount = deliveryOrder.Amount.Value;
                    transaction.CurrencyId = deliveryOrder.Order.CurrenyId;
                    transaction.Date = CurrentDate.Now;

                    //TODO: Agregar transaction del costo de envío.
                }
                else
                {
                    Transaction transaction = new Transaction();

                    transaction.DeliveryOrderId = deliveryOrder.DeliveryOrderId;
                    //transaction.Amount = deliveryOrder.Amount.Value * -1;
                    transaction.Amount = deliveryOrder.Amount.Value;
                    transaction.CurrencyId = deliveryOrder.Order.CurrenyId;
                    //transaction.TransactionTypeId = (int)TransactionTypeEnum.Egreso;
                    transaction.TransactionTypeId = (int)TransactionTypeEnum.Ingreso;
                    transaction.TransactionCategoryId = salesCategoryId;
                    transaction.Date = DateTime.Today;
                    transaction.Created = CurrentDate.Now;
                    transaction.CreatedBy = User.Identity.Name;
                    modelEntities.Transactions.Add(transaction);

                    //TODO: Agregar transaction del costo de envío.
                }

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateCustomerAccountBalance(int customerId)
        {
            try
            {
                var customer = modelEntities.Customers.SingleOrDefault(x => x.CustomerId == customerId);
                var transactions = modelEntities.Transactions.Where(x => (x.DeliveryOrder.Order.CustomerId == customerId || x.CustomerId == customerId || x.Order.CustomerId == customerId) && x.Deleted == null).ToList();

                customer.Balance = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso).Sum(x => x.Amount);
                customer.BalanceUsd = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar).Sum(x => x.Amount);

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SaveDeliveryGuide(HttpPostedFileBase image, DeliveryOrder deliveryOrder)
        {
            if (image != null && image.ContentLength > 0)
            {
                string itemId = deliveryOrder.DeliveryOrderId.ToString();
                var path = Path.Combine(Server.MapPath(GuideFileUploadDirectory), itemId);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string fileName = Path.GetFileName(image.FileName);
                string fileExtension = fileName.Substring(image.FileName.LastIndexOf("."));
                string new_fileName = "image_" + itemId + fileExtension;
                string filePath = Path.Combine(path, new_fileName);

                image.SaveAs(filePath);
                deliveryOrder.Image = new_fileName;
                modelEntities.SaveChanges();
            }
        }
        private void UpdateOrderDeliveryStatus(DeliveryOrder deliveryOrder)
        {
            if (deliveryOrder.Order.hasDeliveries)
            {
                if (!deliveryOrder.Order.HasPendingDeliveries)
                    deliveryOrder.Order.DeliveryStatusId = (int)DeliveryStatusEnum.Entregado;
                else
                    deliveryOrder.Order.DeliveryStatusId = (int)DeliveryStatusEnum.EntregaParcial;
            }
            else
            {
                deliveryOrder.Order.DeliveryStatusId = (int)DeliveryStatusEnum.Pendiente;
            }

            modelEntities.SaveChanges();
        }

        [HttpGet]
        public virtual string SendGuideMail(int deliveryOrderId)
        {
            try
            {
                DeliveryOrder deliveryOrder = modelEntities.DeliveryOrders.Include("Order").FirstOrDefault(x => x.DeliveryOrderId == deliveryOrderId && x.Deleted == null);
                IMailing mailUtil = Mailing.Instance;
                mailUtil.SetTemplatesDirectory(Server.MapPath(Utility.MailTemplates));
                mailUtil.SendDeliveryOrderGuideMail(deliveryOrder);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }

        [HttpPost]
        public JsonResult DeleteDelivery(int id)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var deliveryDeleted = modelEntities.DeliveryOrders.SingleOrDefault(x => x.DeliveryOrderId == id && x.Deleted == null);

                    if (deliveryDeleted == null)
                    {
                        return Json(refreshPageMessage);
                    }

                    deliveryDeleted.Deleted = CurrentDate.Now;
                    deliveryDeleted.DeletedBy = User.Identity.Name;

                    StockService stockService = new StockService();
                    stockService.DeleteDeliveryMovements(deliveryDeleted.DeliveryOrderDetails, ref modelEntities);

                    modelEntities.SaveChanges();
                    UpdateOrderDeliveryStatus(deliveryDeleted);

                    modelEntities.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    return Json(deleteErrorMessage);
                }
            }

            return Json(deleteMessage);
        }

        #region Print

        [HttpGet]
        public virtual ActionResult DeliveryOrderPrint(int Id)
        {
            var deliveryOrder = modelEntities.DeliveryOrders.FirstOrDefault(x => x.DeliveryOrderId == Id && x.Deleted == null);
            return View(deliveryOrder);
        }

        [HttpGet]
        public virtual ActionResult DeliveryOrderPrintLayout(int Id)
        {
            var deliveryOrder = modelEntities.DeliveryOrders.FirstOrDefault(x => x.DeliveryOrderId == Id && x.Deleted == null);
            return View(deliveryOrder);
        }

        [HttpGet]
        [ChildActionOnly]
        public virtual PartialViewResult ShowPrintDeliveryOrderView(DeliveryOrder deliveryOrder)
        {
            deliveryOrder.DeliveryOrderDetails = deliveryOrder.DeliveryOrderDetails.OrderBy(x => x.Product.Title).ToList();
            return PartialView("_DeliveryOrderPrint", deliveryOrder);
        }

        #endregion


        #region Export to Excel



        [HttpGet]
        public ActionResult ExportDeliveryOrderToExcel(int Id)
        {
            var deliveryOrderDetails = modelEntities.DeliveryOrders
                .FirstOrDefault(deliveryOrder => deliveryOrder.DeliveryOrderId == Id)
                .DeliveryOrderDetails.Select(x => new
                {
                    Codigo = x.Product.Code,
                    Producto = x.Product.Title,
                    Talle = x.OrderDetail.Size.Description,
                    Color = x.OrderDetail.Color.Description,
                    Deposito = x.Deposit.Name,
                    Cantidad = x.Quantity.ToString(),
                    Monto = (x.OrderDetail.Price * x.Quantity).ToString()
                }).ToList();

            deliveryOrderDetails.Add(new
            {
                Codigo = "",
                Producto = "",
                Talle = "",
                Color = "",
                Deposito = "",
                Cantidad = "Total",
                Monto = (deliveryOrderDetails.Sum(x => decimal.Parse(x.Monto)).ToString())
            });

            var groupedList = deliveryOrderDetails.GroupBy(g => new { g.Codigo, g.Producto, g.Talle, g.Color, g.Deposito, g.Cantidad, g.Monto})
                .Select(n => new
                {
                    Codigo = n.Key.Codigo,
                    Producto = n.Key.Producto,
                    Talle = n.Key.Talle,
                    Color = n.Key.Color,
                    Deposito = n.Key.Deposito,
                    Cantidad = n.Key.Cantidad,
                    Monto = n.Key.Monto
                })
                .ToList();

            var grid = new System.Web.UI.WebControls.GridView();
            grid.DataSource = groupedList.ToList();
            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=BlancoParis-Entrega-" + Id + ".xls");
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }

        #endregion


        [HttpPost]
        public PartialViewResult DeliveryPartialRefresh(int OrderId)
        {
            return PartialView("_DeliveryOrderRefreshPartial", this.OrderDeliveryOrderToShow(OrderId));
        }

        [HttpPost]
        public PartialViewResult GetDeliveryDetail(int deliveryOrderId)
        {
            var DeliveryOrderDetails = modelEntities.DeliveryOrders
                .FirstOrDefault(deliveryOrder => deliveryOrder.DeliveryOrderId == deliveryOrderId)
                .DeliveryOrderDetails;

            return PartialView("_DeliveryDetailModal", DeliveryOrderDetails);
        }
        [HttpPost]
        public PartialViewResult DeliveryOrderGuideModal(int deliveryOrderId)
        {
            var deliveryOrder = modelEntities.DeliveryOrders.FirstOrDefault(x => x.DeliveryOrderId == deliveryOrderId && x.Deleted == null);
            
            return PartialView("_DeliveryOrderGuideModal", deliveryOrder);
        }

        [HttpGet]
        public ActionResult GetDeliverySkus(int deliveryOrderId)
        {
            DeliveryOrderSkusViewModel model = new DeliveryOrderSkusViewModel();

            model.DeliveryOrder = modelEntities.DeliveryOrders.FirstOrDefault(x => x.DeliveryOrderId == deliveryOrderId);
            model.DeliverySkus = modelEntities.ProductSkus.Where(x => x.DeliverOrderId == deliveryOrderId).ToList();
            
            return View("SkuList", model);
        }

        #region privates

        private Order OrderDeliveryOrderToShow(int OrderId)
        {
            var order = modelEntities.Orders.Include("OrderDetails").FirstOrDefault(x => x.OrderId == OrderId);
            order.OrderDetails = order.OrderDetails.Where(x => x.Active).ToList();

            order.DeliveryOrders = order.DeliveryOrders.Where(x => x.Deleted == null).ToList();

            return order;
        }

        private bool ValidDeliveryOrder(DeliveryOrder DeliveryOrder)
        {
            if (DeliveryOrder.DeliveryOrderDetails.Any(x => x.OrderDetail.PendingDeliveries < 0))
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Warning, "Una de las entregas es mayor a su pendiente");
                return false;
            }
            return true;
        }

        private void DeleteDeliveryOrderDetails(List<DeliveryOrderDetail> deliveryOrderDetails)
        {
            if (deliveryOrderDetails.Count > 0)
            {
                modelEntities.DeliveryOrderDetails.RemoveRange(deliveryOrderDetails);
                modelEntities.SaveChanges();
            }
        }

        private void GenerateStockDraftTempData(int[] OrderDetailIdHidden, bool[] enabledStock, int[] stock, DeliveryOrder DeliveryOrder)
        {
            var count = 0;
            var countStock = 0;
            var StockDraft = new Dictionary<int, int>();

            foreach (int OrderDetailId in OrderDetailIdHidden)
            {
                if (enabledStock[count])
                {
                    StockDraft.Add(OrderDetailId, stock[countStock]);
                    countStock++;
                }
                else
                {
                    StockDraft.Add(OrderDetailId, 0);
                }

                count++;
            }

            TempData["StockDraft"] = StockDraft;
            TempData["Date"] = DeliveryOrder.Date.ToShortDateString();
            TempData["ReferNumber"] = DeliveryOrder.ReferNumber;
            TempData["Comment"] = DeliveryOrder.Comment;
        }

        /*private void GenerateDeliverySkus(DeliveryOrder deliveryOrder, DbModelEntities modelEntities)
        {
            SKUManager manager = new SKUManager();
            foreach (DeliveryOrderDetail item in deliveryOrder.DeliveryOrderDetails)
            {
                manager.GenerateDeliveryProductSKUs(deliveryOrder.OrderId,item.ProductId, deliveryOrder.DeliveryOrderId, item.Quantity, ref modelEntities);
            }
        }*/

        #endregion privates

    }
}