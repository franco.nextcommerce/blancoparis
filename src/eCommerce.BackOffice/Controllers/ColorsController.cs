﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class ColorsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        private const string ColorImagesFileUploadDirectory = "/Content/UploadDirectory/Colors";

        public ActionResult Index()
        {
            return View("ColorList", this.ColorsToList());
        }

        private List<Color> ColorsToList()
        {
            return modelEntities.Colors.Where(x => x.Deleted == null).OrderBy(x => x.Description).ToList();
        }

        [HttpGet]
        public ActionResult ColorUpdate(int Id)
        {
            if (Id != -1)
            {
                var color = modelEntities.Colors.SingleOrDefault(x => x.ColorId == Id && x.Deleted == null);
                return View(color);
            }
            return View(new Color { ColorId = Id });
        }

        [HttpPost]
        public ActionResult ColorUpdate(FormCollection form, HttpPostedFileBase[] image)
        {
            Color color = new Color();

            var colorImage = image != null ? image.Where(x => x != null) : null;

            if (form["ColorId"] != string.Empty && Convert.ToInt16(form["ColorId"]) > 0)
            {
                int colorId = Convert.ToInt16(form["ColorId"]);
                color = modelEntities.Colors.SingleOrDefault(x => x.ColorId == colorId && x.Deleted == null);
            }

            TryUpdateModel(color, form);

            if (color.ColorId <= 0)
            {
                modelEntities.Colors.Add(color);
            }

            modelEntities.SaveChanges();

            #region Images

            try
            {
                string colorId = color.ColorId.ToString();
                var path = Path.Combine(Server.MapPath(ColorImagesFileUploadDirectory), colorId);

                if (colorImage != null)
                {
                    foreach (HttpPostedFileBase imageToSave in colorImage)
                    {
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);
                       
                        string fileName = Path.GetFileName(imageToSave.FileName);
                        string fileExtension = fileName.Substring(imageToSave.FileName.LastIndexOf("."));
                        string new_fileName = "color_" + colorId + fileExtension;
                        //CurrentDate.Now.ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '_')
                        //+ CurrentDate.Now.Millisecond.ToString() + fileExtension;
                        string filePath = Path.Combine(path, new_fileName);
                        
                        imageToSave.SaveAs(filePath);

                        color.ColorThumb = new_fileName;

                        // se utiliza el sleep para asegurarse que el nombre de la imagen siempre vaya a ser unica
                        Thread.Sleep(1);
                    }
                }
                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error grabar las imágenes del color.");
                return RedirectToAction("Index");
            }

            #endregion Images

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult ColorDelete(int Id)
        {
            var color = modelEntities.Colors.SingleOrDefault(x => x.ColorId== Id && x.Deleted == null);
            if (color != null)
            {
                color.Deleted = CurrentDate.Now;
                color.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            return PartialView("_ColorList", this.ColorsToList());
        }

        [HttpGet]
        public virtual ActionResult GetColors()
        {
            var colors = modelEntities.Colors.Select(x => new { x.ColorId, x.Description }).ToList();
            return Json(colors, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ModalCreateColor(string Descripcion)
        {
            try
            {
                var color = new Color();

                color.Description = Descripcion;
                modelEntities.Colors.Add(color);
                modelEntities.SaveChanges();

                return Json(new { tipoMensaje = "success", resultado = "Color creado exitosamente.", ColorId = color.ColorId, Description = color.Description });
            }
            catch (Exception ex)
            {
                return Json(new { tipoMensaje = "error", resultado = "Se ha generado un error al intentar crear el color." });
            }
        }
    }
}