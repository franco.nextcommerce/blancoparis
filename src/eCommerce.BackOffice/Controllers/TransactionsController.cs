﻿using eCommerce.BackOffice.ViewModels;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using eCommerce.Services.Services.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class TransactionsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index(int AccountId = -1)
        {
            var account = modelEntities.Accountings.FirstOrDefault(x => x.AccountId == AccountId);

            if (account != null)
                ViewData["AccountName"] = account.Name;
            else
                ViewData["AccountName"] = "Todos";

            ViewData["AccountId"] = AccountId.ToString();

            return View("TransactionList", this.TransactionsToShow(AccountId));
        }

        private List<Transaction> TransactionsToShow(int accountId)
        {
            List<Transaction> transactions = modelEntities.Transactions.Where(x => (x.AccountId == accountId || accountId == -1) && x.Deleted == null)
                .OrderByDescending(x => x.Date).ToList();

            return transactions;
        }

        [HttpGet]
        public ActionResult TransactionUpdate(int Id, int AccountId = -1)
        {
            var accounts = modelEntities.Accountings.Where(x => x.Deleted == null).OrderBy(x => x.Name);
            var concepts = modelEntities.TransactionCategories.Where(x => x.Deleted == null).OrderBy(x => x.Name);

            CreateOrderPaymentViewModel viewModel = new CreateOrderPaymentViewModel();
            var banks = modelEntities.Banks.Where(x => x.Deleted == null).ToList();
            viewModel.Banks = new SelectList(banks, "BankId", "Name");

            if (Id != -1)
            {
                var operation = modelEntities.Transactions.SingleOrDefault(x => x.TransactionId == Id);
                viewModel.Payment = operation;

                TempData["Accounts"] = new SelectList(accounts,"AccountId","Name", operation.AccountId);
                TempData["Concepts"] = new SelectList(concepts, "TransactionCategoryId", "Name", operation.TransactionCategoryId);

                return View(viewModel);
            }

            if(AccountId != -1)
                TempData["Accounts"] = new SelectList(accounts, "AccountId", "Name", AccountId);
            else
                TempData["Accounts"] = new SelectList(accounts, "AccountId", "Name");

            TempData["Concepts"] = new SelectList(concepts, "TransactionCategoryId", "Name");


            Transaction transaction = new Transaction();
            transaction.AccountId = AccountId;
            viewModel.Payment = transaction;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult TransactionUpdate(CreateOrderPaymentViewModel viewModel)

        {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        Transaction transaction = new Transaction();
                        int originAccountId = -1;

                        if (viewModel.Payment.TransactionId > 0)
                        {
                            transaction = modelEntities.Transactions.SingleOrDefault(x => x.TransactionId == viewModel.Payment.TransactionId && x.Deleted == null);
                            originAccountId = transaction.AccountId.Value;
                        }


                        var transactionCategoryId = viewModel.Payment.TransactionCategoryId;
                        var transactionCategory = modelEntities.TransactionCategories.SingleOrDefault(x => x.TransactionCategoryId == transactionCategoryId);

                        transaction.TransactionCategoryId = transactionCategoryId;
                        transaction.TransactionTypeId = transactionCategory.TransactionTypeId;
                        transaction.Amount = decimal.Parse(Request["Payment.Amount"].Replace(".", ","));
                        transaction.CurrencyId = viewModel.Payment.CurrencyId;
                        transaction.Date = viewModel.Payment.Date;
                        transaction.AccountId = viewModel.Payment.AccountId.Value;
                        transaction.Observations = viewModel.Payment.Observations;

                        //TryUpdateModel(transaction, form);

                        if (transaction.TransactionId <= 0)
                        {
                            transaction.Created = CurrentDate.Now;
                            transaction.CreatedBy = User.Identity.Name;
                            modelEntities.Transactions.Add(transaction);

                            modelEntities.SaveChanges();
                        } else
                        {
                            //remover relaciones del transaction con los cheques.
                            transaction.Checks.Clear();
                        }

                        modelEntities.SaveChanges();

                        AddChecks(viewModel, transaction);

                        modelEntities.SaveChanges();

                        UpdateAccountBalance(transaction.AccountId.Value);

                        //if the account change, update balance of the origin account.
                        if (originAccountId != -1 && transaction.AccountId != originAccountId)
                            UpdateAccountBalance(originAccountId);
                        
                        dbContextTransaction.Commit();

                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al realizar la transferencia.");
            }

            return RedirectToAction("Index", new { AccountId = Request["AccountId"] });
        }

        private void AddChecks(CreateOrderPaymentViewModel viewModel, Transaction transaction)
        {
            if (viewModel.Checks == null && string.IsNullOrEmpty(viewModel.ChecksIds))
                return;

            if (viewModel.Checks != null)
            {
                foreach (var check in viewModel.Checks)
                {
                    //borra el cheque
                    if (!check.Active)
                    {
                        var checkToDelete = modelEntities.Checks.FirstOrDefault(x => x.CheckId == check.CheckId);
                        checkToDelete.Deleted = CurrentDate.Now;
                        checkToDelete.DeletedBy = User.Identity.Name;
                        modelEntities.Entry(checkToDelete).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        //vuelve a generar la relacion entre el cheque y la transaction.
                        var existingCheck = modelEntities.Checks.FirstOrDefault(x => x.CheckId == check.CheckId);
                        if(existingCheck != null)
                            existingCheck.TransactionId = transaction.TransactionId;
                    }

                    //Es un cheque nuevo creado por la empresa.
                    if (check.CheckId == -1)
                    {
                        check.TransactionId = transaction.TransactionId;
                        check.Created = CurrentDate.Now;
                        check.CreatedBy = User.Identity.Name;
                        check.Status = transaction.TransactionCategory.TransactionTypeId == (int)TransactionTypeEnum.Ingreso ? (int)CheckStatus.EnCartera : (int)CheckStatus.Entregado;
                        modelEntities.Checks.Add(check);
                    }
                }
            }

            if (!string.IsNullOrEmpty(viewModel.ChecksIds))
            {
                var ids = viewModel.ChecksIds.Split(',');
                var checksIds = Array.ConvertAll(ids, int.Parse);
                var checks = modelEntities.Checks.Where(x => checksIds.Contains(x.CheckId) && x.Deleted == null).ToList();

                int checkStatus = transaction.TransactionCategory.TransactionTypeId == (int)TransactionTypeEnum.Ingreso ? 
                    (int)CheckStatus.EnCartera : (int)CheckStatus.Entregado;

                foreach (var check in checks)
                {
                    check.TransactionId = transaction.TransactionId;
                    check.Status = checkStatus;
                }
            }

            modelEntities.SaveChanges();
        }

        [HttpGet]
        public virtual PartialViewResult TransactionDelete(int Id)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var transaction = modelEntities.Transactions.SingleOrDefault(x => x.TransactionId == Id && x.Deleted == null);

                    transaction.Deleted = CurrentDate.Now;
                    transaction.DeletedBy = User.Identity.Name;
                    modelEntities.SaveChanges();

                    UpdateAccountBalance(transaction.AccountId.Value);

                    dbContextTransaction.Commit();

                    return PartialView("_TransactionList", this.TransactionsToShow(transaction.AccountId.Value));
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        [HttpGet]
        public virtual PartialViewResult CheckListPartial()
        {
            var checks = modelEntities.Checks.Where(x => x.Status == (int)CheckStatus.EnCartera && x.Deleted == null ).ToList();
            return PartialView("~/Views/Checks/_CheckList.cshtml", checks);
        }
        /************************* ACCOUNTS MOVEMENTS *************************/

        [HttpGet]
        public ActionResult TransferenceUpdate(int Id)
        {
            var accounts = modelEntities.Accountings.Where(x => x.Active).OrderBy(x => x.Name);
            var concepts = modelEntities.TransactionCategories.Where(x => x.Deleted == null).OrderBy(x => x.Name);

            if (Id != -1)
            {
                var operation = modelEntities.Transactions.SingleOrDefault(x => x.TransactionId == Id);

                TempData["OriginAccounts"] = new SelectList(accounts, "AccountId", "Name", operation.AccountId);
                TempData["DestinationAccounts"] = new SelectList(accounts, "AccountId", "Name", operation.AccountId);

                TempData["Concepts"] = new SelectList(concepts, "TransactionCategoryId", "Name", operation.TransactionCategoryId);

                return View(operation);
            }

            TempData["OriginAccounts"] = new SelectList(accounts, "AccountId", "Name");
            TempData["DestinationAccounts"] = new SelectList(accounts, "AccountId", "Name");
            TempData["Concepts"] = new SelectList(concepts, "TransactionCategoryId", "Name");

            return View(new Transaction { AccountId = Id });
        }

        [HttpPost]
        public ActionResult TransferenceUpdate(FormCollection form)
        {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        int originAccountId = int.Parse(form["OriginAccountId"]);
                        int destinationAccountId = int.Parse(form["DestinationAccountId"]);

                        var originAccount = modelEntities.Accountings.FirstOrDefault(x => x.AccountId == originAccountId);
                        var destinationAccount = modelEntities.Accountings.FirstOrDefault(x => x.AccountId == destinationAccountId);

                        //normalize amount value.
                        decimal amount = decimal.Parse(Request["Amount"]);
                        if (amount < 0)
                            amount = amount * -1;

                        //Create origin transaction
                        Transaction origin = new Transaction();
                        origin.AccountId = originAccountId;
                        origin.Amount = amount;
                        origin.Observations = "Orígen: " + originAccount.Name + " - Destino: " + destinationAccount.Name;
                        origin.TransactionTypeId = (int)TransactionTypeEnum.Egreso;
                        origin.TransactionCategoryId = int.Parse(form["TransactionCategoryId"]);
                        origin.CurrencyId = int.Parse(form["CurrencyId"]);
                        origin.Date = DateTime.Parse(form["Date"]);
                        origin.Created = CurrentDate.Now;
                        origin.CreatedBy = User.Identity.Name;
                        modelEntities.Transactions.Add(origin);

                        //Create destination transaction
                        Transaction destination = new Transaction();
                        destination.AccountId = destinationAccountId;
                        destination.Amount = amount;
                        destination.Observations = "Orígen: " + originAccount.Name + " - Destino: " + destinationAccount.Name;
                        destination.TransactionTypeId = (int)TransactionTypeEnum.Ingreso;
                        destination.TransactionCategoryId = int.Parse(form["TransactionCategoryId"]);
                        destination.CurrencyId = int.Parse(form["CurrencyId"]);
                        destination.Date = DateTime.Parse(form["Date"]);
                        destination.Created = CurrentDate.Now;
                        destination.CreatedBy = User.Identity.Name;
                        modelEntities.Transactions.Add(destination);

                        modelEntities.SaveChanges();

                        UpdateAccountBalance(originAccountId);
                        UpdateAccountBalance(destinationAccountId);
                        
                        dbContextTransaction.Commit();

                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al realizar la transferencia.");
            }

            return RedirectToAction("Index","Accounting");
        }

        private void UpdateAccountBalance(int accountId)
        {
            AccountingService.UpdateAccountBalance(accountId, ref modelEntities);
        }
        
    }
}
 
 