﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    //[AuthorizeUser(Roles = "Admin")]
    public class SuppliersController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            List<Supplier> suppliers = modelEntities.Suppliers.Where(x => x.Active == true).ToList();
            return View("SupplierList", suppliers);
        }

        [HttpGet]
        public ActionResult SupplierUpdate(int Id)
        {
            if (Id != -1)
            {
                var Supplier = modelEntities.Suppliers.SingleOrDefault(x => x.SupplierId == Id);
                return View(Supplier);
            }

            return View(new Supplier { SupplierId = Id });
        }

        [HttpPost]
        public ActionResult SupplierUpdate(FormCollection form)
        {
            Supplier supplier = new Supplier();

            if (form["SupplierId"] != string.Empty && Convert.ToInt16(form["SupplierId"]) > 0)
            {
                int supplierId = Convert.ToInt16(form["SupplierId"]);
                supplier = modelEntities.Suppliers.SingleOrDefault(x => x.SupplierId == supplierId && x.Active);
            }

            TryUpdateModel(supplier, form);

            if (supplier.SupplierId <= 0)
            {
                supplier.Active = true;
                supplier.Created = CurrentDate.Now;
                supplier.CreatedBy = User.Identity.Name;
                modelEntities.Suppliers.Add(supplier);
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult SupplierDelete(int Id)
        {
            var Supplier = modelEntities.Suppliers.SingleOrDefault(x => x.SupplierId == Id && x.Active);
            if (Supplier != null)
            {
                Supplier.Active = false;
                Supplier.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.Suppliers.Where(x => x.Active).ToList();
            return PartialView("_SupplierList", list);
        }
    }
}