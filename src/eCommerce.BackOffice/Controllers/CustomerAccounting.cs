﻿using eCommerce.BackOffice.ViewModels;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using eCommerce.Services.Services.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class CustomerAccountingController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index(int Id)
        {
            CustomerAccountingViewModel model = new CustomerAccountingViewModel();

            model.Customer = modelEntities.Customers.FirstOrDefault(x => x.CustomerId == Id);
            model.Transactions = this.CustomerTransactionsToShow(Id);

            return View("CustomerAccountingList", model);
        }

        private List<Transaction> CustomerTransactionsToShow(int Id)
        {
            return modelEntities.Transactions
                .Where(x => (x.CustomerId == Id || x.Order.CustomerId == Id) && x.Deleted == null).OrderByDescending(x => x.Created).ToList();
        }

        [HttpGet]
        public ActionResult TransactionUpdate(int Id, int CustomerId)
        {
            var accounts = modelEntities.Accountings.Where(x => x.Deleted == null).OrderBy(x => x.Name); 
            var concepts = modelEntities.TransactionCategories.Where(x => x.Deleted == null).OrderBy(x => x.Name);
            var customer = modelEntities.Customers.Where(x => x.CustomerId == CustomerId && x.Active).ToList();

            TempData["Customer"] = new SelectList(customer, "CustomerId", "FullName", CustomerId);

            if (Id != -1)
            {
                var operation = modelEntities.Transactions.SingleOrDefault(x => x.TransactionId == Id);

                if (operation.AccountId.HasValue)
                    TempData["Accounts"] = new SelectList(accounts, "AccountId", "Name", operation.AccountId.Value);

                TempData["Concepts"] = new SelectList(concepts, "TransactionCategoryId", "Name", operation.TransactionCategoryId);
                TempData["Accounts"] = new SelectList(accounts, "AccountId", "Name");

                return View(operation);
            }

            TempData["Accounts"] = new SelectList(accounts, "AccountId", "Name");
            TempData["Concepts"] = new SelectList(concepts, "TransactionCategoryId", "Name");

            return View(new Transaction { CustomerId = CustomerId });
        }

        [HttpPost]
        public ActionResult TransactionUpdate(FormCollection form)
        {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        Transaction transaction = new Transaction();
                        transaction.CurrencyId = (int)CurrencyEnum.Peso;

                        if (form["TransactionId"] != string.Empty && Convert.ToInt16(form["TransactionId"]) > 0)
                        {
                            int transactionId = Convert.ToInt16(form["TransactionId"]);
                            transaction = modelEntities.Transactions.SingleOrDefault(x => x.TransactionId == transactionId && x.Deleted == null);
                        }

                        var transactionCategoryId = int.Parse(form["TransactionCategoryId"]);
                        var transactionCategory = modelEntities.TransactionCategories.SingleOrDefault(x => x.TransactionCategoryId == transactionCategoryId);
                        transaction.TransactionTypeId = transactionCategory.TransactionTypeId;

                        TryUpdateModel(transaction, form);

                        if (transaction.AccountId == -1)
                            transaction.AccountId = null;

                        transaction.Amount = decimal.Parse(form["Amount"]);

                        if (transactionCategory.TransactionTypeId == (int)TransactionTypeEnum.Egreso && transaction.Amount > 0)
                            transaction.Amount = transaction.Amount * -1;

                        if (transaction.TransactionId <= 0)
                        {
                            transaction.Created = CurrentDate.Now;
                            transaction.CreatedBy = User.Identity.Name;
                            modelEntities.Transactions.Add(transaction);
                        }

                        modelEntities.SaveChanges();
                        
                        UpdateCustomerAccountBalance(transaction.CustomerId.Value);

                        if (transaction.AccountId.HasValue)
                            UpdateAccountBalance(transaction.AccountId.Value);

                        dbContextTransaction.Commit();

                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al realizar la transferencia.");
            }

            return RedirectToAction("Index", new { Id = Request["CustomerId"] });
        }

        [HttpGet]
        public virtual PartialViewResult TransactionDelete(int Id)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var transaction = modelEntities.Transactions.SingleOrDefault(x => x.TransactionId == Id && x.Deleted == null);

                    transaction.Deleted = CurrentDate.Now;
                    transaction.DeletedBy = User.Identity.Name;
                    modelEntities.SaveChanges();

                    var customerId = transaction.CustomerId.HasValue ? transaction.CustomerId.Value : transaction.Order.CustomerId;
                    UpdateCustomerAccountBalance(customerId);

                    if(transaction.AccountId.HasValue)
                        UpdateAccountBalance(transaction.AccountId.Value);

                    if(transaction.TransactionDetails.Any())
                        UpdateOrdersPaymentStatus(transaction.TransactionDetails.ToList());

                    dbContextTransaction.Commit();

                    return PartialView("_CustomerAccountingList", this.CustomerTransactionsToShow(transaction.CustomerId.Value));
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        private void UpdateCustomerAccountBalance(int customerId)
        {
            AccountingService.UpdateCustomerBalance(customerId, ref modelEntities);
        }
        private void UpdateAccountBalance(int accountId)
        {
            AccountingService.UpdateAccountBalance(accountId, ref modelEntities);
        }

        private void UpdateOrdersPaymentStatus(List<TransactionDetail> transactionDetails)
        {
            //Obtengo los Ids de las ventas a actualizarle el estado de cobro.
            var OrdersId = transactionDetails.Select(x => x.OrderId).ToArray();
            var ordersId = modelEntities.Orders.Where(x => OrdersId.Contains(x.OrderId)).Select(x => x.OrderId).Distinct().ToList();

            foreach (var detail in transactionDetails)
            {
                UpdateOrderPaymentStatusValue(detail.OrderId);
            }
            foreach (var orderId in ordersId)
            {
                UpdateOrderPaymentStatusValue(orderId);
            }

        }
        private void UpdateOrderPaymentStatusValue(int orderId)
        {
            //Order PaymentStatus Update
            var Order = modelEntities.Orders.Find(orderId);
            var paidAmount = Order.TransactionDetails.Where(x => x.Deleted == null && x.Transaction.Deleted == null).Select(x => x.Amount).DefaultIfEmpty(0).Sum();
            if (Order.OrderTotal >= paidAmount)
            {
                Order.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;
            }
            else if (paidAmount > 0 && paidAmount < Order.OrderTotal)
            {
                Order.PaymentStatusId = (int)PaymentStatusEnum.CobroParcial;
            }

            modelEntities.SaveChanges();
        }
        private List<TransactionDetail> GetTransactionDetails(CreateOrderPaymentViewModel viewModel, FormCollection form)
        {
            List<TransactionDetail> items = new List<TransactionDetail>();

            bool create_new_item = false;
            TransactionDetail item = new TransactionDetail();
            foreach (string key in Request.Form.AllKeys)
            {
                if (key.Contains("_"))
                {
                    if (create_new_item)
                    {
                        item = new TransactionDetail();
                        create_new_item = false;
                    }
                    if (key.Contains("OrderAmountToPay"))
                    {
                        if (Request.Form[key] != null && Request.Form[key] != string.Empty)
                        {
                            var OrderId = int.Parse(key.Split('_')[1]);
                            var Order = modelEntities.Orders.Find(OrderId);

                            item.Amount = Convert.ToDecimal(Request.Form[key]);
                            item.Date = viewModel.Payment.Date;
                            item.TransactionId = viewModel.Payment.TransactionId;
                            item.OrderId = OrderId;

                            items.Add(item);
                            create_new_item = true;
                        }
                    }
                }
            }

            return items;
        }
    }
}