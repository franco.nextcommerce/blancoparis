﻿using eCommerce.Backoffice.Utils;
using eCommerce.Backoffice.Utils.Interfaces;
using eCommerce.BackOffice.ViewModels;
using eCommerce.Services;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Model.Exceptions;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using eCommerce.Services.Services.Accounting;

namespace eCommerce.BackOffice.Controllers
{
    public class OrdersController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult IndexOld()
        {
            RBACUser requestingUser = new RBACUser(HttpContext.User.Identity.Name);
            if (requestingUser.HasRole("Mayor"))
            {
                return RedirectToAction("Wholesaler");
            }
            return View("OrderList", this.OrdersToShow());
        }

        public ActionResult Index()
        {
            var from = DateTime.Today.AddMonths(-2);
            OrdersListPageViewModel model = new OrdersListPageViewModel()
            {
                FromDate = new DateTime(from.Year, from.Month, 1).ToShortDateString(),
                ToDate = DateTime.Today.ToShortDateString()
            };

            return View("OrderList2", model);
        }

        [HttpGet]
        public JsonResult GetOrders(string from, string to)
        {
            var result = Json(new { data = this.OrdersToShow(from, to) }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        //public ActionResult Wholesaler()
        //{
        //    return View("OrderList", this.OrdersToShow((int)UserTypeEnum.Mayorista));
        //}

        public ActionResult InCart()
        {
            var from_date = CurrentDate.Now.AddMonths(-2);
            OrderReportsViewModel model = new OrderReportsViewModel()
            {
                FromDate = new DateTime(from_date.Year, from_date.Month, CurrentDate.Now.Day).ToShortDateString(),
                ToDate = DateTime.Today.ToShortDateString()
            };
            return View(model);
        }

        public PartialViewResult GetOrdersInCart(string from, string to)
        {
            DateTime fromDate = string.IsNullOrWhiteSpace(from) ? new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1) : DateTime.Parse(from);
            DateTime toDate = string.IsNullOrWhiteSpace(to) ? DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59) : DateTime.Parse(to).AddHours(23).AddMinutes(59).AddSeconds(59);

            InCartReportViewModel model = new InCartReportViewModel();
            //Creo un listado de ventas en carrito
            List<OrderInCartViewModel> OrderList = new List<OrderInCartViewModel>();

            try
            {
                //Traigo los orderdetails que no estan borrados y que fueron agregados por algun cliente
                var orderDetailsWithCustomerAndNoOrder = modelEntities.OrderDetails.Where(x => x.Active && x.Deleted == null && x.CreatedBy != null && x.OrderId == null && x.GUID != null && x.Quantity > 0/*&& x.Created >= fromDate && x.Created <= toDate*/);
                //Agrupo por createdby(usuario)
                var groupedOrderDetails = orderDetailsWithCustomerAndNoOrder.ToList().GroupBy(x => x.CreatedBy);

                foreach (var orderdetailgroup in groupedOrderDetails)
                {
                    var customercreatedby = orderdetailgroup.FirstOrDefault().CreatedBy;
                    var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == customercreatedby);
                    if (customer != null)
                    {
                        var customerUserType = customer.UserType;
                        foreach (var od in orderdetailgroup)
                        {
                            if (customerUserType == (int)UserTypeEnum.Mayorista)
                            {
                                od.Price = od.Product.WholesalePrice.HasValue ? od.Product.WholesalePrice.Value : 0;
                            }
                            else
                            {
                                od.Price = od.Product.RetailPrice.HasValue ? od.Product.RetailPrice.Value : 0;
                            }
                        }
                        modelEntities.SaveChanges();
                    }

                }

                //Traigo todos los clientes para no tener que buscar en la base por cada listado de OD
                var customers = modelEntities.Customers.Where(x => x.Active && x.Deleted == null).ToList();
                foreach (var orderDetailGroup in groupedOrderDetails)
                {
                    var createdBy = orderDetailGroup.FirstOrDefault().CreatedBy;

                    //Me fijo si el ultimo item agregado esta entre las fechas que yo quiero
                    var customer = customers.FirstOrDefault(c => c.Email == createdBy);
                    if (customer == null)
                        continue;

                    var lastod = orderDetailGroup.OrderByDescending(x => x.Created).FirstOrDefault();
                    if (lastod.Created >= fromDate && lastod.Created <= toDate)
                    {
                        //Creo una "orden" con los datos del cliente
                        OrderInCartViewModel order = new OrderInCartViewModel();

                        order.CustomerEmail = customer.Email != null ? customer.Email : string.Empty;
                        order.CustomerName = customer.FullName != null ? customer.FullName : string.Empty;
                        order.CustomerPhone = customer.PhoneNumber != null ? customer.PhoneNumber : string.Empty;
                        //order.CustomerUserType = customer.UserType == (int)UserTypeEnum.Mayorista ? "Mayorista" : "Minorista";
                        order.CustomerUserType = ((UserTypeEnum)lastod.StoreType).ToString();
                        order.CustomerId = customer.CustomerId;

                        DateTime lastDate = fromDate;

                        int quantity = 0;
                        decimal totalamount = 0;
                        foreach (var od in orderDetailGroup)
                        {
                            quantity += od.Quantity;
                            totalamount += (od.SubTotal);
                            if (od.Created > lastDate)
                                lastDate = od.Created;
                        }

                        order.TotalQuantity = quantity;
                        order.TotalAmount = totalamount;
                        order.LastItemAddedDate = lastDate;
                        order.LastPushDate = customer.CartPushDate.HasValue && customer.CartPushDate.Value != default(DateTime) ? customer.CartPushDate.Value : default(DateTime);
                        OrderList.Add(order);
                    }
                }

                model.Orders = OrderList.OrderByDescending(o => o.LastItemAddedDate).ToList();

                model.TotalQuantity = OrderList.Sum(x => x.TotalQuantity);
                model.TotalAmount = OrderList.Sum(x => x.TotalAmount);
            }
            catch (Exception ex)
            {
                throw;
            }

            return PartialView("_OrdersInCart", model);
        }

        [HttpGet]
        public ActionResult ViewInCartDetail(int CustomerId)
        {

            var customer = modelEntities.Customers.Find(CustomerId);

            CartDetailViewModel model = new CartDetailViewModel();

            model.CustomerName = customer.FullName;
            //Traigo los orderdetails que no estan borrados y que fueron agregados por el actual cliente
            var CustomerOrderDetails = modelEntities.OrderDetails.Where(x => x.Active && x.Deleted == null && x.CreatedBy == customer.Email && x.OrderId == null && x.GUID != null).ToList();



            model.OrderDetails = CustomerOrderDetails;
            return View(model);
        }

        [HttpGet]
        public virtual string GetCustomerPushDate(int CustomerId)
        {
            var customer = modelEntities.Customers.Find(CustomerId);

            return customer.CartPushDate.Value.ToShortDateString();
        }

        [HttpPost]
        public virtual string SendMassivePushEmail(int[] CustomerIds)
        {
            try
            {
                for (int i = 0; i < CustomerIds.Length; i++)
                {

                    int id = CustomerIds[i];
                    var customer = modelEntities.Customers.FirstOrDefault(x => x.CustomerId == id && x.Active && x.Deleted == null);

                    if (customer != null)
                    {
                        var CustomerOrderDetails = modelEntities.OrderDetails.Where(x => x.Active && x.Deleted == null && x.CreatedBy == customer.Email && x.OrderId == null && x.GUID != null).ToList();

                        foreach (var orderdetail in CustomerOrderDetails)
                        {
                            orderdetail.Price = orderdetail.Product.CustomerPrice;
                            modelEntities.SaveChanges();
                        }
                        /*Proform proform = new Proform();
                        proform.OrderId = order.OrderId;
                        proform.Guid = guid.ToString();
                        modelEntities.Proforms.Add(proform);
                        modelEntities.SaveChanges();*/

                        customer.CartPushDate = CurrentDate.Now;
                        modelEntities.SaveChanges();

                        IMailing mailUtil = Mailing.Instance;
                        mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                        mailUtil.SendOrderPushMail(CustomerOrderDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }

        [HttpGet]
        public virtual string SendPushEmail(string CustomerEmail)
        {
            var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == CustomerEmail);

            try
            {

                if (customer != null)
                {
                    var CustomerOrderDetails = modelEntities.OrderDetails.Where(x => x.Active && x.Deleted == null && x.CreatedBy == CustomerEmail && x.OrderId == null && x.GUID != null).ToList();
                    /*Proform proform = new Proform();
                    proform.OrderId = order.OrderId;
                    proform.Guid = guid.ToString();
                    modelEntities.Proforms.Add(proform);
                    modelEntities.SaveChanges();*/
                    foreach (var orderdetail in CustomerOrderDetails)
                    {
                        orderdetail.Price = orderdetail.Product.CustomerPrice;
                        modelEntities.SaveChanges();
                    }

                    customer.CartPushDate = CurrentDate.Now;
                    modelEntities.SaveChanges();

                    IMailing mailUtil = Mailing.Instance;
                    mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                    mailUtil.SendOrderPushMail(CustomerOrderDetails);
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return customer.CustomerId.ToString();
        }

        public ActionResult DeliveryOrders()
        {
            return View("DeliveryOrderList", this.DeliveryOrdersToShow());
        }

        public ActionResult PendingDeliveryOrders()
        {
            return View("PendingDeliveryOrderList", this.OrdersToShow());
        }

        [HttpGet]
        public ActionResult OrderUpdate(int Id)
        {
            /*var customers = modelEntities.Customers.Select(x => new { x.CustomerId, x.FullName, x.Active }).Where(x => x.Active).OrderBy(x => x.FullName).ToList();*/
            var channels = GetChannelsSelectList();
            var paymentMethods = GetPaymentMethodsSelectList();
            var deliveryMehtods = GetDeliveryMethodsSelectList();
            var users = modelEntities.USERS.Where(x => x.Active && x.Deleted == null).ToList();

            var coupons = modelEntities.Coupons.Where(x => x.Active && x.ExpirationDate > CurrentDate.Now)
                .Select(x => new { CouponId = x.CouponId, Text = x.Code + " (" + x.Percentage + "%)" });

            var user = users.FirstOrDefault(x => x.Username == User.Identity.Name && x.Active && x.Deleted == null);

            if (Id != -1)
            {
                var order = modelEntities.Orders.Include("OrderDetails").SingleOrDefault(x => x.OrderId == Id);
                order.OrderDetails = order.OrderDetails.Where(x => x.Active).OrderBy(x => x.OrderDetailId).ToList();

                var productsId = order.OrderDetails.Select(x => x.ProductId).ToArray();
                TempData["Stocks"] = modelEntities.Stocks.Where(x => productsId.Contains(x.ProductId)).ToList();

                var products = modelEntities.Products.Select(x => new { ProductId = x.ProductId, Text = x.Code + " - " + x.Title }).ToList();
                TempData["Products"] = new SelectList(products, "ProductId", "Text");

                var sizes = modelEntities.Sizes.Select(x => new { SizeId = x.SizeId, Text = x.Description }).ToList();
                TempData["Sizes"] = new SelectList(sizes, "SizeId", "Text");

                var colors = modelEntities.Colors.Select(x => new { ColorId = x.ColorId, Text = x.Description }).ToList();
                TempData["Colors"] = new SelectList(colors, "ColorId", "Text");

                //TempData["Customers"] = new SelectList(customers, "CustomerId", "FullName", order.CustomerId);
                TempData["Channels"] = new SelectList(channels, "Value", "Text", order.Channel);
                TempData["PaymentMethods"] = new SelectList(paymentMethods, "Value", "Text", order.PaymentMethodId);
                TempData["DeliveryMethods"] = new SelectList(deliveryMehtods, "Value", "Text", order.DeliveryMethodId);
                TempData["Coupons"] = new SelectList(coupons, "CouponId", "Text", order.CouponId);
                TempData["USERS"] = new SelectList(users, "User_Id", "Username", order.SalerId);

                return View(order);
            }

            //TempData["Customers"] = new SelectList(customers, "CustomerId", "FullName");
            TempData["Channels"] = new SelectList(channels, "Value", "Text");
            TempData["PaymentMethods"] = new SelectList(paymentMethods, "Value", "Text");
            TempData["DeliveryMethods"] = new SelectList(deliveryMehtods, "Value", "Text");
            TempData["Coupons"] = new SelectList(coupons, "CouponId", "Text");

            return View(new Order { OrderId = Id, OrderStatusId = (int)OrderStatusEnum.Confirmado, UseMaxDiscount = true, SalerId = user.User_Id });
        }

        [HttpPost]
        public virtual JsonResult ValidarStock(FormCollection formCollection)
        {
            List<int> productIds = new List<int>();
            List<string> productCodes = new List<string>();
            List<int> stocks = new List<int>();
            List<int> productQuantities = new List<int>();
            List<string> productsNoStock = new List<string>();

            foreach (var key in formCollection)
            {
                if (key.ToString().StartsWith("ProductId_"))
                {
                    int productId = int.Parse(formCollection[key.ToString()]);
                    productIds.Add(productId);
                }
                if (key.ToString().StartsWith("Stock_"))
                {
                    int stock = int.Parse(formCollection[key.ToString()].Split('_')[0]);
                    string code = formCollection[key.ToString()].Split('_')[1];
                    stocks.Add(stock);
                    productCodes.Add(code);
                }
                if (key.ToString().StartsWith("Quantity_"))
                {
                    int quantity = int.Parse(formCollection[key.ToString()]);
                    productQuantities.Add(quantity);
                }
            }

            for (int i = 0; i < productIds.Count(); i++)
            {
                if (productQuantities[i] > stocks[i])
                {
                    productsNoStock.Add(productCodes[i]);
                }
            }

            return Json(productsNoStock);
        }

        [HttpGet]
        public virtual JsonResult ValidateDeleteOrderDetail(int orderDetailId)
        {
            var result = modelEntities.DeliveryOrderDetails.FirstOrDefault(x => x.OrderDetailId == orderDetailId && x.DeliveryOrder.Deleted == null);

            if (result != null)
                return Json(result.DeliveryOrderId.ToString(), JsonRequestBehavior.AllowGet);
            else
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetCoupon(int couponId)
        {
            var coupon = modelEntities.Coupons.FirstOrDefault(x => x.CouponId == couponId && x.Active);

            if (coupon != null)
                return Json(coupon.Percentage.ToString(), JsonRequestBehavior.AllowGet);
            else
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual PartialViewResult SearchByProduct(string productCode)
        {
            var productIds = modelEntities.Products.Where(x => x.Code.ToLower().Contains(productCode.ToLower()) && x.Deleted == null).Select(x => x.ProductId).ToList();
            if (productIds.Count == 0)
                return PartialView("_SearchedProductsListError", "No existe ningun producto con ese código");
            var user = modelEntities.USERS.FirstOrDefault(x => x.Username == User.Identity.Name && x.Active && x.Deleted == null);
            List<Order> orders = new List<Order>();
            if (user.ROLES.FirstOrDefault().RoleName != "SuperAdmin")
            {
                orders = modelEntities.OrderDetails.Where(x => productIds.Contains(x.ProductId) && x.Deleted == null && x.Active && x.Order.Active && x.Order.SalerId == user.User_Id).OrderByDescending(a => a.OrderId).Select(x => x.Order).ToList().Distinct().ToList();
            }
            else
            {
                orders = modelEntities.OrderDetails.Where(x => productIds.Contains(x.ProductId) && x.Deleted == null && x.Active && x.Order.Active).OrderByDescending(a => a.OrderId).Select(x => x.Order).ToList().Distinct().ToList();
            }

            if (orders.Count == 0)
                return PartialView("_SearchedProductsListError", "No hay ninguna orden con ese producto");

            return PartialView("_SearchedProductsList", orders);
        }

        [HttpGet]
        public virtual PartialViewResult SearchDeliveriesByProduct(string productCode)
        {
            var productIds = modelEntities.Products.Where(x => x.Code.ToLower().Contains(productCode.ToLower()) && x.Deleted == null).Select(x => x.ProductId).ToList();
            if (productIds.Count == 0)
                return PartialView("_SearchedProductsListError", "No existe ningun producto con ese código");

            var user = modelEntities.USERS.FirstOrDefault(x => x.Username == User.Identity.Name && x.Active && x.Deleted == null);
            List<Order> orders = new List<Order>();
            if (user.ROLES.FirstOrDefault().RoleName != "SuperAdmin")
            {
                orders = modelEntities.OrderDetails.Where(x => productIds.Contains(x.ProductId) && x.Deleted == null && x.Active && x.Order.Active && x.Order.SalerId == user.User_Id).OrderByDescending(a => a.OrderId).Select(x => x.Order).ToList().Distinct().ToList();
            }
            else
            {
                orders = modelEntities.OrderDetails.Where(x => productIds.Contains(x.ProductId) && x.Deleted == null && x.Active && x.Order.Active).OrderByDescending(a => a.OrderId).Select(x => x.Order).ToList().Distinct().ToList();
            }

            orders = orders
                .Where(x => x.OrderDetails.Any(p => productIds.Contains(p.ProductId)) &&
                    (!x.DeliveryOrders.Any(d =>
                        d.DeliveryOrderDetails.Any(o =>
                            productIds.Contains(o.ProductId)
                            )
                        )
                    ||
                    x.DeliveryOrders.Any(d =>
                        d.DeliveryOrderDetails.Any(o =>
                            productIds.Contains(o.ProductId) &&
                            o.Quantity < (x.OrderDetails.FirstOrDefault(y => y.ProductId == o.ProductId).Quantity)
                            )
                        )
                    )
                )
                .ToList();

            if (orders.Count == 0)
                return PartialView("_SearchedProductsListError", "No hay ninguna entrega pendiente de ese producto");

            return PartialView("_SearchedProductsList", orders);
        }

        [HttpPost]
        public ActionResult OrderUpdate(FormCollection form)
        {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        Order order = new Order();

                        if (form["OrderId"] != string.Empty && Convert.ToInt16(form["OrderId"]) > 0)
                        {
                            int OrderId = Convert.ToInt16(form["OrderId"]);
                            order = modelEntities.Orders.SingleOrDefault(x => x.OrderId == OrderId && x.Active);
                            order.OrderDetails = order.OrderDetails.Where(x => x.Active).ToList();
                        }

                        TryUpdateModel(order, form);

                        if (!string.IsNullOrWhiteSpace(Request["ShippingCost"]))
                            order.ShippingCost = decimal.Parse(Request["ShippingCost"].Replace(".", ","));

                        if (!string.IsNullOrEmpty(Request["Discount_Percent"]))
                            order.Discount_Percent = decimal.Parse(Request["Discount_Percent"].Replace(".", ","));

                        if (!string.IsNullOrEmpty(Request["Discount"]))
                            order.Discount = decimal.Parse(Request["Discount"].Replace(".", ","));

                        if (!string.IsNullOrEmpty(Request["IVA"]))
                            order.IVA = decimal.Parse(Request["IVA"].Replace(".", ","));

                        if (!string.IsNullOrEmpty(Request["IVA_Percent"]))
                            order.IVA_Percent = decimal.Parse(Request["IVA_Percent"].Replace(".", ","));

                        if (!string.IsNullOrEmpty(Request["Surcharge"]))
                            order.Surcharge = decimal.Parse(Request["Surcharge"].Replace(".", ","));

                        if (!string.IsNullOrEmpty(Request["SurchargeAmount"]))
                            order.SurchargeAmount = decimal.Parse(Request["SurchargeAmount"].Replace(".", ","));

                        order.Total = decimal.Parse(Request["Total"].Replace(".", ","));
                        order.CurrenyId = (int)CurrencyEnum.Peso;
                        order.UseMaxDiscount = false;

                        //Recargo MP
                        if (order.PaymentMethodId == (int)PaymentMethodEnum.MercadoPago)
                        {
                            order.Surcharge = Convert.ToDecimal(8);
                        }

                        if (order.OrderId <= 0)
                        {
                            order.Active = true;
                            order.Created = CurrentDate.Now;
                            order.CreatedBy = User.Identity.Name;
                            order.StoreType = int.Parse(Request["UserType"]);

                            order.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;
                            order.DeliveryStatusId = (int)DeliveryStatusEnum.Pendiente;

                            //agregado para parametro del pago-online con decidir desde link.
                            order.GUID = Guid.NewGuid();

                            modelEntities.Orders.Add(order);
                        }
                        else
                        {
                            order.Modified = CurrentDate.Now;
                            order.ModifiedBy = User.Identity.Name;

                            if (!order.GUID.HasValue)
                                order.GUID = Guid.NewGuid();

                            if (order.StoreType == 0)
                                order.StoreType = int.Parse(Request["UserType"]);
                        }

                        var listItems = GetOrderDetails(form, order.OrderId);
                        UpdateOrderDetails(order, listItems);

                        UpdateDeliveryStatus(order);

                        UpdateCustomerAccount(order);

                        UpdateCustomerAccountBalance(order.CustomerId);

                        modelEntities.SaveChanges();

                        UpdateProductsStock(order);

                        UpdatePaymentStatus(order);

                        dbContextTransaction.Commit();
                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (NoStockException ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, ex.Message);
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar guardar la venta.");
            }

            return RedirectToAction("Index");
        }

        private void UpdateProductsStock(Order order)
        {
            StockService stockService = new StockService(new VirtualStock());
            stockService.UpdateStockOrderDetails(order.OrderId, ref modelEntities);
        }

        private void UpdateDeliveryStatus(Order order)
        {
            if (order.hasDeliveries)
            {
                if (!order.HasPendingDeliveries)
                    order.DeliveryStatusId = (int)DeliveryStatusEnum.Entregado;
                else
                    order.DeliveryStatusId = (int)DeliveryStatusEnum.EntregaParcial;
            }
            else
            {
                order.DeliveryStatusId = (int)DeliveryStatusEnum.Pendiente;
            }
        }

        //private void UpdatePaymentStatus(Order order)
        //{
        //    if (order.hasPayments)
        //    {
        //        if (order.IsTotallyPaid)
        //            order.PaymentStatusId = (int)PaymentStatusEnum.Cobrada;
        //        else
        //            order.PaymentStatusId = (int)PaymentStatusEnum.CobroParcial;
        //    }
        //    else
        //    {
        //        order.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;
        //    }
        //    modelEntities.SaveChanges();
        //}

        private void UpdatePaymentStatus(Order order)
        {
            //DeliveryOrder PaymentStatus Update
            var paidAmount = order.TransactionDetails
                .Where(x => x.Deleted == null && x.Transaction.Deleted == null).Select(x => x.Amount).DefaultIfEmpty(0).Sum();
            if (order.OrderTotal <= paidAmount)
            {
                order.PaymentStatusId = (int)PaymentStatusEnum.Cobrada;
            }
            else
            {
                order.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;
            }

            modelEntities.SaveChanges();
        }

        private void UpdateOrderDetails(Order order, List<OrderDetail> details)
        {
            foreach (var detail in order.OrderDetails)
            {
                var item = details.SingleOrDefault(x => x.OrderDetailId == detail.OrderDetailId);

                if (item != null)
                {
                    detail.Modified = CurrentDate.Now;
                    detail.ModifiedBy = User.Identity.Name;
                    detail.Quantity = item.Quantity;
                    detail.Price = item.Price;
                    detail.Discount = item.Discount;
                    if (detail.SizeId != item.SizeId)
                    {
                        detail.SizeId = item.SizeId;
                    }
                    if (detail.ColorId != item.ColorId)
                    {
                        detail.ColorId = item.ColorId;
                    }
                }
                else
                {
                    detail.Active = false;
                    detail.Deleted = CurrentDate.Now;
                    detail.DeletedBy = User.Identity.Name;
                }
            }

            foreach (var item in details.Where(x => x.OrderDetailId == -1))
            {
                item.Order = order;
                item.Active = true;
                item.Created = CurrentDate.Now;
                item.CreatedBy = User.Identity.Name;
                modelEntities.OrderDetails.Add(item);
            }

        }

        //[HttpGet]
        //public virtual PartialViewResult OrderCancel(int Id)
        //{
        //    var order = modelEntities.Orders.SingleOrDefault(x => x.OrderId == Id && x.Active);
        //    if (order != null)
        //    {
        //        if (order.hasPayments || order.hasDeliveries)
        //            throw new Exception();

        //        using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
        //        {
        //            try
        //            {

        //                UpdateOrderStatus(order, OrderStatusEnum.Cancelada);

        //                ReturnOrderStock(order.OrderId);

        //                RemoveCustomerOrderTransaction(order.OrderId);

        //                UpdateCustomerAccountBalance(order.CustomerId);

        //                modelEntities.SaveChanges();

        //                dbContextTransaction.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                dbContextTransaction.Rollback();
        //            }
        //        }
        //    }

        //    return PartialView("_OrderList", this.OrdersToShow());
        //}

        [HttpGet]
        public virtual JsonResult OrderCancel(int Id)
        {
            var error = string.Empty;

            var order = modelEntities.Orders.SingleOrDefault(x => x.OrderId == Id && x.Active);
            if (order != null)
            {
                if (order.hasPayments || order.hasDeliveries)
                {
                    error = "No es posible cancelar una venta con cobros o entregas realizadas.";
                    return Json(new { Error = error }, JsonRequestBehavior.AllowGet);
                }

                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        UpdateOrderStatus(order, OrderStatusEnum.Cancelada);

                        ReturnOrderStock(order.OrderId);

                        RemoveCustomerOrderTransaction(order.OrderId);

                        UpdateCustomerAccountBalance(order.CustomerId);

                        modelEntities.SaveChanges();

                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return Json(new { Error = error }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual string OrderSendMail(int Id)
        {
            try
            {
                var order = modelEntities.Orders.SingleOrDefault(x => x.OrderId == Id && x.Active);

                if (order != null)
                {
                    /*Proform proform = new Proform();
                    proform.OrderId = order.OrderId;
                    proform.Guid = guid.ToString();
                    modelEntities.Proforms.Add(proform);
                    modelEntities.SaveChanges();*/

                    IMailing mailUtil = Mailing.Instance;
                    mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                    mailUtil.SendOrderResumeMail(order);
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }


        [HttpGet]
        public virtual JsonResult OrderDelete(int Id)
        {
            var error = string.Empty;
            var order = modelEntities.Orders.SingleOrDefault(x => x.OrderId == Id && x.Active);

            if (order != null)
            {
                order.Active = false;
                order.Deleted = CurrentDate.Now;
                order.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            return Json(new { Error = error }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult OrderPrint(int Id)
        {
            var order = modelEntities.Orders.FirstOrDefault(x => x.OrderId == Id && x.Active);
            return View(order);
        }

        [HttpGet]
        public virtual ActionResult OrderPrintLayout(int Id)
        {
            var order = modelEntities.Orders.FirstOrDefault(x => x.OrderId == Id && x.Active);
            return View(order);
        }

        [HttpGet]
        [ChildActionOnly]
        public virtual PartialViewResult ShowPrintOrderView(Order order)
        {
            switch (Request["SortBy"])
            {
                default:
                case "WithPrice":
                    order.OrderDetails = order.OrderDetails.Where(x => x.Active).OrderBy(x => x.Product.Title).ToList();
                    return PartialView("_OrderPrintWithPrice", order);

                case "Module":
                    order.OrderDetails = order.OrderDetails.Where(x => x.Active).OrderBy(x => x.Product.PhisicalLocationModule).ThenBy(x => x.Product.PhisicalLocationRow).ToList();
                    return PartialView("_OrderPrintByModule", order);

                case "A-Z":
                    order.OrderDetails = order.OrderDetails.Where(x => x.Active).OrderBy(x => x.Product.Title).ToList();
                    return PartialView("_OrderPrintByAz", order);
            }
        }
        private List<OrderViewModel> OrdersToShow()
        {
            var user = modelEntities.USERS.FirstOrDefault(x => x.Username == User.Identity.Name && x.Active && x.Deleted == null);
            List<OrderViewModel> list = new List<OrderViewModel>();

            list = modelEntities.Orders.Where(x => x.Active).OrderByDescending(x => x.OrderId)
            .Select(x => new OrderViewModel
            {
                Order = x,
                OrderId = x.OrderId,
                CreatedDate = x.Created,
                CustomerName = x.Customer.FullName,
                CustomerId = x.CustomerId,
                Channel = x.Channel,
                DeliveryMethodId = x.DeliveryMethodId,
                PaymentMethodId = x.PaymentMethodId,
                UserType = ((UserTypeEnum)x.Customer.UserType).ToString(),
                Total = x.Total,
                PaymentStatusId = x.PaymentStatusId,
                DeliveryStatusId = x.DeliveryStatusId,
                OrderStatusId = x.OrderStatusId
            })
            .ToList();

            return list;
        }

        private List<OrderListViewModel> OrdersToShow(string sFrom = "", string sTo = "")
        {
            DateTime fromDate = string.IsNullOrWhiteSpace(sFrom) ? new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1) : DateTime.Parse(sFrom);
            DateTime toDate = string.IsNullOrWhiteSpace(sTo) ? DateTime.Today.AddDays(1) : DateTime.Parse(sTo).AddDays(1);

            string ServerRoot = ConfigurationManager.AppSettings["ServerRoot"];

            var list = modelEntities.Orders.Where(x => x.Active & x.Created > fromDate && x.Created <= toDate).OrderByDescending(x => x.OrderId)
            .Select(x => new OrderListViewModel
            {
                OrderId = x.OrderId,
                PagoOnlineUrl = ServerRoot + "pago-online/" + x.GUID.ToString(),
                CreatedDate = x.Created,
                CustomerName = x.Customer.FullName,
                SaleType = ((UserTypeEnum)x.StoreType).ToString(),
                CustomerId = x.CustomerId,
                Channel = x.Channel,
                DeliveryMethodId = x.DeliveryMethodId,
                PaymentMethodId = x.PaymentMethodId,
                Total = x.Total,
                PaymentStatusId = x.PaymentStatusId,
                DeliveryStatusId = x.DeliveryStatusId,
                OrderStatusId = x.OrderStatusId
            }).AsQueryable();

            return list.ToList();
        }

        private List<DeliveryOrderViewModel> DeliveryOrdersToShow()
        {
            var user = modelEntities.USERS.FirstOrDefault(x => x.Username == User.Identity.Name && x.Active && x.Deleted == null);
            List<DeliveryOrderViewModel> list = new List<DeliveryOrderViewModel>();
            if (user.ROLES.FirstOrDefault().RoleName != "SuperAdmin")
            {
                list = modelEntities.DeliveryOrders.Where(x => x.Deleted == null && ((x.Order.Active && x.Order.SalerId == user.User_Id) || x.Refund.Active || x.Exchange.Active)).OrderByDescending(x => x.DeliveryOrderId)
                .Select(x => new DeliveryOrderViewModel
                {
                    Order = x.Order,
                    OrderId = x.OrderId.Value,
                    DeliveryOrderId = x.DeliveryOrderId,
                    Date = x.Date,
                    ReferNumber = x.ReferNumber,
                    Amount = x.Amount,
                    Customer = x.Order.Customer.FullName,
                    Created = x.Created,
                    CreatedBy = x.CreatedBy
                })
                .ToList();
                return list;
            }
            list = modelEntities.DeliveryOrders.Where(x => x.Deleted == null && (x.Order.Active || x.Refund.Active || x.Exchange.Active)).OrderByDescending(x => x.DeliveryOrderId)
            .Select(x => new DeliveryOrderViewModel
            {
                Order = x.Order,
                OrderId = x.OrderId.Value,
                RefundId = x.RefundId,
                ExchangeId = x.ExchangeId,
                DeliveryOrderId = x.DeliveryOrderId,
                Date = x.Date,
                ReferNumber = x.ReferNumber,
                Amount = x.Amount,
                Customer = x.Order != null ? x.Order.Customer.FullName : (x.Refund != null ? x.Refund.Customer.FullName : (x.Exchange != null ? x.Exchange.Customer.FullName : string.Empty)),
                Created = x.Created,
                CreatedBy = x.CreatedBy
            })
            .ToList();
            return list;
        }

        private List<OrderDetail> GetOrderDetails(FormCollection form, int orderId)
        {
            List<OrderDetail> items = new List<OrderDetail>();

            bool create_new_item = false;
            OrderDetail item = new OrderDetail();
            var sizeId = 0;
            var colorId = 0;
            foreach (string key in Request.Form.AllKeys)
            {
                if (key.Contains("_"))
                {
                    if (create_new_item)
                    {
                        item = new OrderDetail();
                        create_new_item = false;
                    }

                    if (key.Contains("Quantity"))
                        item.Quantity = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("ProductId"))
                        item.ProductId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("SizeId"))
                        item.SizeId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("ColorId"))
                        item.ColorId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("OrderDetailId"))
                        item.OrderDetailId = Convert.ToInt32(Request.Form[key]);
                    else if (key.Contains("Price"))
                        item.Price = decimal.Parse(Request.Form[key].Replace(".", ","));
                    else if (key.Contains("Discount") && key != "Discount_Percent" && !key.Contains("MaxDiscount"))
                    {
                        if (!string.IsNullOrWhiteSpace(Request.Form[key]))
                            item.Discount = decimal.Parse(Request.Form[key].Replace(".", ","));

                        item.OrderId = orderId;
                        items.Add(item);
                        create_new_item = true;
                    }
                }
            }

            return items;
        }

        [HttpGet]
        public ActionResult ExportOrdersToExcel()
        {
            var grid = new System.Web.UI.WebControls.GridView();

            var orderDetails = modelEntities.OrderDetails.Where(x => x.Active && x.Deleted == null && x.Order.Active && x.Order.Deleted == null)
                .OrderByDescending(x => x.OrderId)
                .ToList()
                .Select(x => new
                {
                    Fecha = x.Order.Created,
                    Cliente = x.Order.Customer.FullName,
                    Email = x.Order.Customer.Email,
                    Tipo_Usuario = ((UserTypeEnum)x.Order.Customer.UserType).ToString(),
                    Nro_Pedido = x.OrderId,
                    Codigo = x.Product.Code,
                    Talle = x.Size.Description,
                    Color = x.Color.Description,
                    Cantidad = x.Quantity,
                    Precio_Unitario = x.Price,
                    Descuento_Unitario = (x.Discount.HasValue ? x.Discount.Value + "%" : string.Empty),
                    Total = x.SubTotalWithDiscount,
                    Estado = ((OrderStatusEnum)x.Order.OrderStatusId).ToString(),
                    Forma_Pago = ((PaymentMethodEnum)x.Order.PaymentMethodId).ToString(),
                    Descuento_Pedido = (x.Order.Discount_Percent.HasValue ? x.Order.Discount_Percent.Value + "%" : string.Empty),
                    Descuento_Pedido_Monto = x.Order.Discount,
                    Iva = (x.Order.IVA_Percent.HasValue ? x.Order.IVA_Percent.Value + "%" : string.Empty),
                    Iva_valor = (x.Order.IVA.HasValue ? x.Order.IVA.Value.ToString() : string.Empty),
                    Recargo = (x.Order.Surcharge.HasValue ? x.Order.Surcharge.Value + "%" : string.Empty),
                    Recargo_valor = (x.Order.SurchargeAmount.HasValue ? x.Order.SurchargeAmount.Value.ToString() : string.Empty),
                    Total_Pedido = x.Order.Total
                });

            grid.DataSource = orderDetails.ToList();
            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Ventas.xls");
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }

        [HttpGet]
        public ActionResult ExportPaymentsToExcel()
        {
            var grid = new System.Web.UI.WebControls.GridView();

            var checks = modelEntities.Checks.Where(x => x.Deleted == null).ToList();

            var transactions = modelEntities.Transactions.Where(x => x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso && x.Deleted == null && x.Order.Active && x.Order.Deleted == null)
                .OrderByDescending(x => x.Date)
                .ToList()
                .Select(x => new
                {
                    Fecha = x.Order.Created,
                    Nro_Pedido = x.OrderId,
                    Cliente = x.Order.Customer.FullName,
                    Email = x.Order.Customer.Email,
                    Tipo_Usuario = ((UserTypeEnum)x.Order.Customer.UserType).ToString(),
                    Importe = x.Amount,
                    Cuenta_de_Pago = x.Accounting.Name,
                    Tiene_Cheques = (checks.Any(s => s.ChargeTransactionId == x.TransactionId || s.TransactionId == x.TransactionId) ? "Si" : "No")
                });

            grid.DataSource = transactions.ToList();
            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Cobranzas.xls");
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }

        [HttpGet]
        public ActionResult ExportOrderToExcel(int Id)
        {
            var order = modelEntities.Orders.FirstOrDefault(x => x.OrderId == Id);
            var orderDetails = order.OrderDetails.Select(x => new
            {
                Fecha = x.Order.Created.ToString(),
                Cliente = x.Order.Customer.FullName,
                Email = x.Order.Customer.Email,
                Tipo_Usuario = ((UserTypeEnum)x.Order.Customer.UserType).ToString(),
                Nro_Pedido = x.OrderId.ToString(),
                Codigo = x.Product.Code,
                Talle = x.Size.Description,
                Color = x.Color.Description,
                Precio_Unitario = x.Price.ToString(),
                Estado = ((OrderStatusEnum)x.Order.OrderStatusId).ToString(),
                Cantidad = x.Quantity.ToString(),
                Descuento = (x.Discount.HasValue ? x.Discount.Value + "%" : string.Empty),
                Total = x.SubTotalWithDiscount.ToString(),
            }).ToList();

            orderDetails.Add(new
            {
                Fecha = "",
                Cliente = "",
                Email = "",
                Tipo_Usuario = "",
                Nro_Pedido = "",
                Codigo = "",
                Talle = "",
                Color = "",
                Precio_Unitario = "",
                Estado = "",
                Cantidad = (order.OrderDetails.Sum(x => x.Quantity)).ToString(),
                Descuento = (order.Discount.HasValue ? order.Discount.Value + (order.Discount_Percent.HasValue ? "(" + order.Discount_Percent + "%)" : string.Empty) : string.Empty),
                Total = order.SubTotalWithDiscount.ToString()
            });

            var grid = new System.Web.UI.WebControls.GridView();
            grid.DataSource = orderDetails.ToList();
            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Venta-#" + Id + ".xls");
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }

        #region Private Methods

        [HttpGet]
        public virtual ActionResult GetCustomerPriceList(int CustomerId)
        {
            var customer = modelEntities.Customers.FirstOrDefault(x => x.CustomerId == CustomerId && x.Active);
            int currencyId = (int)CurrencyEnum.Peso;

            switch ((PriceListEnum)customer.PriceList)
            {
                default:
                case PriceListEnum.Minorista:
                case PriceListEnum.Mayorista:
                    currencyId = (int)CurrencyEnum.Peso;
                    break;
            }

            return Json(new { PriceList = customer.PriceList, Name = ((PriceListEnum)customer.PriceList).ToString(), CurrencyId = currencyId, UserType = customer.UserType }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetCustomerBalance(int CustomerId)
        {
            var customer = modelEntities.Customers.FirstOrDefault(x => x.CustomerId == CustomerId && x.Active);
            return Json(new { Balance = customer.Balance, BalanceUsd = customer.BalanceUsd }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GetProductPriceByCustomer(int ProductId, int PriceList)
        {
            decimal price = 0;
            var product = modelEntities.Products.FirstOrDefault(x => x.ProductId == ProductId && x.Active);
            switch ((PriceListEnum)PriceList)
            {
                default:
                case PriceListEnum.Minorista:
                    price = product.RetailPrice.HasValue ? product.RetailPrice.Value : 0;
                    break;

                case PriceListEnum.Mayorista:
                    price = product.WholesalePrice.HasValue ? product.WholesalePrice.Value : 0;
                    break;
            }

            return Json(new { CustomerPrice = price, StockVirtual = 0, StockReal = 0, Code = product.Code, MaxDiscount = product.MaxDiscount }, JsonRequestBehavior.AllowGet);
        }


        private void SubstractOrderStock(Order order)
        {
            var orderDetails = order.OrderDetails.Where(x => x.Active).ToList();

            var stockService = new StockService(new VirtualStock());
            string productDescription = string.Empty;

            if (stockService.HasStock(orderDetails, ref productDescription, ref modelEntities))
            {
                stockService.SubstractStock(orderDetails, ref modelEntities);
            }
            else
            {
                throw new NoStockException("No tenemos stock del producto: " + productDescription);
            }
        }

        private void UpdateCustomerAccount(Order order)
        {
            try
            {
                int salesCategoryId = int.Parse(ConfigurationManager.AppSettings["SalesCategoryId"]);

                if (order.Transactions.Any(x => x.OrderId == order.OrderId && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso && x.Deleted == null))
                {
                    var transaction = order.Transactions.FirstOrDefault();
                    transaction.Amount = order.Total * -1;
                    transaction.CurrencyId = order.CurrenyId;
                    transaction.Date = CurrentDate.Now;
                }
                else
                {
                    Transaction transaction = new Transaction();

                    transaction.OrderId = order.OrderId;
                    transaction.Amount = order.OrderTotal * -1;
                    transaction.CurrencyId = order.CurrenyId;
                    transaction.TransactionTypeId = (int)TransactionTypeEnum.Egreso;
                    transaction.TransactionCategoryId = salesCategoryId;
                    transaction.Date = DateTime.Today;
                    transaction.Created = CurrentDate.Now;
                    transaction.CreatedBy = User.Identity.Name;

                    modelEntities.Transactions.Add(transaction);
                }

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectListItem> GetChannelsSelectList()
        {
            Array values = Enum.GetValues(typeof(OrderChannelEnum));
            List<SelectListItem> items = new List<SelectListItem>(values.Length);

            foreach (var i in values)
            {
                items.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(OrderChannelEnum), i),
                    Value = ((int)i).ToString()
                });
            }

            return items;
        }

        public List<SelectListItem> GetPaymentMethodsSelectList()
        {
            Array values = Enum.GetValues(typeof(PaymentMethodEnum));
            List<SelectListItem> items = new List<SelectListItem>(values.Length);

            foreach (var i in values)
            {
                items.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(PaymentMethodEnum), i),
                    Value = ((int)i).ToString()
                });
            }

            return items;
        }

        public List<SelectListItem> GetDeliveryMethodsSelectList()
        {
            Array values = Enum.GetValues(typeof(DeliveryMethodEnum));
            List<SelectListItem> items = new List<SelectListItem>(values.Length);

            foreach (var i in values)
            {
                items.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(DeliveryMethodEnum), i),
                    Value = ((int)i).ToString()
                });
            }

            return items;
        }

        private void UpdateCustomerAccountBalance(int customerId)
        {
            AccountingService.UpdateCustomerBalance(customerId, ref modelEntities);
        }

        /*private void UpdateCustomerAccountBalance(int customerId)
        {
            try
            {
                var customer = modelEntities.Customers.SingleOrDefault(x => x.CustomerId == customerId);
                var transactions = modelEntities.Transactions
                    .Where(x => (x.Order.CustomerId == customerId || x.CustomerId == customerId) && x.Deleted == null).ToList();

                customer.Balance = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso).Sum(x => x.Amount);
                customer.BalanceUsd = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar).Sum(x => x.Amount);

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        private void RemoveCustomerOrderTransaction(int orderId)
        {
            try
            {
                var transaction = modelEntities.Transactions
                    .FirstOrDefault(x => x.OrderId == orderId && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso && x.Deleted == null);

                if (transaction != null)
                {
                    transaction.Deleted = CurrentDate.Now;
                    transaction.DeletedBy = User.Identity.Name;
                    modelEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult UpdateOrderDeliveryStatus(int Id)
        {
            try
            {
                var order = modelEntities.Orders.Find(Id);
                if (order != null)
                {
                    if (order.DeliveryStatusId == (int)DeliveryStatusEnum.Pendiente)
                    {
                        order.DeliveryStatusId = (int)DeliveryStatusEnum.Entregado;
                        order.DeliveryDate = CurrentDate.Now;
                    }
                    else
                    {
                        order.DeliveryStatusId = (int)DeliveryStatusEnum.Pendiente;
                    }

                    modelEntities.SaveChanges();

                }

                return Json(new
                {
                    Status = order.DeliveryStatusId
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = "Se produjo un error." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult UpdateOrderPaymentStatus(int Id)
        {
            try
            {
                var order = modelEntities.Orders.Find(Id);
                if (order != null)
                {
                    if (order.PaymentStatusId == (int)PaymentStatusEnum.Pendiente)
                        order.PaymentStatusId = (int)PaymentStatusEnum.Cobrada;
                    else
                        order.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;

                    modelEntities.SaveChanges();
                }

                return Json(new { Status = order.PaymentStatusId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = "Se produjo un error." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult UpdateOrderStatus(int Id)
        {
            try
            {
                var order = modelEntities.Orders.Find(Id);

                if (order != null)
                {
                    if (order.OrderStatusId == (int)OrderStatusEnum.EnProceso) { 
                        order.OrderStatusId = (int)OrderStatusEnum.Confirmado;
                        order.ConfirmedDate = CurrentDate.Now;
                        OrderSendMail(order.OrderId); //Envio de mail de confimacion de orden
                    }
                    else { 
                        order.OrderStatusId = (int)OrderStatusEnum.EnProceso;
                    }

                    modelEntities.SaveChanges();
                }
                return Json(new { Status = order.OrderStatusId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = "Se produjo un error." }, JsonRequestBehavior.AllowGet);
            }
        }

        private void UpdateOrderStatus(Order order, OrderStatusEnum status)
        {
            order.OrderStatusId = (int)status;
            modelEntities.SaveChanges();
        }

        private void ReturnOrderStock(int orderId)
        {
            var stockService = new StockService(new VirtualStock());
            stockService.ReturnOrderStock(orderId, ref modelEntities);
        }

        #endregion

        [HttpGet]
        public string AddOrdersGuid()
        {
            var orders = modelEntities.Orders.Where(x => !x.GUID.HasValue && !x.Deleted.HasValue).ToList();
            foreach (var order in orders)
            {
                order.GUID = Guid.NewGuid();
            }
            modelEntities.SaveChanges();

            return "ok";
        }
    }
}