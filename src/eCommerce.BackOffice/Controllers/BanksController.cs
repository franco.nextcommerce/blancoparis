﻿using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class BanksController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            return View("BankList", this.BanksToList());
        }

        private List<Bank> BanksToList()
        {
            return modelEntities.Banks.Where(x => x.Deleted == null).OrderBy(x => x.Name).ToList();
        }

        [HttpGet]
        public ActionResult BankUpdate(int Id)
        {
            if (Id != -1)
            {
                var bank = modelEntities.Banks.SingleOrDefault(x => x.BankId == Id);
                return View(bank);
            }

            return View(new Bank { BankId = Id });
        }

        [HttpPost]
        public ActionResult BankUpdate(FormCollection form)
        {
            Bank bank = new Bank();

            if (form["BankId"] != string.Empty && Convert.ToInt16(form["BankId"]) > 0)
            {
                int bankId = Convert.ToInt16(form["BankId"]);
                bank = modelEntities.Banks.SingleOrDefault(x => x.BankId == bankId && x.Deleted == null);
            }

            TryUpdateModel(bank, form);

            if (bank.BankId <= 0)
            {
                bank.Created = CurrentDate.Now;
                bank.CreatedBy = User.Identity.Name;
                modelEntities.Banks.Add(bank);
            }

            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult BankDelete(int Id)
        {
            var bank = modelEntities.Banks.SingleOrDefault(x => x.BankId == Id && x.Deleted == null);
            if (bank != null)
            {
                bank.Deleted = CurrentDate.Now;
                bank.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            return PartialView("_BankList", this.BanksToList());
        }        
    }
}