﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    public class TransactionCategoriesController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            return View("TransactionCategoryList", this.TransactionCategoryToShow());
        }

        private List<TransactionCategory> TransactionCategoryToShow()
        {
            return modelEntities.TransactionCategories.Where(x => x.Deleted == null).OrderBy(x=> x.Name).ToList();
        }

        [HttpGet]
        [OutputCache(Duration = 0)]
        public ActionResult TransactionCategoryUpdate(int Id)
        {
            var TransactionCategories = modelEntities.TransactionCategories.Where(x => x.Deleted == null).ToList();

            if (Id != -1)
            {
                var TransactionCategory = modelEntities.TransactionCategories.SingleOrDefault(x => x.TransactionCategoryId == Id);
                return View(TransactionCategory);
            }

            return View(new TransactionCategory { TransactionCategoryId = Id, TransactionTypeId = (int)TransactionTypeEnum.Ingreso , ApplyForMaaser = true});
        }

        [HttpPost]
        public ActionResult TransactionCategoryUpdate(FormCollection form)
        {
            TransactionCategory transactionCategory = new TransactionCategory();

            if (form["TransactionCategoryId"] != string.Empty && Convert.ToInt16(form["TransactionCategoryId"]) > 0)
            {
                int TransactionCategoryId = Convert.ToInt16(form["TransactionCategoryId"]);
                transactionCategory = modelEntities.TransactionCategories.SingleOrDefault(x => x.TransactionCategoryId == TransactionCategoryId && x.Deleted == null);
            }

            TryUpdateModel(transactionCategory, form);

            if (Request["ApplyForMaaser"] != null && Request["ApplyForMaaser"] == "on")
                transactionCategory.ApplyForMaaser = true;
            else
                transactionCategory.ApplyForMaaser = false;

            if (transactionCategory.TransactionCategoryId <= 0)
            {
                transactionCategory.Created = CurrentDate.Now;
                transactionCategory.CreatedBy = User.Identity.Name;
                modelEntities.TransactionCategories.Add(transactionCategory);
            }

            modelEntities.SaveChanges();

            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult TransactionCategoryDelete(int Id)
        {
            var transactionCategory = modelEntities.TransactionCategories.SingleOrDefault(x => x.TransactionCategoryId == Id && x.Deleted == null);
            if (transactionCategory != null)
            {
                transactionCategory.Deleted = CurrentDate.Now;
                transactionCategory.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            return PartialView("_TransactionCategoryList", this.TransactionCategoryToShow());
        }        
    }
}