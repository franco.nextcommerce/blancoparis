﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Controllers
{
    ////[AuthorizeUser(Roles = "Admin")]
    public class CouponsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            List<Coupon> Coupons = modelEntities.Coupons.Where(x => x.Active == true).ToList();
            return View("CouponList", Coupons);
        }

        [HttpGet]
        public ActionResult CouponUpdate(int Id)
        {
            if (Id != -1)
            {
                var Coupon = modelEntities.Coupons.SingleOrDefault(x => x.CouponId == Id);
                return View(Coupon);
            }

            return View(new Coupon { CouponId = Id });
        }

        [HttpPost]
        public ActionResult CouponUpdate(FormCollection form)
        {
            Coupon Coupon = new Coupon();

            if (form["CouponId"] != string.Empty && Convert.ToInt16(form["CouponId"]) > 0)
            {
                int CouponId = Convert.ToInt16(form["CouponId"]);
                Coupon = modelEntities.Coupons.SingleOrDefault(x => x.CouponId == CouponId && x.Active);
            }

            TryUpdateModel(Coupon, form);

            if (Coupon.CouponId <= 0)
            {
                Coupon.Active = true;
                Coupon.Created = CurrentDate.Now;
                Coupon.CreatedBy = User.Identity.Name;
                modelEntities.Coupons.Add(Coupon);
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult CouponDelete(int Id)
        {
            var Coupon = modelEntities.Coupons.SingleOrDefault(x => x.CouponId == Id && x.Active);
            if (Coupon != null)
            {
                Coupon.Active = false;
                Coupon.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.Coupons.Where(x => x.Active).ToList();
            return PartialView("_CouponList", list);
        }
    }
}