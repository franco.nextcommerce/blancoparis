﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace eCommerce.BackOffice.Controllers
{
    ////[AuthorizeUser(Roles = "Admin")]
    public class InboxController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            return View("ContactList", this.ContactsToShow());
        }

        [HttpGet]
        public virtual PartialViewResult ContactDelete(int Id)
        {
            var contact = modelEntities.Contacts.SingleOrDefault(x => x.ContactId == Id && x.Active);
            if (contact != null)
            {
                contact.Active = false;
                contact.DeletedDate = CurrentDate.Now;
                contact.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            return PartialView("_ContactList", this.ContactsToShow());
        }

        [HttpPost]
        public ActionResult MarkAsRead(int Id)
        {   
            MarkAsRead(Id,true);

            return PartialView("_ContactList", this.ContactsToShow());
        }

        [HttpPost]
        public ActionResult MarkAsNoRead(int Id)
        {
            MarkAsRead(Id, false);

            return PartialView("_ContactList", this.ContactsToShow());
        }

        private List<Contact> ContactsToShow()
        {
            return modelEntities.Contacts.Where(x => x.Active).OrderByDescending(x=> x.ContactId).ToList();
        }

        private void MarkAsRead(int Id,bool readed)
        {
            var contact = modelEntities.Contacts.SingleOrDefault(x => x.ContactId == Id && x.Active);
            if (contact != null)
            {
                contact.Answered = readed;
                modelEntities.SaveChanges();
            }
        }

        #region Export to Excel

        [HttpGet]
        public ActionResult ExportToExcel()
        {
            var grid = new System.Web.UI.WebControls.GridView();

            var inbox = ContactsToShow();

            var contacts = from c in inbox
                select new
                {
                    Id = c.ContactId,
                    Fecha = c.Created.ToString(),
                    Nombre_Completo = c.Fullname,
                    Email = c.EMail,
                    Telefono = c.PhoneNumber,
                    Leido = c.Answered ? "Si" : "No",  
                    Asunto = c.Subject,
                    Mensaje = c.Message,
                };

            grid.DataSource = contacts.ToList();
            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Contactos.xls");
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }

        #endregion
    }
}