﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.UI;
using eCommerce.BackOffice.ViewModels;

namespace eCommerce.BackOffice.Controllers
{
    //[AuthorizeUser(Roles = "Admin")]
    public class NewsSuscribersController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        public ActionResult Index()
        {
            //List<NewsSuscriber> NewsSuscribers = modelEntities.NewsSuscribers.Where(x => x.Active == true).ToList();
            //return View("NewsSuscriberList", NewsSuscribers);
            return View("NewsSuscriberList2");
        }

        [HttpGet]
        public JsonResult GetNewsSuscribers()
        {
            //return Json(new { data = this.NewsSuscribersToShow() }, JsonRequestBehavior.AllowGet);
            var jsonResult = Json(new { data = this.NewsSuscribersToShow() }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        private List<NewsSuscriberViewModel> NewsSuscribersToShow()
        {
            return modelEntities.NewsSuscribers.Where(x => x.Active && x.Deleted == null)
                .Select(x => new NewsSuscriberViewModel
                {
                    NewsSuscriberId = x.NewsSuscriberId,
                    FullName = x.FullName,
                    Email = x.Email,
                    CreatedDate = x.Created
                }).ToList();
        }

        [HttpGet]
        public ActionResult NewsSuscriberUpdate(int Id)
        {
            if (Id != -1)
            {
                var NewsSuscribers = modelEntities.NewsSuscribers.SingleOrDefault(x => x.NewsSuscriberId == Id);
                return View(NewsSuscribers);
            }

            return View(new NewsSuscriber { NewsSuscriberId = Id });
        }

        [HttpPost]
        public ActionResult NewsSuscriberUpdate(FormCollection form)
        {
            NewsSuscriber NewsSuscriber = new NewsSuscriber();

            if (form["NewsSuscriberId"] != string.Empty && Convert.ToInt16(form["NewsSuscriberId"]) > 0)
            {
                int NewsSuscriberId = Convert.ToInt16(form["NewsSuscriberId"]);
                NewsSuscriber = modelEntities.NewsSuscribers.SingleOrDefault(x => x.NewsSuscriberId == NewsSuscriberId && x.Active);
            }

            TryUpdateModel(NewsSuscriber, form);

            if (NewsSuscriber.NewsSuscriberId <= 0)
            {
                NewsSuscriber.Active = true;
                NewsSuscriber.Created = CurrentDate.Now;
                modelEntities.NewsSuscribers.Add(NewsSuscriber);
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult NewsSuscriberDelete(int Id)
        {
            var NewsSuscriber = modelEntities.NewsSuscribers.SingleOrDefault(x => x.NewsSuscriberId == Id && x.Active);
            if (NewsSuscriber != null)
            {
                NewsSuscriber.Active = false;
                NewsSuscriber.Deleted = CurrentDate.Now;
                NewsSuscriber.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.NewsSuscribers.Where(x => x.Active).ToList();
            return PartialView("_NewsSuscriberList", list);
        }

        [HttpGet]
        public ActionResult ExportToExcel()
        {
            var grid = new System.Web.UI.WebControls.GridView();

            //var suscribers = from c in modelEntities.NewsSuscribers
            //                where c.Active == true
            //                select new
            //                {
            //                    Email = c.Email,
            //                    Nombre_Completo = c.FullName,
            //                    Creado = c.Created.ToShortDateString()
            //                };
            var suscribers = modelEntities.NewsSuscribers.Where(x => x.Active && x.Deleted == null)
                .ToList()
                .Select(x => new
                {
                    Email = x.Email,
                    Nombre_Completo = x.FullName,
                    Creado = x.Created.ToShortDateString()
                });

            grid.DataSource = suscribers.ToList();
            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Suscriptos a Newsletter.xls");
            Response.ContentType = "application/excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();

            return null;
        }
    }
}