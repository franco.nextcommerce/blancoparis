﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace eCommerce.BackOffice.Controllers
{
    public class NewsletterController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        // GET: Newsletter
        public ActionResult Index()
        {
            TempData["ServerRoot"] = ConfigurationManager.AppSettings["ServerRoot"];

            return View("NewsletterList", this.NewsLettersToShow());
        }

        [HttpGet]
        public ActionResult NewsletterUpdate(int Id)
        {
            var products = modelEntities.Products
                .Where(x => x.Active)
                .AsEnumerable()
                .Select(s => new
                {
                    ProductId = s.ProductId,
                    Text = $"{s.Code} - {s.Title}"
                }).ToList();
            
            var purchases = modelEntities.Purchases
                .Where(x => x.Active)
                .AsEnumerable()
                .Select(s => new
                {
                    PurchaseId = s.PurchaseId,
                    Text = $"{"Id: " + s.PurchaseId} - {s.Supplier.Name} - {"$"+s.Total}"
                }).ToList();

            TempData["Products"] = new SelectList(products, "ProductId", "Text");
            TempData["Purchases"] = new SelectList(purchases, "PurchaseId", "Text");
            TempData["Brands"] = new SelectList(modelEntities.Brands.Where(x => x.Active), "BrandId", "Name");
            
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem
            {
                Text = "Menor Precio",
                Value = "2"
            });
            list.Add(new SelectListItem
            {
                Text = "Mayor Precio",
                Value = "3"
            });
            list.Add(new SelectListItem
            {
                Text = "Menor Stock",
                Value = "4"
            });
            list.Add(new SelectListItem
            {
                Text = "Mayor Stock",
                Value = "5"
            });


            if (Id != -1)
            {
                var Newsletter = modelEntities.Newsletters.SingleOrDefault(x => x.NewsletterId == Id);
                TempData["OrderByIds"] = new SelectList(list, "Value", "Text", Newsletter.OrderById);
                return View(Newsletter);
            }

            TempData["OrderByIds"] = new SelectList(list, "Value", "Text");

            return View(new Newsletter { NewsletterId = Id, HasSpecialPrice = true, UseMaxDiscount = true });
        }

        [HttpPost]
        public ActionResult NewsletterUpdate(FormCollection form)
        {
            try
            {
                using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        Newsletter newsletter = new Newsletter();
                        if (form["NewsletterId"] != string.Empty && Convert.ToInt16(form["NewsletterId"]) > 0)
                        {
                            int NewsletterId = Convert.ToInt16(form["NewsletterId"]);
                            newsletter = modelEntities.Newsletters.SingleOrDefault(x => x.NewsletterId == NewsletterId && x.Deleted == null);
                        }

                        TryUpdateModel(newsletter, form);

                        newsletter.ShowStock = form["ShowStock"] == "on" ? true : false;
                        newsletter.Sent = form["Sent"] == "on" ? true : false;
                        newsletter.Active = form["Active"] == "on" ? true : false;
                        newsletter.HasSpecialPrice = form["HasSpecialPrice"] == "on" ? false : true;
                        newsletter.UseMaxDiscount = form["UseMaxDiscount"] == "on" ? true : false;

                        if (newsletter.NewsletterId <= 0)
                        {
                            newsletter.Created = CurrentDate.Now;
                            newsletter.CreatedBy = User.Identity.Name;

                            modelEntities.Newsletters.Add(newsletter);
                            modelEntities.SaveChanges();
                        }
                        else
                        {
                            newsletter.Modified = CurrentDate.Now;
                            newsletter.ModifiedBy = User.Identity.Name;
                        }

                        var listItems = GetNewsletterDetails(form, newsletter.NewsletterId);
                        UpdateNewsletterDetails(newsletter, listItems);

                        modelEntities.SaveChanges();

                        dbContextTransaction.Commit();
                        NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Se produjo un error al intentar guardar el Newsletter.");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public virtual PartialViewResult MassiveSearch(int? BrandId, string Title)
        {
            List<Product> products = new List<Product>();
            if (BrandId != null && !string.IsNullOrEmpty(Title))
            {
                products = modelEntities.Products.Where(x => x.BrandId == BrandId && x.Title.Contains(Title) && x.Active).ToList();
            }
            else if (BrandId != null)
            {
                products = modelEntities.Products.Where(x => x.BrandId == BrandId && x.Active).ToList();
            }
            else if (!string.IsNullOrEmpty(Title))
            {
                products = modelEntities.Products.Where(x => x.Title.Contains(Title) && x.Active).ToList();
            }

            if (products.Count == 0)
                return PartialView("_SearchedProductsListError", "No existe ningun producto con esos parámetros");

            return PartialView("_SearchedProductsList", products);
        }

        [HttpPost]
        public virtual PartialViewResult MassiveSearchByPurchase(int PurchaseId)
        {
            var purchase = modelEntities.Purchases.Include("PurchaseDetails").FirstOrDefault(x => x.PurchaseId == PurchaseId);

            List<Product> products = purchase.PurchaseDetails.Where(x => x.Active && x.Deleted == null && x.Product.Active).Select(x => x.Product).ToList();

            if (products.Count == 0)
                return PartialView("_SearchedProductsListError", "No existe ningun producto con esos parámetros");

            return PartialView("_SearchedProductsList", products);
        }

        private List<NewsletterProduct> GetNewsletterDetails(FormCollection form, int newsletterId)
        {
            List<NewsletterProduct> items = new List<NewsletterProduct>();

            var updatePrices = form["HasSpecialPrice"] == "on" ? true : false;
            //var updatePrices = true;

            bool create_new_item = false;
            NewsletterProduct item = new NewsletterProduct();
            Product prod = new Product();
            foreach (string key in Request.Form.AllKeys)
            {
                if (key.Contains("_"))
                {
                    if (create_new_item)
                    {
                        item = new NewsletterProduct();
                        create_new_item = false;
                    }
                    if (key.Contains("ProductId"))
                    {
                        item.ProductId = Convert.ToInt32(Request.Form[key]);
                        prod = modelEntities.Products.SingleOrDefault(x => x.ProductId == item.ProductId && x.Active);
                    }
                    else if (key.Contains("Price"))
                    {
                        if (updatePrices)
                        {
                            prod.WholesalePrice = decimal.Parse(Request.Form[key].Replace(".", "."));
                        } else
                        {
                            Guid guid = Guid.NewGuid();
                            item.SpecialPrice = decimal.Parse(Request.Form[key]);
                            item.GUID = guid.ToString();                            
                        }
                    }
                    else if (key.Contains("Old"))
                    {
                        if (updatePrices)
                        {
                            if (!string.IsNullOrEmpty(Request.Form[key]))
                                prod.OldWholesalePrice = decimal.Parse(Request.Form[key].Replace(".", "."));
                            else
                                prod.OldWholesalePrice = null;
                        } else
                        {
                            if (!string.IsNullOrEmpty(Request.Form[key]))
                                item.OldSpecialPrice = decimal.Parse(Request.Form[key].Replace(".", "."));
                            else
                                item.OldSpecialPrice= null;
                        }
                        
                    }
                    else if (key.Contains("Discount"))
                    {
                        if (!string.IsNullOrEmpty(Request.Form[key]))
                        {
                            item.Discount = decimal.Parse(Request.Form[key]);
                        }
                    }
                    else if (key.Contains("Observation"))
                    {
                        item.Observation = Request.Form[key];
                        item.NewsletterId = newsletterId;
                        items.Add(item);
                        create_new_item = true;
                    }

                }
            }
            return items;
        }

        private void UpdateNewsletterDetails(Newsletter newsletter, List<NewsletterProduct> details)
        {
            modelEntities.NewsletterProducts.RemoveRange(newsletter.NewsletterProducts);
            modelEntities.NewsletterProducts.AddRange(details);
        }

        [HttpGet]
        public ActionResult GetProduct(int productId)
        {
            var prod = modelEntities.Products.FirstOrDefault(x => x.ProductId == productId && x.Active);
            return Json(new { ProductId = prod.ProductId, Code = prod.Code, Title = prod.Title, Category = prod.Category.CompleteName, Price = prod.WholesalePrice, OldPrice = prod.OldWholesalePrice, MaxDiscount = prod.MaxDiscount }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual PartialViewResult NewsletterDelete(int Id)
        {
            var newsletter = modelEntities.Newsletters.FirstOrDefault(x => x.NewsletterId == Id && x.Deleted == null);
            if (newsletter != null)
            {
                newsletter.Deleted = CurrentDate.Now;
                newsletter.DeletedBy = User.Identity.Name;
                modelEntities.SaveChanges();
            }

            return PartialView("_NewsletterList", this.NewsLettersToShow());
        }

        private List<Newsletter> NewsLettersToShow()
        {
            return modelEntities.Newsletters.Where(x => x.Deleted == null).OrderByDescending(x => x.Created).ToList();
        }

        [HttpGet]
        public virtual PartialViewResult NewsletterSend(int Id)
        {
            var newsletter = modelEntities.Newsletters.FirstOrDefault(x => x.NewsletterId == Id && x.Deleted == null);
            if (newsletter != null)
            {
                newsletter.Sent = true;
                newsletter.SentDate = CurrentDate.Now;
                modelEntities.SaveChanges();
            }
            var list = modelEntities.Newsletters.Where(x => x.Active).ToList();
            return PartialView("_NewsletterList", list);
        }




    }
}