﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Notification;
using eCommerce.BackOffice.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using eCommerce.Services;
using eCommerce.Services.Services.Accounting;
using System.Web;
using System.IO;

namespace eCommerce.BackOffice.Controllers
{
    public class LookbookGroupController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        private const string LookbookGroupsFileUploadDirectory = "/Content/UploadDirectory/LookbookGroups";

        [HttpGet]
        public ActionResult Index()
        {
            //var Lookbooks = modelEntities.Lookbooks.Where(x => x.Deleted == null).ToList();
            return View("LookbookGroupList", this.LookbookGroupsToList());
        }

        private List<LookbookGroup> LookbookGroupsToList()
        {
            return modelEntities.LookbookGroups.Where(x => !x.Deleted.HasValue).OrderBy(x => x.Name).ToList();
        }

        [HttpGet]
        public ActionResult LookbookGroupUpdate(int Id)
        {
            //var lookbook = modelEntities.Products.Where(x => !x.Deleted.HasValue).OrderBy(p => p.Title).ToList();

            if (Id != -1)
            {

                //var path = Path.Combine(LookbooksFileUploadDirectory, Id.ToString());
                var LookbookGroup = modelEntities.LookbookGroups.Find(Id);

                //TempData["Products"] = new MultiSelectList(products, "ProductId", "Title", Lookbook.LookbookProducts.Select(x => x.ProductId));
                //if (!string.IsNullOrEmpty(Lookbook.Image))
                //    ViewData["image"] = Path.Combine(path, Lookbook.Image);

                return View(LookbookGroup);
            }

            //TempData["Products"] = new MultiSelectList(products, "ProductId", "Title");
            return View(new LookbookGroup { LookbookGroupId = Id });
        }

        [HttpPost]
        public ActionResult LookbookGroupUpdate(FormCollection form, HttpPostedFileBase image)
        {
            LookbookGroup LookbookGroup = new LookbookGroup();

            if (form["LookbookGroupId"] != string.Empty && Convert.ToInt16(form["LookbookGroupId"]) > 0)
            {
                int LookbookGroupId = Convert.ToInt16(form["LookbookGroupId"]);
                LookbookGroup = modelEntities.LookbookGroups.FirstOrDefault(x => x.LookbookGroupId == LookbookGroupId && !x.Deleted.HasValue);
            }

            if (Request["Active"] != null && Request["Active"] == "on")
                LookbookGroup.Active = true;
            else
                LookbookGroup.Active = false;

            if (Request["IsCampaign"] != null && Request["IsCampaign"] == "on")
                LookbookGroup.IsCampaign = true;
            else
                LookbookGroup.IsCampaign = false;

            TryUpdateModel(LookbookGroup, form);

            if (LookbookGroup.LookbookGroupId <= 0)
            {
                modelEntities.LookbookGroups.Add(LookbookGroup);
            }

            modelEntities.SaveChanges();

            if (image != null && image.ContentLength > 0)
            {
                string itemId = LookbookGroup.LookbookGroupId.ToString();
                var path = Path.Combine(Server.MapPath(LookbookGroupsFileUploadDirectory), itemId);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string fileName = Path.GetFileName(image.FileName);
                string fileExtension = fileName.Substring(image.FileName.LastIndexOf("."));
                string new_fileName = "image_" + itemId + fileExtension;
                string filePath = Path.Combine(path, new_fileName);

                image.SaveAs(filePath);
                LookbookGroup.Image = new_fileName;
                modelEntities.SaveChanges();
            }

            ////Product Lookbooks
            //if (!string.IsNullOrEmpty(form["Products"]))
            //{
            //    var productIds = form["Products"].Split(',').ToList();

            //    //Borro los que estaban antes y creo los nuevos
            //    if (modelEntities.LookbookProducts.Any(x => x.LookbookId == Lookbook.LookbookId))
            //    {
            //        var productsInCurrentLookbook = Lookbook.LookbookProducts;
            //        modelEntities.LookbookProducts.RemoveRange(productsInCurrentLookbook);
            //    }
            //    modelEntities.SaveChanges();

            //    foreach (var prodId in productIds)
            //    {
            //        LookbookProduct prodLookbook = new LookbookProduct();
            //        prodLookbook.ProductId = int.Parse(prodId);
            //        prodLookbook.LookbookId = Lookbook.LookbookId;
            //        modelEntities.LookbookProducts.Add(prodLookbook);
            //    }

            //    modelEntities.SaveChanges();
            //}


            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual PartialViewResult LookbookGroupDelete(int Id)
        {
            var LookbookGroup = modelEntities.LookbookGroups.FirstOrDefault(x => x.LookbookGroupId == Id);
            if (LookbookGroup != null)
            {
                LookbookGroup.Deleted = CurrentDate.Now;
                LookbookGroup.DeletedBy = "Borrado por backoffice";
                modelEntities.SaveChanges();
            }

            return PartialView("_LookbookGroupList", this.LookbookGroupsToList());
        }

    }
}