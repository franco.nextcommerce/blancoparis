﻿using eCommerce.Services.Annotations;
using eCommerce.Services.Model;
using eCommerce.Services.Notification;
using eCommerce.Services.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Accounting.Controllers
{
    public class OperatorsController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        string KeyString = "Gv7L3V15jCdb9P5XGKiPnhHZ7JlKcmU=";

        [RBAC]
        public ActionResult Index()
        {
            List<USER> users = modelEntities.USERS.Where(x => x.Active == true).OrderBy(r => r.Fullname).ToList();            
            return View("OperatorList", users);
        }

        [HttpGet]
        [RBAC]
        public ActionResult OperatorUpdate(int Id)
        {
            var roles = modelEntities.ROLES.Where(x => x.Active).ToList();
            var deposits = modelEntities.Deposits.Where(x => x.Deleted == null).ToList();
            var salerDeposits = modelEntities.SalerDeposits.Where(x => x.SalerId == Id).ToList();
            TempData["Roles"] = new SelectList(roles, "Role_Id", "RoleName");
            TempData["Deposits"] = new MultiSelectList(deposits, "DepositId", "Name", salerDeposits.Select(x => x.DepositId));

            if (Id != -1)
            {
                var user = modelEntities.USERS.SingleOrDefault(x => x.User_Id == Id);
                user.Password = EncryptionService.Decrypt(user.Password, KeyString);

                return View(user);
            }

            return View(new USER { User_Id = Id, Enabled = true});
        }

        [HttpGet]
        public ActionResult MyProfile()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = modelEntities.USERS.SingleOrDefault(x => x.Username == User.Identity.Name && x.Active);
                user.Password = EncryptionService.Decrypt(user.Password, KeyString);

                return View(user);
            }

            return View(new USER { User_Id = -1});
        }

        [HttpPost]
        public ActionResult OperatorUpdate(FormCollection form)
        {
            try
            {
                USER user = new USER();

                if (form["User_Id"] != string.Empty && Convert.ToInt16(form["User_Id"]) > 0)
                {
                    int userId = Convert.ToInt16(form["User_Id"]);
                    user = modelEntities.USERS.SingleOrDefault(x => x.User_Id == userId && x.Active);
                }



                TryUpdateModel(user, form);

                var existentUser = modelEntities.USERS.Where(x => x.Username == user.Username && x.Active && x.User_Id != user.User_Id);
                if (existentUser.Count() > 0)
                {
                    NotificationBar.ShowMessage(this, NotificationMessageType.Warning, "Ya existe una cuenta con el Username ingresado!");
                    return View(user);
                }

                if (Request["Enabled"] != null)
                {
                    if(Request["Enabled"] == "on")
                        user.Enabled = true;
                    else
                        user.Enabled = false;
                }

                user.Password = EncryptionService.Encrypt(user.Password, KeyString);

                if (user.User_Id < 0)
                {
                    user.Active = true;
                    user.Created = CurrentDate.Now;
                    user.CreatedBy = User.Identity.Name;
                    modelEntities.USERS.Add(user);
                }

                modelEntities.SaveChanges();

                if (form["DepositsId"] != string.Empty && form["DepositsId"] != null)
                {
                    List<string> depositsIdsStr = form["DepositsId"].Split(',').ToList();
                    List<int> depositIds = depositsIdsStr.Select(int.Parse).ToList();
                    SalerDeposit salerDeposit = new SalerDeposit();
                    List<SalerDeposit> currentDeposits = modelEntities.SalerDeposits.Where(x => x.SalerId == user.User_Id).ToList();
                    List<SalerDeposit> salerDepositsToAdd = new List<SalerDeposit>();

                    foreach (var item in depositIds)
                    {
                        salerDeposit = new SalerDeposit();
                        salerDeposit.DepositId = item;
                        salerDeposit.SalerId = user.User_Id;
                        if (currentDeposits.Where(x => x.SalerId == salerDeposit.SalerId && x.DepositId == salerDeposit.DepositId).Count() == 0)
                            salerDepositsToAdd.Add(salerDeposit);
                    }
                    modelEntities.SalerDeposits.AddRange(salerDepositsToAdd);
                    //Delete los que ya no estan
                    List<SalerDeposit> depositsToDelete = modelEntities.SalerDeposits.Where(x => x.SalerId == user.User_Id && !depositIds.Contains(x.DepositId)).ToList();
                    modelEntities.SalerDeposits.RemoveRange(depositsToDelete);
                    modelEntities.SaveChanges();
                } else
                {
                    List<SalerDeposit> depositsToDelete = modelEntities.SalerDeposits.Where(x => x.SalerId == user.User_Id).ToList();
                    modelEntities.SalerDeposits.RemoveRange(depositsToDelete);
                    modelEntities.SaveChanges();
                }

                NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                NotificationBar.ShowMessage(this, NotificationMessageType.Error, "Ha ocurrido un error al crear su usuario!");
            }

            return View();
        }

        [HttpGet]
        [RBAC]
        public virtual PartialViewResult OperatorDelete(int Id)
        {
            var user = modelEntities.USERS.SingleOrDefault(x => x.User_Id == Id && x.Active);
            if (user != null)
            {
                user.Active = false;
                user.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.USERS.Where(x => x.Active).ToList();
            return PartialView("_OperatorList", list);
        }

        /***************************************ROLES******************************************/

        [RBAC]
        public ActionResult Roles()
        {
            List<ROLE> roles = modelEntities.ROLES.Where(x => x.Active == true).OrderBy(r => r.RoleName).ToList();
            return View("RoleList", roles);
        }

        [HttpGet]
        [RBAC]
        public ActionResult RoleUpdate(int Id)
        {
            if (Id != -1)
            {
                var user = modelEntities.ROLES.SingleOrDefault(x => x.Role_Id == Id);
                return View(user);
            }

            return View(new ROLE { Role_Id = Id, IsSysAdmin = false });
        }

        [HttpPost]
        public ActionResult RoleUpdate(FormCollection form)
        {
            ROLE role = new ROLE();

            if (form["Role_Id"] != string.Empty && Convert.ToInt16(form["Role_Id"]) > 0)
            {
                int roleId = Convert.ToInt16(form["Role_Id"]);
                role = modelEntities.ROLES.SingleOrDefault(x => x.Role_Id == roleId && x.Active);
            }

            TryUpdateModel(role, form);

            if (role.Role_Id <= 0)
            {
                role.Active = true;
                role.Created = CurrentDate.Now;
                modelEntities.ROLES.Add(role);
            }

            modelEntities.SaveChanges();
            NotificationBar.ShowMessage(this, NotificationMessageType.Success, "Los datos fueron guardados correctamente!");

            return RedirectToAction("Roles");
        }

        [HttpGet]
        [RBAC]
        public virtual PartialViewResult RoleDelete(int Id)
        {
            var role = modelEntities.ROLES.SingleOrDefault(x => x.Role_Id == Id && x.Active);
            if (role != null)
            {
                role.Active = false;
                role.Deleted = CurrentDate.Now;
                modelEntities.SaveChanges();
            }

            var list = modelEntities.ROLES.Where(x => x.Active).ToList();
            return PartialView("_RoleList", list);
        }

        /*********************************ASIGNACIÓN DE ROLES A USUARIO*************************************/

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public PartialViewResult AddUserRole(int id, int userId)
        {
            ROLE role = modelEntities.ROLES.Find(id);
            USER user = modelEntities.USERS.Find(userId);

            if (!role.USERS.Contains(user))
            {
                role.USERS.Add(user);
                modelEntities.SaveChanges();
            }

            return PartialView("_UserRoleList", user.ROLES);
        }

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public PartialViewResult DeleteUserRole(int id, int userId)
        {
            ROLE role = modelEntities.ROLES.Find(id);
            USER user = modelEntities.USERS.Find(userId);

            if (role.USERS.Contains(user))
            {
                role.USERS.Remove(user);
                modelEntities.SaveChanges();
            }

            return PartialView("_UserRoleList", user.ROLES);
        }

        private void SetViewBagData(int _userId)
        {
            ViewBag.UserId = _userId;
            ViewBag.RoleId = new SelectList(modelEntities.ROLES.OrderBy(p => p.RoleName), "Role_Id", "RoleName");
        }

    }

}