﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eCommerce.BackOffice.Startup))]
namespace eCommerce.BackOffice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
