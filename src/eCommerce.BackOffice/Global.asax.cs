﻿using System;
using System.Linq;
using System.Globalization;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Threading;
using eCommerce.Services.Model;
using System.Web;
using System.Web.Security;
using eCommerce.Services.Security;

namespace eCommerce.BackOffice
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;
        }

        protected void Application_BeginRequest()
        {
            //if (!Request.IsLocal && !Context.Request.IsSecureConnection)
            //{
            //    Response.Redirect(Context.Request.Url.ToString().Replace("http://", "https://"));
            //}
        }

        void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if ((Context.User != null) && (Context.User.Identity.IsAuthenticated))
            {
                string userInformation = String.Empty;
                string roles = string.Empty;
                string email = string.Empty;
                string fullName = string.Empty;

                HttpCookie authCookie = Request.Cookies["FibrahumanaBo" + Context.User.Identity.Name];

                if (authCookie == null || string.IsNullOrEmpty(authCookie.Value))
                {
                    DbModelEntities modelEntities = new DbModelEntities();
                    USER user = modelEntities.USERS.FirstOrDefault(a => a.Username == Context.User.Identity.Name && a.Active && a.Deleted == null);

                    if (user != null)
                    {
                        email = Context.User.Identity.Name;
                        fullName = user.Fullname;
                        roles = "Admin";

                        userInformation = roles + ";" + email + ";" + fullName;

                        // Create a cookie authentication ticket.
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                            1,                              // version
                            User.Identity.Name,             // user name
                            CurrentDate.Now,                // issue time
                            CurrentDate.Now.AddHours(1),    // expires every hour
                            false,                          // don't persist cookie
                            userInformation);

                        // Encrypt the ticket
                        String cookieStr = FormsAuthentication.Encrypt(ticket);

                        // Send the cookie to the client
                        Response.Cookies["FibrahumanaBo" + Context.User.Identity.Name].Value = cookieStr;
                        Response.Cookies["FibrahumanaBo" + Context.User.Identity.Name].Path = "/";
                        Response.Cookies["FibrahumanaBo" + Context.User.Identity.Name].Expires = CurrentDate.Now.AddMinutes(30);
                    }
                }
                else
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    userInformation = ticket.UserData;

                    string[] cookieInfo = userInformation.Split(new char[] { ';' });
                    roles = cookieInfo[0];
                    email = cookieInfo[1];
                    fullName = cookieInfo[2];
                }

                CustomIdentity customIdentity = new CustomIdentity(email, fullName,0,2);
                HttpContext.Current.User = new CustomPrincipal(customIdentity, roles);
            }
        }

        void Application_BeginRequest(object sender, EventArgs e)
        {
            CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            culture.DateTimeFormat.LongTimePattern = "HH:mm:ss";
            Thread.CurrentThread.CurrentCulture = culture;
        }
    }
}
