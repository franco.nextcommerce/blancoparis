﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.BackOffice.ViewModels
{
    public class OcaCentroDeImposicion
    {
        public string CentroId { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string CodigoPostal { get; set; }
        public string Telefono { get; set; }
        public string Descripcion { get; set; }
        public string Provincia { get; set; }
        public string Localidad { get; set; }
    }

    public enum OcaServiceType
    {
        AdmisionDePaquetes = 1,
        EntregaDePaquetes = 2
    }
}