﻿using eCommerce.Services.Model;
using System.Collections.Generic;

namespace eCommerce.BackOffice.ViewModels
{
    public class CustomerOrderHistoryViewModel
    {
        public Customer Customer { get; set; }
        public List<OrderViewModel> Orders { get; set; }
    }

}