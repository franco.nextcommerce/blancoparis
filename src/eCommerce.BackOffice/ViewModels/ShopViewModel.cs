﻿using System.Web.Mvc;
using eCommerce.Services.Model;
using System.Collections.Generic;
using System;

namespace eCommerce.BackOffice.ViewModels
{
    public class ShopViewModel
    {

    }

    public class CategoryViewModel {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ProductOrderViewModel {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Order { get; set; }
        public string ImageUrl { get; set; }
    }

}