﻿using System.Web.Mvc;
using eCommerce.Services.Model;
using System.Collections.Generic;
using System;

namespace eCommerce.BackOffice.ViewModels
{
    public class NewsSuscriberViewModel
    {
        public int NewsSuscriberId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateStr
        {
            get
            {
                return this.CreatedDate.ToShortDateString();
            }
        }
    }

}