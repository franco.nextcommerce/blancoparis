﻿using System.Web.Mvc;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using System.Collections.Generic;
using System;

namespace eCommerce.BackOffice.ViewModels
{
    public class StockMovementListViewModel
    {
        public List<StockMovementViewModel> StockMovements { get; set; }
        public StockExchangeViewModel StockExchange { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class StockMovementViewModel
    {
        public int StockMovementId { get; set; }
        public string ProductCode { get; set; }
        public string SizeName{ get; set; }
        public string ColorName { get; set; }
        public string DepositName { get; set; }
        public int MovementType { get; set; }
        public string MovementTypeName { get; set; }
        public string Concept{ get; set; }
        public int Quantity { get; set; }
        public DateTime Date { get; set; }
    }

}