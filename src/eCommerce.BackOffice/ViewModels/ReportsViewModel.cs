﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.BackOffice.ViewModels
{
    public class ReportsViewModel
    {
        public List<IndicatorItem> Items { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public decimal ClosedOrders { get; set; }
        public decimal ProcessOrders { get; set; }
        public decimal CanceledOrders { get; set; }

        public decimal ClosedOrdersUnitsSold { get; set; }
        public decimal ClosedOrdersTotalSales { get; set; }
        public decimal AverageOrderAmount { get; set; }
        public decimal OrderMobileTotalSales { get; set; }
        public decimal OrderDesktopTotalSales { get; set; }
        public decimal ClosedOrdersDesktop { get; set; }
        public decimal ClosedOrdersMobile { get; set; }

        public decimal RegisteredUsers { get; set; }
        public decimal ClosedOrdersByRegistersPercentage { get; set; }
        public decimal OrdersByRegistersPercentage { get; set; }
        public decimal CheckOutAbandonRate { get; set; }

        public List<RegionIndicator> RegionIndicators { get; set; }
        public List<RegionCustomer> RegionCustomers { get; set; }
        public List<MostSoldProductViewModel> MostSoldProducts { get; set; }
        public List<MostSoldUserViewModel> mostSoldUser { get; set; }


        public class RegionIndicator
        {
            public int ProvinceId { get; set; }
            public string ProvinceName { get; set; }
            public int ClosedOrders { get; set; }
            public decimal ClosedOrdersTotal { get; set; }
        }

        public class RegionCustomer
        {
            public int ProvinceId { get; set; }
            public string ProvinceName { get; set; }
            public int Count { get; set; }
        }
        public class IndicatorItem
        {
            public int OrderId { get; set; }
            public string Customer { get; set; }
            public decimal Amount { get; set; }
            public DateTime Date { get; set; }
        }
    }

    public class MostSoldProductViewModel
    {
        public string Title { get; set; }
        public int Count { get; set; }
    }

    public class MostSoldUserViewModel
    {
        public string Title { get; set; }
        public decimal Amount { get; set; }
    }

    public class ExcelReportsViewModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}