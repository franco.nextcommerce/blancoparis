﻿using System.Web.Mvc;
using eCommerce.Services.Model;
using System.Collections.Generic;
using System;

namespace eCommerce.BackOffice.ViewModels
{
    public class DeliveryMethodListViewModel
    {
        public int DeliveryMethodId { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Cost { get; set; }
        public bool RequiresData { get; set; }
        public bool IsTransport { get; set; }
        public bool IsLocal { get; set; }
        public bool IsProvince { get; set; }
        public bool ShowLocales { get; set; }
        public MultiSelectList ExcludedPaymentMethods { get; set; }
        public SelectList Form { get; set; }
    }

    //public class DeliveryMethodUpdateViewModel
    //{
    //    public List<DeliveryMethodListViewModel> DeliveryMethods { get; set; }
    //}
}