﻿namespace eCommerce.BackOffice.ViewModels
{
    public class StockListViewModel
    {
        public int ProductId { get; set; }
        public int SizeId { get; set; }
        public int ColorId { get; set; }
        public string Code { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string CategoryName { get; set; }
        public int StockVirtual { get; set; }
        public int StockReal { get; set; }
        public string DepositName { get; set; }
        public string Title { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? WholesalePrice { get; set; }
        public decimal? DistributorPrice { get; set; }
    }
}