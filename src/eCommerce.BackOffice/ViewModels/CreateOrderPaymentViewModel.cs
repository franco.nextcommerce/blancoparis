﻿using eCommerce.Services.Model;
using System.Collections.Generic;
using System.Web.Mvc;

namespace eCommerce.BackOffice.ViewModels
{
    public class CreateOrderPaymentViewModel
    {
        public Transaction Payment { get; set; }

        public List<TransactionDetailViewModel> OrderList { get; set; }
        
        public SelectList Accounts { get; set; }

        public SelectList Banks { get; set; }

        public List<Check> Checks { get; set; }

        public string ChecksIds { get; set; }

        public int CustomerId { get; set; }

        public int? OrderId { get; set; }

        public int? SalerId { get; set; }

        public Order Order { get; set; }

        public bool Edit { get; set; }
    }

    public class CreateMultipleOrderPaymentViewModel
    {
        public SelectList Accounts { get; set; }

        public SelectList Banks { get; set; }

        public List<Check> Checks { get; set; }

        public string ChecksIds { get; set; }

        public int CustomerId { get; set; }

        public int OrderId { get; set; }

        public Order Order { get; set; }

        public bool Edit { get; set; }

        public System.DateTime Date { get; set; }

        public int AccountId { get; set; }

        public int CurrencyId { get; set; }

        public decimal Amount { get; set; }

        public string Observations { get; set; }
    }

    public class TransactionDetailViewModel
    {
        public Order Order { get; set; }
        public decimal PendingAmountToPay { get; set; }
        public TransactionDetail TransactionDetail { get; set; }
        
    }
}