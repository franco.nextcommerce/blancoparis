﻿namespace eCommerce.BackOffice.ViewModels
{
    public class ProvinceViewModel
    {
        public int ProvinceId { get; set; }
        public string Name { get; set; }
        public decimal? Cost { get; set; }
    }

}