﻿using eCommerce.Services.Model;
using System.Collections.Generic;

namespace eCommerce.BackOffice.ViewModels
{
    public class SupplierAccountingViewModel
    {
        public Supplier Supplier { get; set; }
        public List<Transaction> Transactions { get; set; }
    }

}