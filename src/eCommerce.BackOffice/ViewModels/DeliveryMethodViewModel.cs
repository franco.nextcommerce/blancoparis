﻿using System.Web.Mvc;
using eCommerce.Services.Model;
using System.Collections.Generic;

namespace eCommerce.BackOffice.ViewModels
{
    public class DeliveryOrderSkusViewModel
    {
        public List<ProductSku> DeliverySkus { get; set; }
        public DeliveryOrder DeliveryOrder { get; set; }
    }

}