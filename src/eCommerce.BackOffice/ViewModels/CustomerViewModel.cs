﻿using System.Web.Mvc;
using eCommerce.Services.Model;
using System.Collections.Generic;
using System;

namespace eCommerce.BackOffice.ViewModels
{
    public class CustomerViewModel
    {
        public int CustomerId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Tipo { get; set; }
        public string CUIT { get; set; }
        public bool Enabled { get; set; }
        public bool Confirmed { get; set; }
        public bool Rejected { get; set; }
        public DateTime RegisterDate { get; set; }
        public bool IsFacebookUser { get; set; }
        public string RegisterDateStr
        {
            get
            {
                return this.RegisterDate.ToShortDateString();
            }
        }
    }

}