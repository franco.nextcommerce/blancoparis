﻿namespace eCommerce.BackOffice.ViewModels
{
    public class CityListViewModel
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public string ProvinceName { get; set; }
        public bool IsExclusive { get; set; }
    }

}