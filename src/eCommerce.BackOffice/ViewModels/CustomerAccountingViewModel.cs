﻿using eCommerce.Services.Model;
using System.Collections.Generic;

namespace eCommerce.BackOffice.ViewModels
{
    public class CustomerAccountingViewModel
    {
        public Customer Customer { get; set; }
        public List<Transaction> Transactions { get; set; }
    }

}