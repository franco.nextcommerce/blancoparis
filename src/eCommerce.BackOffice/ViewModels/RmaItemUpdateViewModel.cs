﻿using eCommerce.Services.Model;
using System.Collections.Generic;

namespace eCommerce.BackOffice.ViewModels
{
    public class RmaItemUpdateViewModel
    {
        public RmaItem RmaItem { get; set; }
        public decimal TransactionAmount { get; set; }
    }

}