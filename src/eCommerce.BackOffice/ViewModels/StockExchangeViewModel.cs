﻿using System.Web.Mvc;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using System.Collections.Generic;
using System;

namespace eCommerce.BackOffice.ViewModels
{
    public class StockExchangeViewModel
    {
        public SelectList Deposits { get; set; }
        public SelectList Products { get; set; }
        public SelectList Sizes { get; set; }
        public SelectList Colors { get; set; }
        public int OriginDepositId { get; set; }
        public int DestinyDepositId { get; set; }
        public int ProductId { get; set; }
        public int SizeId { get; set; }
        public int ColorId { get; set; }
        public int Quantity { get; set; }
        public int MovementType { get; set; }
        public string Concept { get; set; }
    }
}