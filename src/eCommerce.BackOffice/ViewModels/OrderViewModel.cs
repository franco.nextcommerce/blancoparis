﻿using System.Web.Mvc;
using eCommerce.Services.Model;
using System.Collections.Generic;
using System;
using eCommerce.Services.Model.Enums;

namespace eCommerce.BackOffice.ViewModels
{
    public class OrdersListPageViewModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class OrderListViewModel
    {
        public int OrderId { get; set; }
        public string PagoOnlineUrl { get; set; }
        public int? SalerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CustomerName { get; set; }
        public string SaleType { get; set; }
        public int CustomerId { get; set; }
        public int Channel { get; set; }
        public int DeliveryMethodId { get; set; }
        public int PaymentMethodId { get; set; }
        public decimal Total { get; set; }
        public decimal? ShippingCost { get; set; }
        public decimal Discount { get; set; }
        public int PaymentStatusId { get; set; }
        public int DeliveryStatusId { get; set; }
        public int OrderStatusId { get; set; }
        public int CurrencyId { get; set; }
        public string OrderType { get; set; }
        public string Saler { get; set; }
        public string CreatedDateStr
        {
            get
            {
                return this.CreatedDate.ToShortDateString();
            }
        }
        public string ChannelLabel
        {
            get
            {
                return GetChannelName(this.Channel);
            }
        }

        public string TotalLabel
        {
            get { return GetTotalName(this.ShippingCost, this.Total); }
        }

        public string DeliveryMethodLabel
        {
            get { return GetDeliveryMethodName(this.DeliveryMethodId); }
        }

        public string PaymentMethodLabel
        {
            get { return GetPaymentMethodName(this.PaymentMethodId); }
        }

        public string PaymentStatusLabel
        {
            get { return GetPaymentStatusName(this.PaymentStatusId); }
        }

        public string DeliveryStatusLabel
        {
            get { return GetDeliveryStatusName(this.DeliveryStatusId); }
        }

        public string OrderStatusLabel
        {
            get { return GetOrderStatusName(this.OrderStatusId); }
        }

        public string ChannelName { get; set; }
        public string PaymentMethodName { get; set; }
        public string DeliveryStatusName { get; set; }
        public string DeliveryMethodName { get; set; }
        public string TotalName { get; set; }
        public string PaymentStatusName { get; set; }
        public string OrderStatusName { get; set; }
        public int TotalRecords { get; set; }

        public static string GetPaymentMethodName(int id)
        {
            switch (id)
            {
                case (int)PaymentMethodEnum.TransferenciaBancaria:
                    return "<label>Transferencia</label>";
                //case (int)PaymentMethodEnum.MercadoPago:
                //    return "<label>Mercado Pago</label>";
                case 17:
                    return "<label>Mercado Pago</label>";
                case 2:
                    return "<label>Mercado Pago</label>";
                case (int)PaymentMethodEnum.Efectivo:
                    return "<label>Efectivo</label>";
                default:
                    return "<label>" + ((PaymentMethodEnum)id) + "</label>";
            }
        }

        public static string GetPaymentStatusName(int id)
        {
            switch (id)
            {
                case (int)PaymentStatusEnum.Pendiente:
                    return "<span class='label label-outline label-danger'>A COBRAR</span>";
                case (int)PaymentStatusEnum.Cobrada:
                    return "<span class='label label-outline label-success'>COBRADA</span>";
                default:
                    return "";
            }
        }

        public static string GetDeliveryMethodName(int id)
        {
            switch (id)
            {
                case (int)DeliveryMethodEnum.MotoEntrega:
                    return "<label>Moto Entrega</label>";
                case (int)DeliveryMethodEnum.OcaPaP:
                    return "<label>OCA - Domicilio</ label >";
                case (int)DeliveryMethodEnum.OcaPaS:
                    return "<label>OCA - Sucursal</label>";
                case (int)DeliveryMethodEnum.RetiraEnCentroDeEntrega:
                    return "<label>ShowRoom</label>";
                case (int)DeliveryMethodEnum.Transporte:
                    return "<label>Transporte </label>";
                case (int)DeliveryMethodEnum.AndreaniD:
                    return "<label>Andreani - Envío a domicilio</label>";
                case (int)DeliveryMethodEnum.AndreaniS:
                    return "<label>Andreani - Envío a sucursal</label>";
                case (int)DeliveryMethodEnum.DomicilioCaba:
                    return "<label>Envio a domicilio CABA</label>";
                case (int)DeliveryMethodEnum.DomicilioGba:
                    return "<label>Envio a domicilio GBA.</label>";
                case (int)DeliveryMethodEnum.AConvenir:
                    return "<label>A convenir</label>";
                case (int)DeliveryMethodEnum.RetiraLocal:
                    return "<label>Retira en local</label>";
                case (int)DeliveryMethodEnum.DomicilioProv:
                    return "<label>Envio a domicilio resto del País</label>";
                default:
                    return "";
            }
        }

        public static string GetDeliveryStatusName(int id)
        {
            switch (id)
            {
                case (int)DeliveryStatusEnum.Pendiente:
                    return "<span class='label label-outline label-danger'>A ENTREGAR</span>";
                case (int)DeliveryStatusEnum.EntregaParcial:
                    return "<span class='label label-outline label-warning'>ENTREGA PARCIAL</span>";
                case (int)DeliveryStatusEnum.Entregado:
                    return "<span class='label label-outline label-success'>ENTREGADO</span>";
                default:
                    return "";
            }
        }

        public static string GetOrderStatusName(int id)
        {
            switch (id)
            {
                case (int)OrderStatusEnum.Confirmado:
                    return "<span class='label label-outline label-primary'>CONFIRMADO</span>";
                case (int)OrderStatusEnum.Cancelada:
                    return "<span class='label label-dark'>CANCELADA</span>";
                case (int)OrderStatusEnum.Consigna:
                    return "<span class='label label-outline label-dark'>CONSIGNA</span>";
                case (int)OrderStatusEnum.EnProceso:
                    return "<span class='label label-outline label-warning'>EN PROCESO</span>";
                default:
                    return "";
            }
        }

        public static string GetTotalName(decimal? shippingCost, decimal total)
        {
            if (shippingCost.HasValue)
                return (total + shippingCost.Value).ToString("c2");

            return total.ToString("c2");
        }

        public static string GetChannelName(int id)
        {
            return ((OrderChannelEnum)id).ToString();
        }
    }

    public class OrderViewModel
    {
        public Order Order { get; set; }
        public int OrderId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CustomerName { get; set; }
        public int CustomerId { get; set; }
        public int Channel { get; set; }
        public int DeliveryMethodId { get; set; }
        public int PaymentMethodId { get; set; }
        public string UserType { get; set; }
        public decimal Total { get; set; }
        public int PaymentStatusId { get; set; }
        public int DeliveryStatusId { get; set; }
        public int OrderStatusId { get; set; }
    }
    public class OrderReportsViewModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class DeliveryOrderViewModel
    {
        public int DeliveryOrderId { get; set; }
        public Order Order { get; set; }
        public int? OrderId { get; set; }
        public int? RefundId { get; set; }
        public int? ExchangeId { get; set; }
        public DateTime Date { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public string ReferNumber { get; set; }
        public decimal? Amount { get; set; }
        public string Customer { get; set; }
    }

    public class InCartReportViewModel
    {

        public List<OrderInCartViewModel> Orders { get; set; }
        public int TotalQuantity { get; set; }
        public decimal TotalAmount { get; set; }


    }

    public class OrderInCartViewModel
    {
        //public List<OrderDetailInCartViewModel> OrderDetails { get; set; }
        public int CustomerId { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
        public string CustomerUserType { get; set; }
        public int TotalQuantity { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime LastItemAddedDate { get; set; }
        public DateTime? LastPushDate { get; set; }
    }

    public class CartDetailViewModel
    {
        public string CustomerName { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
    }
}