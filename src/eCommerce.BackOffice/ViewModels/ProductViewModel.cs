﻿using System.Web.Mvc;
using eCommerce.Services.Model;
using System.Collections.Generic;
using System;

namespace eCommerce.BackOffice.ViewModels
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string Code { get; set; }
        public string Title{ get; set; }
        public string Deposit { get; set; }
        public string CategoryCompleteName { get; set; }
        public string Cloth { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? OldRetailPrice { get; set; }
        public decimal? WholesalePrice { get; set; }
        public decimal? OldWholesalePrice { get; set; }
        public decimal? DistributorPrice { get; set; }
        public decimal? OnlinePrice { get; set; }
        public bool Published{ get; set; }
        public bool ShowInHome { get; set; }

        public bool IsOnSale { get; set; }

        public bool PublishedWholesaler { get; set; }
        public DateTime Created{ get; set; }
        public string PhisicalLocationModule { get; set; }
        public int? PhisicalLocationRow { get; set; }
        public decimal? MaxDiscount { get; set; }
        public int StockVirtual { get; set; }
        public int StockReal { get; set; }
    }

}