﻿$(document).ready(function () {
    
    $(".propertySelect").select2({
        placeholder: "Seleccione una propiedad"
    });
    $('.propertyMultiSelect').multiSelect('refresh');

});

function property1SelectChange(propertyId) {

    $('.changeSelect1').remove();

    if (propertyId != 0) {
        $.ajax({
            cache: false,
            data: { propertyId: propertyId },
            datatype: "text",
            method: "POST",
            success: function (result) { loadProperty1Change(result) },
            url: "/Products/GenerateViewForProperty1Select"
        });
    }

};

function loadProperty1Change(result) {

    $('#propertySelects').append(result);
    $(".propertySelect").select2({
        placeholder: "Seleccione una propiedad"
    });
    $('.propertyMultiSelect').multiSelect('refresh');

};

function property2SelectChange(propertyId) {

    $('.changeSelect2').remove();

    if (propertyId != 0) {
        $.ajax({
            cache: false,
            data: { propertyId: propertyId },
            datatype: "text",
            method: "POST",
            success: function (result) { loadProperty2Change(result) },
            url: "/Products/GenerateViewForProperty2Select"
        });
    }

};

function loadProperty2Change(result) {

    $('#propertySelects').append(result);
    $(".propertySelect").select2({
        placeholder: "Seleccione una propiedad"
    });
    $('.propertyMultiSelect').multiSelect('refresh');

};