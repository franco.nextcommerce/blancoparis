﻿var maximaCantidadImagenes = "1000";

function agregarImagen() {

    var Imagenes = $('#Imagenes');
    var cantidadImagenes = $('#contadorImages').val();
    cantidadImagenes++;

    Imagenes.append('<div class="form-group image_' + cantidadImagenes + '"> ' +
                    '<div class="col-sm-1"> ' +
                    '<div class="row"> ' +
                        '<label class="control-label pull-right"> Imagen ' + cantidadImagenes + '</label> ' +
                    '</div> ' +
                    '<div class="row"> ' +
                    '<div class="pull-right margin-vertical-10"> ' +
                        '<button type="button" class="btn btn-default btnPredeterminado" data-toggle="button" ' +
                        'aria-pressed="false" autocomplete="off" style="padding-left : 0px ; padding-right : 0px ; ' +
                        'padding-top: 0px ; padding-bottom : 0px ; width : 22px ; height: 22px ;" ' +
                        'onclick="toggleclass(this);" id="image_' + cantidadImagenes + '"> ' +
                        '<span class="glyphicon" aria-hidden="true"></span> ' +
                        '</button> ' +
                    '</div> ' +
                    '</div> ' +
                    '</div> ' +
                    '<div class="col-sm-4"> ' +
                    '<input type="file" class="insertImage image_' + cantidadImagenes + '" name="image" ' +
                    'data-plugin="dropify" data-default-file="" ' +
                    'data-disable-remove="false" onblur="actualizarNombreImagen(this)"/> ' +
                    '</div> ' +
                    '<div class="form-group">' +
                    '<label class="col-sm-1 control-label lblColorId">Color</label>' +
                    '<div class="col-sm-2">' +
                    '<select class="form-control stock-input stock-color js-image-color" name="ImageColorId_' + cantidadImagenes + '" id="ImageColorId_' + cantidadImagenes + '" data-plugin="select2">' +
                    '</select>' +
                    '</div>' +
                    '<label class="col-sm-1 control-label hidden">Tamaño</label>' +
                    '<div class="col-sm-2 hidden">' +

                    '<select class="form-control stock-input" name="ImageOrientation_' + cantidadImagenes + '" id="ImageOrientation_' + cantidadImagenes + '" data-plugin="select2">' +
                    '<option value="1">Normal</option>' +
                    '<option value="2">Apaisada</option>' +
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '</div>');

    SetColorsInput(cantidadImagenes);

    $('#ImageOrientation_' + cantidadImagenes).select2();
    var drEvent = $('.insertImage').dropify();

    $('#contadorImages').val(cantidadImagenes);
    drEvent.on('dropify.beforeClear', function (event, element) { return sePuedeBorrarImagenNueva(event, element); });

    bloquearAgregar();

};

function SetColorsInput(n) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Colors/GetColors",
        data: {  },
        success: function (data) {
            control = $("#ImageColorId_" + n);
            control.empty();
            control.select2();
            debugger;
            control.append($('<option value=""></option>'));
            control.append($('<option value="+">(+) Nuevo Color</option>'));
            $.each(data, function (index, item) {
                control.append($('<option value=' + item.ColorId + '>' + item.Description + '</option>'));
            });

            SetColorInputEvents(control);
        }
    });
}

function SetColorInputEvents() {
    control.change(function () {
        if ($(this).val() == "+") {
            $("#NewColor").modal();
        }
    });
}

$(document).ready(function ()
{
    var drEvent = $('.insertImage').dropify();
    drEvent.on('dropify.beforeClear', function (event, element) { return sePuedeBorrar(event, element); });
});


function sePuedeBorrar(event, element)
{
    //if (!esPredeterminado(event.currentTarget.classList[1]))
    //{
    //    agregarImagenesBorradas(element.element.classList[1]);
    //    return true;
    //}
    //else {
    //    toastr.warning('No puede borrarse la imagen predeterminada');
    //    return false;
    //}

    agregarImagenesBorradas(element.element.classList[1]);
    return true;
};

function sePuedeBorrarImagenNueva(event, element) {

    if (esPredeterminado(event.currentTarget.classList[1]))
    {
        toastr.warning('No puede borrarse la imagen predeterminada');
        return false;
    }
    return false;

};

$(document).ready(function () {
    bloquearAgregar();
});

function bloquearAgregar() {

    if ($('#contadorImages').val() == maximaCantidadImagenes) {
        $('#BtnAgregarImagen').prop('disabled', true);
    };

};

function toggleclassBlur(btnPredeterminado) {

    if ($('.' + btnPredeterminado.id + ' img')[0] != null) {
        cambiarPredeterminado(btnPredeterminado.id);
    }
    cambioDeCheck(btnPredeterminado)
};

function toggleclass(btnPredeterminado) {

    if ($('.' + btnPredeterminado.id + ' img')[0] != null) {

        cambioDeCheck(btnPredeterminado)
        cambiarPredeterminado(btnPredeterminado.id);

    }
    else {

        toastr.warning('Debe seleccionar una imagen válida como imagen predeterminada');

    }

};

function cambioDeCheck(btnPredeterminado) {

    $('.btnPredeterminado span').removeClass('glyphicon-ok');
    $('.btnPredeterminado').removeClass('btn-primary');
    $('.btnPredeterminado').removeClass('btn-default');
    $('.btnPredeterminado').prop('disabled', false);

    $('.btnPredeterminado').addClass('btn-default');
    $('#' + btnPredeterminado.id + ' span')[0].classList.add("glyphicon-ok");
    $('#' + btnPredeterminado.id)[0].classList.remove('btn-default');
    $('#' + btnPredeterminado.id)[0].classList.add('btn-primary');
    btnPredeterminado.disabled = true;

}

function esPredeterminado(id)
{
    return ($('#' + id + ' .glyphicon-ok')[0] != null);

};

function actualizarNombreImagen(btnPredeterminado) {

    if (esPredeterminado(btnPredeterminado.classList[1])) {

        cambiarPredeterminado(btnPredeterminado.classList[1]);

    }
    else {

        if ($('#defaultImageName').val() == "") {

            var check = $('#' + btnPredeterminado.classList[1])[0];
            toggleclassBlur(check);

        }

    }

};

function cambiarPredeterminado(id) {

    if ($('#defaultImageName').val() == "") {

        $('#btnSave')[0].disabled = false;

    };

    var nombreImagenPredeterminada = $('.' + id + ' .dropify-filename-inner')[0].innerText;
    $('#defaultImageName').val(nombreImagenPredeterminada);

}

function borrarImagenGuardada(input)
{
    agregarImagenesBorradas(input.classList[1]);

}

function agregarImagenesBorradas(imagen)
{
    var newVal = $('#erasedImages').val() == "" ? imagen : $('#erasedImages').val() + ';' + imagen;
    $('#erasedImages').val(newVal);
};