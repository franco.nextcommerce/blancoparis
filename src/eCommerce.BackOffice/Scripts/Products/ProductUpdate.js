﻿$(document).ready(function () {
    $('.form-horizontal').formValidation({
        framework: "bootstrap",
        button: {
            selector: '#btnSave',
            disabled: 'disabled'
        },
        icon: null,
        fields: {
            Title: {
                validators: {
                    notEmpty: {
                        message: 'El campo Título es requerido y no puede estar vacío.'
                    }
                }
            },
            Price: {
                validators: {
                    numeric: {
                        message: 'Ingrese un valor correcto.',
                        separator: ','
                    }
                }
            },
            Benefit: {
                validators: {
                    numeric: {
                        message: 'Ingrese un valor correcto.',
                        separator: ','
                    }
                }
            },
            Weight: {
                validators: {
                    numeric: {
                        message: 'Ingrese un valor correcto.',
                        separator: ','
                    }
                }
            },
            OldPrice: {
                validators: {
                    numeric: {
                        message: 'Ingrese un valor correcto.',
                        separator: ','
                    }
                }
            }
        }
    });

    $("#HeadingId").select2({
        placeholder: "Seleccione un rubro"
    });

    $("#Categories").select2({
        placeholder: "Seleccione una categoría"
    });

    $("#BrandId").select2({
        placeholder: "Seleccione una marca"
    });

    $('.category_select').select2();

    $('.attribute_select').select2({
        placeholder: "Seleccione un valor"
    });

});

function loadCategories(selectHeadings) {

    $('#categoryId').val("");
    $('#subCategories').html('');
    var headingId = selectHeadings.value;
    var categoriesSelect = $('#Categories');

    $.ajax({
        cache: false,
        data: { headingId: headingId },
        datatype: "text",
        method: "POST",
        success: function (result) { loadCategoriesOption(result, categoriesSelect) },
        url: "/Products/SearchCategoriesByHeading"
    });

};

function loadCategoriesOption(result, select) {

    if (result.Message == "") {
        $('#Categories').prop('disabled', '');
        select.html('');
        select.append($('<option>', {
            value: "",
            text: ""
        }));
        for (var i = 0; i < result.CategoryList.length; i++) {
            select.append($('<option>', {
                value: result.CategoryList[i].CategoryId,
                text: result.CategoryList[i].Name
            }));
        };
        select.val('').change();
    }
    else {
        toastr.warning(result.Message);
    }

};

function loadSubcategories(select) {

    $('#categoryId').val("");
    var categoryId = select.value;
    $.ajax({
        cache: false,
        data: { categoryId: categoryId },
        datatype: "text",
        method: "POST",
        success: function (result) { categoryData(result, select) },
        url: "/Products/SearchCategoriesByFatherCategory"
    });

};

function categoryData(result, select) {

    if (result.CategoryList == "") {

        $('#categoryId').val(select.value);
        RefreshAttributes(select.value, $('#ProductId').val());

        if (select.id == "Categories") {

            $('#subCategories').html('');

        }
        else {

            var treeLevel = select.id.split("_")[1];
            $('#categories_' + treeLevel + '_div').html('');

        }

    }
    else {

        var divId = "";
        var treeLevel = "";
        var newTreeLevel = "";
        if (select.id == "Categories") {

            divId = $('#subCategories');
            divId.html('');

            newTreeLevel = "1"

        }
        else {

            treeLevel = select.id.split("_")[1];
            divId = $('#categories_' + treeLevel + '_div');
            divId.html('');

            newTreeLevel = (parseInt(treeLevel) + 1).toString();

        }

        newSelect(divId, newTreeLevel);
        var selectId = $("#categories_" + newTreeLevel + "_select");
        for (var i = 0; i < result.CategoryList.length; i++) {
            selectId.append($('<option>', {
                value: result.CategoryList[i].CategoryId,
                text: result.CategoryList[i].Name
            }));
        }

    }

};

function newSelect(divId, treeLevel) {

    divId.append(
        '<div class="col-sm-5">' +
        '<select class="form-control" id="categories_' + treeLevel + '_select" ' +
        'onchange="loadSubcategories(this);">' +
        '<option value="" selected="selected"></option>' +
        '</select>' +
        '</div>'
    );

    divId.append(
        '<div class="col-sm-12 margin-top-20" id="categories_' + treeLevel + '_div">' +
        '</div>'
    );

    $("#categories_" + treeLevel + "_select").select2({
        placeholder: "Seleccione una categoria"
    });

};

function RefreshAttributes(categoryId, productId) {

    attributeList = $('#Attributes');

    $.ajax({
        cache: false,
        datatype: "text",
        method: "POST",
        data: {
            categoryId: categoryId,
            productId: productId
        },
        success: function (result) {
            attributeList.html('');
            attributeList.html(result);
            $('.attribute_select').select2({
                placeholder: "Seleccione un valor"
            });
        },
        url: "/Products/SearchAttributesByCategory"
    });

};