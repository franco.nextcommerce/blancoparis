﻿$(document).ready(function () {

    $('#deliveryForm').formValidation({
        framework: "bootstrap",
        button: {
            selector: '#btnSave',
            disabled: 'disabled'
        },
        icon: null,
        fields: {
            Date: {
                validators: {
                    notEmpty: {
                        message: 'Debe ingresarse la Fecha de Entrega'
                    }
                }
            }
        }
    });

    $('.DateInput').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });

    prepareICheck();

    var drEvent = $('#image').dropify();
    drEvent.on('dropify.beforeClear', function (event, element) { $("#flagDeleteImage").val(true); });
});

function prepareICheck() {

    $('.enabledCheck').iCheck({
        checkboxClass: 'icheckbox_flat-blue'
    });
    $('.enabledCheck').on('ifToggled', function (event) {
        toogleTextbox(event.target.id, event.target.checked);
    });
};

function toogleTextbox(checkbox, value)
{
    debugger;
    var textBoxSelector = $('#input_' + checkbox);
    textBoxSelector.attr('disabled', !value);
    textBoxSelector.val('');

    $('#hidden_' + checkbox).val(value);

    GetStockDeposits(checkbox, value);    
}

function GetStockDeposits(index, value)
{
    var productId = $("#productId-" + index).val();
    var sizeId = $("#sizeId-" + index).val();
    var colorId = $("#colorId-" + index).val();
    var control = $('#deposit-' + index);

    if ($('#deposit-' + index + ' option').size() == 0 && value)
    {
        $.ajax({
            url: "/Products/GetDepositsAndStock",
            type: "GET",
            data: { productId: productId, colorId: colorId, sizeId: sizeId },
            success: function (data, textStatus, jqXHR)
            {
                if (data.length > 0)
                {
                    control.empty();
                    $.each(data, function (index, item) {
                        control.append($('<option value=' + item.DepositId + '>' + item.DepositName + ': ' + item.Stock + '</option>'));
                    });
                    control.attr('disabled', false);
                }
                else {
                    toastr.warning("El artículo no posee stock en ningún depósito.");
                }
            },
            error: function (request, status, error) {
                toastr.warning("Se produjo un error al interntar obtener los depósitos.");
            }
        });
    }
    else
    {
        control.attr('disabled', !value);
    }
}

function SelectAll()
{
    event.preventDefault();
    $('.enabledCheck').each(function ()
    {
        if(!$(this).parent().hasClass("checked"))
        {
            $(this).parent().addClass("checked");
            toogleTextbox("chk_" + $(this).attr('id').split('_')[1], true);
            $("#input_chk_" + $(this).attr('id').split('_')[1]).val($("#pending-" + $(this).attr('id').split('_')[1]).val());
            $("#selectAllBtn").text("Deseleccionar Todos");
            $("#selectAllBtn").attr("onclick","UnselectAll()");
        }
        else
        {
            $("#selectAllBtn").text("Deseleccionar Todos");
            $("#selectAllBtn").attr("onclick", "UnselectAll()");
        }
    });
}

function UnselectAll()
{
    event.preventDefault();
    $('.enabledCheck').each(function ()
    {
        if (!$(this).parent().hasClass("checked")) {
            $("#selectAllBtn").text("Seleccionar Todos");
            $("#selectAllBtn").attr("onclick", "SelectAll()");
        }
        else
        {
            $(this).parent().removeClass("checked");
            toogleTextbox("chk_" + $(this).attr('id').split('_')[1], false);
            $("#input_chk_" + $(this).attr('id').split('_')[1]).val('');
            $("#selectAllBtn").text("Seleccionar Todos");
            $("#selectAllBtn").attr("onclick", "SelectAll()");
        }
    });
}