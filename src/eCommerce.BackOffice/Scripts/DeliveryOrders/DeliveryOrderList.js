﻿$(document).ready(function () {
    

});

function deleteDelivery(id, description) {

    swal({
        title: "Borrado de entrega",
        text: "¿Desea anular la entrega correspondiente al remito -" + description + "-?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        closeOnConfirm: true
    },
        function () {
            generateDeliveryDelete(id);
        });

};

function generateDeliveryDelete(id) {

    $.ajax({
        cache: false,
        data: { id: id },
        datatype: "text",
        method: "POST",
        success: function (result) { deliverySaveReturn(result); },
        url: "/DeliveryOrders/DeleteDelivery"
    });

};

function deliverySaveReturn(result) {

    switch (result.messageType) {
        case "info":
            toastr.info(result.result);
            break;
        case "warning":
            toastr.warning(result.result);
            break;
        case "success":
            toastr.success(result.result);
            break;
        case "error":
            toastr.error(result.result);
            break;
        default:
            null;
    }

    refreshDeliveries();

};

function refreshDeliveries() {

    var deliveryList = $('#deliveryList');
    var orderId = $('#orderIdHidden').val();

    $.ajax({
        cache: false,
        datatype: "text",
        method: "POST",
        data: { orderId: orderId },
        success: function (result) {
            deliveryList.html('');
            deliveryList.html(result);
            ///configureDatatableSpanish('#deliveryList table');
            $('#newDeliveryButton')[0].disabled = false
        },
        url: "/DeliveryOrders/DeliveryPartialRefresh"
    });

};

function ShowModalDeliveryDetail(deliveryOrderId) {

    var deliveryDetailDiv = $('#deliveryDetailDiv');
    deliveryDetailDiv.html('');

    $.ajax({
        cache: false,
        data: { deliveryOrderId: deliveryOrderId },
        datatype: "text",
        method: "POST",
        success: function (result) {
            debugger;
            deliveryDetailDiv.html(result);
            $('#ModalDeliveryDetail').modal('show');
            //configureDatatableSpanish('#ModalDeliveryDetail table');
        },
        url: "/DeliveryOrders/GetDeliveryDetail"
    });

};

function ShowModalDeliveryOrderGuide(deliveryOrderId) {

    var div = $('#deliveryOrderGuideDiv');
    div.html('');

    $.ajax({
        cache: false,
        data: { deliveryOrderId: deliveryOrderId },
        datatype: "text",
        method: "POST",
        success: function (result) {
            debugger;
            div.html(result);
            $('#ModalDeliveryOrderGuide').modal('show');
            //configureDatatableSpanish('#ModalDeliveryDetail table');
        },
        url: "/DeliveryOrders/DeliveryOrderGuideModal"
    });

};

function ShowDeliverySkus(deliveryOrderId)
{
    window.location = '/DeliveryOrders/GetDeliverySkus?deliveryOrderId=' + deliveryOrderId;
};

function SendGuideMail(deliveryOrderId)
{
    debugger;
    event.preventDefault();
    $("#SendBtn").attr('disabled', 'true');
    $.ajax({
        cache: false,
        data: { deliveryOrderId: deliveryOrderId },
        datatype: "text",
        method: "GET",
        success: function (result) {
            if (result == "") {
                debugger;
                CloseModal(true);
            } else {
                toastr.error("Ha habido une error al enviar el mail");
            }
        },
        url: "/DeliveryOrders/SendGuideMail"
    });
}

function CloseModal(sendMail)
{
    event.preventDefault();
    $("#ModalDeliveryOrderGuide").modal('toggle');
    if (sendMail) {
        toastr.success("Mail enviado");
    }
}