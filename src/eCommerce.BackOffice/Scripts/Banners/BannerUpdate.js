﻿$(document).ready(function () {

    $('#bannerForm').formValidation({
        framework: "bootstrap",
        button: {
            selector: '#btnSave',
            disabled: 'disabled'
        },
        icon: null,
        fields: {
            Name: {
                validators: {
                    stringLength: {
                        message: 'El nombre del atributo no puede contener mas de 255 caracteres',
                        max: 255
                    },
                    notEmpty: {
                        message: 'Debe ingresarse el Nombre'
                    }
                }
            }
        }
    });

});

var drEvent = $('#fullImage').dropify();
drEvent.on('dropify.beforeClear', function (event, element) { $("#flagFullImageDelete").val(true); });

var drEvent = $('#squareImage').dropify();
drEvent.on('dropify.beforeClear', function (event, element) { $("#flagSquareImageDelete").val(true); });