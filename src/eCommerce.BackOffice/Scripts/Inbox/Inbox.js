﻿$(document).ready(function () {
    //setActionsDialogs();
})

function fDelete(Id) {
    $.ajax({
        url: "/Inbox/ContactDelete/" + Id,
        type: "GET",
        data: null,
        success: function (data, textStatus, jqXHR) {
            $('.panel-body').html(data);
            $('.dataTable').DataTable();
            //setActionsDialogs();
            swal("Eliminado!", "", "success");
        }
    });
}

function fDeleteDialog(contactId)
{
    var contactId = contactId;
    swal({
        title: "Confirma su acción de Eliminar?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#F26A54',
        confirmButtonText: 'Sí, deseo elminarlo!',
        cancelButtonText: 'Cancelar',
        closeOnConfirm: false,
        closeOnCancel: true
    },
    function (isConfirm) {
        if (isConfirm) {
            fDelete(contactId);
        }
    });
}

function fMarkAsReadDialog(contactId) {
    var contactId = contactId;
    swal({
        title: "Marcar el mensaje como leído?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#F26A54',
        confirmButtonText: 'Sí',
        cancelButtonText: 'Cancelar',
        closeOnConfirm: false,
        closeOnCancel: true
    },
    function (isConfirm) {
        if (isConfirm) {
            markAsRead(contactId);
        }
    });
}

function fMarkAsNoReadDialog(contactId) {
    var contactId = contactId;
    swal({
        title: "Marcar el mensaje como no leído?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#F26A54',
        confirmButtonText: 'Sí',
        cancelButtonText: 'Cancelar',
        closeOnConfirm: false,
        closeOnCancel: true
    },
    function (isConfirm) {
        if (isConfirm) {
            markAsNoRead(contactId);
        }
    });
}

function markAsRead(Id)
{
    $.ajax({
        url: "/Inbox/MarkAsRead/" + Id,
        type: "POST",
        data: null,
        success: function (data, textStatus, jqXHR)
        {
            $('.panel-body').html(data);
            $('.dataTable').DataTable();
            //setActionsDialogs();
            swal("Confirmado", "Mensaje guardado como leido.", "success");
        }
    });
}

function markAsNoRead(Id) {
    $.ajax({
        url: "/Inbox/MarkAsNoRead/" + Id,
        type: "POST",
        data: null,
        success: function (data, textStatus, jqXHR) {
            $('.panel-body').html(data);
            $('.dataTable').DataTable();
            //setActionsDialogs();
            swal("Confirmado", "Mensaje guardado como no leido.", "success");
        }
    });
}

