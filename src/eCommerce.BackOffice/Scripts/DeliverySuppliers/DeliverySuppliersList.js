﻿$(document).ready(function () {

    //configureDatatableSpanish('#deliveryList table');

});

function deleteDelivery(id, description) {

    swal({
        title: "Borrado de entrega",
        text: "¿Desea anular la entrega correspondiente al remito -" + description + "-?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        closeOnConfirm: true
    },
        function () {
            generateDeliveryDelete(id);
        });

};

function generateDeliveryDelete(id) {

    $.ajax({
        cache: false,
        data: { id: id },
        datatype: "text",
        method: "POST",
        success: function (result) { deliverySaveReturn(result); },
        url: "/DeliverySuppliers/DeleteDelivery"
    });

};

function deliverySaveReturn(result) {

    switch (result.messageType) {
        case "info":
            toastr.info(result.result);
            break;
        case "warning":
            toastr.warning(result.result);
            break;
        case "success":
            toastr.success(result.result);
            break;
        case "error":
            toastr.error(result.result);
            break;
        default:
            null;
    }

    refreshDeliveries();

};

function refreshDeliveries() {

    var deliveryList = $('#deliveryList');
    var purchaseId = $('#purchaseIdHidden').val();

    $.ajax({
        cache: false,
        datatype: "text",
        method: "POST",
        data: { purchaseId: purchaseId },
        success: function (result) {
            deliveryList.html('');
            deliveryList.html(result);
            //configureDatatableSpanish('#deliveryList table');
            $('#newDeliveryButton')[0].disabled = false
        },
        url: "/DeliverySuppliers/DeliveryPartialRefresh"
    });

};

function ShowModalDeliveryDetail(deliverySuppliersId) {

    var deliveryDetailDiv = $('#deliveryDetailDiv');
    deliveryDetailDiv.html('');

    $.ajax({
        cache: false,
        data: { deliverySuppliersId: deliverySuppliersId },
        datatype: "text",
        method: "POST",
        success: function (result) {
            debugger;
            deliveryDetailDiv.html(result);
            $('#ModalDeliveryDetail').modal('show');
            //configureDatatableSpanish('#ModalDeliveryDetail table');
        },
        url: "/DeliverySuppliers/GetDeliveryDetail"
    });

};