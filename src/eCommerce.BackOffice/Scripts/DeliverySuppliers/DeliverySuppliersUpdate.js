﻿$(document).ready(function () {

    $('#deliveryForm').formValidation({
        framework: "bootstrap",
        button: {
            selector: '#btnSave',
            disabled: 'disabled'
        },
        icon: null,
        fields: {
            Date: {
                validators: {
                    notEmpty: {
                        message: 'Debe ingresarse la Fecha de Entrega'
                    }
                }
            }
        }
    });

    $('.DateInput').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });

    prepareICheck();

});

function prepareICheck() {

    $('.enabledCheck').iCheck({
        checkboxClass: 'icheckbox_flat-blue'
    });
    $('.enabledCheck').on('ifToggled', function (event) {
        toogleTextbox(event.target.id, event.target.checked);
    });

};

function toogleTextbox(checkbox, value) {

    var textBoxSelector = $('#input_' + checkbox);
    textBoxSelector.attr('disabled', !value);
    textBoxSelector.val('');
    $('#hidden_' + checkbox).val(value);

}