﻿$(document).ready(function () {

    //Datos Orden
    $('.validation').formValidation({
        framework: "bootstrap",
        button: {
            selector: '#btnSave',
            disabled: 'disabled'
        },
        icon: null,
        fields: {
            CustomerId: {
                validators: {
                    notEmpty: {
                        message: 'El campo Cliente es requerido.'
                    }
                }
            },
            Date: {
                validators: {
                    notEmpty: {
                        message: 'El campo Fecha es requerido.'
                    }
                }
            },
            Channel: {
                validators: {
                    notEmpty: {
                        message: 'El campo Canal es requerido.'
                    }
                }
            },
            DeliveryMethodId: {
                validators: {
                    notEmpty: {
                        message: 'El campo Forma de Entrega es requerido.'
                    }
                }
            },
            PaymentMethodId: {
                validators: {
                    notEmpty: {
                        message: 'El campo Forma de Pago es requerido.'
                    }
                }
            }
        }
    });

    //Datos nuevo cliente.
    $('#modalCustomer').formValidation({
        framework: "bootstrap",
        button: {
            selector: '#btnModalCustomer'
        },
        icon: null,
        fields: {
            FullName: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese el nombre del cliente.'
                    }
                }
            },
            Email: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese una cuenta de Email'
                    },
                    emailAddress: {
                        message: 'El email ingresado es inválido'
                    }
                }
            },
            UserType: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione el Tipo de Cliente'
                    }
                }
            },
            PriceList: {
                validators: {
                    notEmpty: {
                        message: 'Seleccionar un Lista de Precios'
                    },
                }
            }
        }
    });

    $("#CityId").CascadingDropDown("#Province", '/Customers/AsyncCities/',
    {
        promptText: 'Elija una Ciudad',
        postData: function () {
            return { ProvinceId: $('#Province').val() };
        },
        onLoading: function () {
            $(this).css("background-color", "#ffffff");
        },
        onLoaded: function () {
            //$(this).val('@Model.CityId.ToString()').change();
            $(this).change();
            $(this).animate({ backgroundColor: '#ffffff' }, 300);
        }
    });

    $('#Province').change();

});


function guardarDatosCliente() {

    var newCustomer = {
        FullName: null,
        Email: null,
        UserType: null,
        PriceList: null,
        PhoneNumber: null,
        Cellphone: null
    }

    debugger;

    newCustomer.FullName = $('#FullName').val();
    newCustomer.Email = $('#Email').val();
    newCustomer.UserType = $('#UserType').val();
    newCustomer.PriceList = $('#ddlPriceList').val();
    newCustomer.PhoneNumber = $('#PhoneNumber').val();
    newCustomer.Cellphone = $('#Cellphone').val();
    
    $.ajax({
        cache: false,
        data: { model: newCustomer },
        datatype: "text",
        method: "POST",
        success: function (result) { respuestaGuardadoCliente(result); },
        url: "/Customers/ModalCreateCustomer"
    });
};


function respuestaGuardadoCliente(result)
{
    switch (result.tipoMensaje)
    {
        case "success":
            toastr.success(result.resultado);
            actualizarClientes(result);
            break;

        case "error":
            toastr.error(result.resultado);
            break;
    }
};

function actualizarClientes(result)
{
    $("#CustomerId").append($('<option value=' + result.CustomerId + '>' + result.FullName + '</option>'));
    $("#CustomerId").val(result.CustomerId);
    $("#CustomerId").change();

    //$('#NewCustomer').modal('toggle');
    limpiarModalCliente();
}

function limpiarModalCliente()
{
    $('#FullName').val("");
    $('#Email').val("");
    $('#PhoneNumber').val("");
    $('#Cellphone').val("");
    $('#UserType').val("");
    $('#ddlPriceList').val("");
};

function validarStock()
{
    $.ajax({
        type: "POST",
        cache: false,
        url: "/Orders/ValidarStock",
        data: $(".validation").serializeArray(),
        async: false,
        success: function (data) {
            debugger;
            if (data.length > 0) {
                var msg = "Los siguientes productos no poseen stock suficiente: <br />";
                for (var i=0; i < data.length; i++)
                {
                    msg += data[i] + " <br />";
                }
                toastr.error(msg);
                event.preventDefault();
            }
        }
    })
}
$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});