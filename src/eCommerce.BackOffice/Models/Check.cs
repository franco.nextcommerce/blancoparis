﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerce.BackOffice.Models
{
    [MetadataType(typeof(CheckMetadata))]
    public partial class Check
    {
    }

    public class CheckMetadata
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CheckId { get; set; }
    }
}