﻿using System;

namespace eCommerce.BackOffice.Models
{
    public class BasicFilterViewModel
    {
        public int Status { get; set; }

        public int FilterView { get; set; }

        public string StartingDate { get; set; }

        public string EndingDate { get; set; }

        public DateTime? ParsedStartingDate { get { return !string.IsNullOrWhiteSpace(StartingDate) ? DateTime.ParseExact(StartingDate, "dd/MM/yyyy", null) : new DateTime?(); } }

        public DateTime? ParsedEndingDate { get { return !string.IsNullOrWhiteSpace(StartingDate) ? DateTime.ParseExact(EndingDate, "dd/MM/yyyy", null) : new DateTime?(); } }
    }

    public class ChecksFilterViewModel : BasicFilterViewModel
    {
        public int DaysToDue { get; set; }

        public int SupplierId { get; set; }
    }
}