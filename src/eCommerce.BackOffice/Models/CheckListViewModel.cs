﻿using eCommerce.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.BackOffice.Models
{
    public class CheckListViewModel
    {
        public CheckListViewModel()
        {

        }

        public List<eCommerce.Services.Model.Check> Checks { get; set; }

        public ChecksFilterViewModel Filter { get; set; }

        public SelectList Works { get; set; }

        public SelectList Suppliers { get; set; }

    }
}