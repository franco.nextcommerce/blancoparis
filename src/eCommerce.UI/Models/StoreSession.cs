﻿
using eCommerce.Services.Common;
using eCommerce.UI.Models;
using System.Collections.Generic;

namespace Contabilidad.MvcWebApp.Models
{
    public class StoreSession : SessionInfo<StoreSession>
    {
        public string CurrentHeading { get; set; }

        public Menu Menu { get; set; }

        public ShoppingCart ShoppingCart { get; set; }

    }
}

