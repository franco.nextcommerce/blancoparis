﻿using eCommerce.Services.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace eCommerce.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

           // #region Minoristas

           // routes.MapRoute(
           //     name: "Minoristas home",
           //     url: "minoristas",
           //     defaults: new { controller = "Home", action = "HomeView" }
           // );

           // routes.MapRoute(
           //     name: "Minoristas - Buscar",
           //     url: "minoristas/shop/buscar",
           //     defaults: new { controller = "Shop", action = "Buscar" }
           // );

           // routes.MapRoute(
           //     name: "Minoristas Shop",
           //     url: "minoristas/shop/{categoryName}/{productName}",
           //     defaults: new { controller = "Shop", action = "Index", categoryName = UrlParameter.Optional, productName = UrlParameter.Optional }
           // );

           // routes.MapRoute(
           //      name: "Minoristas login",
           //      url: "minoristas/login",
           //      defaults: new { controller = "Account", action = "Login" }
           // );

           // routes.MapRoute(
           //   name: "Minoristas registro",
           //   url: "minoristas/registro",
           //   defaults: new { controller = "Account", action = "Register" }
           // );

           // routes.MapRoute(
           //   name: "Minoristas contacto",
           //   url: "minoristas/contacto",
           //   defaults: new { controller = "Institucional", action = "Contacto" }
           // );

           // routes.MapRoute(
           //  name: "Minoristas - Locales",
           //  url: "minoristas/puntosdeventa",
           //  defaults: new { controller = "Institucional", action = "Locales" }
           //  );

           // routes.MapRoute(
           //     name: "Minoristas - Faq",
           //     url: "minoristas/faq",
           //     defaults: new { controller = "Institucional", action = "Faq" }
           // );

           // routes.MapRoute(
           //     name: "Minoristas - ComoComprar",
           //     url: "minoristas/comocomprar",
           //     defaults: new { controller = "Institucional", action = "ComoComprar" }
           // );

           // routes.MapRoute(
           //    name: "Minoristas - Carrito",
           //    url: "minoristas/shopping-cart",
           //    defaults: new { controller = "ShoppingCart", action = "Index" }
           // );

           // routes.MapRoute(
           //   name: "Minoristas - Information",
           //   url: "minoristas/account/information",
           //   defaults: new { controller = "Account", action = "Information" }
           // );

           // routes.MapRoute(
           //    name: "Minoristas - Checkout",
           //    url: "minoristas/checkout",
           //    defaults: new { controller = "ShoppingCart", action = "Checkout" }
           // );

           // routes.MapRoute(
           //     name: "Minoristas - Mis Datos",
           //     url: "minoristas/misdatos",
           //     defaults: new { controller = "Account", action = "MyAccount" }
           // );

           // routes.MapRoute(
           //     name: "Minoristas promotional",
           //     url: "minoristas/Account/CheckPromotionsBanners",
           //     defaults: new { controller = "Account", action = "CheckPromotionsBanners" }
           // );

           // #endregion

           // #region Mayoristas

           // routes.MapRoute(
           //     name: "Mayoristas - Home",
           //     url: "mayoristas",
           //     defaults: new { controller = "Home", action = "HomeView" }
           // );

           // routes.MapRoute(
           //    name: "Mayoristas - Buscar",
           //    url: "mayoristas/shop/buscar",
           //    defaults: new { controller = "Shop", action = "Buscar" }
           //);

           // routes.MapRoute(
           //     name: "Mayoristas - Shop",
           //     url: "mayoristas/shop/{categoryName}/{productName}",
           //     defaults: new { controller = "Shop", action = "Index", categoryName = UrlParameter.Optional, productName = UrlParameter.Optional }
           // );

           // routes.MapRoute(
           //      name: "Mayoristas - Login",
           //      url: "mayoristas/login",
           //      defaults: new { controller = "Account", action = "Login" }
           // );

           // routes.MapRoute(
           //   name: "Mayoristas - Registro",
           //   url: "mayoristas/registro",
           //   defaults: new { controller = "Account", action = "Register" }
           // );

           // routes.MapRoute(
           //   name: "Mayoristas - Contacto",
           //   url: "mayoristas/contacto",
           //   defaults: new { controller = "Institucional", action = "Contacto" }
           // );

           // routes.MapRoute(
           //  name: "Mayorista - Locales",
           //  url: "mayoristas/puntosdeventa",
           //  defaults: new { controller = "Institucional", action = "Locales" }
           //  );

           // routes.MapRoute(
           //     name: "Mayorista - Faq",
           //     url: "mayoristas/faq",
           //     defaults: new { controller = "Institucional", action = "Faq" }
           // );

           // routes.MapRoute(
           //     name: "Mayoristas - ComoComprar",
           //     url: "mayoristas/comocomprar",
           //     defaults: new { controller = "Institucional", action = "ComoComprar" }
           // );

           // routes.MapRoute(
           //    name: "Mayorista - Carrito",
           //    url: "mayoristas/shopping-cart",
           //    defaults: new { controller = "ShoppingCart", action = "Index" }
           // );

           // routes.MapRoute(
           //   name: "Mayorista - Information",
           //   url: "mayoristas/account/information",
           //   defaults: new { controller = "Account", action = "Information" }
           // );

           // routes.MapRoute(
           //    name: "Mayorista - Checkout",
           //    url: "mayoristas/checkout",
           //    defaults: new { controller = "ShoppingCart", action = "Checkout" }
           // );

           // routes.MapRoute(
           //     name: "Mayoristas - Mis Datos",
           //     url: "mayoristas/misdatos",
           //     defaults: new { controller = "Account", action = "MyAccount" }
           // );

           // routes.MapRoute(
           //     name: "Mayoristas promotional",
           //     url: "mayoristas/Account/CheckPromotionsBanners",
           //     defaults: new { controller = "Account", action = "CheckPromotionsBanners" }
           // );

           // #endregion

            routes.MapRoute(
                name: "Pago-Online",
                url: "pago-online/{guid}",
                defaults: new { controller = "Payment", action = "PagoDecidir", guid = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "Landing",
            //    url: "landing",
            //    defaults: new { controller = "Home", action = "Landing" }
            //);

            routes.MapRoute(
                name: "Wishlist",
                url: "wishlist",
                defaults: new { controller = "ShoppingCart", action = "Wishlist" }
            );

            routes.MapRoute(
                name: "Search",
                url: "shop/Buscar/{term}",
                defaults: new { controller = "Shop", action = "Buscar", term = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Shop",
                url: "shop/{categoryName}/{productName}",
                defaults: new { controller = "Shop", action = "Index", categoryName = UrlParameter.Optional, productName = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "registro",
              url: "registro",
              defaults: new { controller = "Account", action = "Register" }
            );

            routes.MapRoute(
                 name: "Login",
                 url: "login",
                 defaults: new { controller = "Account", action = "Login" }
            );

            routes.MapRoute(
                name: "ShoppingCart",
                url: "shopping-cart",
                defaults: new { controller = "ShoppingCart", action = "Index" }
            );

            routes.MapRoute(
               name: "Checkout",
               url: "checkout",
               defaults: new { controller = "ShoppingCart", action = "Checkout" }
            );


            routes.MapRoute(
                name: "ComoComprar",
                url: "comocomprar",
                defaults: new { controller = "Institucional", action = "ComoComprar" }
            );

            routes.MapRoute(
               name: "TermsAndCondition",
               url: "TermsAndConditions",
               defaults: new { controller = "Institucional", action = "TermsAndConditions" }
           );

            routes.MapRoute(
                name: "faq",
                url: "faq",
                defaults: new { controller = "institucional", action = "faq" }
            );

            routes.MapRoute(
             name: "Locales",
             url: "puntosdeventa",
             defaults: new { controller = "Institucional", action = "Locales" }
             );

            routes.MapRoute(
                name: "Venta mayorista",
                url: "mayoristas",
                defaults: new { controller = "Institucional", action = "Mayoristas" }
            );

            routes.MapRoute(
                name: "Sobre Nosotros",
                url: "Empresa",
                defaults: new { controller = "Institucional", action = "Empresa" }
                );

            routes.MapRoute(
                name: "Contacto",
                url: "contacto",
                defaults: new { controller = "Institucional", action = "Contacto" }
            );

            routes.MapRoute(
                name: "Empresa",
                url: "empresa",
                defaults: new { controller = "Institucional", action = "Empresa" }
            );

            routes.MapRoute(
                name: "Rss Minoritsa",
                url: "minoristas/rss/{CategoryName}",
                defaults: new { controller = "Rss", action = "Index", CategoryName = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Rss Mayorista",
                url: "mayoristas/rss/{CategoryName}",
                defaults: new { controller = "Rss", action = "Index", CategoryName = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "Mayoristas Grl",
                url: "mayoristas/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Minoristas Grl",
                url: "minoristas/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}
