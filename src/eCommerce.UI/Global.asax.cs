﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Security;
using eCommerce.Services.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace eCommerce.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new ModuleViewEngine());

            AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;
        }

        protected void Application_BeginRequest()
        {
            //if (!Request.IsLocal && !Context.Request.IsSecureConnection)
            //{
            //    Response.Redirect(Context.Request.Url.ToString().Replace("http://", "https://"));
            //}
        }

        //void Application_AuthenticateRequest(object sender, EventArgs e)
        //{
        //    if ((Context.User != null) && (Context.User.Identity.IsAuthenticated))
        //    {
        //        string userInformation = String.Empty; //where the cookie info will be placed
        //        string roles = string.Empty;
        //        string identifier = string.Empty;
        //        string fullName = string.Empty;
        //        int customerPriceList = 0;
        //        int customerId = 0;

        //        DbModelEntities modelEntities = new DbModelEntities();
        //        Customer customer = modelEntities.Customers.
        //            FirstOrDefault(a => a.Active && (a.Email == Context.User.Identity.Name || a.CustomerExternalLogins.Any(c => c.ExternalId == Context.User.Identity.Name)));

        //        if (customer != null)
        //        {
        //            roles = "User";
        //            userInformation = roles + ";" + Context.User.Identity.Name + ";" + customer.FullName + ";" + customer.CustomerId + ";" + customer.PriceList;

        //            identifier = Context.User.Identity.Name;
        //            fullName = customer.FullName;
        //            customerId = customer.CustomerId;
        //            customerPriceList = customer.PriceList;

        //            CustomIdentity customIdentity = new CustomIdentity(identifier, fullName, customerId, customerPriceList);
        //            HttpContext.Current.User = new CustomPrincipal(customIdentity, roles);
        //        }
        //    }
                       
        //}
    }
}
