﻿using System.Linq;
using System.Text;
using System.Globalization;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using eCommerce.Services.Model;

namespace eCommerce.UI.Utils
{
    public static class Utility
    {
        public static string CataloguePrefix = "shop";
        public static string ServerRoot = ConfigurationManager.AppSettings["ServerRoot"];
        public static string BackOfficeUrl = ConfigurationManager.AppSettings["BackOffice"];
        public static string ImgUrl = ConfigurationManager.AppSettings["BackOfficeUrl"];

        public static string AppId = ConfigurationManager.AppSettings["AppId"];
        public static string AppSecret = ConfigurationManager.AppSettings["AppSecret"];
        public static string InstagramAccessToken = ConfigurationManager.AppSettings["InstagramAccessToken"];

        public const string MailTemplates = "/Content/Templates";

        public static string RemoveDiacritics(this string str)
        {
            if (str == null) return null;
            var chars =
                from c in str.Normalize(NormalizationForm.FormD).ToCharArray()
                let uc = CharUnicodeInfo.GetUnicodeCategory(c)
                where uc != UnicodeCategory.NonSpacingMark
                select c;

            var cleanStr = new string(chars.ToArray()).Normalize(NormalizationForm.FormC);

            return cleanStr;
        }

        public static int ProformaOrderId
        {
            get
            {
                if (HttpContext.Current.Session["ProformaOrderId"] != null)
                    return int.Parse(HttpContext.Current.Session["ProformaOrderId"].ToString());
                else
                    return -1;
            }
            set
            {
                HttpContext.Current.Session["ProformaOrderId"] = value;
            }
        }

        public static Order ProformaOrder
        {
            get
            {
                if (HttpContext.Current.Session["ProformaOrder"] != null)
                    return HttpContext.Current.Session["ProformaOrder"] as Order;
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["ProformaOrder"] = value;
            }
        }

        public static int SaleCategoryId { get; internal set; }
    }
}