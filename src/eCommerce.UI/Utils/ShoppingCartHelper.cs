﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.UI.ViewModels;
using MercadoPagoSDK;
using System;
using System.Configuration;
using System.Linq;
using System.Web;

namespace eCommerce.UI.Utils
{
    public static class ShoppingCartHelper
    {

        public static string GetGuid()
        {
            HttpCookie httpCookie = HttpContext.Current.Request.Cookies["ShoppingCart" + HttpContext.Current.Request.Browser.Browser];

            if ((httpCookie == null) || (httpCookie != null && String.IsNullOrEmpty(httpCookie.Value)))
            {
                Guid guid = Guid.NewGuid();
                httpCookie = new HttpCookie("ShoppingCart" + HttpContext.Current.Request.Browser.Browser, guid.ToString());
                httpCookie.Expires = CurrentDate.Now.AddDays(30);
                HttpContext.Current.Response.Cookies.Add(httpCookie);
                return guid.ToString();
            }
            else
            {
                if (httpCookie.Value != "")
                    return httpCookie.Value;

                return string.Empty;
            }
        }


        // Create a checkout preference
        public static Preference CreatePreference(Order order, Customer customer)
        {
            // Set Checkout Helper
            CheckoutHelper ch = new CheckoutHelper();

            // Create token
            Token token = ch.CreateAccessToken(ConfigurationManager.AppSettings["ClientId"], ConfigurationManager.AppSettings["ClientSecret"]);
            ch.AccessToken = token.AccessToken;

            // Set preference
            Preference preference = new Preference();
            preference.BackUrls = new ResponseUrls();
            preference.BackUrls.Failure = ConfigurationManager.AppSettings["FailureUrl"] + "/" + order.OrderId.ToString();
            preference.BackUrls.Pending = ConfigurationManager.AppSettings["PendingUrl"] + "/" + order.OrderId.ToString();
            preference.BackUrls.Success = ConfigurationManager.AppSettings["SuccessUrl"] + "/" + order.OrderId.ToString();
            preference.ExternalReference = order.OrderId.ToString(); // your Id for this transaction
            preference.AutoReturn = "approved";

            // Set cart-items
            preference.Items = new ItemList();
            MercadoPagoSDK.Item item;
            //decimal total = 0;

            item = new MercadoPagoSDK.Item();
            item.Id = order.OrderId.ToString();
            item.Title = "Pedido Web #" + order.OrderId.ToString();
            item.Description = string.Empty;
            item.PictureUrl = string.Empty;
            item.Quantity = 1;
            item.UnitPrice = order.Total;
            item.CurrencyId = ConfigurationManager.AppSettings["CurrencyId"];
            preference.Items.Add(item);

            //if (order.Customer.UserType == (int)UserTypeEnum.Mayorista)
            //{
            //    item = new MercadoPagoSDK.Item();
            //    item.Id = order.OrderId.ToString();
            //    item.Title = "Recargo";
            //    item.Description = string.Empty;
            //    item.PictureUrl = string.Empty;
            //    item.Quantity = 1;
            //    item.UnitPrice = order.Total * 6 / 100;
            //    item.CurrencyId = ConfigurationManager.AppSettings["CurrencyId"];
            //    preference.Items.Add(item);
            //}

            
            preference.Payer = new UserEx();
            preference.Payer.Email = customer.Email;
            preference.Payer.Name = customer.FullName;
            preference.Payer.Surname = string.Empty;

            //Custom Payment methods configuration
            /*preference.CustomPaymentChoices = new PaymentChoices();
            preference.CustomPaymentChoices.ExcludedTypes = new IdList();
            preference.CustomPaymentChoices.ExcludedTypes.Add("prepaid_card");
            preference.CustomPaymentChoices.ExcludedTypes.Add("ticket");
            preference.CustomPaymentChoices.ExcludedTypes.Add("atm");*/

            // Create preference
            preference = ch.CreatePreference(preference);
            return preference;
        }
    }
}