﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using System.Collections.Generic;

namespace eCommerce.UI.Utils.Interfaces
{
    public interface IMailing
    {
        void SetTemplatesDirectory(string templatesDirectory);

        void SendWelcomeMail(string Email);

        void SendContactMail(Contact contact);

        void SendNewPasswordMail(string UrlToResetPassword, string To);

        void SendNewWholeselerMail(Customer customer);

        void SendOrderResumeMail(int orderId, UserTypeEnum storeType);

        void SendOrderInProcessResumeMail(int orderId);

        void SendStockAlertsMail(List<Stock> stock_to_alerts);
    }
}
