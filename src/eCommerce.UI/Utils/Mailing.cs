﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using eCommerce.Services.Model;
using System.Net.Mail;
using System.Net;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using eCommerce.UI.Utils.Interfaces;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Utils;
using System.ComponentModel.DataAnnotations;
using eCommerce.UI.ViewModels;
namespace eCommerce.UI.Utils
{
    public class Mailing : IMailing
    {
        private static string SMTPHost = ConfigurationManager.AppSettings["SMTPHost"];
        private static string SMTPPort = ConfigurationManager.AppSettings["SMTPPort"];
        private static string SMTPMail = ConfigurationManager.AppSettings["SMTPMail"];
        private static string SMTPUser = ConfigurationManager.AppSettings["SMTPUser"];
        private static string SMTPPass = ConfigurationManager.AppSettings["SMTPPass"];

        private static string ServerRoot = ConfigurationManager.AppSettings["ServerRoot"];
        private static string BackOffice = ConfigurationManager.AppSettings["BackOffice"];
        private static string BackOfficeUrl = ConfigurationManager.AppSettings["BackOfficeUrl"];
        private static bool EnableMail = bool.Parse(ConfigurationManager.AppSettings["EnableMail"]);
        private static bool EnableSsl = bool.Parse(ConfigurationManager.AppSettings["EnableSsl"]);
        public static string SiteName = "Blanco Paris";
        private static string TemplatesDirectory;

        private Mailing() { }

        private DbModelEntities modelEntities = new DbModelEntities();

        public static IMailing Instance
        {
            get
            {
                if (EnableMail)
                {
                    return new Mailing();
                }
                else
                {
                    return new DisabledMailing();
                }
            }
        }

        public void SetTemplatesDirectory(string templatesDirectory)
        {
            TemplatesDirectory = templatesDirectory;
        }

        public enum Mailtype
        {
            SendWelcomeMail = 1,
            SendContactMail = 2,
            SendOrderResumeMail = 4,
            SendNewPasswordMail = 6,
            SendOrderPushMail = 7,
            SendNewWholeselerMail = 8,
            SendStockAlertMail = 9
        }

        public void SendOrderResumeMail(int orderId, UserTypeEnum storeType)
        {
            var order = modelEntities.Orders.Find(orderId);

            ListDictionary replacements = new ListDictionary();
            replacements.Add("#ServerRoot#", ServerRoot);
            replacements.Add("#OrderId#", order.OrderId.ToString());
            replacements.Add("#OrderDate#", order.Created.ToString("dd/MM/yyyy"));
            //replacements.Add("#ThankYouInformation#", GetThankYouInfoTextHtml(order.Customer.UserType));
            replacements.Add("#UserInfo#", GetUserInformation(order));
            replacements.Add("#DeliveryMethod#", GetDeliveryMethodHtml(order));
            replacements.Add("#PaymentMethod#", GetPaymentMethodHtml(order));
            replacements.Add("#Observations#", GetObservationsHtml(order.Observations));
            replacements.Add("#SubTotal#", order.SubTotal.ToString());
            replacements.Add("#Total#", order.Total.ToString());
            replacements.Add("#OrderDetails#", this.orderTableGeneration(order.OrderDetails));
           
            this.SendMail(Mailtype.SendOrderResumeMail.ToString(), replacements, SiteName + " - Muchas gracias por tu compra (#" + order.OrderId + ")", order.Customer.Email);

            string company_subject = SiteName + " - Nueva venta Mayorista (#" + order.OrderId + ") - " + order.Customer.FullName;
            this.SendMail(Mailtype.SendOrderResumeMail.ToString(), replacements, company_subject, SMTPMail);
        }

        public void SendOrderPushMail(List<OrderDetail> orderdetails)
        {
            ListDictionary replacements = new ListDictionary();
            replacements.Add("#ServerRoot#", ServerRoot);
            replacements.Add("#OrderDetails#", this.orderTableGenerationPush(orderdetails));

            this.SendMail(Mailtype.SendOrderPushMail.ToString(), replacements, SiteName + " - No pierdas la oportunidad de continuar con tu compra", orderdetails.FirstOrDefault().CreatedBy);
        }

        public void SendOrderInProcessResumeMail(int orderId)
        {
            var order = modelEntities.Orders.Find(orderId);

            ListDictionary replacements = new ListDictionary();
            replacements.Add("#ServerRoot#", ServerRoot);
            replacements.Add("#OrderId#", order.OrderId.ToString());
            replacements.Add("#OrderDate#", order.Created.ToString("dd/MM/yyyy"));
            replacements.Add("#UserInfo#", GetUserInformation(order));
            replacements.Add("#DeliveryMethod#", GetDeliveryMethodHtml(order));
            replacements.Add("#PaymentMethod#", GetPaymentMethodHtml(order));
            replacements.Add("#Observations#", GetObservationsHtml(order.Observations));
            replacements.Add("#SubTotal#", order.SubTotal.ToString());
            replacements.Add("#Total#", order.OrderTotal.ToString());
            replacements.Add("#OrderDetails#", this.orderTableGeneration(order.OrderDetails));

            //this.SendMail(Mailtype.SendOrderResumeMail.ToString(), replacements, "Brishka - Muchas gracias por tu compra (#" + order.OrderId + ")", order.Customer.Email, true);
            this.SendMail(Mailtype.SendOrderResumeMail.ToString(), replacements, SiteName + " - Información de nueva venta en proceso (#" + order.OrderId + ") - " + order.Customer.FullName, SMTPMail);
        }


        private string GetThankYouInfoTextHtml(int UserType)
        {
            var html = "";
            if (UserType == (int)UserTypeEnum.Mayorista)
            {
                html += "En las próximas 24hs a 48hs hábiles se comunicarán contigo desde Atención al Cliente para ultimar los detalles de pago y envío que hayas elegido.";
            }
            else
            {
                html += "En las próximas 24hs a 48hs hábiles se comunicarán contigo desde Atención al Cliente para ultimar los detalles de pago y envío que hayas elegido.";
            }
            return html;
        }

        private string GetObservationsHtml(string observations)
        {
            var html = "";
            if (!string.IsNullOrEmpty(observations))
            {
                html += "<span style='color: #343434; text-transform: uppercase; font-size: 12px; -webkit-font-smoothing: auto; font-weight: bold;'>Aclaraciones</span><br/>";
                html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>" + observations + "</span><br/>";
            }
            
            return html;
        }

        private object GetUserInformation(Order order)
        {
            string html = "<span style='color: #343434; text-transform: uppercase; font-size: 12px; -webkit-font-smoothing: auto; font-weight: bold;'>Información del usuario</span><br/>";
            html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Nombre: " + order.Customer.FullName + "</span><br/>";
            html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Email: " + order.Customer.Email + "</span><br/>";
            if(!string.IsNullOrEmpty(order.Customer.PhoneNumber))
                html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Teléfono: " + order.Customer.PhoneNumber + "</span><br/>";
            if (!string.IsNullOrEmpty(order.Customer.Cellphone))
                html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Celular: " + order.Customer.Cellphone + "</span><br/>";
            if(order.Customer.UserType == (int)UserTypeEnum.Mayorista)
            {
                html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Tipo de usuario: " + ((UserTypeEnum)order.Customer.UserType).ToString() + "</span><br/>";
                html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Provincia: " + ((order.Customer.ProvinceId.HasValue ? order.Customer.Province.Name : string.Empty) + ", " + order.Customer.City + "(" + order.Customer.ZipCode + ")") + "</span><br />";
                html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>CUIT: " + order.Customer.CUIT + "</span><br/>";
                html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Razón Social: " + order.Customer.CorporateName + "</span><br/>";
            }
            return html;
        }

        private object GetPaymentMethodHtml(Order order)
        {
            string html = "<span style='color: #343434; text-transform: uppercase; font-size: 12px; -webkit-font-smoothing: auto; font-weight: bold;'>Forma de pago</span><br/>";
            switch (order.PaymentMethodId)
            {
                case (int)PaymentMethodEnum.Convenir:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Pago en el local - Pagá en alguno de nuestros locales.</span>";
                    break;
                case (int)PaymentMethodEnum.Efectivo:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Efectivo</span>";
                    break;
                case (int)PaymentMethodEnum.TransferenciaBancaria:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Transferencia Bancaria - Se te proporcionarán los datos una vez confirmada la compra</span><br/><br/>";
                    break;
                case (int)PaymentMethodEnum.MercadoPago:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Se te proporcionará el link para pagar una vez confirmada la compra.</span>";
                    break;
                case (int)PaymentMethodEnum.MercadoPagoQR:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Escanea nuestro QR para realizar el pago. Hace <a href='" + ServerRoot + "/content/images/PaymentMethods/Brishka-MP-QR.png' target='_blank'>click acá</a> para escanear nuestro QR.</span>";
                    break;
                case (int)PaymentMethodEnum.Decidir:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Pago con Tarjeta de Crédito o Débito aprovechando las promociones bancarias vigentes.</span>";
                    break;
                default:
                    break;
            }
            return html;
        }

        /*private string GetPaymentInfoHtml(int paymentMethodId)
        {
            string html = "";
            switch (paymentMethodId)
            {
                case (int)PaymentMethodEnum.Convenir:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Pago en el local - Pagá en alguno de nuestros locales.</span>";
                    break;
                case (int)PaymentMethodEnum.TransferenciaBancaria:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Transferencia Bancaria - Se te proporcionarán los datos despues de terminar la compra.</span>";
                    break;
                case (int)PaymentMethodEnum.MercadoPago:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Pago con Tarjeta de Credito y otros medios a través de la plataforma de MercadoPago.</span>";
                    break;
                case (int)PaymentMethodEnum.MercadoPagoQR:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Escanea nuestro QR para realizar el pago. Hace <a href=" + ServerRoot + "'/content/images/PaymentMethods/Brishka-MP-QR.png' target='_blank'>click acá</a> para escanear nuestro QR.</span>";
                    break;
                case (int)PaymentMethodEnum.Decidir:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Pago con Tarjeta de Crédito o Débito aprovechando las promociones bancarias vigentes.</span>"; 
                    break;
                default:
                    break;
            }
            return html;
        }*/

        private string GetDeliveryMethodHtml(Order order)
        {
            bool isFreeShipping = IsFreeShipping(order);
            //string envio_text = "El costo de envío es un estimado y deberá ser abonado a la empresa de logística elegida al recibir ó retirar tu compra.";

            string html = "<span style='color: #343434; text-transform: uppercase; font-size: 12px; -webkit-font-smoothing: auto; font-weight: bold;'>Forma de envío</span><br/>";
            switch (order.DeliveryMethodId)
            {
                case (int)DeliveryMethodEnum.AConvenir:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>A convenir con Atención al Cliente.</span>";
                    break;
                case (int)DeliveryMethodEnum.RetiraLocal:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Retiro en el local.</span>";
                    break;

                case (int)DeliveryMethodEnum.DomicilioProv:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Envio domicilio resto del País.</span>";
                    break;

                case (int)DeliveryMethodEnum.RetiraEnCentroDeEntrega:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Retirá en centro de entrega.</span>";
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'> Domicilio de Entrega: " + order.Store.Address + " </span ><br/>";
                    break;

                case (int)DeliveryMethodEnum.OcaPaP:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>OCA - Envío a domicilio</span><br/>";
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Domicilio de Entrega: " + order.Customer.Address+ " " + order.Customer.AddressNumber +" "+ order.Customer.Floor + " "+order.Customer.Department + "," + order.Customer.City +" "+ order.Customer.Province.Name + " </span ><br/>";

                    if (order.ShippingDays.HasValue && order.ShippingDays.Value > 0)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Dias de envío aproximados: " + order.ShippingDays.Value + " </span><br/>";
                    }
                    if (order.ShippingCost > 0 /*&& !isFreeShipping*/)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Costo de Envío: $" + order.ShippingCost + " </span ><br/><br/>";
                        //html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>" + envio_text + "</span >";
                    }
                    //if (isFreeShipping)
                    //{
                    //    html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #000 !important; font-size: 12px; line-height: 20px; font-weight: bold; padding: 0px 0px 20px 0px;'>Costo de Envío: Bonificado </span ><br/>";
                    //}
                    break;

                case (int)DeliveryMethodEnum.OcaPaS:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>OCA - Envío a sucursal</span><br/>";
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Domicilio de Entrega: " + order.DeliveryAddress + " </span ><br/>";
                    if (order.ShippingDays.HasValue && order.ShippingDays.Value > 0)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Dias de envío aproximados: " + order.ShippingDays.Value + " </span><br/>";
                    }
                    if (order.ShippingCost > 0 && !isFreeShipping)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Costo de Envío: $" + order.ShippingCost + " </span ><br/><br/>";
                        //html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>" + envio_text + "</span >";
                    }
                    if (isFreeShipping)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #000 !important; font-size: 12px; line-height: 20px; font-weight: bold; padding: 0px 0px 20px 0px;'>Costo de Envío: Bonificado </span ><br/>";
                    }
                    break;

                case (int)DeliveryMethodEnum.DomicilioCaba:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>CABA - Envio a domicilio</span><br/>";
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Domicilio de Entrega: " + order.DeliveryAddress + " </span ><br/>";
                    if (order.ShippingCost > 0)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Costo de Envío: $" + order.ShippingCost + " </span ><br/><br/>";
                    }
                    break;

                case (int)DeliveryMethodEnum.DomicilioGba:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>GBA - Envio a domicilio</span><br/>";
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Domicilio de Entrega: " + order.DeliveryAddress + " </span ><br/>";
                    if (order.ShippingCost > 0)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Costo de Envío: $" + order.ShippingCost + " </span ><br/><br/>";
                    }
                    break;

                case (int)DeliveryMethodEnum.Transporte:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Transporte</span><br/>";
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Empresa de Transporte: " + order.Customer.ShipmentCompany+ " </span><br/>";
                    if (order.Customer.ShipmentCompanyAddress != null && order.Customer.ShipmentCompanyAddress != string.Empty)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Dirección: " + order.Customer.ShipmentCompanyAddress + " </span><br/>";
                    }
                    if (order.Customer.ShipmentCompanyTel != null && order.Customer.ShipmentCompanyTel != string.Empty)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Teléfono: " + order.Customer.ShipmentCompanyTel + " </span>";
                    }
                    break;
                case (int)DeliveryMethodEnum.AndreaniD:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Andreani - Envío a domicilio.</span>";
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Domicilio de Entrega: " + order.DeliveryAddress + " </span ><br/>";
                    if (order.ShippingCost > 0 && !isFreeShipping)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Costo de Envío: $" + order.ShippingCost + " </span ><br/><br/>";
                        //html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>" + envio_text + "</span >";
                    }
                    if (isFreeShipping)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #000 !important; font-size: 12px; line-height: 20px; font-weight: bold; padding: 0px 0px 20px 0px;'>Costo de Envío: Bonificado </span ><br/>";
                    }
                    break;
                case (int)DeliveryMethodEnum.AndreaniS:
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Andreani - Envío a sucursal.</span>";
                    html += "<span style='text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Dirección de Entrega: " + order.DeliveryAddress + " </span ><br/>";
                    if (order.ShippingCost > 0 && !isFreeShipping)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>Costo de Envío: $" + order.ShippingCost + " </span ><br/><br/>";
                        //html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #888888 !important; font-size: 12px; line-height: 20px; font-weight: normal; padding: 0px 0px 20px 0px;'>" + envio_text + "</span >";
                    }
                    if (isFreeShipping)
                    {
                        html += "<span style = 'text-align: left; font-family:Arial, sans-serif; color: #000 !important; font-size: 12px; line-height: 20px; font-weight: bold; padding: 0px 0px 20px 0px;'>Costo de Envío: Bonificado </span ><br/>";
                    }
                    break;
                default:
                    break;
            }
            html += "<br/>";
            return html;
        }

        private bool IsFreeShipping(Order order)
        {
            decimal? customerFreeShippingCost;
            switch ((UserTypeEnum)order.Customer.UserType)
            {
                default:
                case UserTypeEnum.Minorista:
                    customerFreeShippingCost = modelEntities.Parameters.FirstOrDefault(x => x.Name == "FreeShippingRetail")?.Value;
                    break;
                case UserTypeEnum.Mayorista:
                    customerFreeShippingCost = modelEntities.Parameters.FirstOrDefault(x => x.Name == "FreeShippingWS")?.Value;
                    break;
            }
            return order.SubTotalWithDiscount >= customerFreeShippingCost.Value;
        }

        private string orderTableGeneration(IEnumerable<OrderDetail> OrderDetails)
        {
            string table = "";
            Order order = OrderDetails.First().Order;
            foreach (OrderDetail orderDetail in OrderDetails)
            {
                var imageName = "/" + orderDetail.Product.getResizedImageUrlByColor(orderDetail.ColorId);
                table += "<tr cellspacing='0' cellpadding='0' style='border-top: 1px solid #E2E2E2; border-bottom: 1px solid #E2E2E2; padding: 0px 0px 10px 0px;' valign='top'>";
                table += "<td colspan='1' width='50' style='padding: 10px 0 10px 10px;'>";
                table += "<a href = '" + ServerRoot + "/" + Utility.CataloguePrefix + "/" + orderDetail.Product.Category.SeoUrl + "/" + orderDetail.Product.SeoUrl + "' >";
                table += "<img src='" + BackOfficeUrl + imageName + "' width='50' />";
                table += "</a>";
                table += "</td>";
                table += "<td colspan='2' class='wrapper' style='font-family:Arial, sans-serif; color: inherit; font-size: 14px; font-weight: normal; text-align: left; padding-left: 30px; padding-bottom: 0px; padding-top: 10px; border-bottom: 1px solid #efefef;text-align:left;' >";
                table += "<a href ='" + ServerRoot + "/" + Utility.CataloguePrefix + "/" + orderDetail.Product.Category.SeoUrl + "/" + orderDetail.Product.SeoUrl + "' target='blank' title='" + orderDetail.Product.Title + "' style='color: #343434 !important; text-decoration: none !important;' >" + orderDetail.Product.Title + "</a>";
                table += "<br /> <span style='color: #343434 !important;display: inline-block; font-size: 12px;' > Cod.: " + orderDetail.Product.Code + "</span>";
                table += "<br /> <span style='color: #343434 !important;display: inline-block; font-size: 12px;' > Talle: " + orderDetail.Size.Description + "</span>";
                table += "<br /> <span style='color: #343434 !important;display: inline-block; font-size: 12px;' > Color: " + orderDetail.Color.Description + "</span>";
                table += "</td>";
                //table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: center; padding-right: 10px; padding-top: 10px; border-bottom: 1px solid #efefef;' > $" + (orderDetail.Order.Customer.UserType == (int)UserTypeEnum.Mayorista ? orderDetail.Product.WholesalePrice.ToString() : orderDetail.Product.RetailPrice.ToString()) + "</td>";
                table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: center; padding-right: 10px; padding-top: 10px; border-bottom: 1px solid #efefef;' > $" + orderDetail.Price.ToString() + "</td>";
                table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: center; padding-right: 10px; padding-top: 10px; border-bottom: 1px solid #efefef;' >" + orderDetail.Quantity + "</td>";
                table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: right; padding-right: 10px; padding-top: 10px; border-bottom: 1px solid #efefef;' > $" + orderDetail.SubTotal.ToString() + "</td>";
                table += "</tr>";
            }

            var total = order.SubTotalWithDiscount;
            if ((order.Discount.HasValue && order.Discount.Value > 0) || (order.SurchargeAmount.HasValue && order.SurchargeAmount.Value > 0) || (order.ShippingCost.HasValue && order.ShippingCost.Value > 0) || (order.IVA.HasValue && order.IVA.Value > 0))
            {
                table += "<tr cellspacing='0' cellpadding='0' style='border-top: 1px solid #E2E2E2; border-bottom: 1px solid #E2E2E2; padding: 0px 0px 10px 0px;' valign='top'>";
                table += "<td colspan='3' style='padding: 10px 0 10px 10px;'></td>";
                table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: bold; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' >" + "Subtotal" + "</td>";
                table += "<td></td>";
                table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' > $" + order.SubTotal.ToString() + "</td>";
                table += "</tr>";
                
                if (order.Discount.HasValue && order.Discount.Value > 0)
                {
                    table += "<tr cellspacing='0' cellpadding='0' style='border-top: 1px solid #E2E2E2; border-bottom: 1px solid #E2E2E2; padding: 0px 0px 10px 0px;' valign='top'>";
                    table += "<td colspan='3' style='padding: 10px 0 10px 10px;'></td>";
                    table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: bold; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' >" + "Descuento" + " (" + (decimal.Round(order.Discount_Percent.Value, 0)).ToString() + "%)" + " </td>";
                    table += "<td></td>";
                    table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' > $" + order.Discount.ToString() + "</td>";
                    table += "</tr>";
                }

                if (order.IVA.HasValue && order.IVA.Value > 0)
                {
                    table += "<tr cellspacing='0' cellpadding='0' style='border-top: 1px solid #E2E2E2; border-bottom: 1px solid #E2E2E2; padding: 0px 0px 10px 0px;' valign='top'>";
                    table += "<td colspan='3' style='padding: 10px 0 10px 10px;'></td>";
                    table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: bold; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' >" + $"IVA ({order.IVA_Percent.ToString()})" + " </td>";
                    table += "<td></td>";
                    table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' > $" + order.IVA.Value.ToString() + "</td>";
                    table += "</tr>";
                }

                if (order.ShippingCost.HasValue && order.ShippingCost.Value > 0)
                {
                    table += "<tr cellspacing='0' cellpadding='0' style='border-top: 1px solid #E2E2E2; border-bottom: 1px solid #E2E2E2; padding: 0px 0px 10px 0px;' valign='top'>";
                    table += "<td colspan='1' style='padding: 10px 0 10px 10px;'></td>";
                    table += "<td valign='middle' colspan='3' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: bold; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' >" + "Costo de envío" + " </td>";
                    table += "<td></td>";
                    table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' > $" + order.ShippingCost.ToString() + "</td>";
                    table += "</tr>";
                }

                if (order.SurchargeAmount.HasValue && order.SurchargeAmount.Value > 0)
                {
                    table += "<tr cellspacing='0' cellpadding='0' style='border-top: 1px solid #E2E2E2; border-bottom: 1px solid #E2E2E2; padding: 0px 0px 10px 0px;' valign='top'>";
                    table += "<td colspan='3' style='padding: 10px 0 10px 10px;'></td>";
                    table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: bold; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' >" + "Recargo Forma de Pago" + " (" + (decimal.Round(order.Surcharge.Value, 0)).ToString() + "%)" + " </td>";
                    table += "<td></td>";
                    table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' > $" + order.SurchargeAmount.ToString() + "</td>";
                    table += "</tr>";
                }
            }
            
            if (order.Surcharge.HasValue && order.SurchargeAmount.HasValue)
            {
                total += order.SurchargeAmount.Value;
            }

            table += "<tr cellspacing='0' cellpadding='0' style='border-top: 1px solid #E2E2E2; border-bottom: 1px solid #E2E2E2; padding: 0px 0px 10px 0px;' valign='top'>";
            table += "<td colspan='3' style='padding: 10px 0 10px 10px;'></td>";
            table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: bold; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' >" + "Total" + "</td>";
            table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: center; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' > " + order.OrderDetails.Sum(x => x.Quantity).ToString() + "</td>";
            table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: right; padding-right: 10px; padding-top: 4px; border-bottom: 1px solid #efefef;' > $" + order.Total.ToString() + "</td>";
            table += "</tr>";

            return table;
        }

        private string orderTableGenerationPush(List<OrderDetail> OrderDetails)
        {
            string table = "";
            Order order = OrderDetails.First().Order;
            foreach (OrderDetail orderDetail in OrderDetails.Where(x => x.Active && x.Deleted == null))
            {
                var imageName = "/" + orderDetail.Product.getResizedImageUrlByColor(orderDetail.ColorId);
                table += "<tr cellspacing='0' cellpadding='0' style='border-top: 1px solid #E2E2E2; border-bottom: 1px solid #E2E2E2; padding: 0px 0px 10px 0px;' valign='top'>";
                table += "<td colspan='1' width='50' style='padding: 10px 0 10px 10px;'>";
                table += "<a href = '" + ServerRoot + "/" + Utility.CataloguePrefix + "/" + orderDetail.Product.Category.SeoUrl + "/" + orderDetail.Product.SeoUrl + "' >";
                table += "<img src='" + BackOfficeUrl + "/Content/UploadDirectory" + imageName + "' width='50' />";
                table += "</a>";
                table += "</td>";
                table += "<td colspan='2' class='wrapper' style='font-family:Arial, sans-serif; color: inherit; font-size: 14px; font-weight: normal; text-align: left; padding-left: 30px; padding-bottom: 0px; padding-top: 10px; border-bottom: 1px solid #efefef;text-align:left;' >";
                table += "<a href ='" + ServerRoot + "/" + Utility.CataloguePrefix + "/" + orderDetail.Product.Category.SeoUrl + "/" + orderDetail.Product.SeoUrl + "' target='blank' title='" + orderDetail.Product.Title + "' style='color: #343434 !important; text-decoration: none !important;' >" + orderDetail.Product.Title + "</a>";
                table += "<br /> <span style='color: #343434 !important;display: inline-block;' > Cod.: " + orderDetail.Product.Code + "</span>";
                table += "<br /> <span style='color: #343434 !important;display: inline-block;' > Talle: " + orderDetail.Size.Description + "</span>";
                table += "<br /> <span style='color: #343434 !important;display: inline-block;' > Color: " + orderDetail.Color.Description + "</span>";
                table += "</td>";
                table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: center; padding-right: 10px; padding-top: 10px; border-bottom: 1px solid #efefef;' > $" + orderDetail.Product.WholesalePrice.ToString() + "</td>";
                table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: center; padding-right: 10px; padding-top: 10px; border-bottom: 1px solid #efefef;' >" + orderDetail.Quantity + "</td>";
                table += "<td valign='middle' colspan='1' style='font-family:Arial, sans-serif; color: #343434; font-size: 14px; font-weight: normal; text-align: right; padding-right: 10px; padding-top: 10px; border-bottom: 1px solid #efefef;' > $" + orderDetail.SubTotal.ToString() + "</td>";
                table += "</tr>";
            }

            return table;
        }

        private string getOrderDiscount(decimal? discount, decimal? discount_percent)
        {
            if (discount.HasValue && discount.Value > 0)
            {
                return @"<tr>
                            <td colspan='4' style='text-align:right;'><strong>Descuento (" + discount_percent.Value.ToString("N") + @"%)</strong></td>
                            <td colspan='2' style='text-align:right;'></td>
                        </tr>";
            }
            return string.Empty;
        }

        private string getShippingCost(decimal? shippingCost)
        {
            if(shippingCost.HasValue && shippingCost > 0)
                return shippingCost.Value.ToString("N");
            else
                return "0,00";
        }

        private string GetPaymentMethod(Order order)
        {
            string paymentMethod = string.Empty;
            switch (order.PaymentMethodId)
            {
                case (int)PaymentMethodEnum.Efectivo:
                    paymentMethod =  "Efectivo";
                    break;

                case (int)PaymentMethodEnum.Cheque:
                    paymentMethod = "Cheque";
                    break;

                case (int)PaymentMethodEnum.MercadoPago:
                    paymentMethod = "MercadoPago";
                    break;

                case (int)PaymentMethodEnum.TransferenciaBancaria:
                    paymentMethod = "Transferencia Bancaria";
                    break;

                default:
                case (int)PaymentMethodEnum.Convenir:
                    return "A convenir";
            }

            if (order.Coupon != null)
                paymentMethod += " (CUPÓN: " + order.Coupon.Code +")";

            return paymentMethod;
        }

        private string GetDeliveryMethod(int deliveryMethodId)
        {
            switch (deliveryMethodId)
            {
                default:
                case (int)DeliveryMethodEnum.RetiraEnCentroDeEntrega:
                    return "Retira en Centro de Entrega";

                case (int)DeliveryMethodEnum.OcaPaP:
                    return "Oca Envíos a Puerta";

                case (int)DeliveryMethodEnum.OcaPaS:
                    return "Oca Envíos a Sucursal";

                case (int)DeliveryMethodEnum.DomicilioCaba:
                    return "Envio a domicilio CABA";
                case (int)DeliveryMethodEnum.DomicilioGba:
                    return "Envio a domicilio GBA";

                case (int)DeliveryMethodEnum.MotoEntrega:
                    return "Moto Entrega";
                case (int)DeliveryMethodEnum.Transporte:
                    return "Transporte";
            }
        }

        private string ContactOrderIdHtml(int orderId)
        {
            var html = "";
            if (orderId > 0)
            {
                html += "Venta #" + orderId;
            }

            return html;
        }
        public void SendNewWholeselerMail(Customer customer)
        {
            int id = modelEntities.Customers.FirstOrDefault(x => x.Email == customer.Email).CustomerId;
            ListDictionary replacements = new ListDictionary();
            replacements.Add("#ServerRoot#", ServerRoot);
            replacements.Add("#BackOffice#", BackOffice);
            replacements.Add("#FullName#", customer.FullName);
            replacements.Add("#UserEmail#", customer.Email);
            replacements.Add("#PhoneNumber#", customer.Cellphone);

            this.SendMail(Mailtype.SendNewWholeselerMail.ToString(), replacements, SiteName + " - Nuevo usuario mayorista - " + customer.FullName, SMTPMail);
        }

        public void SendWelcomeMail(string Email)
        {
            ListDictionary replacements = new ListDictionary();
            replacements.Add("#ServerRoot#", ServerRoot);
            var userMail = Email;
            replacements.Add("#UserMail#", userMail);

            var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == userMail && x.Deleted == null && x.Active);
            replacements.Add("#FirstOrderDiscount#", FirstOrderDiscountText(customer));

            this.SendMail(Mailtype.SendWelcomeMail.ToString(), replacements, SiteName + " - Bienvenido a nuestro portal de compras", userMail);
        }

        private string FirstOrderDiscountText(Customer customer)
        {
            var text = "";

            if (customer.FirstOrderDiscountPeriodDays.HasValue)
            {
                var validDate = CurrentDate.Now.AddDays(customer.FirstOrderDiscountPeriodDays.Value);

                text += "Usá tu descuento por primera compra hasta el " + validDate.ToShortDateString();
            }

            return text;
        }

        public void SendNewPasswordMail(string UrlToResetPassword, string UserMail)
        {
            ListDictionary replacements = new ListDictionary();
            replacements.Add("#ServerRoot#", ServerRoot);
            replacements.Add("#PasswordRecover#", UrlToResetPassword);

            this.SendMail(Mailtype.SendNewPasswordMail.ToString(), replacements, SiteName + " - Restablecer contraseña", UserMail);
        }

        public void SendContactMail(Contact contact)
        {
            var mail = SMTPMail;
            //var subject = ((ContactSubjectEnum)int.Parse(contact.Subject)).GetAttribute<DisplayAttribute>().Name;

            ListDictionary replacements = new ListDictionary();
            replacements.Add("#ServerRoot#", ServerRoot);
            replacements.Add("#ContactName#", contact.Fullname);
            replacements.Add("#ContactMail#", contact.EMail);
            //replacements.Add("#ContactSubject#", contact.Subject);
            replacements.Add("#ContactPhone#", contact.PhoneNumber);
            replacements.Add("#ContactMessage#", contact.Message);

            this.SendMail(Mailtype.SendContactMail.ToString(), replacements, SiteName + " - Nueva consulta Web", mail);
        }

        public void SendStockAlertsMail(List<Stock> stock_to_alerts)
        {
            var mail = SMTPMail;

            ListDictionary replacements = new ListDictionary();
            replacements.Add("#BodyAlerts#", generateBodyAlertsHtml(stock_to_alerts));

            this.SendMail(Mailtype.SendStockAlertMail.ToString(), replacements, SiteName + " - Nueva alerta de stock web", mail);
        }

        private object generateBodyAlertsHtml(List<Stock> stock_to_alerts)
        {
            var body = "";
            var stockAlertValue = ConfigurationManager.AppSettings["StockAlertValue"];
            foreach(var stock in stock_to_alerts)
            {
                body += "El producto " + stock.Product.Code + " - Talle: " + stock.Size.Description + " - Color: " + stock.Color.Description + " posee stock menor a " + stockAlertValue;
                body += "<br />";
            }
            return body;
        }

        #region private

        public void SendMail(string templateName, ListDictionary replacements, string Subject, string to)
        {
            try
            {
                try
                {
                    string body = this.readFile(templateName);

                    MailDefinition mailDefinition = new MailDefinition();
                    mailDefinition.From = SMTPMail;
                    mailDefinition.IsBodyHtml = true;
                    mailDefinition.Subject = Subject;

                    SmtpClient client = null;

                    if (string.IsNullOrEmpty(SMTPPort))
                    {
                        client = new SmtpClient(SMTPHost)
                        {
                            Credentials = new NetworkCredential(SMTPUser, SMTPPass),
                            EnableSsl = EnableSsl
                        };
                    }
                    else
                    {
                        client = new SmtpClient(SMTPHost, int.Parse(SMTPPort))
                        {
                            Credentials = new NetworkCredential(SMTPUser, SMTPPass),
                            EnableSsl = EnableSsl
                        };
                    }

                    MailMessage mailMessage = mailDefinition.CreateMailMessage(to, replacements, body, new System.Web.UI.Control());
                    client.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch
            {
            }
        }

        private string readFile(string templateName)
        {
            try
            {
                var body = "";
                var file = string.Format("{0}Template.html", templateName);
                var path = Path.Combine(TemplatesDirectory, file);

                using (FileStream fs = File.Open(path, FileMode.Open))
                {
                    byte[] b = new byte[1024];
                    UTF8Encoding temp = new UTF8Encoding(true);

                    while (fs.Read(b, 0, b.Length) > 0)
                    {
                        body += temp.GetString(b);
                        b = new byte[1024];
                    }
                }

                return body;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        


        #endregion private

    }

    public class DisabledMailing : IMailing
    {
        public void SetTemplatesDirectory(string templatesDirectory) { }

        public void SendContactMail(Contact contact) { }

        public void SendWelcomeMail(string Email) { }

        public void SendNewPasswordMail(string UrlToResetPassword, string To) { }

        public void SendNewWholeselerMail(Customer customer) { }

        public void SendOrderResumeMail(int orderId, UserTypeEnum storeType) { }

        public void SendOrderInProcessResumeMail(int orderId) { }

        public void SendOrderPushMail(List<OrderDetail> orderdetails) { }

        public void SendStockAlertsMail(List<Stock> stock_to_alerts){}
    }

}