﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model;
using eCommerce.UI.ViewModels;

namespace eCommerce.UI.Utils
{
    public static class MenuHelper
    {

        public static Menu GetHeaderMenu()
        {
            DbModelEntities modelEntities = new DbModelEntities();
            var result = new Menu
            {
                Categories = modelEntities.Categories.Where(x => x.ParentCategoryId == null && x.Active).OrderBy(x => x.DisplayOrder).ToList()
            };

            return result;
        }

        public static List<Category> GetCategories()
        {
            DbModelEntities modelEntities = new DbModelEntities();
            List<Category> categories = new List<Category>();
            categories = modelEntities.Categories.Where(x => x.Active && x.ParentCategoryId == null && x.Enabled).OrderBy(x => x.DisplayOrder).ToList();            
            return categories;
        }

        public static Menu GetCategoryMenu(int categoryId)
        {
            DbModelEntities modelEntities = new DbModelEntities();

            var parentCategory = modelEntities.Categories.FirstOrDefault(x => x.Active && x.ParentCategoryId == null && x.CategoryId == categoryId);
            
            
            Menu result = new Menu();
            result.Categories = GetCategoryTree(parentCategory.CategoryId).ToList();

            return result;
        }

        private static IList<Category> GetCategoryTree(int? parentId = null)
        {
            DbModelEntities modelEntities = new DbModelEntities();
            var categories = modelEntities.Categories.Where(x => x.Active && x.ParentCategoryId == parentId && x.Enabled).OrderBy(x => x.DisplayOrder).ToList();

            foreach (var category1 in categories)
            {
                category1.ChildCategories = category1.ChildCategories.Where(x => x.Active && x.Enabled).ToList();

                foreach (var category2 in category1.ChildCategories)
                {
                    category2.ChildCategories = category2.ChildCategories.Where(x => x.Active && x.Enabled).ToList();
                }
            }
            return categories;
        }


        public static IList<Category> GetHomeCateogories()
        {
            DbModelEntities modelEntities = new DbModelEntities();

            var categories = modelEntities.Categories.Include("Category1").Where(x => x.Active && x.ShowInHome).OrderBy(x => x.DisplayOrder).ToList();

            return categories;

            /*var parentCategories = modelEntities.Categories.Where(c => c.ParentCategoryId == null && c.Active);
            var groupedCategories = new Dictionary<Category, IEnumerable<Category>>();

            foreach (var category in parentCategories)
            {
                category.ChildCategories
                var children = modelEntities.Categories.Where(c => c.ParentCategoryId == category.CategoryId).ToList();

                groupedCategories.Add(category, children);
            }

            return groupedCategories;*/


        }

        public static IList<LookbookGroup> GetHomeLookbookGroups()
        {
            DbModelEntities modelEntities = new DbModelEntities();

            var lookbookgroups = modelEntities.LookbookGroups.Where(x => x.Active && !x.Deleted.HasValue).ToList();

            return lookbookgroups;
        }

    }
}