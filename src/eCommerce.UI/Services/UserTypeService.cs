﻿using eCommerce.Services.Model.Enums;
using System.Web;

namespace eCommerce.UI.Controllers
{
    public class UserTypeService
    {
        public UserTypeEnum GetStoreType(HttpContext context)
        {
            return context.Request.Url.ToString().Contains("minoristas") ? UserTypeEnum.Minorista : UserTypeEnum.Mayorista;
        }

        public string BaseStoreRoot(HttpContext context)
        {
            return this.GetStoreType(context) == UserTypeEnum.Minorista ? "minoristas" : "mayoristas";
        }
    }
}