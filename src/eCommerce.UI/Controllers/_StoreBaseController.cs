﻿using eCommerce.Services.Model.Enums;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.UI.Controllers
{
    public class _StoreBaseController : Controller
    {
        public UserTypeEnum StoreType
        {
            get
            {
                return Request.Url.ToString().ToLower().Contains("minoristas") ? UserTypeEnum.Minorista : UserTypeEnum.Mayorista;
            }
        }

        public string BaseStoreRoot
        {
            get
            {
                return this.StoreType == UserTypeEnum.Minorista ? "minoristas" : "mayorista";
            }
        }
    }
}