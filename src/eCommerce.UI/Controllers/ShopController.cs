﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.UI.ViewModels.Shop;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System;
using eCommerce.Services.Business;
using HTTPCompression.ActionFilters;

namespace eCommerce.UI.Controllers
{
    public class ShopController : Controller
    {
        DbModelEntities _db = new DbModelEntities();
        private HttpContext context = System.Web.HttpContext.Current;

        public ShopController()
        {
        }

        [CompressAttribute]
        [OutputCache(Duration = 60, Location = System.Web.UI.OutputCacheLocation.Client)]
        [HttpGet]
        public ActionResult Buscar(ShopFilterViewModel filter)
        {
            var term = Request.QueryString["term"];

            var dbProducts = GetDbProducts();

            var PercentageDiscounts = _db.Promotions.Where(p => p.PromotionTypeId == (int)PromotionTypeEnum.Porcentaje 
                && !p.Deleted.HasValue && p.StartDate <= CurrentDate.Now && p.EndDate >= CurrentDate.Now).ToList();

            //FilterStoreType(ref dbProducts);

            //FilterStocks(ref dbProducts);

            dbProducts = GetQueryFilterdByTerm(term, dbProducts);

            //Reviso si hay alguna promocion de cantidad activa.
            string promotionText = CheckActiveQuantityPromotion();

            var products = BuildProductViewModelList(dbProducts, PercentageDiscounts, promotionText);

            var ShopViewModel = new ShopViewModel();
            var categories = GetCategories();

            var newCategories = categories.Select(x => new ViewModels.CategoryViewModel
            {
                Name = x.Name,
                SeoUrl = x.SeoUrl,
                Enabled = x.Enabled,
                Active = x.Active,
            }).ToList();

            ShopViewModel.isBuscar = true;
            ShopViewModel.productList = products.ToList();

            List<Color> colorList = BuildColorList(ShopViewModel.productList);
            //List<Size> sizeList = BuildSizeList(ShopViewModel.productList);
            

            SetShopViewModelParams(ShopViewModel.productList, filter, null, ref ShopViewModel, newCategories, colorList);

            return View("Shop", ShopViewModel);
        }

        //[HttpGet]
        //public JsonResult SuggestedProducts(string SearchedTerm)
        //{
        //    var userType = User.Identity.IsAuthenticated ?
        //                   _db.Customers.FirstOrDefault(c => c.Active && !c.Deleted.HasValue && c.Email == User.Identity.Name)?.UserType ??
        //                   (int)UserTypeEnum.Minorista :
        //                   (int)UserTypeEnum.Minorista;

        //    var productsQuery = _db.Products.Where(prod => (prod.Active && !prod.Deleted.HasValue && !prod.SoldOut &&
        //    (prod.Stocks.Where(x => !x.Color.Deleted.HasValue && !x.Size.Deleted.HasValue).Any(stk => stk.StockVirtual >= 0))));
        //    productsQuery = productsQuery.Where(prod => prod.Code.Contains(SearchedTerm) || prod.Title.Contains(SearchedTerm));

        //    var ImagePrefix = (string)ConfigurationManager.AppSettings["BackOfficeProductImagesUrl"];

        //    var SuggestedProductsList = productsQuery.OrderByDescending(p => p.ProductId).Select(prod => new SuggestedProduct
        //    {
        //        ProductId = prod.ProductId,
        //        ProductCode = "Código: " + prod.Code.ToUpper(),
        //        ProductTitle = prod.Title,
        //        ProductLink = this.BaseStoreRoot + "/" + prod.Category.SeoUrl + "/" + prod.SeoUrl,
        //        ImageUrl = ImagePrefix + prod.ProductId + "/" + (prod.Images.FirstOrDefault(img => img.Default) != null ? prod.Images.FirstOrDefault(img => img.Default).ImageName : prod.Images.FirstOrDefault().ImageName)
        //    }).ToList();

        //    return Json(SuggestedProductsList, JsonRequestBehavior.AllowGet);
        //}

        //[CompressAttribute]
        [OutputCache(Duration = 60, Location = System.Web.UI.OutputCacheLocation.Client)]
        public ActionResult Index(string categoryName, string productName, ShopFilterViewModel filter)
        {
            var dbProducts = GetDbProducts();

            var PercentageDiscounts = _db.Promotions.Where(p => p.PromotionTypeId == (int)PromotionTypeEnum.Porcentaje && !p.Deleted.HasValue && p.StartDate <= CurrentDate.Now && p.EndDate >= CurrentDate.Now).ToList();

            //Reviso si hay alguna promocion de cantidad activa.
            string promotionText = CheckActiveQuantityPromotion();

            #region Producto
            if (!string.IsNullOrEmpty(productName))
            {
                var product = dbProducts.FirstOrDefault(x => x.SeoUrl == productName);
                if (product == null) return View("Error", ErrorType.ProductNotFound);

                //var productDiscount = PercentageDiscounts.FirstOrDefault(p => ((p.CategoryPromotions.Any(c => c.CategoryId == product.CategoryId || c.CategoryId == product.Category.ParentCategoryId) || p.CategoryPromotions.Count() < 1) && !p.ExcludedProducts.Any(e => e.ProductId == product.ProductId)) || p.ProductPromotions.Any(pp => pp.ProductId == product.ProductId));
                var productDiscount = PercentageDiscounts.FirstOrDefault(p => (((p.CategoryPromotions.Count() < 1 && p.ProductPromotions.Count() < 1) ||
                                                               (p.CategoryPromotions.Any(c => c.CategoryId == product.CategoryId ||
                                                               c.CategoryId == product.Category.ParentCategoryId)))
                                                               && !p.ExcludedProducts.Any(e => e.ProductId == product.ProductId))
                                                               || p.ProductPromotions.Any(pp => pp.ProductId == product.ProductId));

                //FilterStoreType(ref dbProducts);

                var productModel = new ProductViewModel();

                productModel.Id = product.ProductId;
                productModel.Title = product.Title;
                productModel.Code = product.Code;
                productModel.Cloth = product.Cloth;
                productModel.Description = product.Description;
                productModel.WholesalePrice = product.WholesalePrice;
                productModel.OldWholesalePrice = product.OldWholesalePrice;
                productModel.Price = GetPrice(productModel, productDiscount);
                productModel.OldPrice = GetOldPrice(productModel, productDiscount);
                productModel.SeoUrl = product.SeoUrl;
                productModel.SoldOut = product.SoldOut;
                productModel.OnSale = product.OnSale;
                productModel.CategorySeoUrl = product.Category.SeoUrl;
                productModel.CategoryName = product.Category.Name;
                //productModel.category = product.Category;
                productModel.ShareUrl = Path.Combine(Utils.Utility.ServerRoot, "shop", product.Category.SeoUrl, product.SeoUrl);
                //productModel.ColorList = productStocks.Select(c => c.Color).Distinct().OrderBy(x => x.ColorId).ToList();
                //productModel.SizeList = productStocks.Select(c => c.Size).Distinct().OrderBy(s => s.DisplayOrder).ToList();
                productModel.ColorList_Detail = product.Stocks.Where(x => x.Deleted == null).Select(c => c.Color).Distinct().Select(c => new ProductColorViewModel
                {
                    ColorId = c.ColorId,
                    Description = c.Description,
                    ColorThumb = c.ColorThumb,
                    Enabled = product.SoldOut || c.Stocks.Where(x => x.ProductId == product.ProductId).Any(x => x.StockVirtual > 0)
                }).ToList();
                productModel.SizeList = product.Stocks.Where(x => x.Deleted == null).Select(c => c.Size).OrderBy(s => s.DisplayOrder).ToList();

                if (product.Category.ParentCategoryId.HasValue && product.Category.ParentCategory.SizeTableTypeId.HasValue)
                    productModel.SizeTableUrl = ((SizeTableTypeEnum)product.Category.ParentCategory.SizeTableTypeId).ToString();
                else if (product.Category.SizeTableTypeId.HasValue)
                    productModel.SizeTableUrl = ((SizeTableTypeEnum)product.Category.SizeTableTypeId).ToString();

                productModel.Pay_x_get_y_promotion = promotionText;

                productModel.Images = product.Images.OrderBy(x => x.ColorId).Select(y => new ProductImageViewModel()
                {
                    Id = y.ImageId,
                    ProductId = y.ProductId.Value,
                    ColorId = y.ColorId,
                    Orientation = y.Orientation,
                    ImageName = y.ImageName,
                    Default = y.Default,
                }).ToList();

                return View("Product", productModel);
            }
            #endregion

            #region Listado

            // Traigo las categorías habilitadas y las ordeno por cantidad de productos publicados
            var categories = GetCategories();

            

            var newCategories = categories.Select(x => new ViewModels.CategoryViewModel
            {
                Name = x.Name,
                SeoUrl = x.SeoUrl,
                Enabled = x.Enabled,
                Active = x.Active,
            }).ToList();

            var category = new Category();

            var ShopViewModel = new ShopViewModel();

            if (string.IsNullOrEmpty(categoryName))
            {
                category = categories.FirstOrDefault();
                if (category == null)
                    return CategoryNotFoundError();
                else
                    return IndexCategory(category.SeoUrl);
            }

            category = categories.FirstOrDefault(x => x.SeoUrl == categoryName);
            if (category == null)
                return CategoryNotFoundError();

            //FilterStoreType(ref dbProducts);

            //FilterStocks(ref dbProducts);

            SortProducts(ref dbProducts);

            var isSpecialCategory = false;

            if (category.CategoryId == Services.Utils.Utility.NewCategoryId)
            {
                isSpecialCategory = true;
            }

            if (category != null)
            {
                CheckSpecialCategory(ref isSpecialCategory, category, ref dbProducts);

                //excluyo los productos de venta telefonica.
                dbProducts = dbProducts.Where(x => !x.Category.SeoUrl.Contains("venta-telefonica"));
            }

            //FILTROS
            ApplyFilters(filter, ref dbProducts);

            var itemsCount = dbProducts.Count();

            ShopViewModel.LoadMoreProducts = itemsCount > filter.PageSize;
            ShopViewModel.PageSize = filter.PageSize;
            ShopViewModel.Sort = filter.Sort;
            ShopViewModel.TotalProductCount = itemsCount;

            var products = BuildProductViewModelList(dbProducts, PercentageDiscounts, promotionText);           
            
            var result = products.Take(filter.PageSize).ToList();

            List<Color> colorList = BuildColorList(result);
            //List<Size> sizeList = BuildSizeList(result);

            ApplyPromotionPrice(result, PercentageDiscounts);

            SetShopViewModelParams(result, filter, category, ref ShopViewModel, newCategories, colorList);

            return View("Shop", ShopViewModel);
            #endregion
        }

        public void ApplyPromotionPrice(List<ProductViewModel> products, List<Promotion> promotions)
        {
            var guid = Utils.ShoppingCartHelper.GetGuid();
            var manager = new ShoppingCartManager();

            foreach (var item in products)
            {
                item.Price = GetPrice(item, promotions.FirstOrDefault(p => (((p.CategoryPromotions.Count() < 1 && p.ProductPromotions.Count() < 1) || (p.CategoryPromotions.Any(c => c.CategoryId == item.CategoryId || c.CategoryId == item.ParentCategoryId))) && !p.ExcludedProducts.Any(e => e.ProductId == item.Id)) || p.ProductPromotions.Any(pp => pp.ProductId == item.Id)));
                item.OldPrice = GetOldPrice(item, promotions.FirstOrDefault(p => (((p.CategoryPromotions.Count() < 1 && p.ProductPromotions.Count() < 1) || (p.CategoryPromotions.Any(c => c.CategoryId == item.CategoryId || c.CategoryId == item.ParentCategoryId))) && !p.ExcludedProducts.Any(e => e.ProductId == item.Id)) || p.ProductPromotions.Any(pp => pp.ProductId == item.Id)));
            }
        }

        private string CheckActiveQuantityPromotion()
        {
            var activeQuantityPromotion = _db.Promotions.FirstOrDefault(p => p.PromotionTypeId == (int)PromotionTypeEnum.Cantidad &&
                    !p.Deleted.HasValue && p.StartDate <= CurrentDate.Now && p.EndDate >= CurrentDate.Now);

            string promotionText = activeQuantityPromotion != null ? activeQuantityPromotion.EveryUnits.ToString() + "x" + (activeQuantityPromotion.EveryUnits - activeQuantityPromotion.FreeUnits).ToString()
                : string.Empty;
            return promotionText;
        }

        private IQueryable<Product> ApplyFilters(ShopFilterViewModel filter, ref IQueryable<Product> dbProducts)
        {
            if (!string.IsNullOrEmpty(filter.Size))
            {
                List<int> SizesIds = filter.Size.Split(',').Select(x => int.Parse(x)).ToList();
                dbProducts = dbProducts.Where(x => x.Stocks.Any(y => SizesIds.Contains(y.SizeId)));
            }

            if (!string.IsNullOrEmpty(filter.Color))
            {
                List<int> ColorsIds = filter.Color.Split(',').Select(x => int.Parse(x)).ToList();
                dbProducts = dbProducts.Where(x => x.Stocks.Any(y => ColorsIds.Contains(y.ColorId)));
            }

            if (filter.Desde != null)
            {
                
                    dbProducts = dbProducts.Where(x => x.RetailPrice >= filter.Desde);
                

            }

            if (filter.Hasta != null)
            {
               
                    dbProducts = dbProducts.Where(x => x.RetailPrice <= filter.Hasta);
                
            }

            return dbProducts;
        }

        private void SortProducts(ref IQueryable<Product> dbProducts)
        {
            dbProducts = dbProducts.OrderByDescending(x => x.DisplayOrder.HasValue).ThenBy(x => x.DisplayOrder)
                .ThenByDescending(x => x.PublicationDate.HasValue).ThenByDescending(x => x.PublicationDate).ThenByDescending(x => x.ProductId);
        }

        public List<Color> BuildColorList(List<ProductViewModel> products)
        {
            List<Color> colorList = new List<Color>();
            foreach (var product in products)
            {
                foreach (var color in product.ColorList)
                {
                    if (!colorList.Contains(color))
                    {
                        colorList.Add(color);
                    }
                }
            }

            return colorList;
        }
        public List<Size> BuildSizeList(List<ProductViewModel> products)
        {
            List<Size> sizeList = new List<Size>();
            foreach (var product in products)
            {
                foreach (var size in product.SizeList)
                {
                    if (!sizeList.Contains(size))
                    {
                        sizeList.Add(size);
                    }
                }
            }

            return sizeList;
        }
        public List<Color> GetColorList(Product prod)
        {
            return prod.Stocks.Where(s => s.StockVirtual > 0 && !s.Deleted.HasValue && !s.Color.Deleted.HasValue && !s.Size.Deleted.HasValue).Select(c => c.Color).Distinct().ToList();
        }

        public List<Size> GetSizeList(Product prod)
        {
            return prod.Stocks.Where(s => s.StockVirtual > 0 && !s.Deleted.HasValue && !s.Color.Deleted.HasValue && !s.Size.Deleted.HasValue).Select(c => c.Size).Distinct().OrderBy(s => s.DisplayOrder).ToList();
        }

        public IQueryable<Product> GetDbProducts()
        {
            return _db.Products.Where(x => x.Active && x.Published && !x.Deleted.HasValue && x.Images.Count != 0 /*&& x.Stocks.Any(s => s.Size.Deleted == null && s.Color.Deleted == null && s.StockVirtual > 0 && s.Deleted == null)*/);
        }

        public IQueryable<Product> GetQueryFilterdByTerm(string term, IQueryable<Product> query)
        {
            return query.Where(x => x.Title.Contains(term) || x.Code.Contains(term) || x.Description.Contains(term) || x.Category.CompleteName.Contains(term) || x.Category.Description.Contains(term) || x.Tags.Contains(term));
        }

        public decimal GetPrice(ProductViewModel product, Promotion promotion)
        {
           
                if (promotion != null)
                {
                    return product.WholesalePrice.Value * (100 - promotion.DiscountPercent.Value) / 100;
                }
                else
                {
                    return product.WholesalePrice.HasValue ? product.WholesalePrice.Value : 0;
                }
            
        }

        public decimal GetOldPrice(ProductViewModel product, Promotion promotion)
        {
           
                if (promotion != null)
                {
                    return product.WholesalePrice.Value;
                }
                else
                {
                    return product.OldWholesalePrice.HasValue ? product.OldWholesalePrice.Value : 0;
                }
            
        }

        public IQueryable<ProductViewModel> BuildProductViewModelList(IQueryable<Product> query, List<Promotion> promotions, string promotionText = "")
        {

            var guid = Utils.ShoppingCartHelper.GetGuid();
            var manager = new ShoppingCartManager();

            return query.Select(y => new ProductViewModel
            {
                Id = y.ProductId,
                Title = y.Title,
                Code = y.Code,
                Description = y.Description,        
                CategoryId = y.CategoryId,
                ParentCategoryId = y.Category.ParentCategoryId,
                WholesalePrice = y.WholesalePrice,
                OldWholesalePrice = y.OldWholesalePrice,        
                //Price = GetPrice(y, promotions.FirstOrDefault(p => (((p.CategoryPromotions.Count() < 1 && p.ProductPromotions.Count() < 1) || (p.CategoryPromotions.Any(c => c.CategoryId == y.CategoryId || c.CategoryId == y.Category.ParentCategoryId))) && !p.ExcludedProducts.Any(e => e.ProductId == y.ProductId)) || p.ProductPromotions.Any(pp => pp.ProductId == y.ProductId))),
                //OldPrice =  GetOldPrice(y, promotions.FirstOrDefault(p => (((p.CategoryPromotions.Count() < 1 && p.ProductPromotions.Count() < 1) || (p.CategoryPromotions.Any(c => c.CategoryId == y.CategoryId || c.CategoryId == y.Category.ParentCategoryId))) && !p.ExcludedProducts.Any(e => e.ProductId == y.ProductId)) || p.ProductPromotions.Any(pp => pp.ProductId == y.ProductId))),
                SeoUrl = y.SeoUrl,
                category = y.Category,
                SoldOut = y.SoldOut,
                CategorySeoUrl = y.Category.SeoUrl,
                ColorList = y.Stocks.Where(s => s.StockVirtual > 0 && !s.Deleted.HasValue && !s.Color.Deleted.HasValue && !s.Size.Deleted.HasValue).Select(c => c.Color).Distinct().ToList(),//GetColorList(y),
                //SizeList = y.Stocks.Where(s => s.StockVirtual > 0 && !s.Deleted.HasValue && !s.Color.Deleted.HasValue && !s.Size.Deleted.HasValue).Select(c => c.Size).Distinct().OrderBy(s => s.DisplayOrder).ToList(), // GetSizeList(y),
                ShopImageSize = y.ShopImageSize,
                OnSale = y.OnSale,
                SizeTableId = y.Category.SizeTableTypeId.HasValue ? y.Category.SizeTableTypeId.Value : 0,
                IsInWishlist = y.WishlistItems.Any(x => x.ProductId == y.ProductId && !x.Deleted.HasValue && (x.GUID == guid || (User.Identity.IsAuthenticated && x.Customer.Email == User.Identity.Name && x.Customer.Active))),
                Pay_x_get_y_promotion = promotionText,
                Images = y.Images.Select(z => new ProductImageViewModel()
                {
                    Id = z.ImageId,
                    ProductId = z.ProductId.Value,
                    ColorId = z.ColorId,
                    Orientation = z.Orientation,
                    ImageName = z.ImageName,
                    Default = z.Default,
                }).ToList()
            });
        }

        //public void FilterStoreType(ref IQueryable<Product> dbProducts)
        //{
            
        //        dbProducts = dbProducts.Where(x => x.Published);
            
        //}

        public IQueryable<Product> FilterStocks(ref IQueryable<Product> dbProducts)
        {
            return dbProducts.Where(x => x.Stocks.Any(s => s.Size.Deleted == null && s.Color.Deleted == null && s.StockVirtual > 0 && s.Deleted == null));
        }

        public ActionResult CategoryNotFoundError()
        {
            return View("Error", ErrorType.CategoryNotFound);
        }

        public ActionResult IndexCategory(string seo)
        {
            return Redirect("/shop/" + seo);
            //return RedirectToAction("Index", new { categoryName = seo });
        }

        public void CheckSpecialCategory(ref bool isSpecialCategory, Category referenceCategory, ref IQueryable<Product> listQuery)
        {
            if (referenceCategory.CategoryId == Services.Utils.Utility.AllProductsCategoryId)
            {
                isSpecialCategory = true;
            }

            if (referenceCategory.CategoryId == Services.Utils.Utility.SaleCategoryId)
            {
                isSpecialCategory = true;
                listQuery = listQuery.Where(x => x.OnSale);
            }

            if (referenceCategory.CategoryId == Services.Utils.Utility.DenimCategoryId)
            {
                isSpecialCategory = true;
                listQuery = listQuery.Where(x => x.Denim);
            }

            if (referenceCategory.CategoryId == Services.Utils.Utility.NewCategoryId)
            {
                isSpecialCategory = true;
                listQuery = listQuery.Where(x => x.IsNew);
            }

            if (referenceCategory.CategoryId == Services.Utils.Utility.BestSellersCategoryId)
            {
                isSpecialCategory = true;
                var bestSellersMinimumAmountSold = 10;
                var bestSellersMaximumItems = 50;

                listQuery = listQuery
                    .Where(x => x.OrderDetails
                        .Where(c => c.OrderId.HasValue && c.Deleted == null)
                            .Select(s => s.Quantity)
                            .DefaultIfEmpty(0)
                            .Sum() > bestSellersMinimumAmountSold)
                    .OrderByDescending(o => o.OrderDetails
                        .Where(q => q.OrderId.HasValue && q.Deleted == null)
                            .Select(s => s.Quantity)
                            .DefaultIfEmpty(0)
                            .Sum() > bestSellersMinimumAmountSold)
                    .Take(bestSellersMaximumItems);
            }

            if (!isSpecialCategory)
            {
                if (referenceCategory.ChildCategories.Any())
                {
                    var childs_categories = referenceCategory.ChildCategories.Select(x => x.CategoryId).ToArray();
                    listQuery = listQuery.Where(x => x.CategoryId == referenceCategory.CategoryId || childs_categories.Any(c => c == x.CategoryId));
                }
                else
                    listQuery = listQuery.Where(x => x.CategoryId == referenceCategory.CategoryId);
            }
        }

        //public void ApplyFilters(ref List<ProductViewModel> products, ShopFilterViewModel filter)
        //{
        //    if (filter.Color.HasValue)
        //    {
        //        products = products.Where(x => x.ColorList.Any(c => c.ColorId == filter.Color.Value)).ToList();
        //    }

        //    if (filter.Size.HasValue)
        //    {
        //        products = products.Where(x => x.SizeList.Any(c => c.SizeId == filter.Size.Value)).ToList();
        //    }

        //    if (filter.PriceStart.HasValue)
        //    {
        //        products = products.Where(x => x.Price >= filter.PriceStart.Value).ToList();
        //    }

        //    if (filter.PriceEnd.HasValue)
        //    {
        //        products = products.Where(x => x.Price <= filter.PriceEnd.Value).ToList();
        //    }

        //    if (filter.Sort.HasValue)
        //    {
        //        switch (filter.Sort.Value)
        //        {
        //            case (int)ShopFilterSortEnum.HighestPrice:
        //                products = products.OrderByDescending(x => x.Price).ThenByDescending(x => x.OldPrice).ToList();
        //                break;
        //            case (int)ShopFilterSortEnum.LowestPrice:
        //                products = products.OrderBy(x => x.Price).ThenBy(x => x.OldPrice).ToList();
        //                break;
        //            case (int)ShopFilterSortEnum.Default:
        //            default:
        //                break;
        //        }
        //    }
        //}

        public void SetShopViewModelParams(List<ProductViewModel> products, ShopFilterViewModel filter, Category category, ref ShopViewModel ShopViewModel, List<UI.ViewModels.CategoryViewModel> categories, List<Color> colorList)
        {
            ShopViewModel.LoadMoreProducts = products.Count() > filter.PageSize;
            ShopViewModel.PageSize = filter.PageSize;
            //ShopViewModel.ColorId = filter.Color;
            ShopViewModel.Sort = filter.Sort;
            ShopViewModel.TotalProductCount = products.Count();
            ShopViewModel.CategoryName = category != null ? category.Name : String.Empty;
            ShopViewModel.CategorySeoUrl = category != null ? category.SeoUrl : String.Empty;
            //ShopViewModel.Category = category;
            if (products.Any())
            {
                
                try
                {
                    ShopViewModel.MaxProductPrice = (int)products.Max(x => x.Price);
                }
                catch (Exception ex)
                {
                    try
                    {
                        ShopViewModel.MaxProductPrice = (int)products.Max(x => x.WholesalePrice);
                    }
                    catch (Exception exception)
                    {

                        ShopViewModel.MaxProductPrice = 0;
                    }
                    

                }
            }

            //ShopViewModel.MaxProductPrice = products.Any() ? (int)products.Max(x => x.Price) : 0;
            ShopViewModel.productList = products;
            ShopViewModel.categoryList = categories;
            ShopViewModel.colorList = colorList;
            //ShopViewModel.sizeList = sizeList.OrderByDescending(x => x.DisplayOrder.HasValue).ThenBy(x => x.DisplayOrder).ToList();
        }

        public ActionResult Sale(string productName)
        {          
            return RedirectToAction("Index", new { categoryName = "sale", productName = productName });          
        }

        public ActionResult New(string productName)
        {
           
            return RedirectToAction("Index", new { categoryName = "new", productName = productName });
        }

        public ActionResult Product(int? Id)
        {
            if (!Id.HasValue) return View("Error", ErrorType.ProductNotFound);
            return View();
        }

        public IQueryable<Category> GetCategories()
        {
          
            return _db
                .Categories
                .Where(x => x.Active && x.Enabled && x.Deleted == null)
                .OrderByDescending(o => o.DisplayOrder.HasValue)
                .ThenBy(o => o.DisplayOrder);
        }


    }
}