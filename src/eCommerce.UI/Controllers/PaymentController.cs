﻿using Decidir.Model;
using eCommerce.Services;
using eCommerce.Services.Common;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Services.Accounting;
using eCommerce.Services.Services.Decidir;
using eCommerce.UI.Controllers;
using eCommerce.UI.Utils;
using eCommerce.UI.Utils.Interfaces;
using eCommerce.UI.ViewModels;
using eCommerce.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace eCommerce.Controllers
{
    public class PaymentController : _StoreBaseController
    {
        private DbModelEntities modelEntities;

        public PaymentController()
        {
            modelEntities = new DbModelEntities();
        }

        [HttpGet]
        public ViewResult PaymentSuccessful(string Answer, int Id = -1)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var order = modelEntities.Orders.FirstOrDefault(x => x.OrderId == Id && x.Active);

                    PaymentViewModel model = new PaymentViewModel();

                    model.CustomerName = order.Customer.FullName;
                    model.operationId = order.OrderId;
                    model.OrderTotal = order.Total;
                    if (order.BankInterestId.HasValue)
                    {
                        model.Installments = order.BankInterest.Name;
                    }

                    if (order != null && order.OrderStatusId == (int)OrderStatusEnum.EnProceso)
                    {
                        UpdateOrderStatus(order);

                        UpdateOrderDetails(order);

                        UpdateProductsStock(order);

                        UpdateCustomerAccount(order);

                        SetPaymentStatus(order);

                        UpdateCustomerAccountBalance(order.CustomerId);

                        SendSaleMail(order.OrderId);

                        SendStockAlerts(order);

                        dbContextTransaction.Commit();
                    }
                    return View(model);
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        private void SendStockAlerts(Order order)
        {
            var stockAlertValue = int.Parse(ConfigurationManager.AppSettings["StockAlertValue"]);
            if (stockAlertValue != -1)
            {
                List<Stock> stock_to_alert = new List<Stock>();
                foreach (var orderDetail in order.OrderDetails.Where(x => x.Active))
                {
                    //var stockComprobar = modelEntities.OrderDetails.Where(x => x.);
                    var stock = modelEntities.Stocks.FirstOrDefault(x => x.ProductId == orderDetail.ProductId && x.SizeId == orderDetail.SizeId && x.ColorId == orderDetail.ColorId);
                    if (stock.StockVirtual <= stockAlertValue)
                    {
                        stock_to_alert.Add(stock);
                    }
                }

                IMailing mailUtil = Mailing.Instance;
                mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                //mailUtil.SendStockAlertsMail(stock_to_alert);
            }    
        }
        
        private void ReturnOrderStock()
        {
            Order order = Utility.ProformaOrder;
            var stockService = new StockService(new VirtualStock());

            stockService.ReturnOrderStock(order.OrderId, ref modelEntities);
            modelEntities.SaveChanges();
        }

        private void UpdateProductsStock(Order order)
        {
            if (order.OrderStatusId != (int)OrderStatusEnum.Cancelada)
            {
                StockService stockService = new StockService(new VirtualStock());
                stockService.UpdateStockOrderDetails(order.OrderId, ref modelEntities);
            }

            modelEntities.SaveChanges();
        }

        [HttpGet]       
        public ActionResult PaymentError(string Answer, int Id = -1)
        {
            var order = modelEntities.Orders.FirstOrDefault(x => x.OrderId == Id);
            if (order != null)
            {
                order.OrderStatusId = (int)OrderStatusEnum.Cancelada;
            }

            //clone order details for new user cart.
            OrderDetail orderdetail;
            foreach (var detail in order.OrderDetails.Where(x => x.Deleted == null && x.Active))
            {
                orderdetail = new OrderDetail();
                orderdetail.CreatedBy = detail.CreatedBy;
                orderdetail.Created = detail.Created;
                orderdetail.ProductId = detail.ProductId;
                orderdetail.ColorId = detail.ColorId;
                orderdetail.SizeId = detail.SizeId;
                orderdetail.Price = detail.Price;
                orderdetail.Quantity = detail.Quantity;
                orderdetail.Discount = detail.Discount;
                orderdetail.Active = detail.Active;

                modelEntities.OrderDetails.Add(orderdetail);
            }
            modelEntities.SaveChanges();

            MessageViewModel messageViewModel = new MessageViewModel();
            messageViewModel.Message = TempData["Message"] as string;

            messageViewModel.Message = "No pudimos procesar su pago correctamente, por favor vuelva a intentarlo.";
            TempData["Message"] = messageViewModel.Message;
            messageViewModel.ActionType = ActionType.Home;

            //return View("Information", messageViewModel);
            return RedirectToAction("Information", "Account", messageViewModel);
        }

        private void SubstractOrderStock(Order order)
        {
            var orderDetails = order.OrderDetails.Where(x => x.Active).ToList();

            var stockService = new StockService(new VirtualStock());
            string productDescription = string.Empty;

            if (!stockService.HasStock(orderDetails, ref productDescription, ref modelEntities))
            {
                TempData["Message"] = "No tenemos stock del producto:" + productDescription + ". Disculpe las molestias ocasionadas!";
                TempData["ActionType"] = ActionType.Back;

                //return Redirect("/Account/Information");
            }
            else
            {
                stockService.SubstractStock(orderDetails, ref modelEntities);
                modelEntities.SaveChanges();
            }
        }

        private void UpdateOrderStatus(Order order)
        {
            order.OrderStatusId = (int)OrderStatusEnum.Confirmado;
            modelEntities.SaveChanges();
        }

        private void UpdateOrderDetails(Order order)
        {
            foreach (var orderDetail in order.OrderDetails.Where(x =>x.Active))
            {
                orderDetail.GUID = string.Empty;
            }
            modelEntities.SaveChanges();
        }

        private void UpdateCustomerAccount(Order order)
        {
            try
            {
                if (order.Transactions.Any(x => x.OrderId == order.OrderId && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso && x.Deleted == null))
                {
                    var transaction = order.Transactions.FirstOrDefault();
                    transaction.Amount = order.OrderTotal * -1;
                    transaction.CurrencyId = order.CurrenyId;
                    transaction.Date = CurrentDate.Now;
                }
                else
                {
                    Transaction transaction = new Transaction();

                    transaction.OrderId = order.OrderId;
                    transaction.Amount = order.Total * -1;
                    transaction.CurrencyId = order.CurrenyId;
                    transaction.TransactionTypeId = (int)TransactionTypeEnum.Egreso;
                    transaction.TransactionCategoryId = int.Parse(ConfigurationManager.AppSettings["SalesCategoryId"]);
                    transaction.Date = CurrentDate.Now;
                    transaction.Created = CurrentDate.Now;
                    transaction.CreatedBy = User.Identity.Name;
                    modelEntities.Transactions.Add(transaction);
                }

                modelEntities.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetPaymentStatus(Order order)
        {
            try
            {
                if (order.PaymentStatusId == (int)PaymentStatusEnum.Cobrada)
                {
                    Transaction transaction = new Transaction();

                    transaction.OrderId = order.OrderId;
                    transaction.Amount = order.Total;
                    transaction.CurrencyId = (int)CurrencyEnum.Peso;
                    transaction.TransactionTypeId = (int)TransactionTypeEnum.Ingreso;
                    transaction.TransactionCategoryId = int.Parse(ConfigurationManager.AppSettings["ChargesCategoryId"]);
                    transaction.AccountId = int.Parse(ConfigurationManager.AppSettings["DecidirAccountingId"]);
                    transaction.Date = CurrentDate.Now;
                    transaction.Created = CurrentDate.Now;
                    transaction.CreatedBy = User.Identity.Name;
                    modelEntities.Transactions.Add(transaction);

                    modelEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateCustomerAccountBalance(int customerId)
        {
            AccountingService.UpdateCustomerBalance(customerId, ref modelEntities);
        }

        private void ShowUserMessage(Order order)
        {
            ViewData["Customer"] = " " + order.Customer.FullName.ToUpper();
            ViewData["Amount"] = order.Total;
        }

        private void SendSaleMail(int orderId)
        {
            var storeType = this.StoreType;

            try
            {
                ThreadStart job = delegate 
                {
                    IMailing mailUtil = Mailing.Instance;
                    mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                    mailUtil.SendOrderResumeMail(orderId, this.StoreType);
                };
                new Thread(job).Start();
            }
            catch
            {}
        }


        [HttpGet]
        public ViewResult PagoDecidir(string guid)
        {
            ShoppingCartViewModel model = new ShoppingCartViewModel();

            var orderGuid = Guid.Parse(guid);
            var order = modelEntities.Orders.FirstOrDefault(x => x.GUID == orderGuid);

            if (order != null && order.PaymentStatusId == (int)PaymentStatusEnum.Pendiente)
            {
                model.OrderId = order.OrderId;
                model.Order = order;
                model.Customer = order.Customer;

                model.OrderDetails = order.OrderDetails.Where(x => !x.Deleted.HasValue).Select(x => new OrderDetailViewModel
                {
                    OrderDetailId = x.OrderDetailId,
                    Price = x.Price,
                    Quantity = x.Quantity,
                    ProductId = x.ProductId,
                    Title = x.Product.Title,
                    ColorDescription = x.Color.Description,
                    SizeDescription = x.Size.Description,
                    ProductImage = string.Format("{0}/{1}", Utility.ImgUrl, x.Product.getDefaultImageUrl),
                }).ToList();

                model.BankList = modelEntities.Banks
                    .Where(x => !x.Deleted.HasValue && x.BankInterests.Any(bi => !bi.Deleted.HasValue && bi.Active)).ToList();

            }

            return View(model);
        }

        [HttpPost]
        public ActionResult PagoDecidir(FormCollection Form)
        {
            var orderId = int.Parse(Request["OrderId"]);
            var order = modelEntities.Orders.Find(orderId);

            if (order != null && order.PaymentStatusId == (int)PaymentStatusEnum.Pendiente)
            {
                var selectedBankInterestId = int.Parse(Request["SelectedBankInteresId"]);

                var bankInterest = modelEntities.BankInterests.Include("Bank").FirstOrDefault(x => x.BankInterestId == selectedBankInterestId);
                if (bankInterest != null)
                {
                    order.Surcharge = bankInterest.Percent;
                    if (order.Surcharge.Value > 0)
                    {
                        order.Total += (order.Total * order.Surcharge.Value / 100);
                    }
                    order.PaymentMethodId_Decidir = bankInterest.Bank.PaymenMethodId_Decidir.HasValue ? bankInterest.Bank.PaymenMethodId_Decidir.Value : 1;
                }

                var token = Request["decidir-token"];
                var bin = Request["decidir-bin"];
                var installments = int.Parse(Request["decidir-installments"]);

                DecidirService decidir = new DecidirService();
                var response = decidir.GenerateDecidirPayment(order, token, bin, installments, "single");

                if (response.status == "approved")
                {
                    order.DecidirPaymentId = (int)response.id;
                    order.PaymentStatusId = (int)PaymentStatusEnum.Cobrada;
                    order.OrderStatusId = (int)OrderStatusEnum.EnProceso;
                    modelEntities.SaveChanges();

                    return Redirect("/" + this.BaseStoreRoot + "/Payment/PaymentSuccessful/" + orderId.ToString());
                }
                else
                {
                    var responseMessage = GetDecidirResponseMessage(response);

                    var message = "No pudimos procesar su pago correctamente: <br />" + responseMessage;
                    var actionType = ActionType.Back;

                    return RedirectToAction("Information", new { Message = Url.Encode(message), ActionType = actionType} );
                }
            }
                
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult Information(string Message, ActionType actionType)
        {
            MessageViewModel messageViewModel = new MessageViewModel();
            messageViewModel.Message = WebUtility.UrlDecode(Message);
            messageViewModel.ActionType = actionType;

            return View("Information", messageViewModel);
        }

        private string GetDecidirResponseMessage(PaymentResponse response)
        {
            try
            {
                var messageToReturn = response.status.Substring(response.status.IndexOf("{"), response.status.Length - response.status.IndexOf("{"));
                //var messageToReturn = response.status.ToString().Replace("402 - ", "");
                var deserialisedJson = JsonConvert.DeserializeObject<DecidirErrorResponseModel>(messageToReturn);

                var messageString = deserialisedJson.status_details.error.reason.description;
                return "Se produjo un error al validar los datos de la tarjeta: " + messageString;
            }
            catch (Exception ex)
            {
                return "Se produjo un error al validar los datos de la tarjeta";
            }


            //MOTIVOS.
            //https://decidir.api-docs.io/1.0/tablas-de-referencia-e-informacion-para-el-implementador/2i4sL9WujwGNMcpr7

            // 1 Y 2	PEDIR AUTORIZACION	Solicitar autorización telefónica, en caso de ser aprobada, cargar el código obtenido y dejar la operación en OFFLINE.
            //28  SERVICIO NO DISPONIBLE Momentáneamente el servicio no está disponible.Se debe reintentar en unos segundos.
            //45 Denegada, tarjeta inhibida para operar en cuotas
            //49  ERROR FECHA VENCIM.Verificar el sistema, error en formato de fecha de expiración(vto)
            //51  FONDOS INSUFICIENTES    Denegada, no posee fondos suficientes.
            //61	EXCEDE LIMITE	Denegada, excede límite remanente de la tarjeta.

            //76	LLAMAR AL EMISOR	Solicitar autorización telefónica, en caso de ser aprobada, cargar el código obtenido y dejar la operación en

            throw new NotImplementedException();
        }

    }
}
