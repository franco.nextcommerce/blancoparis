﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Model;
using eCommerce.UI.ViewModels;
using eCommerce.Services.Common;
using eCommerce.ViewModels;
using System.Configuration;
using System.Threading;
using eCommerce.UI.Utils.Interfaces;
using eCommerce.UI.Utils;

namespace eCommerce.UI.Controllers
{
    public class LandingController : Controller
    {
        DbModelEntities modelEntities = new DbModelEntities();
        private static string CompanyMAil = ConfigurationManager.AppSettings["IGWTMail"];

        // GET: Landing
        public ActionResult Index(string Id)
        {
            return Redirect("ropa-por-mayor");
        }

        [HttpGet]
        public ActionResult RopaPorMayor(string Id)
        {
            LandingViewModel model = new LandingViewModel();

            if (Id == null)
            {
                model.Title = "Ropa de mujer por mayor.";
                model.MainTitle = "<span><strong>Ropa de mujer</strong><br/> por mayor.</span>";
                model.CategoryName = "Mayorista de ropa";
                model.Description = "Mayorista de ropa";
                model.Products = GetCategoryProducts(null);
                model.Keywords = GetGenericLandingHtmlDescription();
                model.MetaDescription = GetGenericLandingMetaDescription();
                List<Category> categories = modelEntities.Categories.Where(x => x.Active && x.Enabled).OrderBy(x => x.DisplayOrder).ToList();
                model.Categories = categories;
                model.IsDefault = true;

                return View("Landing", model);
            }

            if (Id != null && Id[0] == '-')
                Id = Id.Substring(1);

            var category = modelEntities.Categories.FirstOrDefault(x => x.Name.ToLower() == Id.ToLower() || x.SeoUrl == Id);
            if (category == null)
            {
                TempData["Message"] = "La pagina solicitada no existe";
                TempData["ActionType"] = ActionType.Home;

                return RedirectToAction("Information");
            }

            model.Categories = modelEntities.Categories.Where(x => x.Active && x.Enabled && x.CategoryId != category.CategoryId).OrderBy(x => x.DisplayOrder).ToList();
            model.Title = "Ropa de " + category.Name + " por mayor";
            model.CategoryName = "Mayorista de ropa de " + category.Name;
            model.Description = category.Name;
            model.Products = GetCategoryProducts(category);
            model.IsDefault = false;
            return View("Landing", model);
        }

        private List<ProductLanding> GetCategoryProducts(Category category)
        {
            Random rnd = new Random();
            var products = modelEntities.Products.Where(x => x.Active && x.Published).AsEnumerable();
            if (category != null)
            {
                return products.Where(x => x.CategoryId == category.CategoryId || (x.Category.ParentCategoryId.HasValue && x.Category.ParentCategoryId == category.CategoryId)).OrderBy(x => x.Category.DisplayOrder).ThenBy(x => rnd.Next())
                    .Select(x => new ProductLanding
                    {
                        ProductId = x.ProductId,
                        Name = x.Title,
                        WholesalePrice = x.WholesalePrice.HasValue ? x.WholesalePrice.Value : 0,
                        CategorySeoUrl = x.Category.SeoUrl,
                        SeoUrl = x.SeoUrl,
                        Images = x.Images.ToList(),
                        Category = x.Category
                    }).Take(20).ToList();
            }
            else
            {
                return products.OrderBy(x => x.Category.DisplayOrder).ThenBy(x => rnd.Next())
                    .Select(x => new ProductLanding
                    {
                        ProductId = x.ProductId,
                        Name = x.Title,
                        WholesalePrice = x.WholesalePrice.HasValue ? x.WholesalePrice.Value : 0,
                        CategorySeoUrl = x.Category.SeoUrl,
                        SeoUrl = x.SeoUrl,
                        Images = x.Images.ToList(),
                        Category = x.Category
                    }).Take(20).ToList();
            }
        }

        public virtual ActionResult Information()
        {
            MessageViewModel messageViewModel = new MessageViewModel();
            messageViewModel.Message = TempData["Message"] as string;

            if (TempData["ActionType"] == null)
                messageViewModel.ActionType = ActionType.Home;
            else
                messageViewModel.ActionType = (ActionType)TempData["ActionType"];

            return View("Information", messageViewModel);
        }

        private string GetGenericLandingHtmlDescription()
        {
            string body = string.Empty;

            //Falta llenar
            //body += "<p>Bienvendidos a Inedita Mayorista. Somos una de las empresas argentinas más importantes en el mundo de la indumentaria casual con 40 años de servicio al cliente y diez puntos de venta distribuidos en CABA.  Comercializamos ropa de dama, hombre, niño, bebé y blanquería.  Y por eso somos un mayorista de ropa multirubro que busca satisfacer las necesidades de los comercios de indumentaria más variados de todo el país con los mejores precios de mercado.</p>";
            //body += "<h2 class='sub-section'>Indumentaria de mujer mayorista</h2>";
            //body += "<p>En nuestro catálogo de <a href='/ropa-por-mayor-mujer'>ropa de mujer por mayor</a> tenemos la colección más variada de remeras, camisas camisolas y blusas para jóvenes y señoras en diferentes telas y texturas. También contamos con una colección de calzas con diversos cortes, modelos y telas Es fundamental destacar la amplísima curva de talles y colores que ofrecemos en cada uno de nuestros modelos. Por otro lado, contamos con un catálogo de lencería y ropa interior de dama en el cual se destacan nuestros modelos de bombachas, corpiños, medias, conjuntos de ropa interior y una línea especial de camisetas térmicas a primer precio.Nuestro mayorista de ropa de mujer en invierno incluye también una amplia variedad de productos en tejido de punto como sweaters, sacos, ruanas, chalinas y ponchos en distintas texturas. Esta colección siempre viene acompañada con camperas en microfibra, polyester y simil cuero. Como complemento a nuestra extensa varidad de calzas de uso continuo en el año, en invierno incorporamos también diferentes calzas térmicas en tonos oscuros combinables con todo el placard.En verano florece nuestra colección de capris, shorts, bermudas, musculosas y una extensa variedad de vestidos largos y cortos en modal, viscosa, seda fría, fibrana, bambula, crepe y algodones estampados.<br />";

            return body;
        }

        private string GetGenericLandingMetaDescription()
        {
            string body = string.Empty;
            body += ""; //Falta llenar

            return body;
        }

        [HttpPost]
        public string Contact(FormCollection form)
        {
            try
            {
                Contact contact = new Contact();
                contact.Fullname = form["name"];
                contact.EMail = form["email"];
                contact.PhoneNumber = form["phone"];
                contact.Subject = "Contacto Mayorista";
                contact.Message = form["description"];
                contact.Created = CurrentDate.Now;
                contact.Active = true;
                contact.Answered = false;
                modelEntities.Contacts.Add(contact);
                modelEntities.SaveChanges();

                ThreadStart job = delegate
                {
                    IMailing mailUtil = Mailing.Instance;
                    mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                    mailUtil.SendContactMail(contact);

                };
                new Thread(job).Start();

                return "success";
            }
            catch (Exception ex)
            {
                return "error";
            }
        }
    }
}