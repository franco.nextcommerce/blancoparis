﻿using eCommerce.Services.Common;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace eCommerce.UI.Controllers
{
    public class NewsletterController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        // GET: Newsletter
        public ActionResult Index(string name, int? orderById)
        {
            var newsletter = modelEntities.Newsletters.FirstOrDefault(x => x.Campaign == name && x.Active && x.Deleted == null);
            if (newsletter == null || (newsletter.ExpirationDate.HasValue && CurrentDate.Now > newsletter.ExpirationDate.Value.AddDays(1)))
            {
                TempData["Message"] = "La página solicitada no se encuentra disponible.";

                MessageViewModel messageViewModel = new MessageViewModel();
                messageViewModel.Message = TempData["Message"] as string;

                if (TempData["ActionType"] == null)
                    messageViewModel.ActionType = ActionType.Home;
                else
                    messageViewModel.ActionType = (ActionType)TempData["ActionType"];

                return View("Information", messageViewModel);
            }
            else
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list.Add(new SelectListItem
                {
                    Text = "Menor Precio",
                    Value = "2"
                });
                list.Add(new SelectListItem
                {
                    Text = "Mayor Precio",
                    Value = "3"
                });
                list.Add(new SelectListItem
                {
                    Text = "Menor Stock",
                    Value = "4"
                });
                list.Add(new SelectListItem
                {
                    Text = "Mayor Stock",
                    Value = "5"
                });
                
                
                if (orderById.HasValue)
                {
                   newsletter.OrderById = orderById.Value;
                }

                TempData["OrderByIds"] = new SelectList(list, "Value", "Text", newsletter.OrderById);
                ViewData["ServerRoot"] = ConfigurationManager.AppSettings["ServerRoot"];

                switch (newsletter.OrderById)
                {
                    case (int)NewsletterOrderByEnum.MenorPrecio:
                        newsletter.NewsletterProducts = newsletter.NewsletterProducts.OrderBy(x => x.Product.WholesalePrice).ToList();
                        break;
                    case (int)NewsletterOrderByEnum.MayorPrecio:
                        newsletter.NewsletterProducts = newsletter.NewsletterProducts.OrderByDescending(x => x.Product.WholesalePrice).ToList();
                        break;
                    //case (int)NewsletterOrderByEnum.MenorStock:
                    //    newsletter.NewsletterProducts = newsletter.NewsletterProducts.OrderBy(x => x.Product.StockVirtual).ToList();
                    //    break;
                    //case (int)NewsletterOrderByEnum.MayorStock:
                    //    newsletter.NewsletterProducts = newsletter.NewsletterProducts.OrderByDescending(x => x.Product.StockVirtual).ToList();
                    //    break;
                    default:
                        break;
                }
                return View("NewsletterTemplateOneColumn", newsletter);
            }
        }

        [HttpGet]
        public ActionResult NewsletterTemplateOneColumn(string Id)
        {
            var newsletter = modelEntities.Newsletters.FirstOrDefault(x => x.Campaign == Id);
            if (newsletter != null)
            {
                ViewData["ServerRoot"] = ConfigurationManager.AppSettings["ServerRoot"];
                return View(newsletter);
            }
            return View();
        }

        public ActionResult GetOneColomnTemplateAsFile(int Id)
        {
            Newsletter model = modelEntities.Newsletters.FirstOrDefault(x => x.NewsletterId == Id);
            ViewData["ServerRoot"] = ConfigurationManager.AppSettings["ServerRoot"];
            ViewEngineResult result = ViewEngines.Engines.FindView(this.ControllerContext, "NewsletterTemplateOneColumn", null);
            string htmlTextView = GetViewToString(this.ControllerContext, result, model);

            byte[] toBytes = Encoding.UTF8.GetBytes(htmlTextView);
            return File(toBytes, "application/file", model.Campaign + ".html");
        }

        private string GetViewToString(ControllerContext context, ViewEngineResult result, object model)
        {
            string viewResult = "";
            var viewData = ViewData;
            viewData.Model = model;
            TempDataDictionary tempData = new TempDataDictionary();
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (HtmlTextWriter output = new HtmlTextWriter(sw))
                {
                    ViewContext viewContext = new ViewContext(context,
                        result.View, viewData, tempData, output);
                    result.View.Render(viewContext, output);
                }
                viewResult = sb.ToString();
            }
            return viewResult;
        }

        
    }
}