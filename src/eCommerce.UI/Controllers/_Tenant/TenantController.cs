﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Tenant;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;

namespace eCommerce.UI.Controllers
{
    public class TenantController : Controller
    {
        public TenantController()
        {
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if(this.CurrentTenant == null) {

                InitializeTenant();
            }  
            
         
            base.OnActionExecuting(filterContext);
        }

        private void InitializeTenant()
        {
            using (DbModelEntities modelEntities = new DbModelEntities())
            {
                CurrentTenant tenant = new CurrentTenant();
                var parameters = modelEntities.Parameters.Where(x => x.Visible.HasValue && x.Visible.Value).ToList();

                var paramName = ConfigurationParametersEnum.Logo.ToString();
                var param = parameters.FirstOrDefault(x => x.Name == paramName);
                var value = param != null && !string.IsNullOrEmpty(param.Text) ? param.Text : string.Empty;
                tenant.Logo = value;

                paramName = ConfigurationParametersEnum.Instagram_Account.ToString();
                param = parameters.FirstOrDefault(x => x.Name == paramName);
                value = param != null && !string.IsNullOrEmpty(param.Text) ? param.Text : string.Empty;
                tenant.Instagram = value;

                paramName = ConfigurationParametersEnum.Facebook_Account.ToString();
                param = parameters.FirstOrDefault(x => x.Name == paramName);
                value = param != null && !string.IsNullOrEmpty(param.Text) ? param.Text : string.Empty;
                tenant.Facebook = value;

                paramName = ConfigurationParametersEnum.Whatsapp.ToString();
                param = parameters.FirstOrDefault(x => x.Name == paramName);
                value = param != null && !string.IsNullOrEmpty(param.Text) ? param.Text : string.Empty;
                tenant.whatsapp = value;

                paramName = ConfigurationParametersEnum.MinimumOrderRetail.ToString();
                param = parameters.FirstOrDefault(x => x.Name == paramName);
                value = param != null && param.Value.HasValue ? param.Value.Value.ToString() : string.Empty;
                tenant.MinimumOrder = value;

                paramName = ConfigurationParametersEnum.CompanyPhoneNumber.ToString();
                param = parameters.FirstOrDefault(x => x.Name == paramName);
                value = param != null && !string.IsNullOrEmpty(param.Text) ? param.Text : string.Empty;
                tenant.CompanyNumber = value;

                paramName = ConfigurationParametersEnum.GoogleAnalitycs_Script.ToString();
                param = parameters.FirstOrDefault(x => x.Name == paramName);
                value = param != null && !string.IsNullOrEmpty(param.Text) ? param.Text : string.Empty;
                tenant.GoogleAnalytics = value;

                paramName = ConfigurationParametersEnum.MercadoPago_ClientId.ToString();
                param = parameters.FirstOrDefault(x => x.Name == paramName);
                value = param != null && !string.IsNullOrEmpty(param.Text) ? param.Text : string.Empty;
                tenant.MPClientId = value;

                paramName = ConfigurationParametersEnum.MercadoPago_ClientSecret.ToString();
                param = parameters.FirstOrDefault(x => x.Name == paramName);
                value = param != null && !string.IsNullOrEmpty(param.Text) ? param.Text : string.Empty;
                tenant.MPClientSecret = value;

                HttpContext.Session["Tenant"] = tenant;
            }
        }

        public CurrentTenant CurrentTenant
        {
            get
            {
                return HttpContext.Session["Tenant"] != null ? (CurrentTenant)HttpContext.Session["Tenant"] : null;
            }
        }



        public UserTypeEnum StoreType
        {
            get
            {
                return Request.Url.ToString().ToLower().Contains("minoristas") ? UserTypeEnum.Minorista : UserTypeEnum.Mayorista;
            }
        }

        public string BaseStoreRoot
        {
            get
            {
                return this.StoreType == UserTypeEnum.Minorista ? "minoristas" : "mayoristas";
            }
        }

    }
}