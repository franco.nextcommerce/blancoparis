﻿using eCommerce.Services.Model;
using eCommerce.UI.Utils;
using eCommerce.UI.Utils.Interfaces;
using eCommerce.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Runtime.Serialization;
using System.Text;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Configuration;
using eCommerce.Services.Notification;
using eCommerce.UI.ViewModels;
using eCommerce.Services.Model.Enums;
using static eCommerce.Services.Utils.Utility;
using System.ComponentModel.DataAnnotations;
using eCommerce.UI.Controllers;

namespace eCommerce.UI.Controllers
{
    public class InstitucionalController : TenantController
    {
        private DbModelEntities modelEntities = new DbModelEntities();

        [HttpGet]
        public ActionResult Contacto()
        {
            return View(new ContactViewModel());
        }

        [HttpGet]
        public ActionResult Locales()
        {
            var stores = modelEntities.Stores.Where(x => x.Deleted == null).ToList();
            var storesViewModel = stores.GroupBy(x => x.Province)
                .Select(x => new StoresViewModel
                {
                    Province = x.Key,
                    Stores = stores.Where(s => s.ProvinceId == x.Key.ProvinceId).ToList()
                });
            return View(storesViewModel);
        }

        [HttpGet]
        public ActionResult Mayoristas()
        {
            return View("Mayoristas");
        }

        [HttpGet]
        public ActionResult SobreNosotros()
        {
            return View("SobreNosotros");
        }

        [HttpGet]
        public ActionResult SolicitarCotizacion()
        {
            return View("SolicitarCotizacion");
        }


        [HttpPost]
        public JsonResult Contacto(ContactViewModel viewdata)
        {
            if (!ValidReCaptcha())
            {
                return Json(new { success = false });
            }

            Contact contact = new Contact();

            contact.Fullname = viewdata.Name;
            contact.EMail = viewdata.Email;
            contact.Subject = viewdata.Subject;
            contact.Message = viewdata.Message;
            contact.PhoneNumber = viewdata.Phone;
            contact.OrderId = viewdata.OrderId;

            contact.Answered = false;
            contact.Active = true;
            contact.Created = CurrentDate.Now;

            modelEntities.Contacts.Add(contact);
            modelEntities.SaveChanges();

            IMailing mailUtil = Mailing.Instance;
            mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
            mailUtil.SendContactMail(contact);

            return Json(new { success = true });
        }

        private bool ValidReCaptcha()
        {
            //start building recaptch api call
            var sb = new StringBuilder();
            sb.Append("https://www.google.com/recaptcha/api/siteverify?secret=");

            //our secret key
            var secretKey = ConfigurationManager.AppSettings["ReCaptchaPrivateKey"];
            sb.Append(secretKey);

            //response from recaptch control
            sb.Append("&");
            sb.Append("response=");
            var reCaptchaResponse = Request["g-recaptcha-response"];
            sb.Append(reCaptchaResponse);

            using (var client = new WebClient())
            {
                var uri = sb.ToString();
                var json = client.DownloadString(uri);
                var serializer = new DataContractJsonSerializer(typeof(RecaptchaApiResponse));
                var ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
                var result = serializer.ReadObject(ms) as RecaptchaApiResponse;

                //--- Check if we are able to call api or not.
                if (result == null)
                {
                    return false;
                }
                else // If Yes
                {
                    //api call contains errors
                    if (result.ErrorCodes != null)
                    {
                        if (result.ErrorCodes.Count > 0)
                        {
                            foreach (var error in result.ErrorCodes)
                            {
                                return false;
                            }
                        }
                    }
                    else //api does not contain errors
                    {
                        if (!result.Success) //captcha was unsuccessful for some reason
                        {
                            return false;
                        }
                        else //---- If successfully verified. Do your rest of logic.
                        {
                            return true;
                        }
                    }

                }
            }
            return false;
        }

        [DataContract]
        public class RecaptchaApiResponse
        {
            [DataMember(Name = "success")]
            public bool Success;

            [DataMember(Name = "error-codes")]
            public List<string> ErrorCodes;
        }

        [HttpGet]
        public ActionResult Faq()
        {
            List<Faq> faqs = modelEntities.Faqs.Where(x => x.Active).OrderBy(x => x.DisplayOrder).ToList();
            return View(faqs);
        }

        [HttpGet]
        public ActionResult Empresa()
        {
            //Institutional info = modelEntities.Institutionals.FirstOrDefault();
            InstitucionalViewModel model = new InstitucionalViewModel();
            model.Company = modelEntities.Institutionals.FirstOrDefault().Company;
            model.Instagram_Account = CurrentTenant.Instagram;
            model.Facebook_Account = CurrentTenant.Facebook;

            return View(model);
        }

        [HttpGet]
        public ActionResult ComoComprar()
        {
            List<Faq> faqs = modelEntities.Faqs.Where(x => x.Active).OrderBy(x => x.DisplayOrder).ToList();
            return View(faqs);
        }

        [HttpGet]
        public ActionResult CambiosYDevoluciones()
        {
            Institutional info = modelEntities.Institutionals.FirstOrDefault();
            return View(info);
        }

        [HttpGet]
        public ActionResult TermsAndConditions()
        {
            Institutional info = modelEntities.Institutionals.FirstOrDefault();
            return View(info);
        }

        [HttpGet]
        public ActionResult PrivacyPolicy()
        {
            Institutional info = modelEntities.Institutionals.FirstOrDefault();
            return View(info);
        }

        [HttpGet]
        public ActionResult MediosDePago()
        {
            Institutional info = modelEntities.Institutionals.FirstOrDefault();
            return View(info);
        }

        [HttpGet]
        public ActionResult InformacionSobreEnvios()
        {
            Institutional info = modelEntities.Institutionals.FirstOrDefault();
            return View("ShippingInfo", info);
        }

        [HttpPost]
        public virtual string RegisterNewsletter(string email)
        {
            try
            {
                var newsLetter = modelEntities.NewsSuscribers.SingleOrDefault(x => x.Email == email && x.Active);
                if (newsLetter == null)
                {
                    NewsSuscriber suscriber = new NewsSuscriber();
                    suscriber.Active = true;
                    suscriber.Created = CurrentDate.Now;
                    suscriber.Email = email;
                    suscriber.FullName = string.Empty;
                    modelEntities.NewsSuscribers.Add(suscriber);
                    modelEntities.SaveChanges();
                    return "suscripto";
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

    }
}