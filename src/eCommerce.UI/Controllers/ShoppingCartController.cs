﻿using Decidir.Model;
using eCommerce.Services;
using eCommerce.Services.Business;
using eCommerce.Services.Common;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Model.Exceptions;
using eCommerce.Services.Security;
using eCommerce.Services.Services.Andreani;
using eCommerce.Services.Services.Decidir;
using eCommerce.UI.Utils;
using eCommerce.UI.Utils.Interfaces;
using eCommerce.UI.ViewModels;
using eCommerce.UI.ViewModels.Shop;
using MercadoPagoSDK;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using TodoPagoConnector;
using TodoPagoConnector.Exceptions;
using eCommerce.Services.Domain;
using TodoPagoConnector.Utils;

namespace eCommerce.UI.Controllers
{
    public class ShoppingCartController : _StoreBaseController
    {
        private readonly ShoppingCartManager _shoppingCartManager;
        private DbModelEntities modelEntities;
        private readonly AndreaniService _andreani;
        private HttpContext context = System.Web.HttpContext.Current;

        public ShoppingCartController()
        {
            _shoppingCartManager = new ShoppingCartManager();
            modelEntities = new DbModelEntities();
            _andreani = new AndreaniService();
        }

        public PartialViewResult LoadProducts(string categoryName, int pageSize, int pageCounter, int? colorId, int? sort)
        {
            var category = modelEntities.Categories.FirstOrDefault(x => x.SeoUrl == categoryName);
            var categoryId = category.CategoryId;
            var pQuery = modelEntities.Products.Where(x => x.Active && x.Deleted == null && x.Published);
            pQuery = pQuery.Where(x => x.Stocks.Any(s => s.Size.Deleted == null && s.Color.Deleted == null && s.StockVirtual > 0));
            pQuery = pQuery.OrderByDescending(x => x.PublicationDate.HasValue).ThenByDescending(x => x.PublicationDate);
            var isSpecial = false;

            if (categoryId == Services.Utils.Utility.AllProductsCategoryId)
            {
                isSpecial = true;
            }

            if (categoryId == Services.Utils.Utility.SaleCategoryId)
            {
                isSpecial = true;
                pQuery = pQuery.Where(x => x.OnSale);
            }

            if (categoryId == Services.Utils.Utility.NewCategoryId)
            {
                isSpecial = true;
                pQuery = pQuery.Where(x => x.IsNew);
            }

            if (categoryId == Services.Utils.Utility.BestSellersCategoryId)
            {
                isSpecial = true;
                var bestSellersMinimumAmountSold = 10;
                var bestSellersMaximumItems = 50;

                pQuery = pQuery
                    .Where(x => x.OrderDetails
                        .Where(c => c.OrderId.HasValue && c.Deleted == null)
                            .Select(s => s.Quantity)
                            .DefaultIfEmpty(0)
                            .Sum() > bestSellersMinimumAmountSold)
                    .OrderByDescending(o => o.OrderDetails
                        .Where(q => q.OrderId.HasValue && q.Deleted == null)
                            .Select(s => s.Quantity)
                            .DefaultIfEmpty(0)
                            .Sum() > bestSellersMinimumAmountSold)
                    .Take(bestSellersMaximumItems);
            }

            if (!isSpecial)
            {
                pQuery = pQuery.Where(x => x.CategoryId == categoryId);
            }

            if (colorId.HasValue)
            {
                pQuery = pQuery.Where(x => x.Stocks.Any(c => c.ColorId == colorId.Value && c.Color.Deleted == null));
            }

            var guid = Utils.ShoppingCartHelper.GetGuid();
            var manager = new ShoppingCartManager();

            var products = pQuery.ToList().Select(y => new ViewModels.Shop.ProductViewModel
            {
                Id = y.ProductId,
                Title = y.Title,
                Description = y.Description,
                Price = y.CustomerPrice,
                OldPrice = y.CustomerOldPrice,
                SeoUrl = y.SeoUrl,
                category = category,
                SoldOut = y.SoldOut,
                CategorySeoUrl = y.Category.SeoUrl,
                ColorList = y.Stocks.Where(s => s.StockReal > 0 && s.Color.Deleted == null && s.Size.Deleted == null).Select(c => c.Color).Distinct().ToList(),
                SizeList = y.Stocks.Where(s => s.StockReal > 0 && s.Color.Deleted == null && s.Size.Deleted == null).Select(c => c.Size).Distinct().OrderBy(s => s.DisplayOrder).ToList(),
                ShopImageSize = y.ShopImageSize,
                IsInWishlist = manager.IsProductInWishlist(y.ProductId, guid, User.Identity.Name),
                Images = y.Images.Select(z => new ProductImageViewModel()
                {
                    Id = z.ImageId,
                    ProductId = z.ProductId.Value,
                    ColorId = z.ColorId,
                    Orientation = z.Orientation,
                    ImageName = z.ImageName,
                    Default = z.Default,
                }).ToList()
            }).ToList();


            if (sort.HasValue)
            {
                switch (sort.Value)
                {
                    case (int)ShopFilterSortEnum.HighestPrice:
                        products = products.OrderByDescending(x => x.Price).ThenByDescending(x => x.OldPrice).ToList();
                        break;
                    case (int)ShopFilterSortEnum.LowestPrice:
                        products = products.OrderBy(x => x.Price).ThenBy(x => x.OldPrice).ToList();
                        break;
                    case (int)ShopFilterSortEnum.Default:
                    default:
                        break;
                }
            }

            products = products.Skip(pageSize * pageCounter).Take(pageSize).ToList();

            return PartialView("Shop/Product/LoadProducts", products);
        }

        //wishlist

        public ActionResult Wishlist()
        {
            var shoppingCartManager = new ShoppingCartManager();
            var guid = ShoppingCartHelper.GetGuid();

            var wishlistItems = new List<WishlistItem>();
            if (User.Identity.IsAuthenticated)
                wishlistItems = shoppingCartManager.GetWishlistItemsByCustomer(guid, User.Identity.Name);
            else
                wishlistItems = shoppingCartManager.GetWishlistItems(guid);

            return View(wishlistItems);
        }

        public JsonResult AddWishlistItemToShoppingCart(int wishlistItemId, int quantity)
        {
            var shoppingCartManager = new ShoppingCartManager();

            var wishlistItem = modelEntities.WishlistItems.FirstOrDefault(x => x.Id == wishlistItemId && x.Deleted == null);
            wishlistItem.Quantity = quantity;
            modelEntities.SaveChanges();

            var validator = shoppingCartManager.ValidateOrderDetail(wishlistItem.ProductId, wishlistItem.SizeId, wishlistItem.ColorId, wishlistItem.Quantity);

            if (!validator.IsApproved)
            {
                return Json(new { Status = "Invalid", Message = validator.Message }, JsonRequestBehavior.AllowGet);
            }

            var orderDetail = new OrderDetail();

            orderDetail.Created = CurrentDate.Now;
            orderDetail.CreatedBy = User.Identity.Name;
            orderDetail.Active = true;

            orderDetail.ProductId = wishlistItem.ProductId;
            orderDetail.SizeId = wishlistItem.SizeId;
            orderDetail.ColorId = wishlistItem.ColorId;
            orderDetail.Quantity = wishlistItem.Quantity;
            orderDetail.Price = wishlistItem.Product.CustomerPrice;

            if (User.Identity.IsAuthenticated)
                orderDetail.CreatedBy = User.Identity.Name;
            else
                orderDetail.GUID = wishlistItem.GUID;

            modelEntities.OrderDetails.Add(orderDetail);

            wishlistItem.Deleted = CurrentDate.Now;
            wishlistItem.DeletedBy = User.Identity.IsAuthenticated ? User.Identity.Name : wishlistItem.GUID;
            modelEntities.SaveChanges();

            return Json(new { Status = "Valid" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddWishlistToShoppingCart()
        {
            var shoppingCartManager = new ShoppingCartManager();
            var guid = ShoppingCartHelper.GetGuid();

            var wishlistItems = new List<WishlistItem>();

            if (User.Identity.IsAuthenticated)
                wishlistItems = shoppingCartManager.GetWishlistItemsByCustomer(guid, User.Identity.Name);
            else
                wishlistItems = shoppingCartManager.GetWishlistItems(guid);

            var valid = true;

            foreach (var wishlistItem in wishlistItems)
            {
                var validator = shoppingCartManager.ValidateOrderDetail(wishlistItem.ProductId, wishlistItem.SizeId, wishlistItem.ColorId, wishlistItem.Quantity);
                if (!validator.IsApproved)
                    valid = false;
            }

            if (!valid)
            {
                return Json(new { Status = "Invalid", Message = "Alguno de los productos no está disponible." }, JsonRequestBehavior.AllowGet);
            }

            var orderDetail = new OrderDetail();

            foreach (var wishlistItem in wishlistItems)
            {
                orderDetail = new OrderDetail();
                orderDetail.Created = CurrentDate.Now;
                orderDetail.CreatedBy = User.Identity.Name;
                orderDetail.Active = true;

                orderDetail.ProductId = wishlistItem.ProductId;
                orderDetail.SizeId = wishlistItem.SizeId;
                orderDetail.ColorId = wishlistItem.ColorId;
                orderDetail.Quantity = wishlistItem.Quantity;
                orderDetail.Price = wishlistItem.Product.CustomerPrice;

                if (User.Identity.IsAuthenticated)
                    orderDetail.CreatedBy = User.Identity.Name;
                else
                    orderDetail.GUID = wishlistItem.GUID;

                modelEntities.OrderDetails.Add(orderDetail);

                var wishlistItemToUpdate = modelEntities.WishlistItems.Find(wishlistItem.Id);
                wishlistItemToUpdate.Deleted = CurrentDate.Now;
                wishlistItemToUpdate.DeletedBy = User.Identity.IsAuthenticated ? User.Identity.Name : wishlistItem.GUID;

                modelEntities.SaveChanges();
            }

            modelEntities.SaveChanges();

            return Json(new { Status = "Valid" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteItemFromWishlist(int wishlistItemId)
        {
            var wishlistItem = modelEntities.WishlistItems.FirstOrDefault(x => x.Id == wishlistItemId && x.Deleted == null);

            if (wishlistItem == null)
                return Json(new { Status = "Invalid", Message = "Item doesn't exist" }, JsonRequestBehavior.AllowGet);

            wishlistItem.Deleted = CurrentDate.Now;
            wishlistItem.DeletedBy = User.Identity.Name;

            modelEntities.SaveChanges();

            return Json(new { Status = "Valid" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult WishlistModal(int id)
        {
            var product = modelEntities.Products.Find(id);

            var model = new ProductQuickViewModel()
            {
                Id = product.ProductId,
                Title = product.Title,
                Description = product.Description,
                Price = product.WholesalePrice.Value,
                OldPrice = product.OldWholesalePrice,
                Cloth = product.Cloth,
                ColorList = product.Stocks.Where(s => s.StockReal > 0 && s.Color.Deleted == null && s.Size.Deleted == null).Select(c => c.Color).Distinct().ToList(),
                SizeList = product.Stocks.Where(s => s.StockReal > 0 && s.Color.Deleted == null && s.Size.Deleted == null).Select(c => c.Size).Distinct().OrderBy(s => s.DisplayOrder).ToList(),
                Images = product.Images.OrderByDescending(x => x.Default).Select(z => new ProductImageViewModel()
                {
                    Id = z.ImageId,
                    ProductId = z.ProductId.Value,
                    ColorId = z.ColorId,
                    Orientation = z.Orientation,
                    ImageName = z.ImageName,
                    Default = z.Default,
                }).ToList()
            };

            return PartialView("Shop/Product/WishlistModal", model);
        }

        public ActionResult AddWishlistItem(int idProducto, int SizeId, int ColorId, int cantidad, bool sumar = false)
        {
            var cartGuid = ShoppingCartHelper.GetGuid();

            var shoppingCartValidation = _shoppingCartManager.ValidateWishlistItem(idProducto, SizeId, ColorId, cantidad);

            if (shoppingCartValidation.IsApproved)
            {
                var product = modelEntities.Stocks.FirstOrDefault(x => !x.Deleted.HasValue && x.ProductId == idProducto && x.SizeId == SizeId && x.ColorId == ColorId).Product;
                var price = product.CustomerPrice;

                _shoppingCartManager.AddToWishlist(idProducto, SizeId, ColorId, cantidad, price, cartGuid, User.Identity.Name);

            }

            var wishlistItemCount = _shoppingCartManager.GetWishlistCount(cartGuid, User.Identity.Name);

            return Json(new { Count = wishlistItemCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetShoppingCartForModal()
        {
            var cart = new ShoppingCartModal();
            var cartGuid = ShoppingCartHelper.GetGuid();
            var shoppingCart = GetShoppingCart(cartGuid);
            cart.OrderDetails = shoppingCart.OrderDetails
                                .Select(od => new OrderDetailModal
                                {
                                    OrderDetailId = od.OrderDetailId,
                                    Quantity = od.Quantity,
                                    Name = od.Title,
                                    Price = od.Price,
                                    Color = "Color: " + od.ColorDescription,
                                    Size = "Talle: " + od.SizeDescription,
                                    Image = od.ProductImageResized
                                }).ToList();
            cart.Total = shoppingCart.TotalOrderDetails;
            cart.TotalQuantity = cart.OrderDetails.Sum(od => od.Quantity);
            cart.IsEmpty = !cart.OrderDetails.Any();
            return Json(cart, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateTable(int idProducto, string orderDetailsJson)
        {
            ShoppigCartValidation shoppingCartValidation = new ShoppigCartValidation();

            List<NoStockItem> noStockList = new List<NoStockItem>();

            var orderDetails = JsonConvert.DeserializeObject<List<OdToAdd>>(orderDetailsJson);
            if (orderDetails.Count() > 0)
            {
                foreach (var od in orderDetails)
                {
                    var validation = _shoppingCartManager.ValidateOrderDetail(idProducto, od.sizeId, od.colorId, od.quantity, noStockList);
                    shoppingCartValidation = validation;
                }
            }
            else
            {
                var reason = string.Format("Intente comprar al menos un producto");
                shoppingCartValidation.Reprove(reason);
            }

            if (noStockList.Count > 0)
            {
                shoppingCartValidation.NoStockItems = new List<NoStockItem>();
                shoppingCartValidation.NoStockItems.AddRange(noStockList);
            }

            return Json(shoppingCartValidation, JsonRequestBehavior.AllowGet);
        }

        public ShoppigCartValidation ValidateOrderDetail(int productId, int SizeId, int ColorId, int quantity)
        {
            var stock = modelEntities.Stocks.FirstOrDefault(x => x.ProductId == productId && x.SizeId == SizeId && x.ColorId == ColorId);
            var shoppingCartValidation = new ShoppigCartValidation();
            shoppingCartValidation.Approve();

            if (stock == null)
            {
                shoppingCartValidation.Reprove("No existe stock para esa combinación de talle y color.");
            }
            else if (stock.Product.SoldOut)
            {
                var reason = "El producto no se puede agregar al carrito de compras porque está agotado.";
                shoppingCartValidation.Reprove(reason);
            }
            else if (stock.Product.HasStockControl && quantity > stock.StockVirtual)
            {
                var reason = string.Format("El producto no cuenta con stock suficiente, pruebe comprar hasta {0} unidades.", stock.StockVirtual);
                shoppingCartValidation.Reprove(reason);
            }

            return shoppingCartValidation;
        }

        public PartialViewResult SlideoutShoppingCart()
        {
            var cartGuid = ShoppingCartHelper.GetGuid();
            List<OrderDetail> cartDetails = _shoppingCartManager.GetShoppingCart(cartGuid, User.Identity.IsAuthenticated ? User.Identity.Name : null, this.StoreType);

            return PartialView("Layout/ShoppingCart/Slideout", cartDetails);
        }

        private void UpdateCartWithCurrentPromotions(ref ShoppingCartViewModel shoppingCart)
        {
            var storeType = this.StoreType;

            //Traigo todas las promos activas
            var promotions = modelEntities.Promotions.Where(x => x.UserType == (int)this.StoreType
                    && x.StartDate <= CurrentDate.Now && x.EndDate >= CurrentDate.Now && !x.Deleted.HasValue);

            //Solo tomoo en cuenta las que no tienen forma de pago, porque todavía no se eligio el metodo.
            var currentPromotions = promotions.Where(x => !x.Promotion_PaymentMethods.Any()).ToList();

            //Por cada promocion activa actualizo el carrito
            foreach (var promotion in currentPromotions)
            {
                UpdateShoppingCartAccordingToPromotion(promotion, ref shoppingCart);
            }
        }


        [HttpGet]
        public JsonResult CheckCurrentPromotions(bool isIndex, int paymentMethodId = -1)
        {
            var cartGuid = ShoppingCartHelper.GetGuid();
            var shoppingCart = GetShoppingCart(cartGuid);

            //Escapo la promoción si es un pedido de venta Telefonica.
            if (shoppingCart.OrderDetails.Any(x => x.CategorySeoUrl.Contains("venta-telefonica")))
                return Json(new { Value = string.Empty }, JsonRequestBehavior.AllowGet);

            //Traer usertuypeenum tipo de promocion
            var storeType = this.StoreType;

            //Traigo todas las promos activas
            var promotions = modelEntities.Promotions.Where(x => x.UserType == (int)this.StoreType && x.StartDate <= CurrentDate.Now && x.EndDate >= CurrentDate.Now && !x.Deleted.HasValue);

            //Si las promociones tienen formas de pagos asociadas se evaluan solo en el checkout
            List<Promotion> currentPromotions;
            if (isIndex)
            {
                promotions = promotions.Where(x => !x.Promotion_PaymentMethods.Any());
                currentPromotions = promotions.ToList();
            }
            else
            {
                shoppingCart.PaymentMethodId = paymentMethodId;
                promotions = promotions.Where(x => x.Promotion_PaymentMethods.Any(p => p.PaymentMethodId == paymentMethodId || paymentMethodId == -1));

                currentPromotions = promotions.ToList();

                //si no hay promociones asociadas a la forma de pago, dejo activas las otras activas sin medio de pago.
                if (currentPromotions.Count == 0)
                {
                    promotions = modelEntities.Promotions.Where(x => x.UserType == (int)this.StoreType && x.StartDate <= CurrentDate.Now && x.EndDate >= CurrentDate.Now && !x.Deleted.HasValue);
                    promotions = promotions.Where(x => !x.Promotion_PaymentMethods.Any());
                    currentPromotions = promotions.ToList();
                }
            }

            //Por cada promocion activa actualizo el carrito
            foreach (var promotion in currentPromotions)
            {
                UpdateShoppingCartAccordingToPromotion(promotion, ref shoppingCart);
            }

            //Armo el objeto de resultados con el que actualizo la vista
            OrderDetailPromotionResultModel model = new OrderDetailPromotionResultModel();
            List<ResultModel> results = new List<ResultModel>();
            model.results = results;

            if (shoppingCart.Discount > 0)
            {
                model.OrderDiscount = shoppingCart.Discount;
            }
            else
            {
                foreach (var od in shoppingCart.OrderDetails)
                {
                    ResultModel result = new ResultModel();
                    result.OrderDetailId = od.OrderDetailId;
                    result.Message = !string.IsNullOrEmpty(od.PromotionMessage) ? od.PromotionMessage : string.Empty;
                    result.Price = od.Price;
                    results.Add(result);
                }
            }

            return Json(new { Model = model }, JsonRequestBehavior.AllowGet);
        }

        private void UpdateShoppingCartAccordingToPromotion(Promotion promotion, ref ShoppingCartViewModel shoppingCart)
        {
            //Parametros que tiene la promocion (aplica segun el tipo de promocion)
            var discountPercent = promotion.DiscountPercent.HasValue ? promotion.DiscountPercent.Value : 0;
            var minOrder = promotion.MinimumOrder.HasValue ? promotion.MinimumOrder.Value : 0;
            //var paymentMethodId = promotion.PaymentMethodId.HasValue ? promotion.PaymentMethodId.Value : -1;
            var paymentMethods = promotion.Promotion_PaymentMethods.ToList();
            var minUnits = promotion.MinimumUnits.HasValue ? promotion.MinimumUnits.Value : -1;
            var unitsTaken = promotion.UnitsTaken.HasValue ? promotion.UnitsTaken.Value : 0;
            var unitsPaid = promotion.UnitsPaid.HasValue ? promotion.UnitsPaid.Value : 0;
            var categoryPromotions = promotion.CategoryPromotions.ToList();
            var productPromotions = promotion.ProductPromotions.ToList();
            var everyUnits = promotion.EveryUnits.HasValue ? promotion.EveryUnits.Value : 0;
            var freeUnits = promotion.FreeUnits.HasValue ? promotion.FreeUnits.Value : 0;
            var excludedProductList = promotion.ExcludedProducts.ToList();

            switch (promotion.PromotionTypeId)
            {
                case (int)PromotionTypeEnum.Escala:
                    //Logica de promocion de escala
                    UpdateShoppingCartScale(discountPercent, minOrder, paymentMethods, ref shoppingCart);
                    break;
                case (int)PromotionTypeEnum.Dividida:
                    //Logica de promocion dividida
                    UpdateShoppingCartDivided(minUnits, unitsPaid, unitsTaken, paymentMethods, categoryPromotions, productPromotions, ref shoppingCart, excludedProductList);
                    break;

                case (int)PromotionTypeEnum.Cantidad:
                    //Logica de promocion por cantidad
                    UpdateShoppingCartQuantity(everyUnits, freeUnits, minUnits, ref shoppingCart);
                    break;

                case (int)PromotionTypeEnum.Articulos:
                    //Logica de promocion por articulos
                    UpdateShoppingCartArticles(discountPercent, categoryPromotions, productPromotions, ref shoppingCart, excludedProductList);
                    break;
                    //case (int)PromotionTypeEnum.Porcentaje:
                    //    //Logica de promocion por porcentaje
                    //    UpdateShoppingCartPercentage(discountPercent, promotion, categoryPromotions, productPromotions, ref shoppingCart, excludedProductList);
                    //    break;
                    //case (int)PromotionTypeEnum.Porcentaje:
                    //    //Logica de promocion por porcentaje
                    //    UpdateShoppingCartArticles(discountPercent, categoryPromotions, productPromotions, ref shoppingCart, excludedProductList);
                    //    break;
            }
        }

        #region Promocion de escala
        private void UpdateShoppingCartScale(decimal discount, decimal minOrder, List<Promotion_PaymentMethods> paymentMethods, ref ShoppingCartViewModel shoppingCart)
        {
            //Me fijo si 
            var selectedPaymentMethodId = shoppingCart.PaymentMethodId;
            if (paymentMethods.Any(x => x.PaymentMethodId == selectedPaymentMethodId))
            {
                UpdateCartDiscount(shoppingCart.TotalOrderDetails, discount, minOrder, ref shoppingCart, shoppingCart.Discount);
            }
            else
            {
                UpdateCartDiscount(shoppingCart.TotalOrderDetails, discount, minOrder, ref shoppingCart, shoppingCart.Discount);
            }
        }

        public void UpdateCartDiscount(decimal totalOrderDetails, decimal discount, decimal minOrder, ref ShoppingCartViewModel shoppingCart, decimal currentDiscount)
        {
            if (totalOrderDetails >= minOrder && discount > currentDiscount)
            {
                shoppingCart.Discount = discount;
            }
        }
        #endregion

        #region Promocion dividida 
        private void UpdateShoppingCartDivided(int minUnits, int unitsPaid, int unitsTaken, List<Promotion_PaymentMethods> paymentMethods, List<CategoryPromotion> categoryPromotionList, List<ProductPromotion> productPromotionList, ref ShoppingCartViewModel shoppingCart, List<ExcludedProduct> excludedProductList)
        {
            var selectedPaymentMethodId = shoppingCart.PaymentMethodId;
            if (paymentMethods.Any(x => x.PaymentMethodId == selectedPaymentMethodId) && shoppingCart.TotalItemsCount >= minUnits)
            {
                var productIds = SelectIdsFromProductPromotion(productPromotionList);
                var categoryIds = SelectIdsFromCategoryPromotion(categoryPromotionList);
                var odsToApplyPromotion = OdsApplyPromotionByProductAndCategory(categoryIds, productIds, shoppingCart.OrderDetails, excludedProductList);

                foreach (var od in odsToApplyPromotion)
                {
                    List<Promotion> matchingPromotions = new List<Promotion>();
                    var orderDetail = od;
                    if (productIds.Contains(od.ProductId))
                    {
                        matchingPromotions = productPromotionList.Where(x => x.Promotion.DiscountPercent.HasValue && x.ProductId == orderDetail.ProductId).Select(y => y.Promotion).ToList();
                        //UpdateOdPriceDivided(ref orderDetail, matchingPromotions);
                    }
                    if ((od.CategoryId.HasValue && categoryIds.Contains(od.CategoryId.Value)) || (od.ParentCategoryId.HasValue && categoryIds.Contains(od.ParentCategoryId.Value)))
                    {
                        matchingPromotions = categoryPromotionList.Where(x => x.Promotion.DiscountPercent.HasValue && (x.CategoryId == orderDetail.CategoryId || x.CategoryId == orderDetail.ParentCategoryId)).Select(y => y.Promotion).ToList();
                        //UpdateOdPriceDivided(ref orderDetail, matchingPromotions);
                    }
                }
            }
        }
        #endregion

        #region Promocion por cantidad
        private void UpdateShoppingCartQuantity(int everyUnits, int freeUnits, int minUnits, ref ShoppingCartViewModel shoppingCart)
        {
            var cartUnits = shoppingCart.OrderDetails.Sum(x => x.Quantity);

            if (cartUnits >= minUnits)
            {
                var orderedItems = shoppingCart.OrderDetails.OrderBy(x => x.Price).ToList();
                var itemsCount = shoppingCart.OrderDetails.Count();

                int totalQuantityToDiscount = cartUnits / everyUnits * freeUnits;
                int pending_to_discount = totalQuantityToDiscount;
                int i = 0;

                while (pending_to_discount > 0 && i < itemsCount)
                {
                    if (orderedItems[i].Quantity <= pending_to_discount)
                    {
                        pending_to_discount -= orderedItems[i].Quantity;

                        orderedItems[i].Price = 0;
                        orderedItems[i].PromotionMessage = orderedItems[i].Quantity == 1 ? orderedItems[i].Quantity + " Unidad gratis" : orderedItems[i].Quantity + " Unidades gratis";
                    }
                    else
                    {
                        orderedItems[i].PromotionMessage = pending_to_discount == 1 ? pending_to_discount + " Unidad gratis" : pending_to_discount + " Unidades gratis";
                        orderedItems[i].Price = Convert.ToInt32(Math.Floor(orderedItems[i].Price * (orderedItems[i].Quantity - pending_to_discount) / orderedItems[i].Quantity));

                        pending_to_discount = 0;
                    }

                    i++;
                }

                /*foreach (var orderDetail in orderDetails)
                {
                    //quantitytodiscount = orderDetail.Quantity / everyUnits * freeUnits;
                    //orderDetail.Price = Convert.ToInt32(Math.Floor(orderDetail.Price * (orderDetail.Quantity - quantitytodiscount) / orderDetail.Quantity));
                    orderDetail.PromotionMessage = quantitytodiscount == 1 ? quantitytodiscount + " Unidad gratis" : quantitytodiscount + " Unidades gratis";
                }*/

                shoppingCart.OrderDetails = orderedItems;
            }


        }

        //private bool CanSumOdQuantity(ref int unitsTaken, int freeUnits, ref OrderDetailViewModel od)
        //{
        //    var canSum = true;

        //    if (unitsTaken + od.Quantity == freeUnits)
        //    {
        //        od.Price = 0;
        //        od.PromotionMessage = freeUnits + " unidades gratis";
        //    }
        //    else if (unitsTaken + od.Quantity < freeUnits)
        //    {
        //        od.Price = 0;
        //        od.PromotionMessage = unitsTaken + " unidades gratis";
        //        unitsTaken += od.Quantity;
        //    }
        //    else
        //    {
        //        canSum = false;
        //        var sumando = freeUnits - unitsTaken;
        //        od.PromotionMessage = unitsTaken + " unidades gratis";
        //        od.Price = od.Price * (od.Quantity - sumando) / od.Quantity;
        //    }

        //    return canSum;
        //}
        #endregion

        #region Promocion por articulos

        private void UpdateShoppingCartArticles(decimal discount, List<CategoryPromotion> categoryPromotionList, List<ProductPromotion> productPromotionList, ref ShoppingCartViewModel shoppingCart, List<ExcludedProduct> excludedProductList)
        {
            var productIds = SelectIdsFromProductPromotion(productPromotionList);
            var categoryIds = SelectIdsFromCategoryPromotion(categoryPromotionList);
            var odsToApplyPromotion = OdsApplyPromotionByProductAndCategory(categoryIds, productIds, shoppingCart.OrderDetails, excludedProductList);

            foreach (var item in odsToApplyPromotion)
            {
                decimal maxDiscount = 0;
                var od = odsToApplyPromotion.FirstOrDefault(x => x.OrderDetailId == item.OrderDetailId);
                UpdateOrderDetailPromotionList(productPromotionList, categoryPromotionList, productIds, categoryIds, od, ref maxDiscount);
                UpdateOd(maxDiscount, ref od, maxDiscount + "% off");
            }

        }

        public List<OrderDetailViewModel> OdsApplyPromotionByProductAndCategory(List<int> categoryIds, List<int> productIds, List<OrderDetailViewModel> ods, List<ExcludedProduct> excludedProductList)
        {
            return ods.Where(x => !excludedProductList.Select(ep => ep.ProductId).Contains(x.ProductId) && (productIds.Contains(x.ProductId) || (x.CategoryId.HasValue && categoryIds.Contains(x.CategoryId.Value)) || (x.ParentCategoryId.HasValue && categoryIds.Contains(x.ParentCategoryId.Value)))).ToList();
        }

        private void UpdateShoppingCartPercentage(decimal discount, Promotion promotion, List<CategoryPromotion> categoryPromotionList, List<ProductPromotion> productPromotionList, ref ShoppingCartViewModel shoppingCart, List<ExcludedProduct> excludedProductList)
        {
            var productIds = SelectIdsFromProductPromotion(productPromotionList);
            var categoryIds = SelectIdsFromCategoryPromotion(categoryPromotionList);
            var odsToApplyPromotion = OdsApplyPercentagePromotion(categoryIds, productIds, shoppingCart.OrderDetails, excludedProductList);

            foreach (var item in odsToApplyPromotion)
            {
                decimal maxDiscount = 0;
                var od = odsToApplyPromotion.FirstOrDefault(x => x.OrderDetailId == item.OrderDetailId);
                //UpdateOrderDetailPromotionPercentage(promotion, productPromotionList, categoryPromotionList, productIds, categoryIds, od, ref maxDiscount);
                UpdateOd(maxDiscount, ref od, maxDiscount + "% off");
            }

        }

        public List<OrderDetailViewModel> OdsApplyPercentagePromotion(List<int> categoryIds, List<int> productIds, List<OrderDetailViewModel> ods, List<ExcludedProduct> excludedProductList)
        {
            return ods.Where(od => ((categoryIds.Count() < 1 && productIds.Count() < 1 || (od.CategoryId.HasValue && categoryIds.Contains(od.CategoryId.Value)) || (od.ParentCategoryId.HasValue && categoryIds.Contains(od.ParentCategoryId.Value))) && !excludedProductList.Select(ep => ep.ProductId).Contains(od.ProductId))).ToList();
        }

        public void UpdateOrderDetailPromotionList(List<ProductPromotion> productPromotionList, List<CategoryPromotion> categoryPromotionList, List<int> productIds, List<int> categoryids, OrderDetailViewModel od, ref decimal maxDiscount)
        {
            List<Promotion> matchingPromotions = new List<Promotion>();
            var orderDetail = od;
            if (productIds.Contains(od.ProductId))
            {
                matchingPromotions = productPromotionList.Where(x => x.Promotion.DiscountPercent.HasValue && x.ProductId == orderDetail.ProductId).Select(y => y.Promotion).ToList();
                UpdateMaxDiscount(matchingPromotions, ref maxDiscount);
            }
            if ((od.CategoryId.HasValue && categoryids.Contains(od.CategoryId.Value)) || (od.ParentCategoryId.HasValue && categoryids.Contains(od.ParentCategoryId.Value)))
            {
                matchingPromotions = categoryPromotionList.Where(x => x.Promotion.DiscountPercent.HasValue && (x.CategoryId == orderDetail.CategoryId || x.CategoryId == orderDetail.ParentCategoryId)).Select(y => y.Promotion).ToList();
                UpdateMaxDiscount(matchingPromotions, ref maxDiscount);
            }
        }

        public void UpdateOrderDetailPromotionPercentage(Promotion promotion, List<ProductPromotion> productPromotionList, List<CategoryPromotion> categoryPromotionList, List<int> productIds, List<int> categoryids, OrderDetailViewModel od, ref decimal maxDiscount)
        {
            List<Promotion> matchingPromotions = new List<Promotion>();
            var orderDetail = od;
            if (productIds.Count() >= 1 || categoryids.Count() >= 1)
            {
                if (productIds.Contains(od.ProductId))
                {
                    matchingPromotions = productPromotionList.Where(x => x.Promotion.DiscountPercent.HasValue && x.ProductId == orderDetail.ProductId).Select(y => y.Promotion).ToList();
                    UpdateMaxDiscount(matchingPromotions, ref maxDiscount);
                }
                if ((od.CategoryId.HasValue && categoryids.Contains(od.CategoryId.Value)) || (od.ParentCategoryId.HasValue && categoryids.Contains(od.ParentCategoryId.Value)))
                {
                    matchingPromotions = categoryPromotionList.Where(x => x.Promotion.DiscountPercent.HasValue && (x.CategoryId == orderDetail.CategoryId || x.CategoryId == orderDetail.ParentCategoryId)).Select(y => y.Promotion).ToList();
                    UpdateMaxDiscount(matchingPromotions, ref maxDiscount);
                }
            }
            else
            {
                matchingPromotions.Add(promotion);
                UpdateMaxDiscount(matchingPromotions, ref maxDiscount);
            }
        }

        public void UpdateOrderDetailCategoryPromotionList(List<CategoryPromotion> categoryPromotionList, OrderDetailViewModel od, ref decimal maxDiscount)
        {
            var orderDetail = od;
            var matchingPromotions = categoryPromotionList.Where(x => x.Promotion.DiscountPercent.HasValue && x.CategoryId == orderDetail.CategoryId).Select(y => y.Promotion).ToList();

            UpdateMaxDiscount(matchingPromotions, ref maxDiscount);
        }

        public void UpdateMaxDiscount(List<Promotion> list, ref decimal maxDiscount)
        {
            var highestDiscount = GetHighestDiscountFromPromotionList(list);
            maxDiscount = highestDiscount > maxDiscount ? highestDiscount : maxDiscount;
        }

        public void UpdateOd(decimal discount, ref OrderDetailViewModel od, string PromotionMessage)
        {
            od.OldPrice = od.Price;
            od.Price -= od.Price * discount / 100;
            od.PromotionMessage = string.IsNullOrEmpty(od.PromotionMessage) ? PromotionMessage : od.PromotionMessage + ", " + PromotionMessage;
        }

        public List<int> SelectIdsFromProductPromotion(List<ProductPromotion> list)
        {
            return list.Select(y => y.ProductId).ToList();
        }

        public List<int> SelectIdsFromCategoryPromotion(List<CategoryPromotion> list)
        {
            return list.Select(y => y.CategoryId).ToList();
        }

        public decimal GetHighestDiscountFromPromotionList(List<Promotion> list)
        {
            return list.OrderByDescending(x => x.DiscountPercent.Value).FirstOrDefault().DiscountPercent.Value;
        }

        #endregion

        [HttpGet]
        public JsonResult GetSizes(int ProductId, int ColorId)
        {
            var product = modelEntities.Products.Find(ProductId);

            var sizes = product
               .Stocks
               .Where(x => x.Color.Deleted == null && x.Size.Deleted == null && x.Deleted == null)
               .Where(x => x.ColorId == ColorId)
               .Select(x => new
               {
                   Id = x.SizeId,
                   Name = x.Size.Description,
                   SoldOut = x.StockVirtual < 0 || x.StockVirtual == 0 || x.Product.SoldOut ? true : false,
                   DisplayOrder = x.Size.DisplayOrder
               }).OrderBy(x => x.DisplayOrder).ToList();

            return Json(new { sizes }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetColorSizeStock(int ProductId, int SizeId, int ColorId)
        {
            var stock = modelEntities.Stocks.FirstOrDefault(x => x.StockVirtual > 0 && x.Color.Deleted == null && x.Size.Deleted == null &&
                        x.ProductId == ProductId && x.SizeId == SizeId && x.ColorId == ColorId);

            var quantity = 0;
            if (stock != null)
            {
                quantity = stock.StockVirtual;
            }

            return Json(new { Stock = quantity }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetPriceDifference(int ProductId, int SizeId)
        {
            var product = modelEntities.Products.Find(ProductId);

            var sizes = product
               .Stocks
               .FirstOrDefault(x => x.Color.Deleted == null && x.Size.Deleted == null && x.Deleted == null && x.SizeId == SizeId).Price;

            if (sizes == null)
            {
                sizes = product.WholesalePrice;
            }


            return Json(new { sizes }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult QuickViewModal(int id)
        {
            var product = modelEntities.Products.Find(id);

            var model = new ProductQuickViewModel()
            {
                Id = product.ProductId,
                Title = product.Title,
                Description = product.Description,
                Code = product.Code,
                ShareUrl = Path.Combine(Utility.ServerRoot, "shop", product.Category.SeoUrl, product.SeoUrl),
                Price = product.CustomerPrice,
                OldPrice = product.CustomerOldPrice,
                Cloth = product.Cloth,
                ColorList = product.Stocks.Where(s => s.StockReal > 0 && s.Color.Deleted == null && s.Size.Deleted == null).Select(c => c.Color).Distinct().ToList(),
                SizeList = product.Stocks.Where(s => s.StockReal > 0 && s.Color.Deleted == null && s.Size.Deleted == null).Select(c => c.Size).Distinct().OrderBy(s => s.DisplayOrder).ToList(),
                Images = product.Images.OrderByDescending(x => x.Default).Select(z => new ProductImageViewModel()
                {
                    Id = z.ImageId,
                    ProductId = z.ProductId.Value,
                    ColorId = z.ColorId,
                    Orientation = z.Orientation,
                    ImageName = z.ImageName,
                    Default = z.Default,
                }).ToList()

            };

            TempData["UserType"] = (int)UserTypeEnum.Mayorista;

            return PartialView("Shop/Product/QuickView", model);
        }

        [HttpGet]
        public string SizeTableTypeImageUrl(int ProductId)
        {
            string url = "";
            var product = modelEntities.Products.Find(ProductId);

            url = "/Content/images/aw19/guiatalles/guia_de_talles.png";

            if (product.Category.SizeTableTypeId.HasValue)
            {

                switch (product.Category.SizeTableTypeId.Value)
                {
                    case ((int)SizeTableTypeEnum.Top):
                        url = "/Content/images/aw19/guiatalles/guia top.jpg";
                        break;
                    case ((int)SizeTableTypeEnum.Bottom):
                        url = "/Content/images/aw19/guiatalles/guia bottom.jpg";
                        break;
                    case ((int)SizeTableTypeEnum.Jeans):
                        url = "/Content/images/aw19/guiatalles/guia jeans.jpg";
                        break;
                    default:
                        url = "/Content/images/aw19/guiatalles/guia_de_talles.png";
                        break;
                }
            }
            else if (product.Category.ParentCategoryId.HasValue && product.Category.ParentCategory.SizeTableTypeId.HasValue)
            {
                switch (product.Category.ParentCategory.SizeTableTypeId.Value)
                {
                    case ((int)SizeTableTypeEnum.Top):
                        url = "/Content/images/aw19/guiatalles/guia top.jpg";
                        break;
                    case ((int)SizeTableTypeEnum.Bottom):
                        url = "/Content/images/aw19/guiatalles/guia bottom.jpg";
                        break;
                    case ((int)SizeTableTypeEnum.Jeans):
                        url = "/Content/images/aw19/guiatalles/guia jeans.jpg";
                        break;
                    default:
                        url = "/Content/images/aw19/guiatalles/guia_de_talles.png";
                        break;
                }
            }

            return url;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var cartGuid = ShoppingCartHelper.GetGuid();
            var shoppingCartViewModel = GetShoppingCart(cartGuid);

            if (shoppingCartViewModel == null || shoppingCartViewModel.OrderDetails.Count == 0)
                return View("EmptyCart");

            UpdateCartPrices(shoppingCartViewModel.OrderDetails);

            return View(shoppingCartViewModel);
        }

        [HttpPost]
        public ActionResult Index(ShoppingCartViewModel viewModel)
        {
            var discount = viewModel.Discount;

            var cartGuid = ShoppingCartHelper.GetGuid();
            var shoppingCartViewModel = GetShoppingCart(cartGuid);

            if (shoppingCartViewModel == null || shoppingCartViewModel.OrderDetails.Count == 0)
                return View("EmptyCart");


            shoppingCartViewModel.Discount = discount;

            var total = shoppingCartViewModel.OrderDetails.Sum(x => x.Price * x.Quantity);
            shoppingCartViewModel.FreeShipping = FreeShipping((int)this.StoreType, total);

            UpdateCartPrices(shoppingCartViewModel.OrderDetails);

            shoppingCartViewModel.DeliveryMethods = modelEntities.DeliveryMethods.Where(x => x.Active && x.Enabled).Select(x => new DeliveryMethodViewModel
            {
                Name = x.Name,
                Description = x.Description,
                ExcludedPaymetnMethods = x.ExcludedPaymetnMethods,
                Cost = x.Cost,
                RequiresData = x.RequiresData,
                IsTransport = x.IsTransport,
                IsLocal = x.IsLocal,
                ShowLocales = x.ShowLocales,
                IsProvince = x.IsProvince,
                DeliveryMethodId = x.DeliveryMethodId
            }).ToList();

            shoppingCartViewModel.PaymentMethods = modelEntities.PaymentMethods.Where(x => x.Active && x.Enabled.HasValue).Select(y => new PaymentMethodViewModel
            {
                Name = y.Name,
                Description = y.Description,
                Cost = y.Cost,
                RequiresData = y.RequiresData,
                PaymentMethodId = y.PaymentMethodId
            }).ToList();

            Session["ShoppingCart"] = shoppingCartViewModel;

            return Redirect("/Checkout");
        }

        [HttpPost]
        public JsonResult UpdateOrderDetailQuantity(int OrderDetailId, int Quantity)
        {
            var orderDetail = modelEntities.OrderDetails.FirstOrDefault(x => x.OrderDetailId == OrderDetailId);

            orderDetail.Quantity = Quantity;

            modelEntities.SaveChanges();

            return Json(new { status = "ok" });
        }

        public JsonResult GetInterestsForSelectedBank(int BankId, int UserType = -1)
        {
            List<BankInterest> bankInterests = new List<BankInterest>();
            UserTypeEnum currentStore = new UserTypeEnum();

            if (UserType != -1)
                currentStore = (UserTypeEnum)UserType;
            else
                currentStore = this.StoreType;

            var query = modelEntities.BankInterests
                .Where(x => !x.Deleted.HasValue && x.BankId == BankId && x.Active && x.StoreType != (int)StoreTypeEnum.NotPublished)
                .OrderByDescending(x => x.DisplayOrder.HasValue).ThenBy(x => x.DisplayOrder).AsQueryable();

            if (currentStore == UserTypeEnum.Mayorista)
                query = query.Where(x => x.StoreType == (int)StoreTypeEnum.WholeSalerOnly || x.StoreType == (int)StoreTypeEnum.PublishedForAll).AsQueryable();
            else
                query = query.Where(x => x.StoreType == (int)StoreTypeEnum.RetailerOnly || x.StoreType == (int)StoreTypeEnum.PublishedForAll).AsQueryable();

            var bankInterestsToShow = query.Select(x => new
            {
                Id = x.BankInterestId,
                Name = x.Name,
                Installments = x.Installments,
                Surcharge = x.Percent,
                MinAmount = x.MinimumAmount,
            }).ToList();

            return Json(new { Interests = bankInterestsToShow }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Checkout()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Account/Login?ReturnUrl=/checkout");
            }

            ShoppingCartViewModel shoppingCartModel = (ShoppingCartViewModel)Session["ShoppingCart"];

            if (shoppingCartModel == null)
                return Redirect("/shopping-cart");

            var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == User.Identity.Name && x.Active && x.Deleted == null);
            shoppingCartModel.TermsAndConditions = modelEntities.Institutionals.SingleOrDefault(x => x.InstitutionalId == 1).TermsAndConditions;
            shoppingCartModel.Customer = customer;
            shoppingCartModel.StoreType = this.StoreType;

            ExtraInfoViewModel extrainfo = new ExtraInfoViewModel();

            var stores = modelEntities.Stores.Where(x => !x.Deleted.HasValue);
            extrainfo.RetailerStores = stores.Where(x => x.RetailDelivery).ToList();
            extrainfo.WholesalerStores = stores.Where(x => x.WholesalerDelivery).ToList();
            extrainfo.Customer = customer;

            shoppingCartModel.ExtraInfo = extrainfo;

            var provinces = modelEntities.Provinces.ToList();
            TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name", customer.ProvinceId);

            if (customer.UserType == (int)UserTypeEnum.Mayorista || this.StoreType == UserTypeEnum.Mayorista)
            {
                var banks = modelEntities.Banks.Where(x => !x.Deleted.HasValue && x.BankInterests.Any(bi => !bi.Deleted.HasValue && bi.Active &&
                (bi.StoreType == (int)StoreTypeEnum.WholeSalerOnly || bi.StoreType == (int)StoreTypeEnum.PublishedForAll)
                && bi.StoreType != (int)StoreTypeEnum.NotPublished)).ToList();
                shoppingCartModel.BankList = banks;
            }
            else
            {
                var banks = modelEntities.Banks.Where(x => !x.Deleted.HasValue && x.BankInterests.Any(bi => !bi.Deleted.HasValue && bi.Active &&
                (bi.StoreType == (int)StoreTypeEnum.RetailerOnly || bi.StoreType == (int)StoreTypeEnum.PublishedForAll)
                && bi.StoreType != (int)StoreTypeEnum.NotPublished)).ToList();
                shoppingCartModel.BankList = banks;
            }

            //Descuento de promociones activas.
            UpdateCartWithCurrentPromotions(ref shoppingCartModel);

            //Descuento de primera compra.
            var config = modelEntities.Configurations.FirstOrDefault();
            var enabled = this.StoreType == UserTypeEnum.Minorista ? config.FirstOrderDiscountEnabled : config.FirstOrderDiscountEnabledWS;
            var periodDays = this.StoreType == UserTypeEnum.Minorista ? config.FirstOrderDiscountDurationPeriodDays : config.FirstOrderDiscountDurationPeriodDaysWS;
            var percent = this.StoreType == UserTypeEnum.Minorista ? config.FirstOrderDiscountPercent : config.FirstOrderDiscountPercentWS;

            if (enabled)
            {
                var hasOrders = modelEntities.Orders.Any(x => x.CustomerId == shoppingCartModel.Customer.CustomerId && x.Deleted == null && x.Active);

                if (!hasOrders)
                {
                    var validDate = shoppingCartModel.Customer.Created;
                    if (customer.FirstOrderDiscountPeriodDays.HasValue)
                        validDate = validDate.AddDays(customer.FirstOrderDiscountPeriodDays.Value);
                    else
                        validDate = validDate.AddDays(periodDays);

                    if (validDate > CurrentDate.Now)
                    {
                        shoppingCartModel.Discount = percent;
                    }
                }
            }

            return View(shoppingCartModel);
        }

        [HttpPost]
        public ActionResult Checkout(FormCollection Form)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var cartGuid = ShoppingCartHelper.GetGuid();
                    var shoppingCartViewModel = GetShoppingCart(cartGuid);

                    if (shoppingCartViewModel == null || shoppingCartViewModel.OrderDetails.Count == 0)
                        return View("EmptyCart");

                    var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == User.Identity.Name && x.Active && x.Deleted == null);

                    UpdateCartWithCurrentPromotions(ref shoppingCartViewModel);

                    Order order = new Order();


                    customer.IvaConditionId = int.Parse(Form["IvaConditionId"]);
                    customer.CUIT = Form["cuit"];
                    customer.BillingAddress = Form["BillingAddress"];
                    customer.FullName = Form["Grl_Name"];
                    customer.CorporateName = Form["CorporateName"];
                    customer.FullName = Form["Grl_Name"];
                    customer.ProvinceId = int.Parse(Form["ProvinceId"]);
                    customer.City = Form["City"];
                    customer.ZipCode = Form["ZipCode"];
                    customer.PhoneNumber = Form["Phone"];

                    var receiptAmount = Form["ReceiptAmount"];

                    CreateOrder(Form, customer, shoppingCartViewModel, ref order, receiptAmount);

                    SetOrderDeliveryInfo(Form, customer, shoppingCartViewModel, ref order);

                    var redirectUrl = SetOrderPaymentInfo(Form, customer, shoppingCartViewModel, ref order);

                    shoppingCartViewModel.OrderId = order.OrderId;

                    Session["ShoppingCart"] = shoppingCartViewModel;

                    modelEntities.SaveChanges();

                    //Si devuelve un mensaje, no se aprobó el pago de tarjeta y hago rollback de la venta.
                    if (redirectUrl.Contains("/Account/Information"))
                        dbContextTransaction.Rollback();
                    else
                        dbContextTransaction.Commit();

                    if (redirectUrl == string.Empty)
                        return Redirect("/Payment/PaymentSuccessful?Id=" + order.OrderId);
                    else
                        return Redirect(redirectUrl);
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();

                    TempData["Message"] = "Se produjo un error al intentar guardar la venta.";
                    TempData["ActionType"] = ActionType.Back;

                    return Redirect("/Account/Information");
                }
            }
        }

        private static void UpdateCustomerCartPushDate(Customer customer)
        {
            if (customer.CartPushDate.HasValue && customer.CartPushDate.Value != default(DateTime))
            {
                customer.CartPushDate = null;
            }
        }

        private string SetOrderPaymentInfo(FormCollection Form, Customer customer, ShoppingCartViewModel shoppingCartViewModel, ref Order order)
        {
            var orderId = order.OrderId;
            //Mercado Pago
            if (order.PaymentMethodId == Int32.Parse(modelEntities.PaymentMethods.FirstOrDefault(y => y.Name.Contains("Mercado")).PaymentMethodId.ToString()))
            {
                Preference preference = ShoppingCartHelper.CreatePreference(order, customer);
                ThreadStart job = delegate
                {
                    IMailing mailUtil = Mailing.Instance;
                    mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                    mailUtil.SendOrderInProcessResumeMail(orderId);
                };


                new Thread(job).Start();
                return preference.InitPoint;
            }

            if (order.PaymentMethodId == (int)PaymentMethodEnum.Decidir)
            {
                var selectedBankInterestId = int.Parse(Request["SelectedBankInteresId"]);

                var bankInterest = modelEntities.BankInterests.Include("Bank").FirstOrDefault(x => x.BankInterestId == selectedBankInterestId);
                if (bankInterest != null)
                {
                    order.Surcharge = bankInterest.Percent;
                    order.PaymentMethodId_Decidir = bankInterest.Bank.PaymenMethodId_Decidir.HasValue ? bankInterest.Bank.PaymenMethodId_Decidir.Value : 1;
                    order.BankInterestId = bankInterest.BankInterestId;
                }

                var token = Request["decidir-token"];
                var bin = Request["decidir-bin"];
                var installments = int.Parse(Request["decidir-installments"]);

                DecidirService decidir = new DecidirService();
                var response = decidir.GenerateDecidirPayment(order, token, bin, installments, "single");

                if (response.status == "approved")
                {
                    order.DecidirPaymentId = (int)response.id;
                    modelEntities.SaveChanges();
                }
                else
                {
                    order.OrderStatusId = (int)OrderStatusEnum.Cancelada;

                    //ACA REASIGNO LOS ORDERDETAILS AL CARRITO
                    /*var orderDetails = order.OrderDetails.ToList();

                    foreach (var od in orderDetails)
                    {
                        OrderDetail reAssignedOrderDetail = new OrderDetail();
                        reAssignedOrderDetail.StoreType = (int)this.StoreType;
                        reAssignedOrderDetail.ColorId = od.ColorId;
                        reAssignedOrderDetail.SizeId = od.SizeId;
                        reAssignedOrderDetail.ProductId = od.ProductId;
                        reAssignedOrderDetail.Price = od.Price;
                        reAssignedOrderDetail.Quantity = od.Quantity;
                        reAssignedOrderDetail.Created = CurrentDate.Now;
                        reAssignedOrderDetail.CreatedBy = od.CreatedBy;
                        reAssignedOrderDetail.Discount = od.Discount;
                        reAssignedOrderDetail.Active = true;

                        modelEntities.OrderDetails.Add(reAssignedOrderDetail);
                    }*/

                    var responseMessage = GetDecidirResponseMessage(response);

                    //modelEntities.SaveChanges();


                    TempData["Message"] = responseMessage;
                    TempData["ActionType"] = ActionType.ShoppingCart;

                    return "/Account/Information";
                }

            }

            return string.Empty;
        }

        private void SetOrderDeliveryInfo(FormCollection Form, Customer customer, ShoppingCartViewModel shoppingCartViewModel, ref Order order)
        {
            if (order.DeliveryMethodId == Services.Utils.Utility.Transporte)
            {
                var deliveryOrder = modelEntities.DeliveryOrders.FirstOrDefault(x => !x.Deleted.HasValue);
                var company = Form["ShipmentCompany"];
                var address = Form["ShipmentCompanyAddress"];
                //var destinationShipment = int.Parse(Request.Form["DestinationShipment"]);

                //order.DestinationShipmentId = destinationShipment;

                shoppingCartViewModel.DeliveryAddress = address;

                customer.ShipmentCompanyAddress = address;
                customer.ShipmentCompany = company;

                customer.ProvinceId = int.Parse(Form["ProvinceId"]);
                customer.City = Form["City"];
                customer.ZipCode = Form["ZipCode"];

                modelEntities.SaveChanges();
            }

            if (order.DeliveryMethodId == Services.Utils.Utility.RetiroLocal)
            {
                var storeId = 0;
                if (this.StoreType == UserTypeEnum.Minorista)
                {
                    storeId = int.Parse(Form["RetailStoreId"]);
                }
                else
                {
                    //storeId = int.Parse(Form["WholesaleStoreId"]);
                }
                //order.StoreId = storeId;
                modelEntities.SaveChanges();
            }

            if (order.DeliveryMethodId == Services.Utils.Utility.DomicilioCaba || order.DeliveryMethodId == Services.Utils.Utility.DomicilioGba)
            {
                var cost = Form["ShippingCost"];

                customer.ZipCode = Form["Grl_ZipCode"];
                customer.Address = Form["Grl_Address"];
                customer.AddressNumber = Form["Grl_AddressNumber"];
                if (!string.IsNullOrEmpty(Form["Grl_Floor"]))
                    customer.Floor = Form["Grl_Floor"];
                if (!string.IsNullOrEmpty(Form["Grl_Department"]))
                    customer.Department = Form["Grl_Department"];
                if (order.DeliveryMethodId == Services.Utils.Utility.DomicilioCaba)
                {
                    customer.ProvinceId = int.Parse("2");
                }
                else
                {
                    customer.ProvinceId = int.Parse("1");
                }
                customer.City = Form["Grl_City"];

                modelEntities.SaveChanges();

                shoppingCartViewModel.DeliveryAddress = $"{customer.Address} {customer.AddressNumber} {customer.Floor} {customer.Department}, {customer.City} {customer.Province.Name}";

                order.ShippingCost = decimal.Parse(Form["ShippingCost"]);
                order.DeliveryAddress = shoppingCartViewModel.DeliveryAddress;

                modelEntities.SaveChanges();

            }

            if (order.DeliveryMethodId == Services.Utils.Utility.DomicilioProv)
            {
                var cost = Form["ShippingCost"];

                customer.ZipCode = Form["Grl_ZipCode"];
                customer.Address = Form["Grl_Address"];
                customer.AddressNumber = Form["Grl_AddressNumber"];
                if (!string.IsNullOrEmpty(Form["Grl_Floor"]))
                    customer.Floor = Form["Grl_Floor"];
                if (!string.IsNullOrEmpty(Form["Grl_Department"]))
                    customer.Department = Form["Grl_Department"];
                customer.ProvinceId = int.Parse(Form["Grl_ProvinceId"]);
                customer.City = Form["Grl_City"];

                modelEntities.SaveChanges();

                shoppingCartViewModel.DeliveryAddress = $"{customer.Address} {customer.AddressNumber} {customer.Floor} {customer.Department}, {customer.City} {customer.Province.Name}";

                order.ShippingCost = decimal.Parse(Form["ShippingCost"]);
                order.DeliveryAddress = shoppingCartViewModel.DeliveryAddress;

                modelEntities.SaveChanges();

            }


            //if (order.DeliveryMethodId == (int)DeliveryMethodEnum.OcaPaS)
            //{
            //    var ocaCenterId = int.Parse(Form["OcaCenterId"]);
            //    order.OcaCenterId = ocaCenterId;
            //    customer.ZipCode = Form["ZipCode"];

            //    var centers = GetOcaCenterList(customer.ZipCode);

            //    shoppingCartViewModel.DeliveryAddress = centers.FirstOrDefault(x => x.Id == ocaCenterId).Nombre;

            //    var cost = GetOcaShippingCost(customer.ZipCode, (int)OcaSendType.PAS);

            //    if (cost.HasValue)
            //        order.ShippingCost = cost.Value;
            //}

            if (order.DeliveryMethodId == (int)DeliveryMethodEnum.OcaPaP)
            {
                customer.ZipCode = Form["ZipCode"];
                //var cost = GetOcaShippingCost(customer.ZipCode, (int)OcaSendType.PAP);
                //if (cost.HasValue)
                //    order.ShippingCost = cost.Value;
                //else
                order.ShippingCost = 150;

                customer.Address = Form["Grl_Address"];
                customer.AddressNumber = Form["Grl_AddressNumber"];
                if (!string.IsNullOrEmpty(Form["Grl_Floor"]))
                    customer.Floor = Form["Grl_Floor"];
                if (!string.IsNullOrEmpty(Form["Grl_Department"]))
                    customer.Department = Form["Grl_Department"];
                customer.ProvinceId = int.Parse(Form["Grl_ProvinceId"]);
                customer.City = Form["City"];

                modelEntities.SaveChanges();

                shoppingCartViewModel.DeliveryAddress = $"{customer.Address} {customer.AddressNumber} {customer.Floor} {customer.Department}, {customer.City} {customer.Province.Name}";
            }
        }

        public JsonResult GetTransportShippingCost()
        {
            var shoppingCartManager = new ShoppingCartManager();
            var guid = ShoppingCartHelper.GetGuid();

            List<OrderDetail> cartDetails = _shoppingCartManager.GetShoppingCart(guid, User.Identity.IsAuthenticated ? User.Identity.Name : null, this.StoreType);

            var total = cartDetails.Sum(y => y.Quantity * y.Price);
            var cost = GetShippingCost(total);
            return Json(new { Cost = cost }, JsonRequestBehavior.AllowGet);
        }

        public decimal GetShippingCost(decimal total)
        {
            //var floor = modelEntities.Parameters.FirstOrDefault(x => x.Name == "MinimumOrderWS").Value;
            decimal ceiling = 0;
            var userType = User.Identity.IsAuthenticated ? ((CustomIdentity)User.Identity).CustomerPriceList : (int)UserTypeEnum.Minorista;
            switch (userType)
            {
                case (int)UserTypeEnum.Minorista:
                    ceiling = modelEntities.Parameters.FirstOrDefault(x => x.Name == "FreeShippingRetail").Value.Value;
                    break;
                case (int)UserTypeEnum.Mayorista:
                    ceiling = modelEntities.Parameters.FirstOrDefault(x => x.Name == "FreeShippingWS").Value.Value;
                    break;
            }
            var cost = modelEntities.Parameters.FirstOrDefault(x => x.Name == "ShipmentCost").Value;
            decimal shippingCost = 0;

            if (ceiling > total && cost.HasValue)
                shippingCost = cost.Value;

            return shippingCost;
        }

        private void UpdateCartPrices(List<OrderDetailViewModel> vOrderDetails)
        {
            var orderDetailsIds = vOrderDetails.Select(x => x.OrderDetailId).ToArray<int>();
            var orderDetails = modelEntities.OrderDetails.Include("Product").Where(x => orderDetailsIds.Contains(x.OrderDetailId)).ToList();
            var PercentageDiscounts = modelEntities.Promotions.Where(p => p.PromotionTypeId == (int)PromotionTypeEnum.Porcentaje && p.UserType == (int)this.StoreType && !p.Deleted.HasValue && p.StartDate <= CurrentDate.Now && p.EndDate >= CurrentDate.Now).ToList();

            foreach (var item in orderDetails)
            {
                item.Price = GetPrice(item.Product, PercentageDiscounts.FirstOrDefault(p => (((p.CategoryPromotions.Count() < 1 && p.ProductPromotions.Count() < 1) || (p.CategoryPromotions.Any(c => c.CategoryId == item.Product.CategoryId || c.CategoryId == item.Product.Category.ParentCategoryId))) && !p.ExcludedProducts.Any(e => e.ProductId == item.Product.ProductId)) || p.ProductPromotions.Any(pp => pp.ProductId == item.Product.ProductId)));
            }

            modelEntities.SaveChanges();
        }


        [HttpPost]
        public virtual string ClearShoppingCart()
        {
            try
            {
                var cartGuid = ShoppingCartHelper.GetGuid();
                var shoppingCart = GetShoppingCart(cartGuid);

                var orderDetailsIds = shoppingCart.OrderDetails.Select(x => x.OrderDetailId).ToArray();
                var orderDetails = modelEntities.OrderDetails.Where(x => orderDetailsIds.Contains(x.OrderDetailId)).ToList();

                foreach (var orderDetail in orderDetails)
                {
                    orderDetail.Active = false;
                    orderDetail.Deleted = CurrentDate.Now;
                    orderDetail.DeletedBy = User.Identity.Name;
                }

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }

        public bool FreeShipping(int userType, decimal orderTotal)
        {
            Parameter parameter = new Parameter();
            switch (userType)
            {
                case (int)UserTypeEnum.Minorista:
                    parameter = modelEntities.Parameters.FirstOrDefault(x => x.Name == "FreeShippingRetail");
                    if (orderTotal >= parameter.Value)
                        return true;
                    else
                        return false;
                case (int)UserTypeEnum.Mayorista:
                    parameter = modelEntities.Parameters.FirstOrDefault(x => x.Name == "FreeShippingWS");
                    if (orderTotal >= parameter.Value)
                        return true;
                    else
                        return false;
            }
            return false;
        }

        private bool CheckMinOrder(int totalPrice)
        {
            Parameter minOrder = new Parameter();

            minOrder = modelEntities.Parameters.FirstOrDefault(x => x.Name == "MinimumOrderRetail");

            //por monto o cantidad de prendas depende como reciba amount.
            var min = minOrder.Value.HasValue ? minOrder.Value : 0;

            TempData["MinimumOrder"] = minOrder.Value;

            var cartGuid = ShoppingCartHelper.GetGuid();

            var totPrice = GetShoppingCart(cartGuid);

            var price = totPrice.OrderDetails.Sum(x => x.Quantity * x.Price);

            if (price < min.Value)
            {
                //TempData["Message"] = "La compra mínima del sitio es de " + Convert.ToInt16(minOrder.Value).ToString() + " prendas.";
                TempData["Message"] = "La compra mínima del sitio es de $" + Convert.ToInt16(minOrder.Value).ToString();
                TempData["ActionType"] = ActionType.ShoppingCart;

                return false;
            }

            return true;
        }

        private void CreateOrder(FormCollection form, Customer customer, ShoppingCartViewModel shoppingCart, ref Order order, string receiptAmount)
        {
            order.Active = true;
            order.Created = CurrentDate.Now;
            order.Channel = (int)OrderChannelEnum.SitioWeb;
            order.OrderStatusId = (int)OrderStatusEnum.EnProceso;
            order.PaymentStatusId = (int)PaymentStatusEnum.Pendiente;
            order.DeliveryStatusId = (int)DeliveryStatusEnum.Pendiente;

            order.StoreType = (int)UserTypeEnum.Mayorista;
            order.ShippingDays = shoppingCart.ShippingDays;
            order.CurrenyId = (int)CurrencyEnum.Peso;

            //initialize surcharge
            order.Surcharge = 0;
            order.SurchargeAmount = 0;

            order.ReceiptAmount = receiptAmount;

            //agregado para parametro del pago-online con decidir desde link.
            order.GUID = Guid.NewGuid();

            var deliveryMethodId = int.Parse(Request["DeliveryMethodId"]);
            var paymentMethodId = int.Parse(Request["PaymentMethodId"]);
            var phone = Request["Phone"];
            var shippingCost = decimal.Parse(Request["ShippingCost"]);
            var discount = decimal.Parse(Request["Discount"]);
            var customerDNI = form["DNI"];

            order.DeliveryMethodId = deliveryMethodId;
            order.PaymentMethodId = paymentMethodId;
            order.Discount = discount;
            order.Customer = customer;

            if (paymentMethodId == (int)PaymentMethodEnum.Decidir)
            {
                order.PaymentStatusId = (int)PaymentStatusEnum.Cobrada;
            }

            //Update customer Info.
            customer.DNI = customerDNI;
            customer.Cellphone = phone;
            customer.CartPushDate = null;

            if (Request["Observations"] != null)
                order.Observations = Request["Observations"];

            var requestcode = Request["CouponCode"];
            if (!string.IsNullOrEmpty(requestcode) && discount > 0)
            {
                var coupon = modelEntities.Coupons.FirstOrDefault(x => !x.Deleted.HasValue && x.Active && x.Code == requestcode);
                order.CouponId = coupon.CouponId;
            }

            order.Total = shoppingCart.TotalOrderDetails;

            if (order.Discount.HasValue && order.Discount.Value > 0)
            {
                order.Discount_Percent = order.Discount.Value;
                order.Discount = order.Total * (order.Discount.Value / 100);

                order.Total -= order.Discount.Value;
            }

            if (shippingCost == 1)
            {
                if (order.DeliveryMethodId == Services.Utils.Utility.Transporte)
                {
                    order.ShippingCost = GetShippingCost(shoppingCart.Total);
                }
            }

            if (shippingCost > 0)
            {
                order.ShippingCost = shippingCost;
                order.Total += shippingCost;
            }

            //Recargo/IVA a mercado pago
            if (order.PaymentMethodId == Services.Utils.Utility.MercadoPago)
            {
                //order.Surcharge = Convert.ToDecimal(8);
                //order.SurchargeAmount = Math.Round(order.Total * Convert.ToDecimal(order.Surcharge.Value / 100), 2);
                //order.Total += order.SurchargeAmount.Value;

                order.Total += (order.Total * 21) / 100;
            }

            //Costo de Envío
            //if (!order.IsFreeShipping && order.ShippingCost.HasValue && order.ShippingCost.Value > 0)
            //{
            //    order.Total += order.ShippingCost.Value;
            //}

            //IVA (solo para mayoristas)
            if (shoppingCart.ApplyIva)
            {
                var defaultIvaPercent = Services.Utils.Utility.IVA_MULTIPLIER;
                order.IVA_Percent = defaultIvaPercent;

                var ivaAmount = order.Total * defaultIvaPercent / 100;
                order.IVA = ivaAmount;
                var total = order.Total + ivaAmount;
                order.Total = total;
                modelEntities.SaveChanges();
            }

            /*if (order.PaymentMethodId == (int)PaymentMethodEnum.Decidir)
            {
                order.SurchargeAmount = Math.Round(order.Total * Convert.ToDecimal(order.Surcharge.Value / 100), 2);
                order.Total += order.SurchargeAmount.Value;
            }*/

            order.DeliveryAddress = shoppingCart.DeliveryAddress;

            shoppingCart.Order = order;

            modelEntities.Orders.Add(order);

            modelEntities.SaveChanges();

            var orderDetails = modelEntities.OrderDetails.Where(x =>
                (x.GUID == shoppingCart.Guid || x.CreatedBy == User.Identity.Name) && x.OrderId == null && x.StoreType == (int)this.StoreType && x.Active && !x.Deleted.HasValue).ToList();

            foreach (var orderDetail in orderDetails)
            {
                orderDetail.Order = order;
                orderDetail.OrderId = order.OrderId;

                var item_cart = shoppingCart.OrderDetails.FirstOrDefault(x => x.ProductId == orderDetail.ProductId && x.SizeId == orderDetail.SizeId && x.ColorId == orderDetail.ColorId);
                if (item_cart != null)
                {
                    orderDetail.Price = item_cart.Price;
                }
            }

            modelEntities.SaveChanges();
        }

        public virtual int GetOrderCurrency(Customer customer)
        {
            return (int)CurrencyEnum.Peso;
        }

        private void SubstractOrderStock(Order order)
        {
            var orderDetails = order.OrderDetails.Where(x => x.Active).ToList();

            var stockService = new StockService(new VirtualStock());
            string productDescription = string.Empty;

            if (stockService.HasStock(orderDetails, ref productDescription, ref modelEntities))
            {
                stockService.SubstractStock(orderDetails, ref modelEntities);
            }
            else
            {
                throw new NoStockException("No tenemos stock del producto: " + productDescription);
            }
        }

        public ActionResult ValidateWishlistItem(int idProducto, int SizeId, int ColorId, int cantidad)
        {
            var shoppingCartValidation = _shoppingCartManager.ValidateWishlistItem(idProducto, SizeId, ColorId, cantidad);

            return Json(shoppingCartValidation, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Validate(int idProducto, int SizeId, int ColorId, int cantidad)
        {
            var shoppingCartValidation = _shoppingCartManager.ValidateOrderDetail(idProducto, SizeId, ColorId, cantidad);
            return Json(shoppingCartValidation, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddOrderDetail(int idProducto, int SizeId, int ColorId, int cantidad, bool sumar = false)
        {
            var cartGuid = ShoppingCartHelper.GetGuid();
            var shoppingCartValidation = _shoppingCartManager.ValidateOrderDetail(idProducto, SizeId, ColorId, cantidad);
            var PercentageDiscounts = modelEntities.Promotions.Where(p => p.PromotionTypeId == (int)PromotionTypeEnum.Porcentaje && p.UserType == (int)this.StoreType && !p.Deleted.HasValue && p.StartDate <= CurrentDate.Now && p.EndDate >= CurrentDate.Now).ToList();

            if (shoppingCartValidation.IsApproved)
            {
                var product = modelEntities.Stocks.FirstOrDefault(x => x.ProductId == idProducto && x.SizeId == SizeId && x.ColorId == ColorId).Product;
                //var price = this.StoreType == UserTypeEnum.Mayorista ? product.WholesalePrice.Value : product.RetailPrice.Value;
                var price = GetPrice(product, PercentageDiscounts.FirstOrDefault(p => (((p.CategoryPromotions.Count() < 1 && p.ProductPromotions.Count() < 1) || (p.CategoryPromotions.Any(c => c.CategoryId == product.CategoryId || c.CategoryId == product.Category.ParentCategoryId))) && !p.ExcludedProducts.Any(e => e.ProductId == product.ProductId)) || p.ProductPromotions.Any(pp => pp.ProductId == product.ProductId)));

                _shoppingCartManager.AddToShoppingCart(idProducto, SizeId, ColorId, cantidad, price, cartGuid, sumar, User.Identity.Name, this.StoreType);
            }

            var shoppingCartViewModel = GetShoppingCart(cartGuid);

            return Json(new
            {
                IsApproved = shoppingCartValidation.IsApproved,
                TotalItemsCount = shoppingCartViewModel.TotalItemsCount,
                Message = shoppingCartValidation.Message
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddOrderDetailTable(int idProducto, string orderDetailsJson, bool sumar = false)
        {
            var cartGuid = ShoppingCartHelper.GetGuid();
            var orderDetails = JsonConvert.DeserializeObject<List<OdToAdd>>(orderDetailsJson);
            var product = modelEntities.Products.FirstOrDefault(p => p.Active && !p.Deleted.HasValue && p.ProductId == idProducto);
            var price = product.CustomerPrice;


            foreach (var od in orderDetails)
            {
                _shoppingCartManager.AddToShoppingCart(idProducto, od.sizeId, od.colorId, od.quantity, price, cartGuid, sumar, User.Identity.Name, this.StoreType);
            }

            var shoppingCartViewModel = GetShoppingCart(cartGuid);

            return Json(shoppingCartViewModel, JsonRequestBehavior.AllowGet);
        }

        private ShoppingCartViewModel GetShoppingCart(string cartGuid)
        {
            List<OrderDetail> cartDetails = _shoppingCartManager.GetShoppingCart(cartGuid, User.Identity.IsAuthenticated ? User.Identity.Name : null, this.StoreType);

            var PercentageDiscounts = modelEntities.Promotions.Where(p => p.PromotionTypeId == (int)PromotionTypeEnum.Porcentaje && p.UserType == (int)this.StoreType && !p.Deleted.HasValue && p.StartDate <= CurrentDate.Now && p.EndDate >= CurrentDate.Now).ToList();

            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                OrderDetails = cartDetails.Select(od => new OrderDetailViewModel()
                {
                    Quantity = od.Quantity,
                    OrderId = od.OrderId,
                    CategorySeoUrl = od.Product.Category.SeoUrl,
                    SeoUrl = od.Product.SeoUrl,
                    Price = GetPrice(od.Product, PercentageDiscounts.FirstOrDefault(p => (((p.CategoryPromotions.Count() < 1 && p.ProductPromotions.Count() < 1) || (p.CategoryPromotions.Any(c => c.CategoryId == od.Product.CategoryId || c.CategoryId == od.Product.Category.ParentCategoryId))) && !p.ExcludedProducts.Any(e => e.ProductId == od.Product.ProductId)) || p.ProductPromotions.Any(pp => pp.ProductId == od.Product.ProductId))),
                    Description = od.Product.Description,
                    ProductId = od.ProductId,
                    SizeId = od.SizeId,
                    ColorId = od.ColorId,
                    SizeDescription = od.Size.Description,
                    ColorDescription = od.Color.Description.ToLower(),
                    OrderDetailId = od.OrderDetailId,
                    Title = od.Product.Title,
                    FormattedTitle = od.Product.FormattedTitle,
                    ProductBrand = od.Product.Brand != null ? od.Product.Brand.Name : string.Empty,
                    ProductImage = string.Format("{0}/{1}", Utility.ImgUrl, od.Product.getDefaultImageUrl),
                    ProductImageResized = string.Format("{0}/{1}", Utils.Utility.ImgUrl, od.Product.getResizedImageUrlByColor(od.ColorId)),
                    MaxDiscount = od.Product.MaxDiscount,
                    CategoryId = od.Product.CategoryId.HasValue ? od.Product.CategoryId.Value : -1,
                    ParentCategoryId = od.Product.Category.ParentCategoryId.HasValue ? od.Product.Category.ParentCategoryId.Value : -1
                }).ToList(),
                Guid = cartGuid.ToString()
            };

            return shoppingCartViewModel;
        }

        public decimal GetPrice(Product product, Promotion promotion)
        {

            if (promotion != null)
            {
                return product.WholesalePrice.Value * (100 - promotion.DiscountPercent.Value) / 100;
            }
            else
            {
                return product.WholesalePrice.HasValue ? product.WholesalePrice.Value : 0;
            }

        }

        public ActionResult Quitar(int orderDetailId)
        {
            var success = _shoppingCartManager.RemoveOrderDetail(orderDetailId);

            return Json(new { success }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSiteMinOrder()
        {
            var price = modelEntities.Parameters.FirstOrDefault(x => x.Name == "MinimumOrderWS").Value;

            return Json(new { price }, JsonRequestBehavior.AllowGet);
        }

        private bool UserHasSignedIn()
        {
            return HttpContext.User.Identity.IsAuthenticated;
        }

        [HttpGet]
        public JsonResult GetColors(int ProductId)
        {
            var product = modelEntities.Products.Find(ProductId);
            var imgRoot = ConfigurationManager.AppSettings["BackOfficeUrl"] + "/Colors/";
            var colors = product
                .Stocks
                .Where(x => x.StockReal > 0 && x.Color.Deleted == null && x.Size.Deleted == null)
                .Where(x => !x.Color.Deleted.HasValue).GroupBy(x => x.Color)
                .Select(x => new
                {
                    Id = x.Key.ColorId,
                    Name = imgRoot + x.Key.ColorId + "/" + x.Key.ColorThumb
                }).ToList();

            return Json(new { colors }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSize(int ProductId, int ColorId)
        {
            var product = modelEntities.Products.Find(ProductId);

            var sizes = product
                .Stocks
                .Where(x => x.StockReal > 0 && x.Color.Deleted == null && x.Size.Deleted == null)
                .Where(x => x.ColorId == ColorId)
                .Select(x => new
                {
                    Id = x.SizeId,
                    Name = x.Size.Description
                }).ToList();

            return Json(new { sizes }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCities(int provinceId)
        {
            var cities = modelEntities.Cities.Where(x => x.ProvinceId == provinceId)
                .Select(s => new { Id = s.CityId, Name = s.Name })
                .ToList();

            return Json(new { cities = cities }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ValidateOrderAmount(int totalPrice)
        {
            var isValid = CheckMinOrder(totalPrice);
            var minimumAmount = (decimal)TempData["MinimumOrder"];

            return Json(new { Valid = isValid, MinimumAmount = minimumAmount }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult GetMotoShippingCost(int CityId)
        {
            decimal shippingCost = 0;

            var city = modelEntities.Cities.FirstOrDefault(x => x.CityId == CityId && x.Active);

            if (city != null && city.ShippingCost.HasValue)
                shippingCost = city.ShippingCost.Value;

            return Json(new { Price = shippingCost, Days = "2", EstimatedDate = "10/5/2016" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetOcaShippingCost(string zipCode)
        {
            return Json(new { Price = "80", Days = "3", EstimatedDate = "10/5/2016" }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetAndreaniSucursales(string cp)
        {
            try
            {
                var sucursales = _andreani.GetSucursales().Select(x => new
                {
                    Text = $"{ x.Descripcion } ({ x.Numero }) - { x.Direccion }",
                    Value = x.Sucursal
                });

                return Json(new { Sucursales = sucursales.OrderBy(x => x.Value) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Error = "Se produjo un error al intentar listar las sucursales." }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CotizarAndreaniSucursal(string cp, string sucursal)
        {
            try
            {
                var alto = ConfigurationManager.AppSettings["AndreaniHeight"];
                var ancho = ConfigurationManager.AppSettings["AndreaniWidth"];
                var largo = ConfigurationManager.AppSettings["AndreaniLength"];

                decimal total = 0;

                var cartGuid = ShoppingCartHelper.GetGuid();
                var shoppingCartViewModel = GetShoppingCart(cartGuid);
                total = shoppingCartViewModel.OrderDetails.Sum(x => x.Price * x.Quantity);

                var cotizacion = _andreani.CotizarEnvio((int)AndreaniTipoEnvio.Sucursal, cp, alto, ancho, largo, "500", sucursal, total);

                var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == User.Identity.Name && !x.Deleted.HasValue && x.Active);
                if (FreeShipping(customer.UserType, total))
                {
                    cotizacion.Tarifa = "0";
                }

                return Json(new { Tarifa = cotizacion.Tarifa }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Error = "Se produjo un error al cotizar." }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CotizarAndreaniDomicilio(string cp)
        {
            try
            {
                var alto = ConfigurationManager.AppSettings["AndreaniHeight"];
                var ancho = ConfigurationManager.AppSettings["AndreaniWidth"];
                var largo = ConfigurationManager.AppSettings["AndreaniLength"];

                decimal total = 0;

                var cartGuid = ShoppingCartHelper.GetGuid();
                var shoppingCartViewModel = GetShoppingCart(cartGuid);
                total = shoppingCartViewModel.OrderDetails.Sum(x => x.Price * x.Quantity);

                var cotizacion = _andreani.CotizarEnvio((int)AndreaniTipoEnvio.DomicilioEstandar, cp, alto, ancho, largo, "500", string.Empty, total);

                var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == User.Identity.Name && !x.Deleted.HasValue && x.Active);
                if (FreeShipping(customer.UserType, total))
                {
                    cotizacion.Tarifa = "0";
                }

                return Json(new { Tarifa = cotizacion.Tarifa }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Error = "Se produjo un error al cotizar." }, JsonRequestBehavior.AllowGet);
            }
        }

        //0.Peso 1.Volumen 2.Bultos
        private string[] GetPesoVolumenBultos()
        {
            var shoppingCartManager = new ShoppingCartManager();
            var cartGuid = ShoppingCartHelper.GetGuid();
            List<OrderDetail> cartDetails = _shoppingCartManager.GetShoppingCart(cartGuid, User.Identity.IsAuthenticated ? User.Identity.Name : null, this.StoreType);

            //default values
            decimal defaultWeight = decimal.Parse(ConfigurationManager.AppSettings["defaultWeight"]);
            decimal defaultWidth = decimal.Parse(ConfigurationManager.AppSettings["defaultWidth"]);
            decimal defaultHigh = decimal.Parse(ConfigurationManager.AppSettings["defaultHigh"]);
            decimal defaultLong = decimal.Parse(ConfigurationManager.AppSettings["defaultLong"]);

            decimal sumPeso = 0;
            decimal sumVolumen = 0;

            foreach (OrderDetail item in cartDetails)
            {
                sumPeso += defaultWeight * item.Quantity;
            }

            int pesoBulto = int.Parse(ConfigurationManager.AppSettings["PesoBulto"].ToString());
            int cantidadDePaquetes = (int)Math.Ceiling(sumPeso / pesoBulto);
            if (cantidadDePaquetes == 0)
                cantidadDePaquetes = 1;

            //cantidad de paquetes por el volumen de una caja estandar.
            sumVolumen = cantidadDePaquetes * (defaultWidth * defaultHigh * defaultLong);

            string pesoTotal = sumPeso.ToString().Replace(",", ".");
            string volumenTotal = (sumVolumen / 1000000).ToString().Replace(",", ".");

            string[] result = new string[3] { pesoTotal, volumenTotal, cantidadDePaquetes.ToString() };
            return result;
        }

        private string GetGuid()
        {
            List<OrderDetail> orderDetail = modelEntities.OrderDetails
                .Where(x => x.CreatedBy == User.Identity.Name && x.OrderId == null && x.Active && x.StoreType == (int)this.StoreType).OrderByDescending(x => x.Created).Take(1).ToList();

            if (orderDetail.Count == 0)
            {
                Guid guid = Guid.NewGuid();
                return guid.ToString();
            }
            else
            {
                return orderDetail[0].GUID;
            }
        }

        [HttpPost]
        public virtual decimal ValidarCupon(string cuponCode)
        {
            decimal descuento = 0;
            Coupon cupon = modelEntities.Coupons.FirstOrDefault(x => x.Code == cuponCode && DateTime.Today >= x.FromDate && DateTime.Today <= x.ExpirationDate
                && x.CouponType == (int)this.StoreType && x.Active);

            if (cupon != null)
            {
                descuento = cupon.Percentage;
            }

            return descuento;
        }

        [HttpPost]
        public virtual JsonResult ValidarStock(FormCollection formCollection)
        {
            List<int> orderDetailIds = new List<int>();
            List<int> quantities = new List<int>();

            foreach (var key in formCollection)
            {
                if (key.ToString().StartsWith("Quantity_"))
                {
                    var orderDetailId = int.Parse(key.ToString().Split('_')[1]);
                    orderDetailIds.Add(orderDetailId);
                    quantities.Add(int.Parse(formCollection["Quantity_" + orderDetailId]));
                }
            }

            var orderDetails = modelEntities.OrderDetails.Where(x => orderDetailIds.Contains(x.OrderDetailId) && x.Active).ToList();
            int[] productIds = orderDetails.Select(s => s.ProductId).ToArray();

            var products = modelEntities.Stocks.Where(x => productIds.Contains(x.ProductId) && x.Product.Active).ToList();

            List<List<string>> noStock = new List<List<string>>();
            for (int i = 0; i < orderDetails.Count(); i++)
            {
                var item_stock = products.FirstOrDefault(x =>
                       x.ProductId == orderDetails[i].ProductId &&
                       x.ColorId == orderDetails[i].ColorId &&
                       x.SizeId == orderDetails[i].SizeId);

                if (item_stock != null)
                {
                    if (!item_stock.Product.Published || (item_stock.Product.HasStockControl && item_stock.StockVirtual < quantities[i]) || item_stock.Product.Deleted.HasValue || item_stock.Product.SoldOut)
                    {
                        var noStockItem = new List<string>();
                        noStockItem.Add(orderDetails[i].OrderDetailId.ToString());
                        noStockItem.Add(item_stock.StockVirtual.ToString());
                        noStock.Add(noStockItem);
                    }
                }
                else
                {
                    var noStockItem = new List<string>();
                    noStockItem.Add(orderDetails[i].OrderDetailId.ToString());
                    noStockItem.Add("0");
                    noStock.Add(noStockItem);
                }
            }

            return Json(noStock);
        }

        [HttpGet]
        public ActionResult RepeatOrder(int Id)
        {
            try
            {
                ClearShoppingCart();

                var orderDetails = modelEntities.OrderDetails.Where(o => o.OrderId == Id && o.Active).OrderByDescending(x => x.ProductId).ToList();

                List<OrderDetail> newOrderDetails = new List<OrderDetail>();
                for (int p = 0; p < orderDetails.Count; p++)
                {
                    OrderDetail newOrderDetail = new OrderDetail();
                    modelEntities.OrderDetails.Add(newOrderDetail);
                    var sourceValues = modelEntities.Entry(orderDetails[p]).CurrentValues;
                    modelEntities.Entry(newOrderDetail).CurrentValues.SetValues(sourceValues);
                    newOrderDetail.OrderId = null;
                    newOrderDetail.Created = CurrentDate.Now;
                    newOrderDetail.Active = true;
                    newOrderDetail.CreatedBy = User.Identity.Name;
                    newOrderDetails.Add(newOrderDetail);
                }
                //get products ids from order to repeat
                List<int> productsIds = new List<int>();
                foreach (var item in orderDetails)
                {
                    productsIds.Add(item.ProductId);
                }

                //get products actual stocks and validate for the new order
                var stocks = modelEntities.Stocks
                    .Where
                    (x => x.Product.Active && productsIds.Contains(x.ProductId))
                    .OrderByDescending(x => x.ProductId).ToList();


                List<OrderDetail> itemsToRemove = new List<OrderDetail>();
                for (int i = 0; i < newOrderDetails.Count; i++)
                {
                    var item_stock = stocks.FirstOrDefault(x =>
                        x.ProductId == newOrderDetails[i].ProductId &&
                        x.ColorId == newOrderDetails[i].ColorId &&
                        x.SizeId == newOrderDetails[i].SizeId);

                    if (newOrderDetails[i].Quantity > item_stock.StockVirtual)
                    {
                        if (item_stock.StockVirtual == 0)
                            itemsToRemove.Add(newOrderDetails[i]);
                        else
                            newOrderDetails[i].Quantity = item_stock.StockVirtual;
                    }
                }

                foreach (var item in itemsToRemove)
                {
                    modelEntities.OrderDetails.Remove(item);
                }


                modelEntities.SaveChanges();
                return RedirectToAction("Step1");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult GetOcaCenters(string zipCode)
        {
            string error = "";
            List<OcaCentroDeImposicion> centros = new List<OcaCentroDeImposicion>();
            try
            {
                centros = GetOcaCenterList(zipCode);
                if (centros.Count == 0) error = "Código postal inválido.";
            }
            catch (Exception ex)
            {
                error = "Se produjo un error al intentar conseguir los centros de entrega.";
            }

            var results = new { Centers = centros.ToList().OrderBy(x => x.Calle), Error = error };
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        private string GetDecidirResponseMessage(PaymentResponse response)
        {
            try
            {
                var messageToReturn = response.status.Substring(response.status.IndexOf("{"), response.status.Length - response.status.IndexOf("{"));
                //var messageToReturn = response.status.ToString().Replace("402 - ", "");
                var deserialisedJson = JsonConvert.DeserializeObject<DecidirErrorResponseModel>(messageToReturn);

                var messageString = deserialisedJson.status_details.error.reason.description;
                return "Se produjo un error al validar los datos de la tarjeta: " + messageString;
            }
            catch (Exception ex)
            {
                return "Se produjo un error al validar los datos de la tarjeta";
            }


            //MOTIVOS.
            //https://decidir.api-docs.io/1.0/tablas-de-referencia-e-informacion-para-el-implementador/2i4sL9WujwGNMcpr7

            // 1 Y 2	PEDIR AUTORIZACION	Solicitar autorización telefónica, en caso de ser aprobada, cargar el código obtenido y dejar la operación en OFFLINE.
            //28  SERVICIO NO DISPONIBLE Momentáneamente el servicio no está disponible.Se debe reintentar en unos segundos.
            //45 Denegada, tarjeta inhibida para operar en cuotas
            //49  ERROR FECHA VENCIM.Verificar el sistema, error en formato de fecha de expiración(vto)
            //51  FONDOS INSUFICIENTES    Denegada, no posee fondos suficientes.
            //61	EXCEDE LIMITE	Denegada, excede límite remanente de la tarjeta.

            //76	LLAMAR AL EMISOR	Solicitar autorización telefónica, en caso de ser aprobada, cargar el código obtenido y dejar la operación en

            throw new NotImplementedException();
        }

        private List<OcaCentroDeImposicion> GetOcaCenterList(string zipCode)
        {
            List<OcaCentroDeImposicion> centros = new List<OcaCentroDeImposicion>();
            try
            {
                OCAWebService.Oep_TrackingSoapClient ocaShippingService = new OCAWebService.Oep_TrackingSoapClient();
                System.Data.DataSet ds = ocaShippingService.GetCentrosImposicionPorCP(zipCode);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    OcaCentroDeImposicion centro;
                    foreach (System.Data.DataRow row in ds.Tables[0].Rows)
                    {
                        centro = new OcaCentroDeImposicion();
                        centro.Id = int.Parse(row["IdCentroImposicion"].ToString().Trim());
                        centro.Calle = row["Calle"].ToString().Trim();
                        centro.Numero = row["Numero"].ToString().Trim();
                        centro.CodigoPostal = row["CodigoPostal"].ToString().Trim();
                        centro.Provincia = row["Provincia"].ToString().Trim();
                        centro.Localidad = row["Localidad"].ToString().Trim();
                        centro.Descripcion = row["Descripcion"].ToString().Trim();

                        centros.Add(centro);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return centros;
        }

        [HttpGet]
        public JsonResult GetOcaCost(string zipCode, int sendType)
        {
            var cost = GetOcaShippingCost(zipCode, sendType);
            string error = "";
            if (!cost.HasValue)
                error = "Se produjo un error al intentar calcular el costo.";

            var total = cost.HasValue ? cost.Value : 0;
            return Json(new { Cost = total.ToString("c2"), Error = error }, JsonRequestBehavior.AllowGet);
        }

        private decimal? GetOcaShippingCost(string zipCode, int type)
        {
            OCAWebService.Oep_TrackingSoapClient ocaShippingService = new OCAWebService.Oep_TrackingSoapClient();
            int codigoPostalDestino = int.Parse(zipCode);
            int codigoPostalOrigen = int.Parse(ConfigurationManager.AppSettings["CodigoPostalOrigen"]);

            string cuit = ConfigurationManager.AppSettings["CUIT"];
            string operativa = type == (int)OcaSendType.PAP ? ConfigurationManager.AppSettings["OperativaPaP"] : ConfigurationManager.AppSettings["Operativa"];

            try
            {
                string[] result = GetPesoVolumenBultos();

                string peso = result[0];
                string volumen = result[1];
                long bultos = long.Parse(result[2]);

                System.Data.DataSet dsTarifa = ocaShippingService.Tarifar_Envio_Corporativo(peso, volumen, codigoPostalOrigen, codigoPostalDestino, bultos, cuit, operativa);
                if (dsTarifa.Tables.Count > 0 && dsTarifa.Tables[0].Rows.Count > 0)
                {
                    var total = decimal.Parse(dsTarifa.Tables[0].Rows[0]["Total"].ToString());
                    return Math.Ceiling(total * 121 / 100);
                }
            }
            catch (Exception ex)
            {
                return (decimal?)null;
            }

            return (decimal?)null;
        }

        #region Todo Pago

        private string CreatePreferenceTodoPago(ShoppingCartViewModel shoppingCart, Order order)
        {
            //Todo Pago
            var headers = new Dictionary<String, String>();
            headers.Add("Authorization", "PRISMA 912EC803B2CE49E4A541068D495AB570");

            //String endPointDev = "https://developers.todopago.com.ar/"; // EndPoint de Developer
            //String endPointPrd = "https://apis.todopago.com.ar/"; // EndPoint de Production

            String endpoint = ConfigurationManager.AppSettings["TodoPagoEndpoint"];

            TPConnector tpc = new TPConnector(endpoint, headers);

            TodoPagoConnector.Model.User user = AuthorizeConnector(tpc);

            var securityToken = user.getApiKey();
            var merchantId = user.getMerchant();

            var url_Ok = ConfigurationManager.AppSettings["SuccessUrl"];
            var url_Error = ConfigurationManager.AppSettings["FailureUrl"];

            var request = new Dictionary<String, String>();
            request.Add(ElementNames.SECURITY, securityToken); // Token de seguridad, provisto por TODO PAGO. MANDATORIO.
            request.Add(ElementNames.SESSION, "ABCDEF-1234-12221-FDE1-00000200");
            request.Add(ElementNames.MERCHANT, merchantId);

            request.Add(ElementNames.URL_OK, url_Ok + "?Id=" + shoppingCart.Order.OrderId.ToString());
            request.Add(ElementNames.URL_ERROR, url_Error + "?Id=" + shoppingCart.Order.OrderId.ToString());

            request.Add(ElementNames.ENCODING_METHOD, "XML");

            var payload = new Dictionary<string, string>();

            var total = shoppingCart.OrderDetails.Sum(x => x.Quantity * x.Price);

            if (shoppingCart.Discount > 0)
                total += shoppingCart.Discount * -1;

            if (shoppingCart.ShippingCost > 0)
                total += shoppingCart.ShippingCost;

            if (shoppingCart.Order.SurchargeAmount.HasValue)
                total += shoppingCart.Order.SurchargeAmount.Value;


            payload.Add(ElementNames.MERCHANT, merchantId); //dato fijo (número identificador del comercio).
            payload.Add(ElementNames.OPERATIONID, "Venta #" + shoppingCart.Order.OrderId.ToString()); //número único que identifica la operación, generado por el comercio.
            //payload.Add(ElementNames.OPERATIONID, "8000"); //número único que identifica la operación, generado por el comercio.
            payload.Add(ElementNames.CURRENCYCODE, "032"); //por el momento es el único tipo de moneda aceptada.

            var totalTP = shoppingCart.Order.OrderTotal;
            if (shoppingCart.Order.ShippingCost.HasValue)
            {
                totalTP += shoppingCart.Order.ShippingCost.Value;
            }
            payload.Add(ElementNames.AMOUNT, totalTP.ToString().Replace(".", "").Replace(",", "."));
            //payload.Add(ElementNames.AMOUNT, "150.00");
            payload.Add(ElementNames.EMAILCLIENTE, shoppingCart.Customer.Email);
            payload.Add(ElementNames.MAXINSTALLMENTS, "12"); //NO MANDATORIO, Maxima cantidad de cuotas, valor maximo 12.

            SetPayloadExtraParameters(shoppingCart, payload);

            try
            {
                Dictionary<string, object> res = tpc.SendAuthorizeRequest(request, payload);
                order.RequestKey = res["RequestKey"].ToString();
                modelEntities.SaveChanges();
                return res["URL_Request"].ToString();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private TodoPagoConnector.Model.User AuthorizeConnector(TPConnector tpc)
        {
            string username = ConfigurationManager.AppSettings["TodoPagoUsername"];
            string password = ConfigurationManager.AppSettings["TodoPagoPassword"];

            TodoPagoConnector.Model.User user = new TodoPagoConnector.Model.User(username, password);// user y pass de TodoPago

            try
            {
                user = tpc.getCredentials(user);
                tpc.setAuthorize(user.getApiKey());// set de la APIKey a TodoPagoConector

            }
            catch (EmptyFieldException ex)
            {
            }
            catch (ResponseException ex)
            { //se debe realizar catch User y pass invalidos

            }
            return user;
        }

        private void SetPayloadExtraParameters(ShoppingCartViewModel shoppingCart, Dictionary<string, string> payload)
        {
            payload.Add("CSBTCITY", shoppingCart.Customer.City); //Ciudad de facturación, MANDATORIO.		
            payload.Add("CSBTCOUNTRY", "AR");//País de facturación. MANDATORIO. Código ISO. (http://apps.cybersource.com/library/documentation/sbc/quickref/countries_alpha_list.pdf)	
            payload.Add("CSBTCUSTOMERID", shoppingCart.Customer.CustomerId.ToString()); //Identificador del usuario al que se le emite la factura. MANDATORIO. No puede contener un correo electrónico.		

            //Cambiar para que se ponga la IP del usuario (en ambiente local hardcodear)
            payload.Add("CSBTIPADDRESS", "192.0.0.4"); //IP de la PC del comprador. MANDATORIO.	
                                                       //payload.Add("CSBTIPADDRESS", Request.UserHostAddress.ToString()); //IP de la PC del comprador. MANDATORIO.	
            string firstName = shoppingCart.Customer.FullName.Replace(",", "").Split(' ')[0];
            string lastName = shoppingCart.Customer.FullName.Replace(",", "").Split(' ')[1];
            payload.Add("CSBTEMAIL", shoppingCart.Customer.Email); //Mail del usuario al que se le emite la factura. MANDATORIO.
            payload.Add("CSBTFIRSTNAME", firstName);//Nombre del usuario al que se le emite la factura. MANDATORIO.		
            payload.Add("CSBTLASTNAME", lastName);//Apellido del usuario al que se le emite la factura. MANDATORIO.
            if (shoppingCart.Customer.PhoneNumber != null)
                payload.Add("CSBTPHONENUMBER", shoppingCart.Customer.PhoneNumber);//Teléfono del usuario al que se le emite la factura. No utilizar guiones, puntos o espacios. Incluir código de país. MANDATORIO.		
            else
                payload.Add("CSBTPHONENUMBER", "1111-1111");//Teléfono del usuario al que se le emite la factura. No utilizar guiones, puntos o espacios. Incluir código de país. MANDATORIO.		

            if (shoppingCart.Customer.ZipCode != null)
                payload.Add("CSBTPOSTALCODE", shoppingCart.Customer.ZipCode);//Código Postal de la dirección de facturación. MANDATORIO.	
            else
                payload.Add("CSBTPOSTALCODE", "0000");//Código Postal de la dirección de facturación. MANDATORIO.


            payload.Add("CSBTSTATE", shoppingCart.Customer.Province.Code);//Provincia de la dirección de facturación. MANDATORIO. Ver tabla anexa de provincias.	
            if (shoppingCart.Customer.Address != null)
                payload.Add("CSBTSTREET1", shoppingCart.Customer.Address);//Domicilio de facturación (calle y nro). MANDATORIO.			
            else
                payload.Add("CSBTSTREET1", "Dato Vacio");//Domicilio de facturación (calle y nro). MANDATORIO.

            //payload.Add("CSBTSTREET2", "Piso 8");//Complemento del domicilio. (piso, departamento). NO MANDATORIO.
            payload.Add("CSPTCURRENCY", "ARS");//Moneda. MANDATORIO.	

            var total = shoppingCart.Order.OrderTotal;
            if (shoppingCart.Order.ShippingCost.HasValue)
            {
                total += shoppingCart.Order.ShippingCost.Value;
            }
            payload.Add("CSPTGRANDTOTALAMOUNT", total.ToString().Replace(".", "").Replace(",", "."));//Con decimales opcional usando el puntos como separador de decimales. No se permiten comas, ni como separador de miles ni como separador de decimales. MANDATORIO.(Ejemplos:$125,38-> 125.38 $12-> 12 o 12.00)		
            //payload.Add("CSPTGRANDTOTALAMOUNT", "1.00");

            //Retail
            payload.Add("CSSTCITY", shoppingCart.Customer.City);//Ciudad de enví­o de la orden. MANDATORIO.	
            payload.Add("CSSTCOUNTRY", "AR");//País de envío de la orden. MANDATORIO.	
            payload.Add("CSSTEMAIL", shoppingCart.Customer.Email);//Mail del destinatario, MANDATORIO.			
            payload.Add("CSSTFIRSTNAME", firstName);//Nombre del destinatario. MANDATORIO.		
            payload.Add("CSSTLASTNAME", lastName);//Apellido del destinatario. MANDATORIO.		

            if (shoppingCart.Customer.PhoneNumber != null)
                payload.Add("CSSTPHONENUMBER", shoppingCart.Customer.PhoneNumber);//Número de teléfono del destinatario. MANDATORIO.
            else
                payload.Add("CSSTPHONENUMBER", "1111-1111");//Número de teléfono del destinatario. MANDATORIO.

            payload.Add("CSSTPOSTALCODE", shoppingCart.Customer.ZipCode);//Código postal del domicilio de envío. MANDATORIO.		
            payload.Add("CSSTSTATE", shoppingCart.Customer.Province.Code);//Provincia de envío. MANDATORIO. Son de 1 caracter			
            payload.Add("CSSTSTREET1", shoppingCart.Customer.Address);//Domicilio de envío. MANDATORIO.

            //datos a enviar por cada producto, los valores deben estar separado con #:

            payload.Add("CSITPRODUCTCODE", "Pedido Web Nro. " + shoppingCart.Order.OrderId.ToString());//Código de producto. MANDATORIO. Valores posibles(adult_content;coupon;default;electronic_good;electronic_software;gift_certificate;handling_only;service;shipping_and_handling;shipping_only;subscription)	
            payload.Add("CSITPRODUCTDESCRIPTION", "Pedido Web Nro. " + shoppingCart.Order.OrderId.ToString());//Descripción del producto. MANDATORIO.	
            payload.Add("CSITPRODUCTNAME", "Pedido Web Nro. " + shoppingCart.Order.OrderId.ToString());//Nombre del producto. CONDICIONAL.
            payload.Add("CSITPRODUCTSKU", "Pedido Web Nro. " + shoppingCart.Order.OrderId.ToString());//Código identificador del producto. MANDATORIO.		

            payload.Add("CSITTOTALAMOUNT", total.ToString().Replace(".", "").Replace(",", "."));//CSITTOTALAMOUNT=CSITUNITPRICE*CSITQUANTITY "999999[.CC]" Con decimales opcional usando el punto como separador de decimales. No se permiten comas, ni como separador de miles ni como separador de decimales. MANDATORIO.		

            payload.Add("CSITQUANTITY", shoppingCart.Order.OrderDetails.Sum(x => x.Quantity).ToString());//Cantidad del producto. CONDICIONAL.		
            payload.Add("CSITUNITPRICE", total.ToString().Replace(".", "").Replace(",", "."));//Formato Idem CSITTOTALAMOUNT. CONDICIONAL.	

        }

        #endregion
    }
}