﻿using eCommerce.Services.Business;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.UI.Utils;
using eCommerce.UI.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using HTTPCompression.ActionFilters;
using eCommerce.UI.ViewModels.Shop;

namespace eCommerce.UI.Controllers
{
    public class HomeController : _StoreBaseController
    {
        DbModelEntities modelEntities = new DbModelEntities();
        private readonly CategoryManager _categoryManager;

        public HomeController()
        {
            _categoryManager = new CategoryManager();
        }

        //public ActionResult Index()
        //{
        //    return View("Landing");
        //}

        public ActionResult Landing()
        {
            var redirectUrl = ConfigurationManager.AppSettings["LandingPageUrl"];
            return RedirectPermanent(redirectUrl);
        }

        //[CompressAttribute]
        [OutputCache(Duration = 60, Location = System.Web.UI.OutputCacheLocation.Client)]
        public ActionResult Index()
        {
            HomeViewModel model = new HomeViewModel();
            var banners = modelEntities.Banners.Where(x => x.Active && x.Deleted == null)
                .OrderBy(x => x.Order).ToList();

            model.BannersSlider = banners.Where(x => x.Position == (int)BannerPositionEnum.Central).ToList();
            model.BannersCategories = banners.Where(x => x.Position == (int)BannerPositionEnum.Categoria).ToList();
            model.BannersCategoriesTwo = banners.Where(x => x.Position == (int)BannerPositionEnum.CategoriaDos).ToList();

            model.StoreType = this.StoreType;

            return View(model);
        }

        public virtual ActionResult RenderSlideOutMenu()
        {
            SiteMenuViewModel viewModel = new SiteMenuViewModel();

            //List<CategoryViewModel> categories = new List<CategoryViewModel>();

            var categories = modelEntities.Categories.Where(x => x.Active && x.Enabled && x.Deleted == null && !x.ParentCategoryId.HasValue)
                .OrderByDescending(o => o.DisplayOrder.Value)
                .ThenBy(o => o.DisplayOrder)
                .Select(c => new CategoryViewModel{
                    CategoryId = c.CategoryId,
                    Name = c.Name,
                    ParentCategoryId = c.ParentCategoryId,
                    DisplayOrder = c.DisplayOrder,
                    SeoUrl = c.SeoUrl
                }).ToList();

            viewModel.Categories = categories;

            return PartialView("SlideOutMenu", viewModel);
        }

        [HttpGet]
        [ChildActionOnly]
        public virtual ActionResult RenderHomeMenu()
        {
            SiteMenuViewModel viewModel = new SiteMenuViewModel();
            //viewModel.Categories = MenuHelper.GetHomeCateogories();

            var categories = modelEntities.Categories.Where(x => x.Active && x.Enabled && x.Deleted == null && !x.ParentCategoryId.HasValue)
            .OrderByDescending(o => o.DisplayOrder.Value)
            .ThenBy(o => o.DisplayOrder)
            .Select(c => new CategoryViewModel
            {
                CategoryId = c.CategoryId,
                Name = c.Name,
                ParentCategoryId = c.ParentCategoryId,
                DisplayOrder = c.DisplayOrder,
                SeoUrl = c.SeoUrl
            }).ToList();

            viewModel.Categories = categories;

            return PartialView("_HomeMenu", viewModel);
        }

        //[HttpGet]
        //public virtual ActionResult GetNewProducts()
        //{
        //    Random rnd = new Random();
        //    var cartGuid = Utils.ShoppingCartHelper.GetGuid();
        //    int? customerId = null;
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        customerId = modelEntities.Customers.Where(x => x.Email == User.Identity.Name && x.Active).Select(x => x.CustomerId).FirstOrDefault();
        //    }

        //    var query = modelEntities.Products.Where(x => x.Stocks.Any(s => s.StockVirtual > 0));

        //    var products = query.Where(x => x.Published && x.IsNew && x.Active)
        //            .Select(p => new ProductViewModel
        //            {
        //                ProductId = p.ProductId,
        //                OnSale = p.OnSale,
        //                IsNew = p.IsNew,
        //                WholesalePrice = p.WholesalePrice,
        //                OldWholesalePrice = p.OldWholesalePrice,
        //                RetailPrice = p.RetailPrice,
        //                OldRetailPrice = p.OldRetailPrice,
        //                IsLastUnits = p.LastUnits,
        //                Outstanding = p.Outstanding,
        //                IsInWishList = p.WishlistItems.Any(x => x.ProductId == p.ProductId && !x.Deleted.HasValue && (x.GUID == cartGuid || (customerId.HasValue && x.CustomerId == customerId))),
        //                CategoryName = (p.Category.ParentCategoryId != null ? p.Category.Category2.Name + " " + p.Category.Name : p.Category.Name),
        //                Title = p.Title,
        //                DistributorPrice = p.DistributorPrice,
        //                OldDistributorPrice = p.OldDistributorPrice,
        //                SeoUrl = p.SeoUrl,
        //                CategorySeoUrl = p.Category.SeoUrl
        //            })
        //            .AsEnumerable().OrderBy((x => rnd.Next()))
        //            .Take(10);

        //    return PartialView("_HomeProducts", products);
        //}

        [HttpGet]
        public virtual ActionResult RenderHomeProducts()
        {
            Random rnd = new Random();
            var products = modelEntities.Stocks.Where(x => x.Product.Published && x.StockVirtual > 0 && x.Product.Deleted == null)
                .Select(x => x.Product).Distinct()
                .Select(p => new ProductViewModel
                {
                    Id = p.ProductId,
                    OnSale = p.OnSale,
                    IsNew = p.IsNew,
                    //IsLastUnits = p.LastUnits,
                    //Outstanding = p.Outstanding,
                    CategoryName = (p.Category.ParentCategoryId != null ? p.Category.Category2.Name + " " + p.Category.Name : p.Category.Name),
                    Title = p.Title,
                    StoreType = this.StoreType,
                    WholesalePrice = p.WholesalePrice,
                    OldWholesalePrice = p.OldWholesalePrice,
                    RetailPrice = p.RetailPrice,
                    OldRetailPrice = p.OldRetailPrice,
                    //DistributorPrice = p.DistributorPrice,
                    //OldDistributorPrice = p.OldDistributorPrice,
                    SeoUrl = p.SeoUrl,
                    CategorySeoUrl = p.Category.SeoUrl,
                    Images = p.Images.Select(z => new ProductImageViewModel()
                    {
                        Id = z.ImageId,
                        ProductId = z.ProductId.Value,
                        ColorId = z.ColorId,
                        Orientation = z.Orientation,
                        ImageName = z.ImageName,
                        Default = z.Default,
                    }).ToList()
                })
                .AsEnumerable().OrderBy((x => rnd.Next()))
                .Take(6);

            return PartialView("HomeProducts", products);
        }

        [HttpPost]
        public ActionResult Search(string term)
        {
            var isWholesaler = false;

            if (User.Identity.IsAuthenticated)
            {
                var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == User.Identity.Name && x.Active && x.Confirmed);
                if (customer != null)
                    isWholesaler = customer.UserType == (int)UserTypeEnum.Mayorista ? true : false;
            }

            List<Product> persistedProducts = modelEntities.Products
                .Where(x => x.Active && x.Published && x.PublishedWholesaler == isWholesaler && (x.Title.ToLower().Contains(term.ToLower()) || x.Description.Contains(term) || x.Tags.Contains(term) || x.Code.Contains(term)))
                .OrderBy(y => y.Title).Take(5).ToList();

            List<Product> listToShow = new List<Product>();

            foreach (Product product in persistedProducts)
            {
                listToShow.Add(new Product()
                {
                    Title = product.Title,
                    ProductId = product.ProductId,
                    DefaultImageUrl = string.Format("{0}/{1}",Utils.Utility.ImgUrl,product.getDefaultResizedImageUrl)
                });
            }

            var list = JsonConvert.SerializeObject(listToShow, Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
        }
        public virtual PartialViewResult LandingCategories()
        {
            List<Category> categoryList = modelEntities.Categories.Where(x => x.Active && x.Enabled).ToList();

            return PartialView("_LandingCategories", categoryList);
        }


        public JsonResult GetProvinceList()
        {
            List<Province> province = new List<Province>();

            var query = modelEntities.Provinces.AsQueryable();

            var provinceToShow = query.Select(x => new {
                Id = x.ProvinceId,
                Name = x.Name,
            }).ToList();

            return Json(new { Province = provinceToShow }, JsonRequestBehavior.AllowGet);
        }

    }
}