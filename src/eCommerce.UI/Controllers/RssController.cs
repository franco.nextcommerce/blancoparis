﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace eCommerce.UI.Controllers
{
    public class RssController : _StoreBaseController
    {
        // GET: Rss
        public string Index(string CategoryName)
        {
            try
            {
                WriteRssCategory(CategoryName);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return string.Empty;
        }

        private void WriteRssCategory(string categoryName)
        {
            // Get list of 20 most recent posts
            List<RssProductItem> products = GetCategoryProducts(categoryName);

            if (products.Count == 0)
            {
                Response.Write("No se encontraron productos en la categoría " + categoryName);
                return;
            }

            // Clear any previous output from the buffer
            Response.Clear();
            Response.ContentType = "text/xml";
            XmlTextWriter feedWriter = new XmlTextWriter(Response.OutputStream, Encoding.UTF8);

            feedWriter.WriteStartDocument();

            // These are RSS Tags
            feedWriter.WriteStartElement("rss");
            feedWriter.WriteAttributeString("xmlns:g", "http://base.google.com/ns/1.0");
            feedWriter.WriteAttributeString("version", "2.0");

            feedWriter.WriteStartElement("channel");
            feedWriter.WriteElementString("title", "Fibrahumana - RSS de Productos");
            feedWriter.WriteElementString("link", "https://" + Request.Url.Host);
            feedWriter.WriteElementString("description", "Listado de Productos por categoría");

            // Write all Posts in the rss feed
            foreach (RssProductItem item in products)
            {
                feedWriter.WriteStartElement("item");
                feedWriter.WriteElementString("g:id", item.id.ToString());
                feedWriter.WriteElementString("g:title", item.title);
                feedWriter.WriteElementString("g:description", item.description);
                feedWriter.WriteElementString("g:link",item.link);
                feedWriter.WriteElementString("g:image_link", string.Format("{0}/{1}", Utility.ImgUrl, item.image_link) );
                feedWriter.WriteElementString("g:brand", item.brand);
                feedWriter.WriteElementString("g:condition", item.condition);
                feedWriter.WriteElementString("g:availability", item.availability);
                feedWriter.WriteElementString("g:price", item.price);
                feedWriter.WriteElementString("g:google_product_category", item.productCategory);
                feedWriter.WriteEndElement();
            }

            // Close all open tags tags
            feedWriter.WriteEndElement();
            feedWriter.WriteEndElement();
            feedWriter.WriteEndDocument();
            feedWriter.Flush();
            feedWriter.Close();

            Response.End();
        }

        private List<RssProductItem> GetCategoryProducts(string categoryName)
        {
            using (DbModelEntities modelEntities = new DbModelEntities())
            {
                string serverRoot = "https://" + Request.Url.Host + "/" + BaseStoreRoot;

                var products = modelEntities.Stocks.Where(x => x.StockVirtual > 0 && x.Product.Published && !x.Product.SoldOut && x.Product.Active && !x.Product.Deleted.HasValue);

                if (!string.IsNullOrEmpty(categoryName))
                {
                    var category = modelEntities.Categories.First(c => c.SeoUrl == categoryName);

                    products = products.Where(x => x.Product.CategoryId == category.CategoryId || x.Product.Category.ParentCategoryId == category.CategoryId);
                }

                if (StoreType == UserTypeEnum.Mayorista)
                    products = products.Where(p => p.Product.PublishedWholesaler);

                var filteredProducts = products
                    .Select(x => x.Product).Distinct()
                    .Select(x => new RssProductItem
                    {
                        id = x.ProductId,
                        title = x.Title,
                        description = x.Description,
                        link = serverRoot + "/" + Utility.CataloguePrefix + "/" + x.Category.SeoUrl + "/" + x.SeoUrl,
                        brand = "Fibrahumana",
                        condition = "new",
                        categoryId = x.CategoryId.Value,
                        availability = "in stock",
                        price = StoreType == UserTypeEnum.Minorista ? x.RetailPrice.ToString().Replace(",", ".") + " ARS" : x.WholesalePrice.ToString().Replace(",", ".") + " ARS",
                    }).OrderByDescending(x => x.id).ToList();

                foreach (var item in filteredProducts)
                {
                    item.image_link = GetProductDefaultImageUrl(modelEntities, item.id);
                    item.productCategory = ObtainCompleteName(modelEntities, item.categoryId);
                }

                return filteredProducts;
            }
        }

        private string GetProductDefaultImageUrl(DbModelEntities modelEntities, int productId)
        {
            var defaultImage = modelEntities.Images.FirstOrDefault(x => x.ProductId == productId && x.Default);

            if (defaultImage != null)
                return string.Format("products/{0}/{1}", productId, defaultImage.ImageName);
            else
                return "products/DefaultProductImage.jpeg";
        }

        private string ObtainCompleteName(DbModelEntities modelEntities, int categoryId)
        {
            var category = modelEntities.Categories.Find(categoryId);

            if (!category.ParentCategoryId.HasValue)
            {
                return category.Name;
            }
            else
            {
                return ObtainCompleteName(modelEntities, category.ParentCategoryId.Value) + " > " + category.Name;
            }
        }
    }
}

public class RssProductItem
{
    public int id { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public string link { get; set; }
    public string image_link { get; set; }
    public string brand { get; set; }
    public string condition { get; set; }
    public string availability { get; set; }
    public string price { get; set; }

    public int categoryId { get; set; }
    public string productCategory { get; set; }
}