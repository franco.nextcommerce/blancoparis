﻿using eCommerce.Services.Model;
using eCommerce.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.UI.Controllers
{
    public class CotizacionController : Controller
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        // GET: Cotizacion
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Respuesta(int respuesta, Guid guid)
        {
            ProformViewModel proform = new ProformViewModel();
            /*ProformResponse response = modelEntities.ProformResponses.SingleOrDefault(x => x.Guid == guid);
            Order order = modelEntities.Orders.SingleOrDefault(x => x.OrderId == response.OrderId && x.Active);
            proform.order = order;
            proform.guid = guid;

            if (respuesta == 1)
            {
                proform.respuesta = true;
            } else
            {
                proform.respuesta = false;
            }
            */
            return View(proform);
        }
        [HttpPost]
        public ActionResult Respuesta(FormCollection formCollection)
        {
            Guid guid = Guid.Parse(formCollection["guid"]);
            string comentario = formCollection["Comment"];
            bool respuesta = bool.Parse(formCollection["respuesta"]);

            /*ProformResponse proform = modelEntities.ProformResponses.SingleOrDefault(x => x.Guid == guid);
            proform.Comments = comentario;
            proform.Response = respuesta;
            modelEntities.SaveChanges();*/
            return View("RespuestaSuccessful");
        }
    }
}