﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.UI.Controllers
{
    public class StoryController : Controller
    {
        DbModelEntities _db = new DbModelEntities();

        public ActionResult Index(string seo)
        {
            var allStories = _db.Stories;

            Story story;
            if (allStories.Any(x => x.SeoUrl == seo))
            {
                story = allStories.FirstOrDefault(x => x.SeoUrl == seo);
            }
            else
            {
                story = allStories.FirstOrDefault();
            }
            //return RedirectToAction("Index", allStories.FirstOrDefault().SeoUrl);
            //story = _db.Stories.FirstOrDefault(x => x.SeoUrl == seo) != null ? _db.Stories.FirstOrDefault(x => x.SeoUrl == seo) : _db.st;
            if (story == null)
            {
                return View("Error", ErrorType.StoryNotFound);
            }

            story.StoryImages = story.StoryImages.OrderBy(x => x.DisplayOrder).ToList();

            var path = Path.Combine(Utility.BackOfficeUrl + Utility.StoriesFileUploadDirectory, story.Id.ToString());

            foreach (var item in story.StoryImages)
            {
                item.URL = Path.Combine(path, item.ImageName);
            }

            return View(story);
        }
    }
}