﻿using eCommerce.Services.Business;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Arquitecture;
using eCommerce.Services.Tenant;
using eCommerce.Services.Utils;
using eCommerce.UI.ViewModels.Shop;
using eCommerce.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.UI.Controllers
{
    [ChildActionOnly]
    //[AjaxOrChildRequest]
    public class ModulesController : TenantController
    {
        private readonly ShoppingCartManager _shoppingCartManager;
        DbModelEntities _db = new DbModelEntities();
        
        public ModulesController()
        {
            _shoppingCartManager = new ShoppingCartManager();
        }

        //public ModulesController()
        //{
        //    _shoppingCartManager = new ShoppingCartManager();
        //    Tenant _tenant = new Tenant();
        //    this._tenant = _tenant;
        //}

        #region Shop modules

        public PartialViewResult ShopHeader(ShopViewModel Shop)
        {
            return PartialView("Shop/Header/Header", Shop);
        }

        public PartialViewResult ShopContent(ShopViewModel Shop)
        {
            //Shop.categoryList = Shop.categoryList.Where(x => x.CategoryId != Utility.SaleCategoryId).ToList();
            Shop.categoryList = Shop.categoryList.Where(x => x.Active && x.Enabled).ToList();
            return PartialView("Shop/Content/Content", Shop);
        }

        public PartialViewResult ShopFilter(ShopViewModel Shop)
        {
            return PartialView("Shop/Filter/Filter", Shop);
        }


        public PartialViewResult ShopList(ShopViewModel Shop)
        {
            return PartialView("Shop/List/List", Shop);
        }

        public PartialViewResult ShopItem(ViewModels.Shop.ProductViewModel product)
        {
            return PartialView("Shop/Product/Product", product);
        }

        public PartialViewResult RelatedProducts(int productId)
        {
            var product = _db.Products.Find(productId);

            var pQuery = _db.Products.Where(x => x.Active && x.Deleted == null && x.Published);

            pQuery = pQuery.Where(x => x.CategoryId == product.CategoryId);
            pQuery = pQuery.Where(x => x.Stocks.Any(s => s.Size.Deleted == null && s.Color.Deleted == null && s.StockVirtual > 0));
            pQuery = pQuery.OrderByDescending(x => x.PublicationDate.HasValue).ThenByDescending(x => x.PublicationDate);

            var relatedProducts = pQuery.Select(y => new ViewModels.Shop.ProductViewModel
            {
                Id = y.ProductId,
                Title = y.Title,
                Description = y.Description,
                Price = y.WholesalePrice,
                OldPrice = y.OldWholesalePrice,
                SeoUrl = y.SeoUrl,
                //category = product.Category,
                SoldOut = y.SoldOut,
                CategorySeoUrl = y.Category.SeoUrl,
                ColorList = y.Stocks.Where(x => !x.Deleted.HasValue && x.StockReal > 0 && x.StockVirtual > 0).Select(c => c.Color).OrderBy(x => x.ColorId).ToList(),
                SizeList = y.Stocks.Where(x => !x.Deleted.HasValue && x.StockReal > 0 && x.StockVirtual > 0).Select(c => c.Size).Distinct().OrderBy(s => s.DisplayOrder).ToList(),
                ShopImageSize = y.ShopImageSize,
                Images = y.Images.Select(z => new ProductImageViewModel()
                {
                    Id = z.ImageId,
                    ProductId = z.ProductId.Value,
                    ColorId = z.ColorId,
                    Orientation = z.Orientation,
                    ImageName = z.ImageName,
                    Default = z.Default,
                }).ToList()
            }).Take(3).ToList();

            return PartialView("Shop/Product/RelatedProducts", relatedProducts);
        }

        #endregion

        #region Layout modules

        public PartialViewResult LY_Logo()
        {
            var model = new ParametersViewModel()
            {
                Url = this.CurrentTenant.Logo
                //Url = "LasLocas.jpg"
            };

            return PartialView("Header/Logo", model);
        }

        public PartialViewResult LY_GoogleAnalytics()
        {
            var model = new ParametersViewModel()
            {
                GoogleAnalytics = this.CurrentTenant.GoogleAnalytics
            };

            return PartialView("Tracking/GoogleAnalytics", model);
        }

        public PartialViewResult LY_contactBar()
        {
            var model = new ParametersViewModel()
            {
                MinimumOrder = this.CurrentTenant.MinimumOrder,
                CompanyNumbre = this.CurrentTenant.CompanyNumber,
                Whatsapp = this.CurrentTenant.whatsapp
            };

            return PartialView("Layout/ContactBar/ContactBar", model);
        }

        public PartialViewResult LY_Redes()
        {
            var model = new ParametersViewModel()
            {
                Facebook_Account = this.CurrentTenant.Facebook,
                Instagram_Account = this.CurrentTenant.Instagram
            };

            return PartialView("Layout/RedesFooter/RedesFooter", model);
        }

        public PartialViewResult LY_Whatsapp()
        {
            var model = new ParametersViewModel()
            {
                Whatsapp = this.CurrentTenant.whatsapp
            };

            return PartialView("Layout/Whatsapp/Whatsapp", model);

        }

        public PartialViewResult NavMenu()
        {
            return PartialView("Layout/NavMenu/NavMenu");
        }

        public PartialViewResult MenuShop()
        {
            //List<Category> categories = new List<Category>();
            //List<int> exlcude_categories = new List<int>();

            //todos los productos.
            //exlcude_categories.Add(int.Parse(ConfigurationManager.AppSettings["AllProductsCategoryId"]));
            //exlcude_categories.Add(int.Parse(ConfigurationManager.AppSettings["SaleCategoryId"]));

            var categories = _db
              .Categories
              .Where(x => x.Active && x.Enabled && x.Deleted == null /*&& !exlcude_categories.Contains(x.CategoryId)*/)
              .OrderByDescending(o => o.DisplayOrder.HasValue)
              .ThenBy(o => o.DisplayOrder)
              .Select(c => new CategoryViewModel
              {
                  CategoryId = c.CategoryId,
                  ParentCategoryId = c.ParentCategoryId,
                  ShowInHeader = c.ShowInHeader,
                  Active = c.Active,
                  Enabled = c.Enabled,
                  DisplayOrder = c.DisplayOrder,
                  Name = c.Name,
                  SeoUrl = c.SeoUrl,
              }).ToList();

            //var categories = _db
            //              .Categories
            //              .Where(x => x.Active && x.Deleted == null && x.Enabled)
            //              .OrderByDescending(o => o.DisplayOrder.HasValue)
            //              .ThenBy(o => o.DisplayOrder).Select(x => new CategoryViewModel
            //              {
            //                  CategoryId = x.CategoryId,
            //                  ParentCategoryId = x.ParentCategoryId,
            //                  Name = x.Name,
            //                  SeoUrl = x.SeoUrl,
            //                  MenuClickable = x.MenuClickable,
            //                  WholesaleHeader = x.WholesalerHeader,
            //                  RetailHeader = x.RetailHeader,
            //                  StoreType = (int)this.StoreType
            //              }).ToList();

            return PartialView("Header/ShopCategories", categories);
        }

        public PartialViewResult MenuShopMobile()
        {

            var categories = _db
                          .Categories
                          .Where(x => x.Active && x.Enabled && x.Deleted == null && x.Enabled)
                          .OrderByDescending(o => o.DisplayOrder.HasValue)
                          .ThenBy(o => o.DisplayOrder).Select(x => new CategoryViewModel
                          {
                              CategoryId = x.CategoryId,
                              ParentCategoryId = x.ParentCategoryId,
                              ShowInHeader = x.ShowInHeader,
                              Active = x.Active,
                              Enabled = x.Enabled,
                              DisplayOrder = x.DisplayOrder,
                              Name = x.Name,
                              SeoUrl = x.SeoUrl,
                          }).ToList();


            return PartialView("Header/ShopCategoriesMobile", categories);
        }

        #endregion
    }
}