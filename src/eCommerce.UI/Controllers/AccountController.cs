﻿using eCommerce.Services.Common;
using eCommerce.Services.Model;
using eCommerce.Services.Security;
using eCommerce.UI.Models;
using eCommerce.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;
using eCommerce.Services.Business;
using eCommerce.UI.ViewModels;
using eCommerce.UI.Utils;
using eCommerce.Services.Model.Enums;
using Facebook;
using System.Configuration;
using eCommerce.UI.Utils.Interfaces;
using System.Collections.Generic;
using System.Web;
using System.Threading.Tasks;
using eCommerce.Services.Services;

namespace eCommerce.UI.Controllers
{
    [Authorize]
    public class AccountController : _StoreBaseController
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        private ShoppingCartManager _shoppingCartManager;
        private readonly string _facebookAppId;
        private readonly string _facebookAppSecret;
        private HttpContext context = System.Web.HttpContext.Current;

        String KeyString = "Gv7L3V15jCdb9P5XGKiPnhHZ7JlKcmU=";

        public IFormsAuthenticationService FormsService { get; private set; }
        public IMembershipService MembershipService { get; private set; }

        public AccountController() : this(null, null) { }

        public AccountController(IFormsAuthenticationService formsService, IMembershipService membershipService)
        {
            FormsService = formsService ?? new FormsAuthenticationService();
            MembershipService = membershipService ?? new MembershipService();
            _facebookAppId = ConfigurationManager.AppSettings.Get("AppId");
            _facebookAppSecret = ConfigurationManager.AppSettings.Get("AppSecret");
            _shoppingCartManager = new ShoppingCartManager();
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (Request.IsAuthenticated)
            {
                if (!String.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return Redirect("/");
                }
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult FacebookLogin(string uid, string accessToken, string returnUrl)
        {
            if (string.IsNullOrWhiteSpace(uid) || string.IsNullOrWhiteSpace(accessToken))
                return Json(new { error = "Invalid login credentials" });

            Session["uid"] = uid;
            Session["accessToken"] = accessToken;

            FacebookClient facebookApi = CreateFacebookClient(accessToken);

            dynamic me = facebookApi.Get("/me?fields=first_name,last_name,email");

            var customerExternalLogin = MembershipService.GetFacebookCustomer(uid);

            if (customerExternalLogin != null)
            {
                FormsService.SignIn(customerExternalLogin.Customer.Email, false);

                if (!string.IsNullOrWhiteSpace(returnUrl) || !string.IsNullOrWhiteSpace(HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["ReturnUrl"]))
                {
                    if (String.IsNullOrEmpty(returnUrl))
                        returnUrl = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["returnUrl"];

                    return Redirect( "/" + returnUrl);
                }
                else
                {
                    return Redirect("/shop");
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(me.email))
                {
                    string email = me.email;
                    var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == email && x.Active && x.Deleted == null);

                    //Si ya existe el usuario lo asocia al actual
                    if (customer != null)
                    {
                        MembershipCreationStatus result = MembershipService.CreateExternalCustomer(customer, me.id, false);
                        if (result == MembershipCreationStatus.Success)
                        {
                            FormsService.SignIn(customer.Email, false);

                            if (!string.IsNullOrWhiteSpace(returnUrl) || !string.IsNullOrWhiteSpace(HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["ReturnUrl"]))
                            {
                                if (String.IsNullOrEmpty(returnUrl))
                                    returnUrl = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["returnUrl"];

                                return Redirect("/" + returnUrl);
                            }
                            else
                            {
                                return Redirect("/shop");
                            }
                        }
                        else
                        {
                            TempData["Message"] = "Se produjo un error al intentar asociar su cuenta de Facebook.";
                            TempData["ActionType"] = ActionType.Back;

                            return Redirect("/Account/Information");
                        }
                    }
                    else
                    {
                        var fbCustomer = new Customer();
                        fbCustomer.Email = me.email;
                        fbCustomer.FullName = $"{me.first_name} {me.last_name}";
                        fbCustomer.UserType = (int)UserTypeEnum.Minorista;

                        var result = MembershipService.CreateExternalCustomer(fbCustomer, me.id, true);

                        if (result == MembershipCreationStatus.Success)
                        {
                            FormsService.SignIn(fbCustomer.Email, false);

                            if (!string.IsNullOrWhiteSpace(returnUrl) || !string.IsNullOrWhiteSpace(HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["ReturnUrl"]))
                            {
                                if (String.IsNullOrEmpty(returnUrl))
                                    returnUrl = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["returnUrl"];

                                return Redirect("/" + returnUrl);
                            }
                            else
                            {
                                return Redirect("/shop");
                            }
                        }
                        else
                        {
                            TempData["Message"] = "Se produjo un error al intentar crear su cuenta de Facebook.";
                            TempData["ActionType"] = ActionType.Back;

                            return Redirect("/Account/Information");
                        }
                    }
                }
                else
                {
                    //return RedirectToAction("CreateFromLogOnFacebook");
                    TempData["Message"] = "Se produjo un error al intentar crear su cuenta de Facebook.";
                    TempData["ActionType"] = ActionType.Back;

                    return Redirect("/Account/Information");
                }
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult Login(LoginViewModel model, string returnUrl)
        {
            string email = Request["Email"];
            string password = Request["Password"];
            bool rememberMe = Request["RememberMe"] != null ? true : false;
            bool isValid = false;

            isValid = MembershipService.ValidateCustomer(email, password);

            if (isValid)
            {
                Customer customer = modelEntities.Customers.FirstOrDefault(x => (x.Email.ToLower() == email.ToLower() || x.Username.ToLower() == email.ToLower()) && x.Active && x.Deleted == null);

                FormsService.SignIn(customer.Email, rememberMe);

                AssingCartItems(email);

                if (!string.IsNullOrWhiteSpace(returnUrl) || !string.IsNullOrWhiteSpace(HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["ReturnUrl"]))
                {
                    if (String.IsNullOrEmpty(returnUrl))
                    {
                        returnUrl =  HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["returnUrl"];
                    }
                    return Redirect(returnUrl);
                }
                else
                {
                    return Redirect("/Shop");
                }
            }

            TempData["Message"] = "El Email o la Clave ingresada son inccorrectos!";
            TempData["ActionType"] = ActionType.Back;

            return Redirect("/Account/Information");
        }

        private void AssingCartItems(string email)
        {
            var shoppingCartManager = new ShoppingCartManager();
            var cartGuid = ShoppingCartHelper.GetGuid();

            List<OrderDetail> cartDetails = _shoppingCartManager.GetShoppingCart(cartGuid, User.Identity.IsAuthenticated ? User.Identity.Name : null, this.StoreType);
            var orderDetailsIds = cartDetails.Select(x => x.OrderDetailId).ToArray();
            var detailsToUpdate = modelEntities.OrderDetails.Where(x => orderDetailsIds.Contains(x.OrderDetailId)).ToList();

            foreach (var detail in detailsToUpdate)
            {
                detail.CreatedBy = email;
            }

            modelEntities.SaveChanges();
        }

        private void UpdateCartPrices()
        {
            var cartGuid = ShoppingCartHelper.GetGuid();
            var shoppingCartViewModel = GetUserOrderDetails(cartGuid);
            var orderDetailsIds = shoppingCartViewModel.OrderDetails.Select(x => x.OrderDetailId).ToArray<int>();
            var orderDetails = modelEntities.OrderDetails.Include("Product").Where(x => orderDetailsIds.Contains(x.OrderDetailId)).ToList();

            foreach (var item in orderDetails)
            {
                item.Price = item.Product.CustomerPrice;

            }
            modelEntities.SaveChanges();
        }

        private ShoppingCartViewModel GetUserOrderDetails(string cartGuid)
        {
            List<OrderDetail> cartDetails = _shoppingCartManager.GetShoppingCart(cartGuid, User.Identity.IsAuthenticated ? User.Identity.Name : null, this.StoreType);

            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                OrderDetails = cartDetails.Select(od => new OrderDetailViewModel()
                {
                    Quantity = od.Quantity,
                    OrderId = od.OrderId,
                    CategorySeoUrl = od.Product.Category.SeoUrl,
                    SeoUrl = od.Product.SeoUrl,
                    Price = od.Product.CustomerPrice,
                    Description = od.Product.Description,
                    ProductId = od.ProductId,
                    SizeId = od.SizeId,
                    ColorId = od.ColorId,
                    SizeDescription = od.Size.Description,
                    ColorDescription = od.Color.Description.ToLower(),
                    OrderDetailId = od.OrderDetailId,
                    Title = od.Product.Title,
                    FormattedTitle = od.Product.FormattedTitle,
                    ProductBrand = od.Product.Brand != null ? od.Product.Brand.Name : string.Empty,
                    ProductImage = string.Format("{0}/{1}", Utility.ImgUrl, od.Product.getDefaultImageUrl),
                    ProductImageResized = string.Format("{0}/{1}", Utils.Utility.ImgUrl, od.Product.getResizedImageUrlByColor(od.ColorId)),
                    MaxDiscount = od.Product.MaxDiscount
                }).ToList(),
                Guid = cartGuid.ToString()
                //Customer = customer
            };

            return shoppingCartViewModel;
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult LogOut()
        {
            FormsService.SignOut();
            return Redirect( "/Account/Login");
        }

        [AllowAnonymous]
        public virtual ActionResult CreateFromLogOnFacebook()
        {
            var customer = new Customer();

            FacebookClient facebookApi = CreateFacebookClient(Session["accessToken"].ToString());
            dynamic me = facebookApi.Get("/me?fields=first_name,last_name,email");

            customer.Email = me.email;
            customer.FullName = me.first_name + ", " + me.last_name;
            //ViewBag.FirstName = me.first_name;
            //ViewBag.LastName = me.last_name;

            TempData["Provinces"] = new SelectList(modelEntities.Provinces.ToList(), "ProvinceId", "Name");

            return View("Register", customer);
        }

        private FacebookClient CreateFacebookClient(string accessToken)
        {
            var facebookApi = new FacebookClient();
            facebookApi.AccessToken = accessToken;
            facebookApi.AppSecret = _facebookAppSecret;
            facebookApi.AppId = _facebookAppId;
            return facebookApi;
        }

        public static IEnumerable<SelectListItem> GetEnumSelectList<T>()
        {
            return (Enum.GetValues(typeof(T)).Cast<int>().Select(e => new SelectListItem() { Text = Enum.GetName(typeof(T), e), Value = e.ToString() })).ToList();
        }

        //[HttpGet]
        //[AllowAnonymous]
        //public JsonResult CheckPromotionsBanners()
        //{
        //    var showBanner = modelEntities.Parameters.FirstOrDefault(x => x.Name == "PromotionalBanner") != null;
        //    var bannerText = modelEntities.Parameters.FirstOrDefault(x => x.Name == "PromotionalBanner").Text;
        //    var minimumOrder = modelEntities.Parameters.FirstOrDefault(x => x.Name == "MinimumOrderWS").Value;
        //    var bannerValue = "";

        //    HttpCookie promotionCookie = Request.Cookies["FibrahumanaPromotionBanner"];

        //    if (promotionCookie != null)
        //    {
        //        bool.TryParse(promotionCookie.Value, out showBanner);
        //    }
        //    else
        //    {
        //        HttpCookie myCookie = new HttpCookie("FibrahumanaPromotionBanner");
        //        myCookie.Expires = DateTime.Now.AddDays(1d);
        //        myCookie.Value = (true).ToString();

        //        Response.Cookies.Add(myCookie);
        //    }
        //    if (showBanner && !string.IsNullOrEmpty(bannerText))
        //    {
        //        bannerValue = bannerText;
        //    }

        //    if (string.IsNullOrEmpty(bannerText)) showBanner = false;

        //    var results = new { ShowBanner = showBanner, BannerValue = bannerValue };
        //    return Json(results, JsonRequestBehavior.AllowGet);
        //}

        [HttpGet]
        [AllowAnonymous]
        public JsonResult CheckPromotionsBanners()
        {
            //var userType = (int)UserTypeEnum.Minorista;
            var userType = (int)this.StoreType;
            HttpCookie promotionCookie = Request.Cookies["BlancoParisPromotionBanner"];
            bool cookieShowBanner = true;
            string bannerValue = "";
            if (promotionCookie != null)
            {
                bool.TryParse(promotionCookie.Value, out cookieShowBanner);
            }
            else
            {
                HttpCookie myCookie = new HttpCookie("BlancoParisPromotionBanner");
                myCookie.Expires = DateTime.Now.AddDays(1d);
                myCookie.Value = (true).ToString();

                Response.Cookies.Add(myCookie);
            }

            //if (User.Identity.IsAuthenticated)
            //{
            //    userType = modelEntities.Customers.FirstOrDefault(c => c.Active && c.Deleted == null && c.Email == User.Identity.Name)?.UserType ?? 1;
            //}

            var promText = modelEntities.PromotionalTexts.FirstOrDefault(promtext => promtext.Active && promtext.UserType == userType);

           
            var minimumOrder = modelEntities.Parameters.FirstOrDefault(x => x.Name == "MinimumOrderRetail").Value;
            

            var minimoText = !string.IsNullOrWhiteSpace(promText.Text) && !string.IsNullOrEmpty(promText.Text) ? "Mínimo de compra $" + minimumOrder.ToString().Split(',')[0] : "Mínimo de compra $" + minimumOrder.ToString().Split(',')[0];

            minimoText += promText.Text;

            var showBanner = false;
            var color = "#000";

            if (promText != null)
            {
                showBanner = promText != null && cookieShowBanner;
                bannerValue = promText?.Text ?? "";
                color = promText.BackgroundColor;
            }
            

            var results = new { ShowBanner = showBanner, BannerValue = minimoText, Color = color };
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult UpdatePromotionBanner()
        {
            bool result = true;
            string errorMessage = string.Empty;

            try
            {
                HttpCookie promotionCookie = Request.Cookies["FibrahumanaPromotionBanner"];
                if (promotionCookie != null)
                {
                    promotionCookie.Value = (false).ToString();
                    Response.Cookies.Add(promotionCookie);
                }
            }
            catch (Exception ex)
            {
                result = false;
                errorMessage = ex.Message;
            }


            return Json(new { Success = result, ErrorMessage = errorMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetUsername()
        {
            return Json(
                new
                {
                    Name = !User.Identity.IsAuthenticated ? string.Empty :
                        modelEntities.Customers.FirstOrDefault(x => x.Deleted == null && x.Active && x.Email == User.Identity.Name).FullName
                },
                JsonRequestBehavior.AllowGet
            );
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetPopupSrc()
        {
            var Popup = modelEntities.Banners.FirstOrDefault(x => x.Active && !x.Deleted.HasValue && x.Position == (int)BannerPositionEnum.Popup);

            if (Popup == null)
            {
                return Json(new { Src = "", Link = "" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Src = Popup.BackImageUrl, Link = Popup.Link }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult RegisterWithFacebook(Customer customer)
        {
            FacebookClient facebookApi = CreateFacebookClient(Session["accessToken"].ToString());

            dynamic me = facebookApi.Get("/me?fields=first_name,last_name,email");

            customer.FullName = string.Format("{0} {1}", me.first_name, me.last_name);
            customer.IsFacebookUser = true;
            customer.IsSiteUser = false;

            MembershipCreationStatus membershipCreationStatus = MembershipService.CreateExternalCustomer(customer, me.id, true);

            if (membershipCreationStatus == MembershipCreationStatus.Success && !string.IsNullOrEmpty(Request["redirectUrl"]))
            {
                return Redirect(Request["redirectUrl"]);
            }

            FormsService.SignIn(customer.Email, false);

            TempData["Message"] = "Tu cuenta ha sido creada con éxito, disfruta tus compras!";
            TempData["ActionType"] = ActionType.Catalogue;
            TempData["ShowSuccessfulIcon"] = true;

            TempData["UserType"] = customer.UserType;
            //return RedirectToAction("Information");

            return View("CompleteRegistration");
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult Register()
        {
            var provinces = modelEntities.Provinces.ToList();
            TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name");

            if (User.Identity.IsAuthenticated)
                return RedirectToAction("MyAccount");

            var customer = new Customer();
            customer.UserType = (int)this.StoreType;

            return View("Register", customer);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Register(Customer customer)
        {
            customer.FullName = customer.Name + " " + customer.Surname;

            MembershipCreationStatus membershipCreationStatus = RegisterCustomer(customer);

            if (membershipCreationStatus == MembershipCreationStatus.Success && !string.IsNullOrEmpty(Request["redirectUrl"]))
            {
                FormsService.SignIn(customer.Email, false);
                return Redirect(Request["redirectUrl"]);
            }

            return Redirect("/Account/Information");
            //return View("CompleteRegistration");
        }

        private MembershipCreationStatus RegisterCustomer(Customer customer)
        {
            MembershipCreationStatus membershipCreationStatus = MembershipService.CreateCustomer(customer);
            switch (membershipCreationStatus)
            {
                case MembershipCreationStatus.Success:
                    //enviar mail de bienvenida
                    #region MailSend

                    IMailing mailUtil = Mailing.Instance;
                    mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));

                    //Mail de bienvenida al clinte.
                    mailUtil.SendWelcomeMail(customer.Email);

                    //Si el usuario es mayorista aviso a la cuenta.
                    if (customer.UserType == (int)UserTypeEnum.Mayorista)
                        mailUtil.SendNewWholeselerMail(customer);

                    #endregion

                    if (Request.Form["Newsletter"] != null && Request.Form["Newsletter"] == "on")
                        RegisterNewsSuscriber(customer);

                    FormsService.SignIn(customer.Email, false);
                    TempData["Message"] = "Tu cuenta ha sido creada con éxito, disfruta tus compras!";
                    TempData["ActionType"] = ActionType.Catalogue;
                    TempData["ShowSuccessfulIcon"] = true;
                    break;

                //case MembershipCreationStatus.Wholesaler:
                //    //enviar mail de bienvenida
                //    #region MailSend
                //    IMailing mailWholesaler = Mailing.Instance;
                //    mailWholesaler.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                //    mailWholesaler.SendWelcomeMail(customer.Email);
                //    #endregion MailSend

                //    if (Request.Form["Newsletter"] != null && Request.Form["Newsletter"] == "on")
                //        RegisterNewsSuscriber(customer);

                //    FormsService.SignIn(customer.Email, false);
                //    TempData["Message"] = "Tu cuenta ha sido creada con éxito, disfruta tus compras!";
                //    TempData["ActionType"] = ActionType.Catalogue;
                //    TempData["ShowSuccessfulIcon"] = true;
                //    break;

                case MembershipCreationStatus.RequiresConfirmation:

                    #region MailSend
                    IMailing mailConfirmation = Mailing.Instance;
                    mailConfirmation.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                    mailConfirmation.SendNewWholeselerMail(customer);
                    #endregion MailSend

                    if (Request.Form["Newsletter"] != null && Request.Form["Newsletter"] == "on")
                        RegisterNewsSuscriber(customer);

                    TempData["Message"] = @"Gracias por registrarte en Fibrahumana. Estamos verificando tus datos para poder habilitarte cómo cliente mayorista, te notificaremos por mail cuando este proceso haya sido completado.";
                    TempData["ActionType"] = ActionType.Catalogue;
                    break;

                case MembershipCreationStatus.DuplicateEmail:
                    TempData["Message"] = "Ya existe una cuenta con el E-mail ingresado!";
                    TempData["ActionType"] = ActionType.Back;
                    break;

                case MembershipCreationStatus.DupiclateCuit:
                    TempData["Message"] = "Ya existe una cuenta con el Cuit ingresado!";
                    TempData["ActionType"] = ActionType.Back;
                    break;

                case MembershipCreationStatus.InvalidCUIT:
                    TempData["Message"] = "El CUIT ingresado no es válido.";
                    TempData["ActionType"] = ActionType.Back;
                    break;

                case MembershipCreationStatus.Fail:
                    TempData["Message"] = "Ha ocurrido un error al crear su usuario!";
                    TempData["ActionType"] = ActionType.Back;
                    break;
            }

            return membershipCreationStatus;
        }

        public void RegisterNewsSuscriber(Customer customer)
        {
            try
            {
                NewsSuscriber news = new NewsSuscriber();
                news.FullName = customer.FullName;
                news.Email = customer.Email;

                news.Created = DateTime.Now;
                news.Active = true;
                modelEntities.NewsSuscribers.Add(news);
                modelEntities.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpGet]
        public virtual ActionResult MyAccount()
        {
            var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == User.Identity.Name && x.Active && x.Deleted == null);
            var provinces = modelEntities.Provinces.ToList();

            if (customer.ProvinceId.HasValue)
                TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name", customer.ProvinceId.Value);
            else
                TempData["Provinces"] = new SelectList(provinces, "ProvinceId", "Name");

            return View(customer);
        }

        [HttpPost]
        public virtual ActionResult MyAccount(Customer customer)
        {
            try
            {
                var _customer = modelEntities.Customers.Find(customer.CustomerId);

                _customer.FullName = customer.FullName;
                _customer.UserType = customer.UserType;
                _customer.ProvinceId = customer.ProvinceId;
                _customer.City = customer.City;
                _customer.Address = customer.Address;
                _customer.AddressNumber = customer.AddressNumber;
                _customer.Floor = customer.Floor;
                _customer.Department = customer.Department;
                _customer.ZipCode = customer.ZipCode;
                _customer.PhoneNumber = customer.PhoneNumber;
                _customer.Cellphone = customer.Cellphone;
                _customer.DNI = customer.DNI;

                if (_customer.UserType == (int)UserTypeEnum.Mayorista)
                {
                    _customer.BillingAddress = customer.BillingAddress;
                    _customer.IvaConditionId = customer.IvaConditionId;
                }

                if (customer.CorporateName != null)
                    _customer.CorporateName = customer.CorporateName;

                if (customer.CUIT != null)
                    _customer.CUIT = customer.CUIT;

                modelEntities.SaveChanges();

                TempData["Message"] = "Su información ha sido guardada correctamente.";
                TempData["ActionType"] = ActionType.Catalogue;
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Ha ocurrido un error al modificar su información.";
                TempData["ActionType"] = ActionType.Back;
            }

            return Redirect("/Account/Information");
        }

        [AllowAnonymous]
        public virtual ActionResult Information()
        {
            MessageViewModel messageViewModel = new MessageViewModel();
            messageViewModel.Message = TempData["Message"] as string;

            if (TempData["ShowSuccessfulIcon"] != null)
                messageViewModel.ShowSuccessfulIcon = true;
            else
                messageViewModel.ShowSuccessfulIcon = false;

            if (TempData["ActionType"] == null)
                messageViewModel.ActionType = ActionType.Home;
            else
                messageViewModel.ActionType = (ActionType)TempData["ActionType"];

            return View("Information", messageViewModel);
        }

        [HttpGet]
        public virtual ActionResult ShoppingHistory()
        {
            //var user = HttpContext.User.Identity as CustomIdentity;
            //List<int> listStatusRejected = new List<int>();

            //listStatusRejected.Add((int)OrderStatusEnum.EnProceso);
            //listStatusRejected.Add((int)OrderStatusEnum.Carrito);
            //listStatusRejected.Add((int)OrderStatusEnum.Cancelada);

            var orders = modelEntities.Orders.Where(o => o.Active && o.Customer.Email == User.Identity.Name).OrderByDescending(x => x.OrderId).ToList();

            return View(orders);
        }

        //[HttpGet]
        //[AllowAnonymous]
        //public JsonResult CheckPromotionsBanners()
        //{
        //    var showBanner = false;
        //    var isWholesaler = false;
        //    var couponText = "";
        //    var couponDescription = "";
        //    if (User.Identity.IsAuthenticated && User.Identity is CustomIdentity)
        //    {
        //        var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == User.Identity.Name && x.Active);
        //        if(customer.ShowTopBanner)
        //        {
        //            if (customer.UserType == (int)UserTypeEnum.Mayorista)
        //            {
        //                isWholesaler = true;
        //                showBanner = modelEntities.Coupons.Any(x => x.Active && x.CouponType == (int)CouponTypeEnum.Mayorista &&
        //                    x.FromDate <= DateTime.Today && x.ExpirationDate >= DateTime.Today);
        //                var coupon = modelEntities.Coupons.Where(x => x.Active && x.CouponType == (int)CouponTypeEnum.Mayorista &&
        //                    x.FromDate <= DateTime.Today && x.ExpirationDate >= DateTime.Today).OrderByDescending(t => t.CouponId).FirstOrDefault();
        //                if (coupon != null)
        //                {
        //                    couponText = coupon.Text;
        //                    couponDescription = coupon.Description;
        //                }
        //            }
        //            else
        //            {
        //                showBanner = modelEntities.Coupons.Any(x => x.Active && x.FromDate <= DateTime.Today && x.ExpirationDate >= DateTime.Today && x.CouponType == (int)CouponTypeEnum.Minorista);
        //                var coupon = modelEntities.Coupons.Where(x => x.Active && x.CouponType == (int)CouponTypeEnum.Minorista &&
        //                        x.FromDate <= DateTime.Today && x.ExpirationDate >= DateTime.Today).OrderByDescending(t => t.CouponId).FirstOrDefault();
        //                if (coupon != null)
        //                {
        //                    couponText = coupon.Text;
        //                    couponDescription = coupon.Description;
        //                }
        //            }

        //        }
        //    }
        //    else
        //    {
        //        showBanner = modelEntities.Coupons.Any(x => x.Active && x.FromDate <= DateTime.Today && x.ExpirationDate >= DateTime.Today && x.CouponType == (int)CouponTypeEnum.Minorista);
        //        var coupon = modelEntities.Coupons.Where(x => x.Active && x.CouponType == (int)CouponTypeEnum.Minorista &&
        //                x.FromDate <= DateTime.Today && x.ExpirationDate >= DateTime.Today).OrderByDescending(t => t.CouponId).FirstOrDefault();
        //        if (coupon != null)
        //        {
        //            couponText = coupon.Text;
        //            couponDescription = coupon.Description;
        //        }
        //    }



        //    var results = new { ShowBanner = showBanner, isWholesaler = isWholesaler, CouponText = couponText, CouponDescription = couponDescription };
        //    return Json(results, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult UpdatePromotionBanner()
        //{
        //    bool result = true;
        //    string errorMessage = string.Empty;
        //    if (User.Identity.IsAuthenticated && User.Identity is CustomIdentity)
        //    {
        //        try
        //        {
        //            var customer = modelEntities.Customers.FirstOrDefault(x => x.Email == User.Identity.Name && x.Active);
        //            customer.ShowTopBanner = false;

        //            modelEntities.SaveChanges();

        //        }
        //        catch (Exception ex)
        //        {
        //            result = false;
        //            errorMessage = ex.Message;
        //        }
        //    }

        //    return Json(new { Success = result, ErrorMessage = errorMessage }, JsonRequestBehavior.AllowGet);
        //}


        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var email = model.Email;
                var user = modelEntities.Customers.SingleOrDefault(x => x.Email == email && x.Active);

                if (user == null)
                {
                    TempData["errorForgot"] = "No existe un usuario con este mail o aún no fue confirmado.";
                    return View("ForgotPassword");
                }
                else if (user.IsFacebookUser)
                {
                    TempData["errorForgot"] = "No se le puede cambiar la contraseña a un usuario de facebook.";
                    return View("ForgotPassword");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                Guid guid = Guid.NewGuid();

                user.ResetPassword = guid;
                modelEntities.SaveChanges();

                var callbackUrl = Url.Action("ResetPassword", "Account", new { guid = guid }, protocol: Request.Url.Scheme);

                #region MailSend

                IMailing mailUtil = Mailing.Instance;
                mailUtil.SetTemplatesDirectory(Server.MapPath(Services.Utils.Utility.MailTemplates));
                mailUtil.SendNewPasswordMail(callbackUrl, model.Email);

                #endregion MailSend

                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string guid)
        {
            if (guid != null)
            {
                ResetPasswordViewModel rpvm = new ResetPasswordViewModel();
                rpvm.Guid = guid;
                return View(rpvm);
            }
            else
            {
                TempData["Message"] = "Hubo un error, intente entrar de nuevo a este sitio a través de su mail.";
                TempData["ActionType"] = ActionType.Back;

                return Redirect("/Account/Information");
            }

        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var email = model.Email;
            var user = modelEntities.Customers.SingleOrDefault(x => x.Email == email && x.Active);
            if (user != null)
            {
                if (user.ResetPassword == Guid.Parse(model.Guid))
                {
                    user.Password = EncryptionService.Encrypt(model.Password, KeyString);
                    modelEntities.SaveChanges();
                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }
                else
                {
                    TempData["Message"] = "El mail ingresado no coincide con el link de blanqueo";
                }
            }
            else
            {
                TempData["Message"] = "El mail ingresado no existe";
            }
            TempData["ActionType"] = ActionType.Back;

            return Redirect("/Account/Information");
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult OrderDetail(int Id)
        {
            var shoppingCartViewModel = GetShoppingCart(Id);
            return View("OrderDetail", shoppingCartViewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult DeliveryDetail(int Id)
        {
            //var shoppingCartViewModel = GetShoppingCart(Id);

            var order = modelEntities.Orders.FirstOrDefault(x => x.OrderId == Id && x.Active);

            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                OrderId = Id,
                Order = order,
                Discount = order.Discount.HasValue ? order.Discount.Value : 0,
                ShippingCost = order.ShippingCost.HasValue ? order.ShippingCost.Value : 0,
            };

            return View(shoppingCartViewModel);
        }


        private ShoppingCartViewModel GetShoppingCart(int orderId)
        {
            var order = modelEntities.Orders.Include("OrderDetails").FirstOrDefault(x => x.OrderId == orderId && x.Active);
            var cartDetails = order.OrderDetails.Where(o => o.OrderId == orderId && o.Active).OrderBy(x => x.Product.Title).ToList();

            //check order and items discount
            var discount = order.Discount.HasValue ? order.Discount.Value : 0;
            discount += cartDetails.Sum(x => x.SubTotal) - cartDetails.Sum(x => x.SubTotalWithDiscount);

            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                OrderId = orderId,
                Discount = discount,
                ShippingCost = order.ShippingCost.HasValue ? order.ShippingCost.Value : 0,
                OrderDetails = cartDetails.Select(od => new OrderDetailViewModel()
                {
                    Quantity = od.Quantity,
                    Price = od.Price,
                    CategorySeoUrl = od.Product.Category.SeoUrl,
                    SeoUrl = od.Product.SeoUrl,
                    Description = od.Product.Description,
                    ProductId = od.ProductId,
                    SizeId = od.SizeId,
                    ColorId = od.ColorId,
                    SizeDescription = od.Size.Description,
                    ColorDescription = od.Color.Description.ToLower(),
                    OrderDetailId = od.OrderDetailId,
                    Title = od.Product.Title,
                    ProductBrand = od.Product.Brand != null ? od.Product.Brand.Name : string.Empty,
                    ProductImage = string.Format("{0}/{1}", Utility.ImgUrl, od.Product.getDefaultImageUrl),
                    ProductImageResized = string.Format("{0}/{1}", Utils.Utility.ImgUrl, od.Product.getDefaultResizedImageUrl)
                }).ToList()
            };

            SetStockAvailability(cartDetails, shoppingCartViewModel);

            return shoppingCartViewModel;
        }

        private void SetStockAvailability(List<OrderDetail> cartDetails, ShoppingCartViewModel shoppingCartViewModel)
        {
            List<int> productsIds = new List<int>();
            List<string> disponibilidad = new List<string>();

            foreach (var item in cartDetails)
            {
                productsIds.Add(item.ProductId);
            }

            //var stocks = modelEntities.Products.Where(x => x.Active && productsIds.Contains(x.ProductId)).OrderByDescending(x => x.ProductId).Select(q => q.StockVirtual).ToList();
            var stocks = modelEntities.Stocks
                .Where
                (x => x.Product.Active && productsIds.Contains(x.ProductId))
                .OrderByDescending(x => x.ProductId).ToList();

            for (int i = 0; i < cartDetails.Count(); i++)
            {
                var item_stock = stocks.FirstOrDefault(x =>
                    x.ProductId == cartDetails[i].ProductId &&
                    x.ColorId == cartDetails[i].ColorId &&
                    x.SizeId == cartDetails[i].SizeId);
                if (item_stock != null)
                {
                    if (cartDetails[i].Quantity > item_stock.StockVirtual)
                    {
                        if (item_stock.StockVirtual > 0)
                            disponibilidad.Add("Parcial");
                        else
                            disponibilidad.Add("No disponible");
                    }
                    else
                    {
                        disponibilidad.Add("Disponible");
                    }
                }
                else
                {
                    disponibilidad.Add("No disponible");
                }

            }
            //for (int i = 0; i < stocks.Count; i++)
            //{
            //    if (cartDetails[i].Quantity > stocks[i])
            //    {
            //        if (stocks[i] > 0)
            //            disponibilidad.Add("Parcial");
            //        else
            //            disponibilidad.Add("No disponible");
            //    }
            //    else
            //    {
            //        disponibilidad.Add("Disponible");
            //    }
            //}

            int t = 0;
            foreach (var item in shoppingCartViewModel.OrderDetails)
            {
                item.Stock = disponibilidad[t];
                t++;
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult GetUser()
        {
            string userName = string.Empty;

            if ((User.Identity != null) && (User.Identity.IsAuthenticated) && User.Identity is CustomIdentity)
                userName = "¡Bienvenido/a " + ((CustomIdentity)(User.Identity)).FullName + "!";
            else
                userName = string.Empty;

            var results = new { Value = userName };
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult GetMinimumOrder()
        {
            Parameter param;            
            param = modelEntities.Parameters.FirstOrDefault(x => x.Name == "MinimumOrderRetail");

            decimal value = 0;
            if (param != null)
            {
                value = param.Value.Value;
            }
            var results = new { Value = value };
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual bool IsCartEmpty()
        {
            var shoppingCartManager = new ShoppingCartManager();
            var cartGuid = ShoppingCartHelper.GetGuid();

            return _shoppingCartManager.IsCartEmptyBy(cartGuid, User.Identity.Name);
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult ItemsQuantity()
        {
            var shoppingCartManager = new ShoppingCartManager();
            var cartGuid = ShoppingCartHelper.GetGuid();

            List<OrderDetail> cartDetails = _shoppingCartManager.GetShoppingCart(cartGuid, User.Identity.IsAuthenticated ? User.Identity.Name : null, this.StoreType);

            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                OrderDetails = cartDetails.Select(od => new OrderDetailViewModel()
                {
                    Quantity = od.Quantity,
                    Price = od.Product.CustomerPrice,
                    Description = od.Product.Description,
                    ProductId = od.ProductId,
                    OrderDetailId = od.OrderDetailId,
                    Title = od.Product.Title,
                    ProductBrand = od.Product.Brand != null ? od.Product.Brand.Name : string.Empty,
                    ProductImage = string.Format("{0}/{1}", Utils.Utility.ImgUrl, od.Product.getDefaultImageUrl),
                    ProductImageResized = string.Format("{0}/{1}", Utils.Utility.ImgUrl, od.Product.getDefaultResizedImageUrl),
                }).ToList(),
                Guid = cartGuid.ToString()
            };

            return Json(shoppingCartViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult WishlistItemsQuantity()
        {
            var shoppingCartManager = new ShoppingCartManager();
            var cartGuid = ShoppingCartHelper.GetGuid();

            //var cartDetails =  shoppingCartManager.GetShoppingCart(cartGuid);
            var wishlistItemCount = 0;
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                wishlistItemCount = _shoppingCartManager.GetWishlistCount(cartGuid, User.Identity.Name);
            }
            else
                wishlistItemCount = _shoppingCartManager.GetWishlistCount(cartGuid, string.Empty);

            return Json(new { Count = wishlistItemCount }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult AsyncCities(string ProvinceId)
        {
            var provinceId = Convert.ToInt32(ProvinceId);
            var cities = modelEntities.Cities.Where(x => x.ProvinceId == provinceId && !x.Deleted.HasValue).ToList().OrderBy(x => x.Name).Select(a => new SelectListItem()
            {
                Text = a.Name,
                Value = a.CityId.ToString(),
            });

            return Json(cities);
        }

        [AllowAnonymous]
        public JsonResult ValidateCUIT(string CUIT)
        {
            FiscalService _fiscal = new FiscalService();
            var valid = _fiscal.ValidateCUIT(CUIT);

            return Json(new { Valid = valid }, JsonRequestBehavior.AllowGet);
        }
    }
}