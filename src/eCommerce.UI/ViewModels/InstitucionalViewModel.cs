﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using System.Collections.Generic;

namespace eCommerce.UI.ViewModels
{
    public class InstitucionalViewModel
    {
        public string Instagram_Account { get; set; }
        public string Facebook_Account { get; set; }
        public string Company { get; set; }
    }

}