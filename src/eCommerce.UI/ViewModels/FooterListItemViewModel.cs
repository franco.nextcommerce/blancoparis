﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model;

namespace eCommerce.UI.ViewModels
{
    public class FooterListItemViewModel
    {
        public string Title { get; set; }
        public List<Product> Products = new List<Product>();
    }
}