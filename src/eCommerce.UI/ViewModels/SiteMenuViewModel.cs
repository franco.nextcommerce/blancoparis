﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model;

namespace eCommerce.UI.ViewModels
{
    public class SiteMenuViewModel
    {
        //public IList<Category> ParentCategories = new List<Category>();
        //public IList<Category> ChildCategories = new List<Category>();
        public IList<CategoryViewModel> Categories = new List<CategoryViewModel>();
        //public IList<LookbookGroup> Collections = new List<LookbookGroup>();
    }

}