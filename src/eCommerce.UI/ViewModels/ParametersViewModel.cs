﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Security;
using eCommerce.Services.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.UI.ViewModels
{
    public class ParametersViewModel
    {
        public string Url { get; set; }
        public string Facebook_Account { get; set; }
        public string Instagram_Account { get; set; }
        public string Whatsapp { get; set; }
        public string CompanyNumbre { get; set; }
        public string MinimumOrder { get; set; }
        public string GoogleAnalytics { get; set; }
    }


}