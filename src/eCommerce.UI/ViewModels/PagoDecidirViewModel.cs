﻿using eCommerce.Services.Model;
using System.Collections.Generic;

namespace eCommerce.UI.ViewModels
{
    public class PagoDecidirViewModel
    {
        public int orderId { get; set; }
        public decimal Total { get; set; }
        public string CustomerName { get; set; }
        public List<Bank> BankList { get; set; }
    }
}