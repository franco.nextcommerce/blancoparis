﻿using System.Collections.Generic;
using System.Web;
using System.Linq;

namespace eCommerce.UI.ViewModels
{
    public class SelfSuggestSearcherViewModel
    {
        public class SuggestedProduct
        {
            public int ProductId { get; set; }
            public string ProductCode { get; set; }
            public string ProductTitle { get; set; }
            public string ImageUrl { get; set; }
            public string ProductLink { get; set; }
        }

        public class SearchSuggestion
        {
            public List<SuggestedProduct> Products { get; set; }
        }
    }
}