﻿using System.Collections.Generic;
using System.Web;
using System.Linq;
using eCommerce.Services.Model;

namespace eCommerce.UI.ViewModels
{
    public class ColorViewModel
    {
        DbModelEntities modelEntities = new DbModelEntities();
        public string colorName { get; set; }
        public int colorId { get; set; }
        public string imageName { get; set; }
        //public string getImageUrl {
        //    get
        //    {
        //        var imgs = modelEntities.Images.Where(x => x.ColorId == this.colorId).ToList();
        //        return (imgs.Count > 0 && imgs.Any(i => i.Default)) ?
        //            string.Format("products/resized/{0}/{1}",
        //                this.colorId,
        //                imgs.FirstOrDefault(i => i.Default).ImageName) : "products/DefaultProductImage.jpeg";
        //    }
        //}
    }
}