﻿using System.Collections.Generic;
using System.Web;
using System.Linq;

namespace eCommerce.UI.ViewModels
{
    public class CatalogoFilter
    {
        public CatalogoFilter()
        {            
            var queryString = HttpContext.Current.Request.QueryString;

            Propiedades = queryString.AllKeys.Where(k => k.StartsWith("propiedad_"))
                                .ToDictionary(k => k.Substring(k.IndexOf("_")+1), k => HttpContext.Current.Request.QueryString.GetValues(k).ToList());

            PageSize = 24;
        }

        public int PageSize { get; set; }

        public int? Pagina { get; set; }

        //public string Rubro { get; set; }

        //public string Categoria { get; set; }

        public decimal? PrecioDesde { get; set; }

        public decimal? PrecioHasta { get; set; }

        public string[] Marcas { get; set; }
        public string[] Talles { get; set; }
        public string[] Colores { get; set; }

        public string Orden { get; set; }

        public string Vista { get; set; }

        public string Atributo { get; set; }

        public string Term { get; set; }

        public Dictionary<string, List<string>> Propiedades { get; set; }

    }
}