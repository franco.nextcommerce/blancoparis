﻿using System.Collections.Generic;
using System.Configuration;
using eCommerce.Services.Model;

namespace eCommerce.UI.ViewModels
{
    public class CategoriesViewModel
    {
        private const string CategoryItemDirectory = "Categories";

        public CategoriesViewModel()
        {
            var BackOfficeUrl = ConfigurationManager.AppSettings["BackOfficeUrl"];
            var imagePath = ConfigurationManager.AppSettings["ImagesUrlPath"];

            _BackOfficeUrl = string.Format("{0}{1}", BackOfficeUrl, imagePath );
        }

        public Dictionary<Category, IEnumerable<Category>> Categories { get; set; }
        private readonly string _BackOfficeUrl;

        public string ImageUrl(int categoryId, string imageName)
        {
            return _BackOfficeUrl
                .Replace("{Item}", CategoryItemDirectory)
                .Replace("{CategoryId}", categoryId.ToString())
                .Replace("{ImageName}", imageName);
        }
    }
}