﻿using eCommerce.Services.Model;
using System.Collections.Generic;

namespace eCommerce.UI.ViewModels
{
    public class ProductDetailViewModel
    {
        public Product Product { get; set; }

        public int ColorId { get; set; }

        public List<Category> Breadcrumb { get; set; }

        public Coupon ActiveCoupon { get; set; }

        public List<Color> ProductColors = new List<Color>();

        public List<Size> ProductColorSizes = new List<Size>();

        public decimal? SpecialPrice { get; set; }

        public string BackOfficeUrl { get; set; }
        
        public bool ShowMaxDiscount
        {
            get
            {
                return this.ActiveCoupon != null && this.ActiveCoupon.Percentage > 0 ? true : false;
            }
        }
    }
}