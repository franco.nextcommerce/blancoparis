﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Security;
using eCommerce.Services.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.UI.ViewModels
{
    public class BreadCrumbViewModel
    {
        //public Category Category { get; set; }
        public string CategoryName { get; set; }
        public string CategorySeoUrl { get; set; }
        public Shop.ProductViewModel Product { get; set;}
        //public bool isBuscar{ get; set; }
    }


}