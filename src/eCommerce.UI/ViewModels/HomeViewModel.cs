﻿using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using System.Collections.Generic;

namespace eCommerce.UI.ViewModels
{
    public class HomeViewModel
    {
        public string ImageTemplateUrl;
        //public List<Product> Products = new List<Product>();
         
        public List<Banner> BannersSlider { get; set; }
        public List<Banner> BannersSliderMobile { get; set; }
        public List<Banner> BannersCategories { get; set; }
        public List<Banner> BannersCategoriesMobile { get; set; }
        public List<Banner> BannersCategoriesTwo { get; set; }
        public List<Banner> BannersCategoriesMobileTwo { get; set; }
        //public Banner BannerVideo { get; set; }
        //public Banner BannerLeft { get; set; }
        //public Banner BannerRight { get; set; }
        //public Banner BannerFacebook { get; set; }

        //public List<Lookbook> LookbookItems { get; set; }
        public List<Product> HomeProducts { get; set; }

        public UserTypeEnum StoreType { get; set; }
    }

}