﻿
using eCommerce.Services.Model;
using System;

namespace eCommerce.ViewModels
{
    public class ProformViewModel
    {
        public Order order { get; set; }
        public bool respuesta { get; set; }
        public Guid guid { get; set; }
    }
}