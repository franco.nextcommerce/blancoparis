﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model;

namespace eCommerce.UI.ViewModels
{
    public class StoresViewModel
    {
        public Province Province { get; set; }
        public List<Store> Stores { get; set; }
    }
}