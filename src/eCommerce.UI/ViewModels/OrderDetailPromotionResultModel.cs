﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model;

namespace eCommerce.UI.ViewModels
{
    public class OrderDetailPromotionResultModel
    {
        public decimal OrderDiscount { get; set; }
        public List<ResultModel> results { get; set; }
    }

    public class ResultModel
    {
        public int OrderDetailId { get; set; }
        public decimal Price { get; set; }
        public string Message { get; set; }
    }

}