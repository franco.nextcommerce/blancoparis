﻿using System.Collections.Generic;
using eCommerce.Services.Model;
using System.Web;
using System.Linq;
using System.Text;

namespace eCommerce.UI.ViewModels
{
    public class CatalogoViewModel
    {
        public string HeadingName { get; set; }
        public string HeadingFormatedName { get; set; }
        public int CategoryId { get; set; }
        public int ParentCategoryId { get; set; }

        public string CategoryName { get; set; }

        public CatalogoFilter Filter { get; set; }

        //public IList<ProductViewModel> Products { get; set; }

        public IList<SizeFilter> Sizes { get; set; }
        public IList<ColorFilter> Colors{ get; set; }

        public string BackOfficeUrl { get; set; }

        public Coupon ActiveCoupon { get; set; }

        public int Count { get; set; }

        //public int PageCount { get { return Count / CatalogoFilter.PageSize; } }

        public string GetPageUrl(int page)
        {
            if (Filter == null) return null;

            var keys = HttpContext.Current.Request.QueryString.AllKeys.ToList().Where(k => !string.Equals(k, "pagina")).ToList();

            var parts = new List<string>();

            keys.ForEach(k =>
            {
                parts.Add(string.Format("{0}={1}", k, HttpContext.Current.Request.QueryString[k]));
            });

            parts.Add(string.Format("pagina={0}", page));

            return string.Format("{0}?{1}", HttpContext.Current.Request.Url.AbsolutePath, string.Join("&", parts));
        }

        public string GetOrderUrl(string order)
        {
            if (Filter == null) return null;
            var keys = HttpContext.Current.Request.QueryString.AllKeys.ToList().Where(k => !string.Equals(k, "orden") && !string.Equals(k, "pagina")).ToList();
            var parts = new List<string>();

            keys.ForEach(k =>
            {
                parts.Add(string.Format("{0}={1}", k, HttpContext.Current.Request.QueryString[k]));
            });

            parts.Add(string.Format("orden={0}", order));

            return string.Format("{0}?{1}", HttpContext.Current.Request.Url.AbsolutePath, string.Join("&", parts));
        }

        public string GetViewUrl(string view)
        {
            if (Filter == null) return null;
            var keys = HttpContext.Current.Request.QueryString.AllKeys.ToList().Where(k => !string.Equals(k, "vista") ).ToList();
            var parts = new List<string>();

            keys.ForEach(k => {
                parts.Add(string.Format("{0}={1}", k, HttpContext.Current.Request.QueryString[k]));
            });

            parts.Add(string.Format("vista={0}", view));


            return string.Format("{0}?{1}", HttpContext.Current.Request.Url.AbsolutePath, string.Join("&", parts));
        }

        public string GetCategoryUrl(string category, string attribute = null)
        {
            if (Filter == null) return null;
            var keys = HttpContext.Current.Request.QueryString.AllKeys.ToList().Where(k => string.Equals(k, "rubro") || string.Equals(k, "vista")).ToList();
            var parts = new List<string>();

            keys.ForEach(k => {
                parts.Add(string.Format("{0}={1}", k, HttpContext.Current.Request.QueryString[k]));
            });

            if (!string.IsNullOrEmpty(category))
            {
                parts.Add(string.Format("categoria={0}", category));
            }

            var result = string.Format("{0}?{1}", HttpContext.Current.Request.Url.AbsolutePath, string.Join("&", parts));

            return result;
        }

        public bool ShowMaxDiscount
        {
            get
            {
                return this.ActiveCoupon != null && this.ActiveCoupon.Percentage > 0 ? true : false;
            }
        }
    }

    public class SizeFilter
    {
        public string SizeName { get; set; }
        public int? DisplayOrder { get; set; }
        public int Count { get; set; }
    }

    public class ColorFilter
    {
        public int ColorId { get; set; }
        public string ColorName { get; set; }

        public int Count { get; set; }
    }
}