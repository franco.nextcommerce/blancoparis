﻿using System.Collections.Generic;
using eCommerce.Services.Model;
using System.Web;
using System.Linq;
using System.Text;

namespace eCommerce.UI.ViewModels
{
    public class DecidirErrorResponseModel
    {
        public int id { get; set; }
        public string site_transaction_id { get; set; }
        public int payment_method_id { get; set; }
        public string card_brand { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string status { get; set; }
        public StatusDetailsModel status_details { get; set; }
        public string date { get; set; }
        public string customer { get; set; }
        public string bin { get; set; }
        public int installments { get; set; }
        public string first_installment_expiration_date { get; set; }
        public string payment_type { get; set; }
        public List<string> sub_payments { get; set; }
        public string site_id { get; set; }
        public FraudDetectionModel fraud_detection { get; set; }
        public string aggregate_data { get; set; }
        public string establishment_name { get; set; }
        public string spv { get; set; }
        public string confirmed { get; set; }
        public string pan { get; set; }
        public string customer_token { get; set; }
        public string card_data { get; set; }
    }

    public class StatusDetailsModel
    {
        public string ticket { get; set; }
        public string card_authorization_code { get; set; }
        public string address_validation_code { get; set; }
        public Error error { get; set; }
    }

    public class FraudDetectionModel
    {
        public string status { get; set; }
    }

    public class Error
    {
        public string type { get; set; }
        public Reason reason { get; set; }
    }

    public class Reason
    {
        public int id { get; set; }
        public string description { get; set; }
        public string additional_description { get; set; }
    }
}
