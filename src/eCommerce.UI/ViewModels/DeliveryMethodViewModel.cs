﻿using eCommerce.ViewModels;
using eCommerce.Services.Model.Enums;
using System.Web.Mvc;

namespace eCommerce.UI.ViewModels
{
    public class DeliveryMethodViewModel
    {
        public int DeliveryMethodId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ExcludedPaymetnMethods { get; set; }
        public decimal? Cost { get; set; }
        public bool? RequiresData { get; set; }
        public bool IsTransport { get; set; }
        public bool IsLocal { get; set; }
        public bool ShowLocales { get; set; }
        public bool IsProvince { get; set; }
    }

}