﻿using eCommerce.Services.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.UI.ViewModels.Shop
{
    public class ProductColorViewModel
    {
        public int ColorId { get; set; }
        public string Description { get; set; }
        public string ColorThumb { get; set; }
        public bool Enabled { get; set; }
    }
    public class ProductImageViewModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int? ColorId { get; set; }
        public string ImageName { get; set; }
        public int Orientation { get; set; }
        public bool Default { get; set; }

        public string Url
        {
            get
            {
                return $"{Utility.ImgUrl}/Products/{ProductId}/{ImageName}";
            }
        }

        public string ResizedUrl
        {
            get
            {
                return $"{Utility.ImgUrl}/Products/Resized/{ProductId}/{ImageName}";
            }
        }

    }
}