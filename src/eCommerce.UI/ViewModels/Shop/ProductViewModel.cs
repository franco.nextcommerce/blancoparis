﻿using eCommerce.Services.Model;
using eCommerce.Services.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model.Enums;

namespace eCommerce.UI.ViewModels.Shop
{

    public class ShopViewModel
    {
        public bool hasFilter { get; set; }
        public List<CategoryViewModel> categoryList { get; set; }
        public List<ProductViewModel> productList { get; set; }
        public List<Color> colorList { get; set; }
        public List<Size> sizeList { get; set; }
        //public List<Category> categoryListToFilter { get; set; }
        public bool isBuscar { get; set; }
        public int PageSize { get; set; }
        public bool LoadMoreProducts { get; set; }
        public int? ColorId { get; set; }
        public int? Sort { get; set; }
        public string CategoryName { get; set; }
        public string CategorySeoUrl { get; set; }

        public int TotalProductCount { get; set; }

        public Category Category { get; set; }
        
        public int MaxProductPrice { get; set; }
    }

    public class ProductViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string Cloth { get; set; }
        public decimal? WholesalePrice { get; set; }
        public decimal? OldWholesalePrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? OldRetailPrice { get; set; }
        public decimal? Price { get; set; }
        public decimal? OldPrice { get; set; }
        public string Description { get; set; }
        public List<ProductImageViewModel> Images { get; set; }
        public UserTypeEnum StoreType { get; set; }
        public Category category { get; set; }
        public List<Color> ColorList { get; set; }
        public List<Size> SizeList { get; set; }
        public List<ProductColorViewModel> ColorList_Detail { get; set; }
        public int SizeId { get; set; }
        public int SizeTableId { get; set; }
        public int? ShopImageSize { get; set; }
        public string CategoryName { get; set; }
        public string SeoUrl { get; set; }
        public string CategorySeoUrl { get; set; }
        public bool IsInWishlist { get; set; }
        public bool OnSale { get; set; }
        public string ShareUrl { get; set; }
        public bool ShowQuickView { get; set; }
        public ProductViewModel NextProduct { get; set; }
        public ProductViewModel PreviousProduct { get; set; }
        public bool SoldOut { get; set; }
        public string ShippingInformation { get; set; }
        public string ExchangeAndRefundsInformation { get; set; }
        public int? CategoryId { get; set; }
        public int? ParentCategoryId { get; set; }
        public string SizeTableUrl { get; set; }

        //public string Cloth { get; set; }

        public string Pay_x_get_y_promotion { get; set; }

        public string Url
        {
            get
            {
                return $"/{Utility.CataloguePrefix}/{CategorySeoUrl}/{SeoUrl}";
            }
        }

        public string CategoryUrl
        {
            get
            {
                return $"/{Utility.CataloguePrefix}/{CategorySeoUrl}";
            }
        }

        public decimal? PriceDifference
        {
            get
            {
                if (!OldPrice.HasValue || OldPrice.Value <= 0) return null;
                return Math.Round((OldPrice.Value - Price.Value) / OldPrice.Value * 100, 0);

            }
        }

        public string getResizedImageUrlByColor(int colorId)
        {
            var img = this.Images.FirstOrDefault(x => x.ProductId == this.Id && x.ColorId == colorId);
            return (img != null) ?
                string.Format("products/resized/{0}/{1}",
                    this.Id,
                    img.ImageName) : "products/DefaultProductImage.jpeg";
        }

        public string getSecondaryDefaultResizedImageUrl
        {
            get
            {
                var imgs = this.Images.Where(x => x.ProductId == this.Id).ToList();
                return (imgs.Count > 0 && imgs.Any(i => i.Default)) ?
                    string.Format("products/resized/{0}/{1}",
                        this.Id,
                        imgs.FirstOrDefault(i => !i.Default).ImageName) : "products/DefaultProductImage.jpeg";
            }
        }

        public string getDefaultResizedImageUrl
        {
            get
            {
                var imgs = this.Images.Where(x => x.ProductId == this.Id).ToList();
                return (imgs.Count > 0 && imgs.Any(i => i.Default)) ?
                    string.Format("products/resized/{0}/{1}",
                        this.Id,
                        imgs.FirstOrDefault(i => i.Default).ImageName) : "products/DefaultProductImage.jpeg";
            }
        }

        public string getDefaultImageUrl
        {
            get
            {
                var imgs = this.Images.Where(x => x.ProductId == this.Id).ToList();
                return (imgs.Count > 0 && imgs.Any(i => i.Default)) ?
                    string.Format("products/{0}/{1}",
                        this.Id,
                        imgs.FirstOrDefault(i => i.Default).ImageName)
                        :
                        string.Format("products/{0}/{1}",
                        this.Id,
                        imgs.FirstOrDefault().ImageName);
            }
        }

        public bool IsNew { get; internal set; }
    }
}