﻿//using eCommerce.Services.Model;
//using eCommerce.Services.Utils;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace eCommerce.UI.ViewModels.Shop
//{
//    public class ShopFilterViewModel
//    {
//        public ShopFilterViewModel()
//        {
//            var query = HttpContext.Current.Request.QueryString;

//            Filtros = query.AllKeys.ToDictionary(x => x, y => query[y]);

//            Sort = query.AllKeys.Contains("sort") ? int.Parse(Filtros["sort"]) : (int?)null;
//            Color = query.AllKeys.Contains("color") ? int.Parse(Filtros["color"]) : (int?)null;
//            Size = query.AllKeys.Contains("size") ? int.Parse(Filtros["size"]) : (int?)null;

//            var priceStart = (decimal)0;
//            if(query.AllKeys.Contains("priceStart") && decimal.TryParse(Filtros["priceStart"], out priceStart))
//            {
//                PriceStart = priceStart > 0 ? priceStart : (decimal?)null;
//            }

//            var priceEnd = (decimal)0;
//            if (query.AllKeys.Contains("priceEnd") && decimal.TryParse(Filtros["priceEnd"], out priceEnd))
//            {
//                PriceEnd = priceEnd > 0 ? priceEnd : (decimal?)null;
//            }

//            ReferenceCategorySeoUrl = query.AllKeys.Contains("ref-category") ? Filtros["ref-category"] : string.Empty;

//            PageSize = 48;
//        }

//        private Dictionary<string, string> Filtros { get; set; }

//        public string ReferenceCategorySeoUrl { get; set; }

//        public int? Sort { get; set; }
//        public int? Color { get; set; }
//        public int? Size { get; set; }
//        public int PageSize { get; set; }
//        public decimal? PriceStart { get; set; }
//        public decimal? PriceEnd { get; set; }
//    }
//}

using eCommerce.Services.Model;
using eCommerce.Services.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.UI.ViewModels.Shop
{
    public class ShopFilterViewModel
    {
        public ShopFilterViewModel()
        {
            var query = HttpContext.Current.Request.QueryString;

            Filtros = query.AllKeys.ToDictionary(x => x, y => query[y]);

            Sort = query.AllKeys.Contains("sort") ? int.Parse(Filtros["sort"]) : (int?)null;
            Color = query.AllKeys.Contains("color") ? Filtros["color"] : string.Empty;
            Size = query.AllKeys.Contains("talle") ? Filtros["talle"] : string.Empty;
            Desde = query.AllKeys.Contains("desde") ? decimal.Parse(Filtros["desde"]) : (decimal?)null;
            Hasta = query.AllKeys.Contains("hasta") ? decimal.Parse(Filtros["hasta"]) : (decimal?)null;
            Sort = query.AllKeys.Contains("sort") ? int.Parse(Filtros["sort"]) : (int?)null;
            ReferenceCategorySeoUrl = query.AllKeys.Contains("ref-category") ? Filtros["ref-category"] : string.Empty;

            PageSize = 48;
        }

        private Dictionary<string, string> Filtros { get; set; }

        public string ReferenceCategorySeoUrl { get; set; }

        public int? Sort { get; set; }
        //public int? Color { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public decimal? Desde { get; set; }
        public decimal? Hasta { get; set; }
        public int PageSize { get; set; }
    }
}