﻿using eCommerce.Services.Model;
using eCommerce.Services.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.UI.ViewModels.Shop
{
    public class ProductQuickViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public decimal? OldPrice { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string ShareUrl { get; set; }
        public List<ProductImageViewModel> Images { get; set; }
        public Category category { get; set; }
        public List<Color> ColorList { get; set; }
        public List<Size> SizeList { get; set; }

        public string SizeTableUrl { get; set; }
        public string Cloth { get; set; }

        public decimal? PriceDifference
        {
            get
            {
                if (!OldPrice.HasValue) return null;
                return Math.Round((OldPrice.Value - Price) / OldPrice.Value * 100, 0);

            }
        }
    }
}