﻿using eCommerce.ViewModels;
using eCommerce.Services.Model.Enums;
using System.Web.Mvc;

namespace eCommerce.UI.ViewModels
{
    public class PaymentMethodViewModel
    {
        public int PaymentMethodId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Cost { get; set; }
        public bool? RequiresData { get; set; }
    }

}