﻿using System;
using System.Collections.Generic;
using eCommerce.Services.Model;
using eCommerce.UI.ViewModels;
using eCommerce.Services.Model.Enums;
using System.Web.Mvc;
using System.Linq;
using eCommerce.Services.Utils;
using eCommerce.Services.Security;
using System.Web;

namespace eCommerce.UI.ViewModels
{
    public class ShoppingCartViewModel
    {
        private DbModelEntities modelEntities = new DbModelEntities();
        public ShoppingCartViewModel()
        {
            OrderDetails = new List<OrderDetailViewModel>();
            ShippingCost = 0;
            ShippingDays = 0;
            Discount = 0;
        }

        public List<OrderDetailViewModel> OrderDetails { get; set; }
        public List<DeliveryMethodViewModel> DeliveryMethods { get; set; }
        public List<PaymentMethodViewModel> PaymentMethods { get; set; }

        public UserTypeEnum StoreType { get; set; }

        public Order Order { get; set; }
        public string Guid { get; set; }
        public ExtraInfoViewModel ExtraInfo { get; set; }
        public List<Bank> BankList { get; set; }
        public Customer Customer { get; set; }
        public decimal ShippingCost { get; set; }
        public int ShippingDays { get; set; }
        public decimal Discount { get; set; }
        public string CouponCode { get; set; }
        public int OrderId { get; set; }
        public string DeliveryAddress { get; set; }
        public bool FreeShipping { get; set; }

        public bool IsMercadoPagoEnabled
        {
            get
            {
                if (this.Customer != null)
                    return (this.Customer.UserType == (int)UserTypeEnum.Minorista);
                else
                    return false;
            }
        }

        public decimal TotalItemsCount
        {
            get
            {
                return OrderDetails.Sum(o => o.Quantity);
            }
        }

        public decimal TotalOrderDetails
        {
            get
            {
                return OrderDetails.Sum(o => o.Price * o.Quantity);
            }
        }

        public decimal IvaAmount
        {
            get
            {
                return 0;

                /*var userType = HttpContext.Current.User.Identity.IsAuthenticated
                    ? ((CustomIdentity)HttpContext.Current.User.Identity).CustomerPriceList
                    : (int)PriceListEnum.Minorista;
                if (userType != (int)PriceListEnum.Mayorista) return 0;
                return TotalWithDiscount * Utility.IVA_MULTIPLIER / 100;*/
            }
        }

        public bool ApplyIva
        {
            get
            {
                return false;

                /*var userType = HttpContext.Current.User.Identity.IsAuthenticated
                    ? ((CustomIdentity)HttpContext.Current.User.Identity).CustomerPriceList
                    : (int)PriceListEnum.Minorista;
                return userType == (int)PriceListEnum.Mayorista;*/
            }
        }

        public decimal DiscountAmount
        {
            get
            {
                if (Discount == 0) return 0;
                return TotalOrderDetails * Discount / 100;
            }
        }

        public decimal TotalWithDiscount
        {
            get
            {
                return (this.TotalOrderDetails - this.DiscountAmount);
            }
        }

        public decimal Total
        {
            get
            {
                return this.TotalWithDiscount + this.IvaAmount + this.ShippingCost;
            }
        }

        public string DeliveryMethod { get; set; }
        public DeliveryMethodEnum DeliveryMethodType
        {
            get
            {
                switch (DeliveryMethod)
                {
                    case "Retiro":
                        return DeliveryMethodEnum.RetiraEnCentroDeEntrega;
                    case "OCA_PaP":
                        return DeliveryMethodEnum.OcaPaP;
                    case "OCA_PaS":
                        return DeliveryMethodEnum.OcaPaS;
                    case "Moto":
                        return DeliveryMethodEnum.MotoEntrega;
                    case "Transporte":
                        return DeliveryMethodEnum.Transporte;
                    case "AConvenir":
                        return DeliveryMethodEnum.AConvenir;
                    default:
                        return DeliveryMethodEnum.RetiraEnCentroDeEntrega;
                }
            }
        }

        public string PaymentMethod { get; set; }
        public int PaymentMethodId { get; set; }

        public SelectList Provinces { get; set; }
        public SelectList Cities { get; set; }
        public string OcaCenter { get; set; }
        public string TermsAndConditions { get; internal set; }
    }

    public class ShoppingCartModal
    {
        public List<OrderDetailModal> OrderDetails { get; set; }
        public decimal Total { get; set; }
        public int TotalQuantity { get; set; }
        public bool IsEmpty { get; set; }
    }

    public class OcaCentroDeImposicion
    {
        public int Id { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string CodigoPostal { get; set; }
        public string Telefono { get; set; }
        public string Descripcion { get; set; }
        public string Provincia { get; set; }
        public string Localidad { get; set; }

          public string Nombre => $"{Calle} {Numero} - {Descripcion} - {Localidad} - CP: {CodigoPostal}";
    }

    public class ExtraInfoViewModel
    {
        public Customer Customer { get; set; }
        public List<Store> WholesalerStores { get; set; }
        public List<Store> RetailerStores { get; set; }
        public int DestinationShipmentId { get; set; }
    }

    public class OdToAdd
    {
        public int sizeId { get; set; }
        public int colorId { get; set; }
        public int quantity { get; set; }
    }
}