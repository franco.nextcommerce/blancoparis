﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model;

namespace eCommerce.UI.ViewModels
{
    public class CurrentSelection
    {
        public CurrentSelection()
        {
            PageSize = 18;
        }

        public int MenuIndex;
        public List<Product> Products = new List<Product>();
        public Product ProductSelected;

        //public List<Stock> ProductStock = new List<Stock>();

        public List<Category> CategoriesMenu = new List<Category>();
        public Category CurrentCategory;
        public Category SubCategory;

        public CategoryTreeDataHelper CategoryTreeData = new CategoryTreeDataHelper();

        public List<Category> BrebreadCrumbsHierarchy = new List<Category>();
        
        public int PageSize;
        
        public int PageNumber;
        public string Action;
    }

    public class CategoryTreeDataHelper
    {
        public Category ParentCategory { get; set; }
        public int ParentProductsCount { get; set; }
        public Dictionary<Category, int> ChildCategories = new Dictionary<Category, int>();
    }
}