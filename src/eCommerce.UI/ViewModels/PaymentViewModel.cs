﻿using eCommerce.Services.Model;
using System.Collections.Generic;

namespace eCommerce.UI.ViewModels
{
    public class PaymentViewModel
    {
        public int operationId { get; set; }
        public string CustomerName { get; set; }
        public string Installments { get; set; }
        public decimal OrderTotal { get; set; }
    }
}