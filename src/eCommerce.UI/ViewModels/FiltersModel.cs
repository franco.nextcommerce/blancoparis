﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCommerce.Services.Model;

namespace eCommerce.ViewModels
{
    public class FiltersModel
    {
        /*public decimal Price1 { get; set; }
        public decimal Price2 { get; set; }
        public List<int> Colors { get; set; }
        public List<int> Sizes { get; set; }
        public string Sort { get; set; }
        public int? Count { get; set; }

        public FiltersModel()
        { 
        }

        public FiltersModel(FormCollection collection)
        {
            if (collection["submitValue"] == "1")
            {
                if (collection["price1"] != null)
                {
                    decimal price;
                    if (Decimal.TryParse(collection["price1"], out price))
                    {
                        Price1 = price;
                    }
                }


                if (collection["price2"] != null)
                {
                    decimal price;
                    if (Decimal.TryParse(collection["price2"], out price))
                    {
                        Price2 = price;
                    }
                }

                if (collection["colors[]"] != null)
                {
                    Colors = new List<int>();

                    foreach (var colorId in collection["colors[]"].Split(','))
                    {
                        Colors.Add(Convert.ToInt32(colorId));
                    }
                }

                if (collection["sizes[]"] != null)
                {
                    Sizes = new List<int>();

                    foreach (var sizeId in collection["sizes[]"].Split(','))
                    {
                        Sizes.Add(Convert.ToInt32(sizeId));
                    }
                }

                if (collection["sort"] != null)
                {
                    Sort = collection["sort"];
                }

                if (collection["count"] != null)
                {
                    Count = Convert.ToInt32(collection["count"]);
                }
            }
        }

        public List<Product> Apply(List<Product> products, bool isWholeSaler, bool isDistributor)
        {

            // filter by price 
            if (Price1 > 0 || Price2 > 0)
            {
                if (Price1 > 0)
                {
                    products =
                        products.Where(
                            x => (isWholeSaler && x.WholesalePrice > Price1) || (isDistributor && x.DistributorPrice > Price1) || (!isWholeSaler && !isDistributor && x.RetailPrice > Price1)).ToList();
                }

                if (Price2 > 0)
                {
                    products =
                        products.Where(
                            x => (isWholeSaler && x.WholesalePrice < Price2) || (isDistributor && x.DistributorPrice < Price2) || (!isWholeSaler && !isDistributor && x.RetailPrice < Price2)).ToList();
                }
                
            }

            // filter by colors
            if (Colors != null && Colors.Count > 0)
            {
                modelEntities modelEntities = new modelEntities();
                var productIds = (from stockColor in modelEntities.Stocks
                                     where stockColor.VirtualQuantity > 0 && stockColor.Active && Colors.Contains(stockColor.ColorId) 
                                     select stockColor.ProductId).Distinct().ToList();

                products = products.Where(x => productIds.Contains(x.ProductId)).ToList();
            }

            // filter by sizes
            if (Sizes != null && Sizes.Count > 0)
            {
                modelEntities modelEntities = new modelEntities();
                var productIds = (from stockColor in modelEntities.Stocks
                                  where stockColor.VirtualQuantity > 0 && stockColor.Active && Sizes.Contains(stockColor.SizeId)
                                  select stockColor.ProductId).Distinct().ToList();

                products = products.Where(x => productIds.Contains(x.ProductId)).ToList();
            }

            return products;
        }

        public List<Product> SortProducts(List<Product> products, bool isWholeSaler, bool isDistributor)
        {
            if (Sort != null)
            {
                if (isWholeSaler)
                {
                    if (Sort == "asc")
                    {
                        products = products.OrderBy(x => x.WholesalePrice).ToList();
                    }
                    else
                    {
                        products = products.OrderByDescending(x => x.WholesalePrice).ToList();
                    }

                }
                else if (isDistributor)
                {
                    if (Sort == "asc")
                    {
                        products = products.OrderBy(x => x.DistributorPrice).ToList();
                    }
                    else
                    {
                        products = products.OrderByDescending(x => x.DistributorPrice).ToList();
                    }
                }
                else
                {
                    if (Sort == "asc")
                    {
                        products = products.OrderBy(x => x.RetailPrice).ToList();
                    }
                    else
                    {
                        products = products.OrderByDescending(x => x.RetailPrice).ToList();
                    }
                }
            }

            /*if (Count.HasValue)
            {
                products = products.Take(Count.Value).ToList();
            }

            return products;
        } 
        */
    }
}