﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model;

namespace eCommerce.UI.ViewModels
{
    public class LandingViewModel
    {
        public string Title { get; set; }
        public string MainTitle { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string MetaDescription { get; set; }
        public List<ProductLanding> Products { get; set; }
        public List<Category> Categories { get; set; }
        public bool IsDefault { get; set; }
    }

    public class ProductLanding
    {

        DbModelEntities modelEntities = new DbModelEntities();
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string SeoUrl { get; set; }
        public string CategorySeoUrl { get; set; }
        public decimal WholesalePrice { get; set; }
        public List<Image> Images { get; set; }
        public Category Category;
        public string getDefaultResizedImageUrl
        {
            get
            {
                var imgs = modelEntities.Images.Where(x => x.ProductId == this.ProductId).ToList();
                return (imgs.Count > 0 && imgs.Any(i => i.Default)) ?
                    string.Format("products/resized/{0}/{1}",
                        this.ProductId,
                        imgs.FirstOrDefault(i => i.Default).ImageName) : "products/DefaultProductImage.jpeg";
            }
        }
    }
}