﻿using eCommerce.Services.Model;
using System.Collections.Generic;

namespace eCommerce.UI.ViewModels
{
    public class Menu
    {
        public List<Category> Categories { get; set; }
    }
}