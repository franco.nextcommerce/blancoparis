﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using eCommerce.Services.Model;

namespace eCommerce.UI.ViewModels
{
    public class CategoryViewModel
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string SeoUrl { get; set; }
        public int? ParentCategoryId { get; set; }
        public bool Active { get; set;}
        public bool Enabled { get; set; }
        public int? DisplayOrder { get; set; }
        public bool ShowInHeader { get; set; }

        public bool MenuClickable { get; set; }
    }
}