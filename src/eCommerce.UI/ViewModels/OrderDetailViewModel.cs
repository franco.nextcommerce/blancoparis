﻿using eCommerce.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.UI.ViewModels
{
    public class OrderDetailViewModel
    {
        public int Quantity { get; set; }
        public string CategorySeoUrl { get; set; }
        public int? OrderId { get; set; }
        public string SeoUrl { get; set; }
        public decimal Price { get; set; }
        public decimal OldPrice { get; set; }
        public string Description { get; set; }
        public int ProductId { get; set; }
        public int SizeId { get; set; }
        public int ColorId { get; set; }
        public string SizeDescription { get; set; }
        public string ColorDescription { get; set; }
        public int OrderDetailId { get; set; }
        public string Title { get; set; }
        public string FormattedTitle { get; set; }
        public string ProductBrand { get; set; }
        public string ProductImage{ get; set; }
        public string ProductImageResized { get; set; }
        public string Stock { get; set; }
        public decimal? MaxDiscount { get; set; }
        public int? CategoryId { get; set; }
        public int? ParentCategoryId { get; set; }

        //para pasar el mensaje de descuento sobre el order detail.
        public string PromotionMessage { get; set; }
    }

    public class OrderDetailModal
    {
        public int OrderDetailId { get; set; }
        public int Quantity { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }
    }
}