﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model;
using eCommerce.Services.Common;

namespace eCommerce.ViewModels
{
    public class MessageViewModel
    {
        public string Message;
        public ActionType ActionType;
        public bool ShowSuccessfulIcon;
    }
}