﻿String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};


function searchProduct(term) {
    if (term == "") {
        toastr.warning("Ingrese una palabra para buscar.");
    }
    else {
        window.location = "/Catalogo/Buscar?term=" + term;
    }
}
