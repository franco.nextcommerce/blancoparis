﻿
function GetSubTotal() {
    var total = 0;

    $(".subtotal").each(function () {
        subTotal = $(this).text().replace(".", "").replace(",", ".").replace("$", "");
        total += parseFloat(subTotal);
    });
    var descuento = 0;
    if ($("#descuentoCupon").val() > 0) {
        descuento = total * $("#descuentoCupon").val() / 100;
        $(".discount").text(descuento).formatCurrency();
    }
    $(".summary-subtotal").text(total).formatCurrency();
    $(".summary-total").text(total - descuento).formatCurrency();

    return total;
}

function handleOnKeyDown(e) {
    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
}

function handleOnChange(orderDetailId) {
    currentCant = parseFloat($("#cantidad-" + orderDetailId).val());

    if ((currentCant != currentCant) || (currentCant <= 1)) {
        currentCant = 1;
        $("#cantidad-" + orderDetailId).val('1');
    }

    precioUnitario = $("#preciounitario-" + orderDetailId).text().replace(".", "").replace("$", "");
    currentTotal = parseFloat(precioUnitario) * (currentCant);

    $("#subtotal-" + orderDetailId).text(currentTotal).formatCurrency();

    GetSubTotal();


}

function handleEliminarCarrito(context) {
    var json = context.get_data();
    var data = Sys.Serialization.JavaScriptSerializer.deserialize(json);

    $('#row-' + data.Id).fadeOut(300, function () {
        $(this).remove();
        $("#pedido-subtotal").text(GetSubTotal()).formatCurrency();
    });

    showOrderOrMessage();
}

function showOrderOrMessage() {
    var rowCount = $('.prenda_elegida').length;

    if (rowCount == 1) {
        parent.$("#itemsCarrito").text("0 Artículos");
        parent.$("#summary-total").text("$ 0,00");
        $("#divOrderDetail").hide();
        $("#carro_vacio").show();
    }
    else {
        $.ajax({
            type: "POST",
            cache: false,
            url: "/Account/ItemsQuantity",
            data: $(this).serializeArray(),
            success: function (data) {
                var totalPrice = data.TotalPrice;
                $("#itemsCarrito").text(data.Quantity + " Artículos");
                $("#summary-total").text(totalPrice).formatCurrency();
            }
        });
    }
}

function handleReset(context) {
    var json = context.get_data();
    var data = Sys.Serialization.JavaScriptSerializer.deserialize(json);

    window.location.href = "/ShoppingCart/Step1";
}

function aceptarTerminos(id) {
    if (document.getElementById("form_acep").checked) {
        return true;
    } else {
        alert("Por favor lea los Terminos y Condiciones y luego aceptelos antes de confirmar el pedido");
        event.preventDefault();
    }

}

function validarCupon() {
    //validar que no llegue vacio
    $.ajax({
        type: "POST",
        cache: false,
        url: "/ShoppingCart/ValidarCupon",
        data: { cuponCode: $("#txtCoupon").val() },
        success: function (data) {
            if (data == 0) {
                toastr.warning('El cupon ingresado no es valido');
            } else {
                //guardar el porcentajje en un hidden
                $("#descuentoCupon").val(data);
                toastr.success('Descuento aplicado correctamente');
                //llamar a la funciom qie calcule el total
                GetSubTotal();
                //actualizar texto de descuento y subtotal
            }
        }
    });
}

function validarStock() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "/ShoppingCart/ValidarStock",
        data: $(".form").serializeArray(),
        async: false,
        success: function (data) {
            debugger;
            if (data.length > 0) {
                var msg = "Los siguientes productos no poseen stock suficiente: <br />";
                for (var i = 0; i < data.length; i++) {
                    msg += "- " + data[i] + " <br />";
                }
                toastr.error(msg);
                event.preventDefault();
            }
        }
    })
}

function validarDisponibilidad(orderId)
{
    debugger;
    event.preventDefault();
    var hayDisponible = false;
    var hayParcial = false;
    var hayNoDisponible = false;
    var mostrar = false;
    var noAvanzar = false;
    $("span[id^=" + "disponibilidad" + "]").each(function () {
        switch ($(this).text()) {
            case "Disponible":
                hayDisponible = true;
                break;
            case "Parcial":
                hayParcial = true;
                break;
            case "No disponible":
                hayNoDisponible = true;
                break;
            default:
        }
    });
    var texto = "";
    if (hayParcial || hayDisponible)
    {

        if(hayParcial)
        {
            texto += "Uno o mas productos estan disponible parcialmente, se agregarán al carrito con su disponibilidad actual.<br/>";
            mostrar = true;
        }
        if(hayNoDisponible)
        {
            texto += "Uno o mas productos no estan disponibles y no se agregarán al carrito.<br/>";
            mostrar = true;
        }
        // TODO validacion
        if ($(".count").text() != 0)
        {
            texto += "Al realizar esta operación se reemplazarán los productos del carrito actual por los del pedido a repetir.<br/>";
            mostrar = true;
        }
    } else {
        noAvanzar = true;
        texto += "Ninguno de los productos de esta compra esta disponible";
        mostrar = true;
    }
    if (mostrar)
    {
        $('#mensajeValidacion').html(texto);
        $('#validationAlert').modal({ show: false });
        $('#validationAlert').modal('show');
        if (noAvanzar)
        {
            $("#btnAceptar").hide();
        }
    } else {
        window.location.href = "/ShoppingCart/RepeatOrder/" + orderId;
    }
}
function aceptar(orderId)
{
     window.location.href = "/ShoppingCart/RepeatOrder/" + orderId;
}