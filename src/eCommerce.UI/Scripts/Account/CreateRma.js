﻿$(document).ready(function () {
    var i = 1;
    $("#add_row").click(function () {
        $('#addr' + i).html("<td class='text-center'>" + (i + 1) +"</td>" +
                "<td>" +
                    "<input name='SKU_" + i + "' type='text' id='SKU_" + i + "' placeholder='SKU' class='form-control input-md' onchange='CheckWarranty(this);' />" +
                "</td>" +
                "<input type='hidden' name='ProductId_" + i + "' id='ProductId_" + i + "' value='' />" +
                "<input type='hidden' name='RmaItemId_" + i + "' id='RmaItemId_" + i + "' value='-1' />" +
                "<td class='text-center' style='vertical-align:middle;' id='IsInWarranty_" + i + "' name='IsInWarranty_" + i + "'> " +
                    "<i aria-hidden='true' data-toggle='tooltip' data-original-title='Vacio'></i>"+
                "</td>"+
                "<td>"+
                    "<input  disabled name='FaultDescription_" + i + "' type='text' id='FaultDescription_" + i + "' placeholder='Descripción'  class='form-control input-md'>" +
                "</td>");

        $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
        i++;
    });
    $("#delete_row").click(function () {
        if (i > 1) {
            $("#addr" + (i - 1)).html('');
            i--;
        }
    });

});

function CheckWarranty(txt) {
    debugger;
    var row_number = $(txt).attr("name").split('_')[1];

    $.ajax({
        type: "GET",
        cache: false,
        url: "/Account/CheckProductWarranty",
        data: { productSku: $(txt).val() },
        success: function (data) {
            debugger;
            if (data.IsInWarranty) {
                debugger;
                $("#IsInWarranty_" + row_number).html('<i class="icon fa-check" aria-hidden="true" data-toggle="tooltip" data-original-title="En garantia"></i>');
                $("#ProductId_" + row_number).val(data.ProductId);
                $("#FaultDescription_" + row_number).prop("disabled", false);
            }
            else {
                $("#IsInWarranty_" + row_number).html('<i class="icon fa-ban" aria-hidden="true" data-toggle="tooltip" data-original-title="Fuera de garantia"></i>');
                $("#FaultDescription_" + row_number).prop("disabled", true);
            }
        }
    });
}
$("#grabar").click(function validarGarantias() {
    debugger;
    event.preventDefault();
    var hayError = false;
    $('td[id^="IsInWarranty"]').each(function () {
        var asd = $(this).children().data('original-title');
        if ($(this).children().data('original-title') == "Fuera de garantia" || $(this).children().data('original-title') == "Vacio")
        {
            
            hayError = true;
        }
    });

    if (hayError)
    {
        texto="Hubo un error en alguno de los productos"
        $('#mensajeValidacion').html(texto);
        $('#validationAlert').modal({ show: false });
        $('#validationAlert').modal('show');
    } else {
        $("#form1").submit();
    }
        
});