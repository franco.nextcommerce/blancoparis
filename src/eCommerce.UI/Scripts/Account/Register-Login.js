﻿function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);

    if (response.status === 'connected') {
        window.location.href = '/Account/FacebookLogin?uid=' + response.authResponse.userID + '&accessToken=' + response.authResponse.accessToken;
    }
    else if (response.status === 'not_authorized') {
        fb_login();
    }
    else {
        fb_login();
    }
}

function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function () {
    FB.init({
        appId: '@System.Configuration.ConfigurationManager.AppSettings.Get("AppId")',
        cookie: true,  // enable cookies to allow the server to access the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.2' // use version 2.2
    });
};

// Load the SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function fb_login() {
    FB.login(function () {
        //checkLoginState();
    }, { scope: 'email,public_profile', return_scopes: true });
    //FB.login(function () { }, { scope: 'email,public_profile,user_friends', return_scopes: true });
}

$('#btnFacebookLogin').click(function () {
    checkLoginState();
});




/////////////////////////////////////////////////////////


function LoginPopup() {
    $('.popup-login').css('display', 'flex');
    $('body').toggleClass('m--popup-banner', 'true');
}

$('.js-close-popup-login').click(function () {
    $('.popup-login').css('display', 'none');
    $('body').removeClass('m--popup-banner');
});

//Register
$(".register-button").click(function () {
    $('.popup-register').css('display', 'flex');
    $('.popup-login').css('display', 'none');
});

$('.js-close-popup-register').click(function () {
    $('.popup-register').css('display', 'none');
    $('body').removeClass('m--popup-banner');
});


function ProvinceList() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Home/GetProvinceList",
        data: null,
        success: function (data) {
            var $Province = data.Province;
            if ($Province != null) {
                if ($Province.length > 0) {
                    var selectToAppend = $('#provinceSelect');

                    selectToAppend.empty();
                    $($Province).each(function (i, item) {
                        selectToAppend.append('<option value="' + item.Id + '">' + item.Name + '</option>');
                    });

                    $('#provinceSelect').change();
                }
                else {
                    console.log("error");
                }
            }
            else {
                console.log("error");
            }
        }
    });
}


//Register

var validCUIT = false;

$('#js-cuit').change(function () {
    validCUIT = false;
    debugger;
    $('#js-cuit').addClass('__form-input--invalid');
});

$('#js-cuit').on('change', function () {
    var cuit = $(this).val();
    debugger;

    //isDNI
    if (cuit.length < 13) {
        validCUIT = true;
        $("#js-cuit").attr('name', 'dni');
    }

    if (!validCUIT) {
        $.ajax({
            type: "GET",
            cache: false,
            url: "/Account/ValidateCUIT",
            data: { CUIT: cuit },
            success: function (data) {
                validCUIT = data.Valid;
                if (!validCUIT) {
                    $('#js-cuit').addClass('__form-input--invalid');
                    toastr.warning('El CUIT ingresado no es válido.');
                } else {
                    $('#js-cuit').removeClass('__form-input--invalid');
                }
            }
        });
    }
});