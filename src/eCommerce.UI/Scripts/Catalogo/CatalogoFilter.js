﻿$(function () {
    $('#btnFiltro').click(function () {
        var queryString = window.location.search.replace('?', '').split('&');
        var queryStringFiltered = '';
        var queryStringFilteredOriginal = '';
        debugger;

        for (var i = 0; i < queryString.length; i++) {
            debugger;
            if (queryString[i].indexOf('term') == -1 && queryString[i].indexOf('rubro') == -1 && queryString[i].indexOf('categoria') == -1 && queryString[i].indexOf('vista') == -1) continue;

            if (queryStringFiltered.includes('?')) {
                queryStringFiltered = queryStringFiltered + '&' + queryString[i];
            } else {
                queryStringFiltered = queryStringFiltered + '?' + queryString[i];
            }
            
        }

        //queryStringFiltered = queryStringFiltered.substring(1);
        queryStringFilteredOriginal = queryStringFiltered;
        var checks = $('.producto-color:checked');
        debugger;
        if (checks[0] != null) {
            for (var i = 0; i < checks.length ; i++) {
                if (queryStringFiltered.includes('?')) {
                    queryStringFiltered = queryStringFiltered + '&colores=' + $(checks[i]).attr('value');
                } else {
                    queryStringFiltered = queryStringFiltered + '?colores=' + $(checks[i]).attr('value');
                }
                
            }
        }

        var sizeChecks = $('.producto-talle:checked');
        debugger;
        if (sizeChecks[0] != null) {
            for (var i = 0; i < sizeChecks.length ; i++) {
                if (queryStringFiltered.includes('?')) {
                    queryStringFiltered = queryStringFiltered + '&talles=' + $(sizeChecks[i]).attr('value');
                } else {
                    queryStringFiltered = queryStringFiltered + '?talles=' + $(sizeChecks[i]).attr('value');
                }

            }
        }

        var precioDesde = $('#precioDesde').val();
        var precioHasta = $('#precioHasta').val();
        if (precioDesde && precioHasta)
        {
            if (queryStringFiltered.includes('?')) {
                queryStringFiltered = queryStringFiltered + '&precioDesde=' + precioDesde;
                queryStringFiltered = queryStringFiltered + '&precioHasta=' + precioHasta;
            } else {
                queryStringFiltered = queryStringFiltered + '?precioDesde=' + precioDesde;
                queryStringFiltered = queryStringFiltered + '&precioHasta=' + precioHasta;
            }
            
        }
        if (queryStringFiltered != queryStringFilteredOriginal) {
            var url = window.location.pathname + queryStringFiltered;
            window.location = url;
        }
        return false;
    });
});

$(function () {
    $('#btnSacarFiltro').click(function () {
        debugger;

        var queryString = window.location.search.replace('?', '').split('&');
        var queryStringFiltered = '';

        for (var i = 0; i < queryString.length; i++) {
            if (queryString[i].indexOf('color') == -1 && queryString[i].indexOf('talle') == -1 && queryString[i].indexOf('desde') == -1 && queryString[i].indexOf('hasta')== -1) continue;

            queryStringFiltered = queryStringFiltered + '&' + queryString[i];
        }

        queryStringFiltered = queryStringFiltered.substring(1);
        debugger;

        var url = window.location.pathname + queryStringFiltered;

        window.location = url;

        return false;
    });
});