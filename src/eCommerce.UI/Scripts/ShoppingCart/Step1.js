﻿
function GetSubTotal()
{
    var total = 0;

    $(".subtotal").each(function ()
    {
        subTotal = $(this).text().replace(".", "").replace(",", ".").replace("$", "");
        total += parseFloat(subTotal);
    });

    var discount = 0;
    if ($("#descuentoCupon").val() != 0)
    {
        //calculateSave(total);
        var percentage = parseFloat($("#descuentoCupon").val());
        discount = parseFloat(total) * (percentage / 100);
        $(".discount").text(discount * -1).formatCurrency();
        $("#discountAmount").val(discount);
        $(".discount-percentage").text("(" + percentage + "%)");
    }

    $(".summary-subtotal").text(total).formatCurrency();
    $(".summary-total").text(total - discount).formatCurrency();

    return total;
}

function ApplyItemsDiscount(percentage)
{
    var descuento = 0;
    $(".cart-item").each(function ()
    {
        var currentDiscount = percentage;

        //set old unit-price
        var unitPrice = $(this).find(".unit-price-current").text();
        $(this).find(".unit-price-prev").text(unitPrice);

        //set item custom discount
        if ($(this).data("max-discount") != null && $(this).data("max-discount") < percentage)
        {
            currentDiscount = $(this).data("max-discount");
            $(this).find(".max-discount").removeClass("hide");

            if (currentDiscount == 0)
                $(this).find(".label-off-text").text("Producto en Oferta (no aplica cupón) ");
            else
                $(this).find(".label-off-text").text("Este producto tiene un descuento máximo aplicable del " + currentDiscount.toFixed(0) + "%");
        }

        //set new unit-price
        unitPrice = unitPrice.replace(".", "").replace(",", ".").replace("$", "");
        var off_unitPrice = parseFloat(unitPrice) * ((100 - currentDiscount) / 100);

        $(this).find(".unit-price-current").text(off_unitPrice).formatCurrency();

        //set old subtotal
        var currentSubtotal = $(this).find(".price-current").text();
        $(this).find(".price-prev").text(currentSubtotal);

        //set new subtotal
        var subtotal = currentSubtotal.replace(".", "").replace(",", ".").replace("$", "");
        var off_discount = parseFloat(subtotal) * (currentDiscount / 100);
        var off_subtotal = parseFloat(subtotal) - off_discount;

        $(this).find(".price-current").text(off_subtotal).formatCurrency();
        $(this).find(".item-discount").val(currentDiscount);
        $(this).find(".discount-applied > span").text(currentDiscount + "% desc. aplicado");
    });

    return descuento;    
}

function calculateSave(total)
{
    var descuento_total = 0;
    $(".price-prev").each(function ()
    {
        if ($(this).text() != '')
        {
            subTotal = $(this).text().replace(".", "").replace(",", ".").replace("$", "");
            descuento_total += parseFloat(subTotal);
        }
    });

    var save = descuento_total - total;
    $(".discount").text(save).formatCurrency();
    $("#descuentoCupon").val(save);
}

function handleOnKeyDown(e) {
    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
}

function handleOnChange(orderDetailId)
{
    currentCant = parseFloat($("#cantidad-" + orderDetailId).val());

    if ((currentCant != currentCant) || (currentCant <= 1))
    {
        currentCant = 1;
        $("#cantidad-" + orderDetailId).val('1');
    }

    //update current price
    precioUnitario = $("#preciounitario-" + orderDetailId).text().replace(".", "").replace(",", ".").replace("$", "");
    currentTotal = parseFloat(precioUnitario) * (currentCant);
    $("#subtotal-" + orderDetailId).text(currentTotal).formatCurrency();

    //update off-price
    //if ($("#descuentoCupon").val() != "0")
    //{
    //    var item = $("div[orderdetailid='" + orderDetailId + "']");
    //    var off_price = item.find(".unit-price-prev").text().replace(".", "").replace(",", ".").replace("$", "");
    //    var off_subtotal = parseFloat(off_price) * (currentCant);
    //    item.find(".price-prev").text(off_subtotal).formatCurrency();
    //}

    GetSubTotal();
}

function handleEliminarCarrito(context)
{
    var json = context.get_data();
    var data = Sys.Serialization.JavaScriptSerializer.deserialize(json);

    $('#row-' + data.Id).fadeOut(300, function () {
        $(this).remove();
        $("#pedido-subtotal").text(GetSubTotal()).formatCurrency();
    });

    showOrderOrMessage();
}

function showOrderOrMessage()
{
    var rowCount = $('.prenda_elegida').length;

    if (rowCount == 1)
    {
        parent.$("#itemsCarrito").text("0 Artículos");
        parent.$("#summary-total").text("$ 0,00");
        $("#divOrderDetail").hide();
        $("#carro_vacio").show();
    }
    else {
        $.ajax({
            type: "POST",
            cache: false,
            url: "/Account/ItemsQuantity",
            data: $(this).serializeArray(),
            success: function (data) {
                var totalPrice = data.TotalPrice;
                $("#itemsCarrito").text(data.Quantity + " Artículos");
                $("#summary-total").text(totalPrice).formatCurrency();
            }
        });
    }
}

function handleReset(context)
{
    var json = context.get_data();
    var data = Sys.Serialization.JavaScriptSerializer.deserialize(json);

    window.location.href = "/ShoppingCart/Step1";
}

function aceptarTerminos(id)
{
    if (document.getElementById("form_acep").checked) {
        return true;
    } else {
        alert("Por favor lea los Terminos y Condiciones y luego aceptelos antes de confirmar el pedido");
        event.preventDefault();
    }
}

function validarCupon()
{
    if ($("#descuentoCupon").val() == "0")
    {
        //validar que no llegue vacio
        $.ajax({
            type: "POST",
            cache: false,
            url: "/ShoppingCart/ValidarCupon",
            data: { cuponCode: $("#txtCoupon").val() },
            success: function (data)
            {
                if (data == 0) {
                    toastr.warning('El cupon ingresado es inválido');
                }
                else
                {
                    //guardar el porcentaje en un hidden
                    $("#descuentoCupon").val(data);

                    //Aplicar descuentos
                    // ApplyItemsDiscount(data);
                    //ApplyDiscount(data);

                    //calcular totales
                    GetSubTotal();

                    toastr.success('¡La promoción se aplicó a tu carrito!');
                }
            }
        });
    }
}

function validarStock()
{
    $.ajax({
        type: "POST",
        cache: false,
        url: "/ShoppingCart/ValidarStock",
        data: $(".form").serializeArray(),
        async: false,
        success: function (data)
        {
            if (data.length > 0)
            {
                var msg = "Productos sin stock suficiente: <br />";
                for (var i = 0; i < data.length; i++) {
                    msg += "- " + data[i] + " <br />";
                }
                toastr.warning(msg);
                event.preventDefault();
                return false;
            }
            return true;
        }
    })
}

function fConfirmClear()
{
    $('#validationAlert').modal('show');
}


function fClearShoppingCart()
{
    $.ajax({
        type: "POST",
        cache: false,
        url: "/ShoppingCart/ClearShoppingCart",
        data: null,
        async: false,
        success: function (data)
        {
            if (data.length == 0) {
                location.reload();
            }
            else
            {
                toastr.warning("Se produjo un error al intentar vaciar el carrito.");
            }
        }
    })
}