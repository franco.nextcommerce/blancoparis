﻿function SetTotal()
{
    var total = 0;
    $(".summary-price").each(function () {
        value = $(this).text().replace(".", "").replace(",", ".").replace("$", "");
        total += parseFloat(value);
    });
    var discount = ($(".discount").text()).replace(".", "").replace(",", ".").replace("$", "").replace("(", "").replace(")", "");
    var shippingCost = ($(".shipment-cost").text()).replace(".", "").replace(",", ".").replace("$", "").replace("(", "").replace(")", "");
    total += parseFloat(shippingCost);

    $(".summary-total").text(total - discount).formatCurrency();
    ScrollTop();
}

function ScrollTop()
{
    var body = $("html, body");
    body.stop().animate({ scrollTop: $(".cart-summary").offset().top }, '500', 'swing', function () {
        $(".le-button big").focus();
    });
}
