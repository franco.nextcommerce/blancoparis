﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace eCommerce.Services.Utils
{
    public class ModuleViewEngine : RazorViewEngine
    {
        public ModuleViewEngine()
        {
            var viewLocations = new[] {
                "~/Views/Modules/{1}/{0}.cshtml",
                "~/Views/Modules/{0}.cshtml",
                "~/Modules/{1}/{0}.cshtml",
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml"
            };

            var partialViewLocations = new[] {
                "~/Views/Modules/{1}/{0}.cshtml",
                "~/Views/Modules/{0}.cshtml",
                "~/Modules/{1}/{0}.cshtml",

                "~/Views/Modules/{1}/_{0}.cshtml",
                "~/Views/Modules/_{0}.cshtml",
                "~/Modules/{1}/_{0}.cshtml",

                "~/Views/{1}/_{0}.cshtml",
                "~/Views/Shared/_{0}.cshtml",

            };

            this.PartialViewLocationFormats = partialViewLocations;
            this.ViewLocationFormats = viewLocations;
        }
    }
}
