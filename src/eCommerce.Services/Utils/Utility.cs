﻿using System.Linq;
using System.Text;
using System.Globalization;
using System.Configuration;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System;
using System.Reflection;

namespace eCommerce.Services.Utils
{
    public static class Utility
    {
        public static string WholesalerDomain = "mayorista";
        public static string RetailerDomain = "minorista";


        public const decimal IVA_MULTIPLIER = 21;
        public static string CataloguePrefix = "shop";
        public static string ImgUrl = ConfigurationManager.AppSettings["BackOfficeUrl"];

        public static string AppId = ConfigurationManager.AppSettings["AppId"];
        public static string AppSecret = ConfigurationManager.AppSettings["AppSecret"];

        public const string MailTemplates = "/Content/Templates";

        public static int AllProductsCategoryId = ConfigurationManager.AppSettings["AllProductsCategoryId"] != null ? int.Parse(ConfigurationManager.AppSettings["AllProductsCategoryId"]) : 0;
        public static int SaleCategoryId = ConfigurationManager.AppSettings["SaleCategoryId"] != null ? int.Parse(ConfigurationManager.AppSettings["SaleCategoryId"]) : 0;
        public static int DenimCategoryId = ConfigurationManager.AppSettings["DenimCategoryId"] != null ? int.Parse(ConfigurationManager.AppSettings["DenimCategoryId"]) : 0;
        public static int NewCategoryId = ConfigurationManager.AppSettings["SaleCategoryId"] != null ? int.Parse(ConfigurationManager.AppSettings["NewCategoryId"]) : 0;
        public static int BestSellersCategoryId = ConfigurationManager.AppSettings["BestSellersCategoryId"] != null ? int.Parse(ConfigurationManager.AppSettings["BestSellersCategoryId"]) : 0;

        //PaymentMethods
        public static int Efectivo = ConfigurationManager.AppSettings["Efectivo"] != null ? int.Parse(ConfigurationManager.AppSettings["Efectivo"]) : 0;
        public static int MercadoPago = ConfigurationManager.AppSettings["MercadoPago"] != null ? int.Parse(ConfigurationManager.AppSettings["MercadoPago"]) : 0;
        public static int TransferenciaDepos = ConfigurationManager.AppSettings["TransferenciaDepos"] != null ? int.Parse(ConfigurationManager.AppSettings["TransferenciaDepos"]) : 0;
        //DeliveryMethods
        public static int Transporte = ConfigurationManager.AppSettings["Transporte"] != null ? int.Parse(ConfigurationManager.AppSettings["Transporte"]) : 0;
        public static int DomicilioCaba = ConfigurationManager.AppSettings["DomicilioCaba"] != null ? int.Parse(ConfigurationManager.AppSettings["DomicilioCaba"]) : 0;
        public static int DomicilioGba = ConfigurationManager.AppSettings["DomicilioGba"] != null ? int.Parse(ConfigurationManager.AppSettings["DomicilioGba"]) : 0;
        public static int RetiroLocal = ConfigurationManager.AppSettings["RetiroLocal"] != null ? int.Parse(ConfigurationManager.AppSettings["RetiroLocal"]) : 0;
        public static int DomicilioProv = ConfigurationManager.AppSettings["DomicilioProv"] != null ? int.Parse(ConfigurationManager.AppSettings["DomicilioProv"]) : 0;

        public static string WebUrl = ConfigurationManager.AppSettings["ServerRoot"] != null ? ConfigurationManager.AppSettings["ServerRoot"] : string.Empty;
        public static string BackOfficeUrl = ConfigurationManager.AppSettings["BackOffice"] != null ? ConfigurationManager.AppSettings["BackOffice"] : string.Empty;
        public const string StoriesFileUploadDirectory = "/Content/UploadDirectory/Stories";
        
        public static string RemoveDiacritics(this string str)
        {
            if (str == null) return null;
            var chars =
                from c in str.Normalize(NormalizationForm.FormD).ToCharArray()
                let uc = CharUnicodeInfo.GetUnicodeCategory(c)
                where uc != UnicodeCategory.NonSpacingMark
                select c;

            var cleanStr = new string(chars.ToArray()).Normalize(NormalizationForm.FormC);

            return cleanStr;
        }

        public static class EnumUtil
        {
            public static IEnumerable<T> GetValues<T>()
            {
                return Enum.GetValues(typeof(T)).Cast<T>();
            }
        }

        public static TAttribute GetAttribute<TAttribute>(this Enum enumValue)
        where TAttribute : Attribute
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<TAttribute>();
        }

        public static int ProformaOrderId
        {
            get
            {
                if (HttpContext.Current.Session["ProformaOrderId"] != null)
                    return int.Parse(HttpContext.Current.Session["ProformaOrderId"].ToString());
                else
                    return -1;
            }
            set
            {
                HttpContext.Current.Session["ProformaOrderId"] = value;
            }
        }

        public static string GenerateSeoString(string name)
        {
            var seo = Regex.Replace(name, "[^a-z0-9ñáéíóú -/]", "", RegexOptions.IgnoreCase);
            seo = Regex.Replace(seo, " ", "-");
            seo = Regex.Replace(seo, "/", "-");
            seo = Regex.Replace(seo, "á", "a");
            seo = Regex.Replace(seo, "é", "e");
            seo = Regex.Replace(seo, "í", "i");
            seo = Regex.Replace(seo, "ó", "o");
            seo = Regex.Replace(seo, "ú", "u");
            seo = seo.ToLower();
            return seo;
        }

    }
}