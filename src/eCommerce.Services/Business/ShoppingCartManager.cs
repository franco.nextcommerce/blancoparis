﻿using System;
using System.Collections.Generic;
using System.Linq;
using eCommerce.Services.Domain;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using System.Web;
using eCommerce.Services.Utils;

namespace eCommerce.Services.Business
{
    public class ShoppingCartManager
    {
        private DbModelEntities _db;

        public ShoppingCartManager()
        {
            _db = new DbModelEntities();
        }

        public List<OrderDetail> GetShoppingCart(string cartGuid, string customerEmail, UserTypeEnum storeType)
        {
            var orderDetails = _db.OrderDetails.Include("Product").Include("Size").Include("Color").
                Where(x => x.Active  && x.OrderId == null && !x.Product.Deleted.HasValue);

            if (!string.IsNullOrEmpty(customerEmail))
                orderDetails = orderDetails.Where(x => (x.GUID == cartGuid || x.CreatedBy == customerEmail));
            else
                orderDetails = orderDetails.Where(x => x.GUID == cartGuid);

            //filtro los orderdetails del tipo de comercio.
            orderDetails = orderDetails.Where(x => x.StoreType == (int)storeType);

            return orderDetails.ToList();
        }

        public List<WishlistItem> GetWishlistItems(string cartGuid)
        {
            var wishlistItems = _db.WishlistItems.Include("Product").Include("Size").Include("Color").
                Where(x => x.Deleted == null && x.GUID == cartGuid);

            return wishlistItems.ToList();
        }

        public List<WishlistItem> GetWishlistItemsByCustomer(string cartGuid, string customerEmail)
        {
            var customer = _db.Customers.FirstOrDefault(x => x.Email == customerEmail);
            var wishlistItems = _db.WishlistItems.Include("Product").Include("Size").Include("Color").Where(x => x.Deleted == null &&
                (x.GUID == cartGuid || x.CustomerId == customer.CustomerId));

            return wishlistItems.ToList();
        }

        public bool IsCartEmptyBy(string cartGuid, string customerEmail)
        {
            return !_db.OrderDetails.Any(orderDetail =>
                (orderDetail.GUID == cartGuid || (orderDetail.CreatedBy == customerEmail && orderDetail.OrderId == null)) && orderDetail.Active);
        }

        public ShoppigCartValidation ValidateOrderDetail(int productId, int SizeId, int ColorId, int quantity, List<NoStockItem> noStockList = null)
        {
            var stock = _db.Stocks.FirstOrDefault(x => x.ProductId == productId && x.SizeId == SizeId && x.ColorId == ColorId);
            var shoppingCartValidation = new ShoppigCartValidation();

            shoppingCartValidation.Approve();
            
            if (stock == null)
            {
                shoppingCartValidation.Reprove("No existe stock para esa combinación de talle y color.");
                //noStockList.Add(new NoStockItem { ColorId = ColorId, SizeId = SizeId, productId = productId });
            }
            else if (stock.Product.SoldOut)
            {
                var reason = "El producto no se puede agregar al carrito de compras porque está agotado.";
                shoppingCartValidation.Reprove(reason);
                //noStockList.Add(new NoStockItem { ColorId = stock.ColorId, SizeId = stock.SizeId, productId = stock.ProductId });
            }
            else if (stock.Product.HasStockControl && quantity > stock.StockVirtual)
            {
                var reason = string.Format("El producto no cuenta con stock suficiente, pruebe comprar hasta {0} unidades.", stock.StockVirtual);
                shoppingCartValidation.Reprove(reason);
                //noStockList.Add(new NoStockItem { ColorId = stock.ColorId, SizeId = stock.SizeId, productId = stock.ProductId });
            }

            /*if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var customer = _db.Customers.FirstOrDefault(x => !x.Deleted.HasValue && x.Email == HttpContext.Current.User.Identity.Name && x.Active);
                if (customer.UserType == (int)UserTypeEnum.Mayorista)
                {
                    shoppingCartValidation.Approve();
                }
            }*/

            return shoppingCartValidation;
        }

        public ShoppigCartValidation ValidateWishlistItem(int productId, int SizeId, int ColorId, int quantity)
        {
            var stock = _db.Stocks.FirstOrDefault(x => x.ProductId == productId && x.SizeId == SizeId && x.ColorId == ColorId);
            var shoppingCartValidation = new ShoppigCartValidation();
            shoppingCartValidation.Approve();

            if (stock == null)
            {
                shoppingCartValidation.Reprove("No existe stock para esa combinación de talle y color.");
            }

            return shoppingCartValidation;
        }

        public void AddToShoppingCart(int productId, int SizeId, int ColorId, int quantity, decimal price, string cartGuid, bool sumar, string customerEmail, UserTypeEnum storeType)
        {
            OrderDetail orderDetail;
            if (string.IsNullOrWhiteSpace(customerEmail))
                orderDetail = _db.OrderDetails.FirstOrDefault(detail => detail.GUID == cartGuid && detail.ProductId == productId && detail.SizeId == SizeId && detail.ColorId == ColorId && detail.Active && detail.StoreType == (int)storeType);
            else
                orderDetail = _db.OrderDetails.FirstOrDefault(detail => (detail.GUID == cartGuid || (detail.CreatedBy == customerEmail && detail.OrderId == null))
                                    && detail.ProductId == productId && detail.SizeId == SizeId && detail.ColorId == ColorId && detail.Active && detail.StoreType == (int)storeType);

            if (orderDetail == null)
            {
                orderDetail = new OrderDetail { Created = CurrentDate.Now };
                _db.OrderDetails.Add(orderDetail);
            }

            orderDetail.Price = price;
            orderDetail.ProductId = productId;
            orderDetail.SizeId = SizeId;
            orderDetail.ColorId = ColorId;
            orderDetail.Quantity = sumar ? orderDetail.Quantity + quantity : quantity;
            orderDetail.GUID = cartGuid;
            orderDetail.OrderId = null;
            orderDetail.Active = true;
            orderDetail.Created = CurrentDate.Now;
            orderDetail.CreatedBy = (HttpContext.Current.User.Identity.IsAuthenticated) ? HttpContext.Current.User.Identity.Name : null;

            //Add store type to difference shopping carts.
            orderDetail.StoreType = (int)storeType;

            _db.SaveChanges();
        }

        public bool IsProductInWishlist(int productId, string cartGuid, string customerEmail)
        {   
            var isInWishlist = false;
            if (string.IsNullOrWhiteSpace(customerEmail))
                isInWishlist = _db.WishlistItems.Any(x => x.GUID == cartGuid && x.ProductId == productId && x.Deleted == null);
            else
            {
                var customer = _db.Customers.FirstOrDefault(x => x.Email == customerEmail);

                isInWishlist = _db.WishlistItems.Any(x => (x.GUID == cartGuid || (x.CustomerId == customer.CustomerId))
                                    && x.ProductId == productId && x.Deleted == null);
            }

            return isInWishlist;
        }

        public void AddToWishlist(int productId, int SizeId, int ColorId, int quantity, decimal price, string cartGuid, string customerEmail)
        {
            var wishlistItem = new WishlistItem();
            if (string.IsNullOrWhiteSpace(customerEmail))
                wishlistItem = _db.WishlistItems.FirstOrDefault(x => x.GUID == cartGuid && x.ProductId == productId && x.SizeId == SizeId && x.ColorId == ColorId && x.Deleted == null);
            else
            {
                var customer = _db.Customers.FirstOrDefault(x => x.Email == customerEmail);

                wishlistItem = _db.WishlistItems.FirstOrDefault(x => (x.GUID == cartGuid || (x.CustomerId == customer.CustomerId))
                                    && x.ProductId == productId && x.SizeId == SizeId && x.ColorId == ColorId && x.Deleted == null);
            }

            if (wishlistItem == null)
            {
                wishlistItem = new WishlistItem { Created = CurrentDate.Now };
                _db.WishlistItems.Add(wishlistItem);
            }
            
            wishlistItem.ProductId = productId;
            wishlistItem.SizeId = SizeId;
            wishlistItem.ColorId = ColorId;
            wishlistItem.Quantity = quantity;
            wishlistItem.GUID = cartGuid;
            wishlistItem.Created = CurrentDate.Now;
            wishlistItem.CreatedBy = (HttpContext.Current.User.Identity.IsAuthenticated) ? HttpContext.Current.User.Identity.Name : null;

            _db.SaveChanges();
        }

        public int GetWishlistCount(string cartGuid, string customerEmail)
        {
            var wishlistItemCount = 0;

            if (string.IsNullOrWhiteSpace(customerEmail))
                wishlistItemCount = _db.WishlistItems
                    .Where(x => x.GUID == cartGuid && x.Deleted == null).Count();
            else
            {
                var customer = _db.Customers.FirstOrDefault(x => x.Email == customerEmail);

                wishlistItemCount = _db.WishlistItems.Where(x => (x.GUID == cartGuid || (x.CustomerId == customer.CustomerId)) && x.Deleted == null).Select(x => x.Quantity).Count();
            }

            return wishlistItemCount;
        }

        public bool RemoveOrderDetail(int orderDetailId)
        {
            var orderDetail = _db.OrderDetails.FirstOrDefault(order => order.OrderDetailId == orderDetailId);
            var status = false;
            if (orderDetail != null)
            {
                orderDetail.Active = false;
                _db.SaveChanges();
                status = true;
            }

            return status;
        }

        private Order GetCustomerShoppingCart(int customerId)
        {
            var shoppingCart = _db.Orders.Where(o => o.CustomerId == customerId).OrderByDescending(o => o.Created).FirstOrDefault();

            if (CustomerDoesNotHaveShoppingCart(shoppingCart))
            {
                shoppingCart = CreateNewOrderForCustomer(customerId);
            }

            return shoppingCart;
        }

        private Order CreateNewOrderForCustomer(int customerId)
        {
            var newOrder = new Order
            {
                CustomerId = customerId,
                OrderStatusId = ((int)OrderStatusEnum.Carrito),
            };

            _db.Orders.Add(newOrder);
            _db.SaveChanges();

            return newOrder;
        }

        private bool CustomerDoesNotHaveShoppingCart(Order lastCustomerOrder)
        {
            return lastCustomerOrder == null || lastCustomerOrder.OrderStatusId != ((int)OrderStatusEnum.Carrito);
        }
    }
}


