﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Services.Model;

namespace eCommerce.Services.Business
{
    public class CategoryManager
    {
        private readonly DbModelEntities _db;

        public CategoryManager()
        {
            _db = new DbModelEntities();
        }
        
        public  Dictionary<Category, IEnumerable<Category>> GetGroupedCategories()
        {
            var parentCategories = _db.Categories.Where(c => c.ParentCategoryId == null && c.Active);
            var groupedCategories = new Dictionary<Category, IEnumerable<Category>>();

            foreach (var category in parentCategories)
            {
                var children = _db.Categories.Where(c => c.ParentCategoryId == category.CategoryId).ToList();

                groupedCategories.Add(category, children);
            }

            return groupedCategories;
        }


    }
}
