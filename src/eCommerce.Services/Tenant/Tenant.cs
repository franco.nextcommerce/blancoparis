﻿using System;
using System.Linq;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using System.Web;

namespace eCommerce.Services.Tenant
{
    public class Tenant
    {
        public int Id => HttpContext.Current.Session["Tenant"] != null ? ((CurrentTenant)HttpContext.Current.Session["Tenant"]).Id : 0;
        public string Name => ((CurrentTenant)HttpContext.Current.Session["Tenant"])?.Name;
        public string Domain => ((CurrentTenant)HttpContext.Current.Session["Tenant"])?.Domain;
        public string Logo => ((CurrentTenant)HttpContext.Current.Session["Tenant"])?.Logo;
        public string Bucket => ((CurrentTenant)HttpContext.Current.Session["Tenant"])?.BucketName;
    }

    //public class TenantViewConfiguration
    //{
    //    public List<AccountViewModuleDTO> ViewModules { get; set; }
    //}

    //public class TenantFormFieldsConfiguration
    //{
    //    public List<FormFieldDTO> FormFields { get; set; }
    //}
}
