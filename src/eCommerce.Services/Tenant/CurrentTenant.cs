﻿using System;
using System.Linq;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;

namespace eCommerce.Services.Tenant
{
    public class CurrentTenant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Domain { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Logo { get; set; }
        public string CompanyNumber { get; set; }
        public string MinimumOrder { get; set; }
        public string whatsapp { get; set; }
        public string BucketName { get; set; }

        public string MPClientId { get; set; }
        public string MPClientSecret { get; set; }

        public string GoogleAnalytics { get; set; }

        //public TenantViewConfiguration ViewModuleConfiguration { get; set; }
        //public MenuDTO Menu { get; set; }
        //public TenantFormFieldsConfiguration FormFieldsConfiguration { get; set; }
        //public AccountSiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
