﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eCommerce.Services.Notification
{
    public static class NotificationBar
    {
        public static void ShowMessage(Controller controller, NotificationMessageType type, string message)
        {
            controller.TempData["NotificationMessage"] = message;

            switch (type)
            {
                case NotificationMessageType.Success:
                    controller.TempData["NotificationMessageType"] = "success";
                    break;

                case NotificationMessageType.Info:
                    controller.TempData["NotificationMessageType"] = "info";
                    break;

                case NotificationMessageType.Error:
                    controller.TempData["NotificationMessageType"] = "error";
                    break;

                case NotificationMessageType.Warning:
                    controller.TempData["NotificationMessageType"] = "warning";
                    break;
            }
        }
    }

    public enum NotificationMessageType
    {
        Success,
        Error,
        Info,
        Warning
    }
}