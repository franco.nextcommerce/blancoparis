﻿using System;
using System.Collections.Generic;
using System.Linq;

using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;

using EntityFramework.BulkInsert.Extensions;

namespace eCommerce.Services.Utils
{
    public class SKUManager
    {
        //private DbModelEntities modelEntities = new DbModelEntities();

        public void GenerateProductSKUs(int productId, int purchaseId, int quantity, int? unitsPerBox, ProductSKU_Origin origin)
        {
            //[11] - Código de Producto 
            //[1] - Origen (0)Venta (1)Compra
            //[1] - (0) Etiqueta Maestra o (1) Etiqueta Producto 
            //[7] - Autonumérico incremental del producto

            using (DbModelEntities modelEntities = new DbModelEntities())
            {
                using (var transactionScope = modelEntities.Database.BeginTransaction())
                {
                    try
                    {
                        var product = modelEntities.Products.SingleOrDefault(x => x.ProductId == productId);
                        var lastItem = modelEntities.ProductSkus.OrderByDescending(x => x.ProductSkuId).FirstOrDefault();
                        int lastId = lastItem == null ? 1 : lastItem.ProductSkuId;

                        int length;
                        Guid box_guid = Guid.Empty;
                        if (unitsPerBox.HasValue)
                        {
                            length = quantity + (quantity / unitsPerBox.Value); //Si genera SKU Maestros se lo sumo al array.
                            box_guid = Guid.NewGuid();
                        }
                        else
                            length = quantity;

                        int units = 1;
                        ProductSku[] items = new ProductSku[length];
                        for (int i = 0; i < length; i++)
                        {
                            items[i] = new ProductSku();
                            items[i].ProductId = productId;
                            items[i].PurchaseId = purchaseId;

                            if (box_guid != Guid.Empty)
                                items[i].BoxGuid = box_guid.ToString();
                            
                            lastId++;
                            string code = lastId.ToString().PadLeft(7, '0');

                            ProductSKU_LabelType labelType = ProductSKU_LabelType.Product;
                            if (i != 0 && unitsPerBox.HasValue)
                            {
                                if (units == unitsPerBox.Value)
                                {
                                    items[i].IsMaster = true;
                                    labelType = ProductSKU_LabelType.Master;

                                    if ((length - i) > unitsPerBox.Value)
                                        box_guid = Guid.NewGuid();
                                    else
                                        box_guid = Guid.Empty;

                                    units = 0;
                                }
                                else
                                    units++;
                            }

                            items[i].SKU = product.Code + "-" + (int)labelType + "-" + (int)ProductSKU_Origin.Purchase + "-" + code;
                        }

                        List<ProductSku> list = new List<ProductSku>();
                        for (int i = 0; i < length; i++)
                        {
                            list.Add(items[i]);
                        }

                        modelEntities.BulkInsert(list);

                        modelEntities.SaveChanges();
                        transactionScope.Commit();
                    }
                    catch (Exception ex)
                    {
                        transactionScope.Rollback();
                        throw ex;
                    }
                }

            }
        }

        public void GenerateDeliveryProductSKUs(int orderId, int productId, int deliveryOrderId, int quantity, ref DbModelEntities modelEntities)
        {
            //[10] - Código de Producto 
            //[1] - Origen (0)Venta (1)Compra
            //[1] - (0) Etiqueta Maestra o (1) Etiqueta Producto 
            //[7] - Autonumérico incremental del producto

            //using (DbModelEntities modelEntities = new DbModelEntities())
            //{
            //    using (var transactionScope = modelEntities.Database.BeginTransaction())
            //    {
                    try
                    {
                        var product = modelEntities.Products.SingleOrDefault(x => x.ProductId == productId);
                        var lastItem = modelEntities.ProductSkus.OrderByDescending(x => x.ProductSkuId).FirstOrDefault();
                        int lastId = lastItem == null ? 1 : lastItem.ProductSkuId;

                        int length = quantity;

                        int units = 1;
                        ProductSku[] items = new ProductSku[length];
                        for (int i = 0; i < length; i++)
                        {
                            items[i] = new ProductSku();
                            items[i].ProductId = productId;
                            items[i].DeliverOrderId = deliveryOrderId;
                            items[i].OrderId = orderId;
                            items[i].IsMaster = false;

                            lastId++;
                            string code = lastId.ToString().PadLeft(7, '0');
                            string product_code = product.Code.PadLeft(10, '0');

                            items[i].SKU = product_code + "-" + (int)ProductSKU_LabelType.Product + "-" + (int)ProductSKU_Origin.Sale + "-" + code;
                        }

                        List<ProductSku> list = new List<ProductSku>();
                        for (int i = 0; i < length; i++)
                        {
                            list.Add(items[i]);
                        }

                        modelEntities.ProductSkus.AddRange(list);

                        modelEntities.SaveChanges();
                        //transactionScope.Commit();
                    }
                    catch (Exception ex)
                    {
                        //transactionScope.Rollback();
                        throw ex;
                    }
            //    }
            //}
        }


        public void DeleteOrderDeliverySKUs(int deliveryOrderId)
        {
            DbModelEntities modelEntities = new DbModelEntities();

            try
            {
                modelEntities.ProductSkus.RemoveRange(modelEntities.ProductSkus.Where(x => x.DeliverOrderId == deliveryOrderId));

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
