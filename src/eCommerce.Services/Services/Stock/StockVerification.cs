﻿using System;
using System.Collections.Generic;
using System.Linq;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using EntityFramework.Extensions;
using System.Web;

namespace eCommerce.Services
{
    public interface IStockValidate
    {
        bool IsValidStock(Product product, int quantity);
        bool HasStock(Product product);
        void AddStock(Product product, int quantity);
        void SubstractStock(Product product, int quantity);
    }

    /*public class RealStock : IStockValidate
    {
        public bool IsValidStock(Product product, int quantity)
        {
            return (product.StockReal >= quantity);
        }

        public bool HasStock(Product product)
        {
            return (product.StockReal > 0);
        }

        public void AddStock(Product product, int quantity)
        {
            product.StockReal += quantity;
        }

        public void SubstractStock(Product product, int quantity)
        {
            product.StockReal -= quantity;
        }
    }*/


    public class VirtualStock : IStockValidate
    {
        public bool IsValidStock(Product product, int quantity)
        {
            //return (product.StockVirtual >= quantity);
            return true;
        }

        public bool HasStock(Product product)
        {
            return true;
            //return (product.StockVirtual > 0);
        }

        public void AddStock(Product product, int quantity)
        {
            //product.StockVirtual += quantity;
        }

        public void SubstractStock(Product product, int quantity)
        {
            //product.StockVirtual -= quantity;
        }
    }

    public class StockService
    {
        DbModelEntities modelEntities = new DbModelEntities();
        IStockValidate stockValidate;

        #region Constructor 

        public StockService() { }

        public StockService(IStockValidate stockValidateParam)
        {
            stockValidate = stockValidateParam;
        }

        #endregion

        public bool HasStock(List<OrderDetail> orderDetails, ref string productDescription, ref DbModelEntities context)
        {
            if (stockValidate == null)
                return false;

            foreach (var orderDetail in orderDetails)
            {
                var productId = orderDetail.ProductId;
                var quantity = orderDetail.Quantity;

                var product = context.Products.SingleOrDefault(x => x.ProductId == productId && x.Active);

                if (product != null)
                {
                    if (!stockValidate.IsValidStock(product, quantity))
                    {
                        productDescription = orderDetail.Product.Title + " (" + orderDetail.Product.Code + ")";
                        return false;
                    }
                }
            }

            return true;
        }

        public void AddStock(int productId, int sizeId, int colorId, int depositId, int quantity, string concept)
        {
            this.CreateStockMovement(depositId, StockMovementTypeEnum.Ingreso, productId, sizeId, colorId, quantity, concept);
        }

        public void UpdateStocks(Stock stock)
        {
            UpdateStockReal(stock);

            UpdateStockVirtual(stock);
        }

        public void UpdateStocks(Stock stock, ref DbModelEntities context)
        {
            UpdateStockReal(stock, ref context);

            UpdateStockVirtual(stock.ProductId, stock.SizeId, stock.ColorId, ref context);
        }

        public void SubstractStock(List<OrderDetail> orderDetails, ref DbModelEntities context)
        {
            foreach (var orderDetail in orderDetails.Where(x => x.Product.HasStockControl)) //Itero solo sobre los que tienen contról de stock
            {
                var productId = orderDetail.ProductId;
                var quantity = orderDetail.Quantity;

                var product = context.Products.SingleOrDefault(x => x.ProductId == productId && x.Active);

                if (product != null)
                {
                    stockValidate.SubstractStock(product, quantity);
                    context.SaveChanges();
                }
            }
        }

        /*public void ReturnOrderStock(Order order, ref DbModelEntities context)
        {
            if (stockValidate == null)
                return;

            foreach (var orderDetail in order.OrderDetails.Where(x => x.Active))
            {
                var productId = orderDetail.ProductId;
                var quantity = orderDetail.Quantity;

                var product = context.Products.SingleOrDefault(x => x.ProductId == productId && x.Active);

                if (product != null)
                {
                    stockValidate.AddStock(product, quantity);
                }
            }
        }*/

        public void CreateStockMovement(int depositId, StockMovementTypeEnum movementType, int productId, int sizeId, int colorId, int quantity, string concept, decimal price = 0)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var stock = modelEntities.Stocks.FirstOrDefault(x => x.ProductId == productId && x.SizeId == sizeId && x.ColorId == colorId && x.DepositId == depositId);

                    if (stock == null)
                        stock = new Stock();

                    if (stock.Deleted.HasValue)
                    {
                        stock.Deleted = null;
                        stock.DeletedBy = null;
                    }


                    stock.ProductId = productId;
                    stock.SizeId = sizeId;
                    stock.ColorId = colorId;
                    stock.DepositId = depositId;
                    stock.Price = price;

                    if (stock.StockId == 0)
                        modelEntities.Stocks.Add(stock);

                    AddStockMovement(stock, (int)movementType, quantity, concept);

                    UpdateStocks(stock);

                    modelEntities.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        public void StockExchange(int originDepositId, int destinyDepositId, int productId, int sizeId, int colorId, int quantity)
        {
            using (var dbContextTransaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    var originStock = modelEntities.Stocks.FirstOrDefault(x => x.ProductId == productId && x.SizeId == sizeId && x.ColorId == colorId
                        && x.DepositId == originDepositId);

                    var destinyDeposit = modelEntities.Deposits.FirstOrDefault(x => x.DepositId == destinyDepositId);

                    this.AddStockMovement(originStock, (int)StockMovementTypeEnum.Egreso, quantity, "Movimiento de mercadería al depósito " + destinyDeposit.Name);
                    this.UpdateStockReal(originStock);

                    var destinyStock = modelEntities.Stocks.FirstOrDefault(x => x.ProductId == originStock.ProductId && x.SizeId == originStock.SizeId && x.ColorId == originStock.ColorId
                        && x.DepositId == destinyDeposit.DepositId);

                    if (destinyStock == null)
                    {
                        destinyStock = new Stock();
                        destinyStock.ProductId = originStock.ProductId;
                        destinyStock.SizeId = originStock.SizeId;
                        destinyStock.ColorId = originStock.ColorId;
                        destinyStock.StockVirtual = originStock.StockVirtual;
                        destinyStock.DepositId = destinyDeposit.DepositId;

                        modelEntities.Stocks.Add(destinyStock);
                        modelEntities.SaveChanges();
                    }

                    this.AddStockMovement(destinyStock, (int)StockMovementTypeEnum.Ingreso, quantity, "Movimiento de mercadería desde " + originStock.Deposit.Name);
                    this.UpdateStockReal(destinyStock);

                    modelEntities.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        public void UpdateStockReal(Stock stock)
        {
            this.UpdateStockReal(stock, ref this.modelEntities);
        }

        public void UpdateStockReal(Stock stock, ref DbModelEntities context, bool saveChanges = true)
        {
            var ingresos = context.StockMovements.Where(x => x.StockId == stock.StockId && x.MovementType == (int)StockMovementTypeEnum.Ingreso)
                .Select(x => x.Quantity).DefaultIfEmpty(0).Sum();

            var egresos = context.StockMovements.Where(x => x.StockId == stock.StockId && x.MovementType == (int)StockMovementTypeEnum.Egreso)
                .Select(x => x.Quantity).DefaultIfEmpty(0).Sum();

            stock.StockReal = ingresos - egresos;

            if (saveChanges)
                context.SaveChanges();
        }

        public void UpdateStockVirtual(Stock stock)
        {
            var ingresos = modelEntities.StockMovements.Where(x => x.Stock.StockId == stock.StockId && x.MovementType == (int)StockMovementTypeEnum.Ingreso)
                .Select(x => x.Quantity).DefaultIfEmpty(0).Sum();

            //excluyo las entregas porque estan comprendidas por las ventas.
            var egresos = modelEntities.StockMovements.Where(x => x.Stock.ProductId == stock.ProductId && x.Stock.SizeId == stock.SizeId && x.Stock.ColorId == stock.ColorId && x.MovementType == (int)StockMovementTypeEnum.Egreso && !x.DeliveryOrderDetails.Any())
                .Select(x => x.Quantity).DefaultIfEmpty(0).Sum();

            var sold_quantity = modelEntities.OrderDetails.Where(x => x.ProductId == stock.ProductId && x.Product.HasStockControl && x.SizeId == stock.SizeId && x.ColorId == stock.ColorId
                && x.Deleted == null && x.OrderId != null && x.Quantity > 0 && x.Order.OrderStatusId != (int)OrderStatusEnum.Cancelada && x.Order.OrderStatusId != (int)OrderStatusEnum.EnProceso).Select(x => x.Quantity).DefaultIfEmpty(0).Sum();

            int stockVirtual = ingresos - egresos - sold_quantity;

            var stocks = modelEntities.Stocks
                .Where(x => x.ProductId == stock.ProductId && x.SizeId == stock.SizeId && x.ColorId == stock.ColorId)
                .Update(s => new Stock { StockVirtual = stockVirtual });

            modelEntities.SaveChanges();
        }

        public void UpdateStockVirtual(int productId, int sizeId, int colorId, ref DbModelEntities context)
        {
            var ingresos = context.StockMovements.Where(x => x.Stock.ProductId == productId && x.Stock.SizeId == sizeId && x.Stock.ColorId == colorId && x.MovementType == (int)StockMovementTypeEnum.Ingreso)
                .Select(x => x.Quantity).DefaultIfEmpty(0).Sum();

            //excluyo las entregas porque estan comprendidas por las ventas.
            var egresos = context.StockMovements.Where(x => x.Stock.ProductId == productId && x.Stock.SizeId == sizeId && x.Stock.ColorId == colorId && x.MovementType == (int)StockMovementTypeEnum.Egreso && !x.DeliveryOrderDetails.Any())
                .Select(x => x.Quantity).DefaultIfEmpty(0).Sum();

            var sold_quantity = context.OrderDetails.Where(x => x.ProductId == productId && x.SizeId == sizeId && x.ColorId == colorId
                && x.Deleted == null && x.OrderId != null && x.Quantity > 0 && x.Order.OrderStatusId != (int)OrderStatusEnum.Cancelada && x.Order.OrderStatusId != (int)OrderStatusEnum.EnProceso).Select(x => x.Quantity).DefaultIfEmpty(0).Sum();

            int stockVirtual = ingresos - egresos - sold_quantity;
            context.UpdateStockVirtual(productId, sizeId, colorId, stockVirtual);

            context.SaveChanges();
        }

        public void UpdateStockOrderDetails(int orderId, ref DbModelEntities context)
        {
            var orderdetails = context.OrderDetails.Where(x => x.OrderId == orderId && x.Product.HasStockControl) //Traigo solo los que tienen contról de stock
                .Select(x => new { ProductId = x.ProductId, SizeId = x.SizeId, ColorId = x.ColorId });

            foreach (var item in orderdetails)
            {
                UpdateStockVirtual(item.ProductId, item.SizeId, item.ColorId, ref context);
            }
        }

        public void CreateDeliveryMovements(DeliveryOrder deliveryOrder, ref DbModelEntities context)
        {
            foreach (var item in deliveryOrder.DeliveryOrderDetails.Where(x => x.Product.HasStockControl)) //Itero solo sobre los que tienen contról de stock
            {
                var productStock = context.Stocks.FirstOrDefault(x => x.ProductId == item.ProductId && x.SizeId == item.OrderDetail.SizeId &&
                    x.ColorId == item.OrderDetail.ColorId && x.DepositId == item.DepositId);

                var movement = AddStockMovement(productStock, (int)StockMovementTypeEnum.Egreso, item.Quantity, "Entrega #" + item.DeliveryOrderId + " por Venta #" + deliveryOrder.OrderId, ref context);

                UpdateStockReal(productStock, ref context);

                item.StockMovementId = movement.StockMovementId;
            }
        }

        public void CreateDeliveryItemMovement(DeliveryOrderDetail item, ref DbModelEntities context)
        {
            if (item.Product.HasStockControl)
            {
                var productStock = context.Stocks.FirstOrDefault(x => x.ProductId == item.ProductId && x.SizeId == item.OrderDetail.SizeId && x.ColorId == item.OrderDetail.ColorId && x.DepositId == item.DepositId);

                var movement = AddStockMovement(productStock, (int)StockMovementTypeEnum.Egreso, item.Quantity, "Entrega #" + item.DeliveryOrderId + " por Venta #" + item.DeliveryOrder.OrderId, ref context);

                //UpdateStockReal(productStock, ref context);

                item.StockMovementId = movement.StockMovementId;
            }
        }

        public void UpdateDeliveryMovements(ICollection<DeliveryOrderDetail> deliveryItems, ref DbModelEntities context)
        {
            foreach (var item in deliveryItems)
            {
                /*AddStockMovement()
                UpdateStockVirtual(item.ProductId, item.SizeId, item.ColorId, ref context);*/
            }
        }

        ////Add para manejo de stock nuevo
        //public void DeleteStockMovements(ICollection<Stock> stocks, ref DbModelEntities context)
        //{
        //    if (stocks.Count > 0)
        //    {
        //        foreach (var item in stocks)
        //        {
        //            var movementsToDelete = context.StockMovements.Where(x => x.StockId == item.StockId);

        //            context.StockMovements.RemoveRange(movementsToDelete);
        //            context.SaveChanges();

        //            UpdateStockReal(item, ref context, false);
        //        }
        //        context.SaveChanges();
        //    }
        //}

        //Add para manejo de stock nuevo
        public void DeleteStockMovements(ICollection<Stock> stocks, ref DbModelEntities context)
        {
            if (stocks.Count > 0)
            {
                foreach (var item in stocks)
                {
                    item.Deleted = DateTime.Now;
                    item.DeletedBy = HttpContext.Current.User.Identity.Name;
                    context.SaveChanges();

                    UpdateStockReal(item, ref context, false);
                }
                context.SaveChanges();
            }
        }

        public void DeleteDeliveryMovements(ICollection<DeliveryOrderDetail> deliveryItems, ref DbModelEntities context)
        {
            if (deliveryItems.Count > 0)
            {
                var movementsToDelete = deliveryItems.Where(x => x.StockMovementId.HasValue).Select(x => x.StockMovement);

                if (movementsToDelete != null)
                {
                    var stocks = movementsToDelete.Select(x => x.Stock).ToList();

                    context.StockMovements.RemoveRange(movementsToDelete);
                    context.SaveChanges();

                    foreach (var stock in stocks)
                    {
                        UpdateStockReal(stock, ref context, false);
                    }
                    context.SaveChanges();
                }
            }
        }

        public void DeleteRefundMovements(ICollection<RefundDetail> refundItems, ref DbModelEntities context)
        {
            var stockMovements = refundItems.Where(x => x.StockMovement != null).ToList();
            if (refundItems.Count > 0 && stockMovements.Count > 0)
            {
                var movementsToDelete = refundItems.Select(x => x.StockMovement).ToList();
                if (movementsToDelete != null)
                {
                    var stocks = movementsToDelete.Select(x => x.Stock).ToList();

                    context.StockMovements.RemoveRange(movementsToDelete);
                    context.SaveChanges();

                    foreach (var stock in stocks)
                    {
                        UpdateStocks(stock, ref context);
                    }

                    context.SaveChanges();
                }
            }
        }

        public void DeleteExchangeMovements(ICollection<ExchangeDetail> exchangeItems, ref DbModelEntities context)
        {
            if (exchangeItems.Count > 0)
            {
                var movementsToDelete = exchangeItems.Select(x => x.StockMovement);
                var stocks = movementsToDelete.Select(x => x.Stock).ToList();
                context.StockMovements.RemoveRange(movementsToDelete);
                context.SaveChanges();

                foreach (var stock in stocks)
                {
                    UpdateStockReal(stock, ref context);

                    UpdateStockVirtual(stock.ProductId, stock.SizeId, stock.ColorId, ref context);
                }
                context.SaveChanges();
            }
        }

        public void ReturnOrderStock(int orderId, ref DbModelEntities context)
        {
            var orderdetails = modelEntities.OrderDetails.Where(x => x.OrderId == orderId && x.Active)
                        .Select(x => new { ProductId = x.ProductId, SizeId = x.SizeId, ColorId = x.ColorId });

            foreach (var item in orderdetails)
            {
                UpdateStockVirtual(item.ProductId, item.SizeId, item.ColorId, ref context);
            }

            context.SaveChanges();
        }

        private void AddStockMovement(Stock stock, int movementType, int quantity, string concept)
        {
            this.AddStockMovement(stock, movementType, quantity, concept, ref this.modelEntities);
        }

        private StockMovement AddStockMovement(Stock stock, int MovementType, int Quantity, string Concept, ref DbModelEntities context)
        {
            StockMovement movement = new StockMovement();
            movement.StockId = stock.StockId;
            movement.Concept = Concept;
            movement.MovementType = MovementType;
            movement.Quantity = Quantity;
            movement.Created = CurrentDate.Now;
            movement.CreatedBy = HttpContext.Current.User.Identity.Name;

            context.StockMovements.Add(movement);
            context.SaveChanges();

            return movement;
        }

        public void UpdateRefundMovements(List<RefundDetail> refundDetails, ref DbModelEntities context)
        {
            foreach (var item in refundDetails)
            {
                if (item.StockMovementId.HasValue)
                {
                    //Update de movimiento
                    var productStock = context.Stocks.FirstOrDefault(x => x.ProductId == item.ProductId && x.SizeId == item.SizeId &&
                    x.ColorId == item.ColorId && x.DepositId == item.Refund.DepositId);

                    if (productStock == null)
                        productStock = new Stock();

                    productStock.ProductId = item.ProductId;
                    productStock.SizeId = item.SizeId;
                    productStock.ColorId = item.ColorId;
                    productStock.DepositId = item.Refund.DepositId;

                    if (productStock.StockId == 0)
                        context.Stocks.Add(productStock);

                    var movement = context.StockMovements.FirstOrDefault(x => x.StockMovementId == item.StockMovementId.Value);

                    movement.StockId = productStock.StockId;
                    movement.Quantity = item.Quantity;

                    context.SaveChanges();

                    //UpdateStockReal(productStock, ref context);
                    UpdateStockReal(productStock, ref context);
                    UpdateStockVirtual(productStock.ProductId, productStock.SizeId, productStock.ColorId, ref context);
                }
                else
                {
                    var productStock = context.Stocks.FirstOrDefault(x => x.ProductId == item.ProductId && x.SizeId == item.SizeId &&
                    x.ColorId == item.ColorId && x.DepositId == item.Refund.DepositId);

                    if (productStock == null)
                        productStock = new Stock();

                    productStock.ProductId = item.ProductId;
                    productStock.SizeId = item.SizeId;
                    productStock.ColorId = item.ColorId;
                    productStock.DepositId = item.Refund.DepositId;

                    if (productStock.StockId == 0)
                        context.Stocks.Add(productStock);

                    var movement = AddStockMovement(productStock, (int)StockMovementTypeEnum.Ingreso, item.Quantity, "Devolución #" + item.Refund.RefundId, ref context);

                    item.StockMovementId = movement.StockMovementId;

                    //UpdateStockReal(productStock, ref context);
                    UpdateStockReal(productStock, ref context);
                    UpdateStockVirtual(productStock.ProductId, productStock.SizeId, productStock.ColorId, ref context);
                }
            }
        }

        public void UpdateExchangeMovements(List<ExchangeDetail> exchangeDetails, ref DbModelEntities context)
        {
            foreach (var item in exchangeDetails)
            {
                if (item.StockMovementId.HasValue)
                {
                    //Update de movimiento
                    var productStock = context.Stocks.FirstOrDefault(x => x.ProductId == item.ProductId && x.SizeId == item.SizeId &&
                    x.ColorId == item.ColorId && x.DepositId == item.Exchange.DepositId);

                    if (productStock == null)
                        productStock = new Stock();

                    productStock.ProductId = item.ProductId;
                    productStock.SizeId = item.SizeId;
                    productStock.ColorId = item.ColorId;
                    productStock.DepositId = item.Exchange.DepositId;

                    if (productStock.StockId == 0)
                        context.Stocks.Add(productStock);

                    var movement = context.StockMovements.FirstOrDefault(x => x.StockMovementId == item.StockMovementId.Value);

                    movement.StockId = productStock.StockId;
                    movement.Quantity = item.Quantity;
                    movement.MovementType = item.ExchangeDetailTypeId;

                    context.SaveChanges();

                    UpdateStockReal(productStock, ref context);

                    UpdateStockVirtual(productStock.ProductId, productStock.SizeId, productStock.ColorId, ref context);
                }
                else
                {
                    var productStock = context.Stocks.FirstOrDefault(x => x.ProductId == item.ProductId && x.SizeId == item.SizeId &&
                    x.ColorId == item.ColorId && x.DepositId == item.Exchange.DepositId);

                    if (productStock == null)
                        productStock = new Stock();

                    productStock.ProductId = item.ProductId;
                    productStock.SizeId = item.SizeId;
                    productStock.ColorId = item.ColorId;
                    productStock.DepositId = item.Exchange.DepositId;

                    if (productStock.StockId == 0)
                        context.Stocks.Add(productStock);

                    var movement = AddStockMovement(productStock, item.ExchangeDetailTypeId, item.Quantity, "Cambio #" + item.Exchange.ExchangeId, ref context);

                    item.StockMovementId = movement.StockMovementId;

                    UpdateStockReal(productStock, ref context);

                    UpdateStockVirtual(productStock.ProductId, productStock.SizeId, productStock.ColorId, ref context);
                }
            }
        }
    }

}
