﻿using System.Threading;
using System.Configuration;

namespace eCommerce.Services
{

    public class MailService
    {
        /*private static void SendMail(string from, string to, string subject, string body)
        {
            System.Web.Mail.MailMessage Message = new System.Web.Mail.MailMessage();
            Message.To = to;
            Message.From = from;
            Message.Subject = subject;
            Message.Body = body;
            Message.BodyFormat = MailFormat.Html;
            Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");
            Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");
            Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", ConfigurationManager.AppSettings["SendUserName"]);
            Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationManager.AppSettings["SendPassword"]);
            // Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", ConfigurationManager.AppSettings["SmtpServerPort"]);    
            Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", ConfigurationManager.AppSettings["SmtpServer"]);
            Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpUseSSL"]));

            SmtpMail.Send(Message);
        }*/

        private static void SendMail(string from, string to, string replyTo,string subject, string body)
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(from,to,subject,body);
            message.IsBodyHtml = true;
            message.Priority = System.Net.Mail.MailPriority.Normal;
            message.Headers.Add("Reply-To",replyTo);
            message.ReplyToList.Add(replyTo);
            
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = ConfigurationManager.AppSettings["SmtpServer"];

            smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SendUserName"], ConfigurationManager.AppSettings["SendPassword"]);
            smtp.Send(message);
        }
  
        public static void Send(string from, string to, string subject, string body)
        {
            /*try
            {
                ThreadStart job = delegate { SendMail(from, to, subject, body); };
                new Thread(job).Start();
            }
            catch { }*/
        }

        public static void Send(string from, string to, string replyTo, string subject, string body)
        {
            try
            {
                ThreadStart job = delegate { SendMail(from, to, replyTo,subject, body); };
                new Thread(job).Start();
            }
            catch { }
        }
    }
}
