﻿using Decidir;
using Decidir.Constants;
using Decidir.Exceptions;
using Decidir.Model;
using eCommerce.Services.Model;
using System;
using System.Net;

namespace eCommerce.Services.Services.Decidir
{
    public class DecidirService
    {
        //SANDBOX
        //private const string PRIVATE_API_KEY = "92b71cf711ca41f78362a7134f87ff65";
        //private const string PUBLIC_API_KEY = "e9cdb99fff374b5f91da4480c8dca741";

        //PROD - Verified Fibra Humana
        private const string PUBLIC_API_KEY = "18809269472249b8981f617f744f2e0a";
        private const string PRIVATE_API_KEY = "0f3f0a513e5041589c63a40df5f19ba2";


        private readonly int AMBIENTE = Ambiente.AMBIENTE_PRODUCCION;

        private readonly DecidirConnector _service;

        public DecidirService()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            _service = new DecidirConnector(AMBIENTE, PRIVATE_API_KEY, PUBLIC_API_KEY);
        }

        public PaymentResponse GenerateDecidirPayment(Order order, string token, string bin, int installments, string paymentType)
        {
            try
            {
                Payment paymentDecidir = new Payment();

                string newGuid = Guid.NewGuid().ToString();
                //List<object> objectList = new List<object>();
                //paymentDecidir.site_transaction_id = order.OrderId.ToString();
                paymentDecidir.site_transaction_id = newGuid;

                //VISA = 1 - MasterCard Credito = 104 - MasterCard Debito = 105 - Amex = ???
                paymentDecidir.payment_method_id = order.PaymentMethodId_Decidir;

                paymentDecidir.token = token;
                paymentDecidir.bin = bin;
                paymentDecidir.amount = double.Parse(order.Total.ToString());
                paymentDecidir.currency = "ARS";
                paymentDecidir.installments = installments;
                paymentDecidir.description = "Fibra Humana - Pedido Web #" + order.OrderId.ToString();
                paymentDecidir.payment_type = paymentType;
                //paymentDecidir.sub_payments = objectList;

                PaymentResponse resultPaymentResponse = _service.Payment(paymentDecidir);

                return resultPaymentResponse;
            }
            catch (ResponseException ex)
            {
                PaymentResponse response = new PaymentResponse();
                response.status = ex.Message;
                return response;
            }
            catch(Exception ex)
            {
                PaymentResponse response = new PaymentResponse();
                response.status = ex.Message;
                return response;
            }
        }
    }
}
