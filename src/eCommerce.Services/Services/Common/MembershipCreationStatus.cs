﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eCommerce.Services.Security
{
    public enum MembershipCreationStatus
    {
        Success,
        DuplicateEmail,
        DupiclateCuit,
        Fail,
        RequiresConfirmation,
        InvalidCUIT,
        Wholesaler
    }
}
