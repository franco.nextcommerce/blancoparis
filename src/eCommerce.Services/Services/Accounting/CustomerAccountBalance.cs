﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;

namespace eCommerce.Services.Services.Accounting
{
    class CustomerAccountBalance
    {
        DbModelEntities modelEntities = new DbModelEntities();
        public void Update(int CustomerId)
        {
            try
            {
                var customer = modelEntities.Customers.Find(CustomerId);
                var transactions = modelEntities.Transactions
                    .Where(x => 
                        (x.Order.CustomerId == CustomerId ||
                        x.CustomerId == CustomerId ||
                        x.DeliveryOrder.Order.CustomerId == CustomerId ||
                        x.Refund.CustomerId == CustomerId || 
                        x.Exchange.CustomerId == CustomerId) && x.Deleted == null).ToList();

                customer.Balance = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso).Sum(x => x.Amount);
                customer.BalanceUsd = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar).Sum(x => x.Amount);

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
