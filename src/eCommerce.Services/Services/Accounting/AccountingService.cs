﻿using System;
using System.Linq;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;

namespace eCommerce.Services.Services.Accounting
{
    public static class AccountingService
    {
        public static void UpdateCustomerBalance(int CustomerId, ref DbModelEntities modelEntities)
        {
            try
            {
                var customer = modelEntities.Customers.Find(CustomerId);

                var transactions = modelEntities.Transactions
                    .Where(x => 
                        (x.Order.CustomerId == CustomerId ||
                        x.CustomerId == CustomerId ||
                        //x.DeliveryOrder.Order.CustomerId == CustomerId ||
                        x.Refund.CustomerId == CustomerId || 
                        x.Exchange.CustomerId == CustomerId) && x.Deleted == null).ToList();

                customer.Balance = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso).Sum(x => x.Amount);
                customer.BalanceUsd = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar).Sum(x => x.Amount);

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateAccountBalance(int accountId, ref DbModelEntities modelEntities)
        {
            try
            {
                var account = modelEntities.Accountings.FirstOrDefault(x => x.AccountId == accountId);
                var transactions = modelEntities.Transactions.Where(x => x.AccountId == account.AccountId && x.Deleted == null).ToList();

                decimal ingresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso && x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso).Sum(x => x.Amount);
                decimal egresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Peso && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso).Sum(x => x.Amount);
                account.Balance = ingresos - egresos;

                ingresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar && x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso).Sum(x => x.Amount);
                egresos = transactions.Where(x => x.CurrencyId == (int)CurrencyEnum.Dolar && x.TransactionTypeId == (int)TransactionTypeEnum.Egreso).Sum(x => x.Amount);
                account.BalanceUsd = ingresos - egresos;

                modelEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
