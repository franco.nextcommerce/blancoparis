﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace eCommerce.Services.Services
{
    public class FiscalService
    {
        public bool ValidateCUIT(string CUIT)
        {
            if (CUIT.Contains('-') && CUIT.Length < 13)
                return false;

            if (!CUIT.Contains('-') && CUIT.Length < 11)
                return false;

            // Remplazo los "-" del CUIT recibido por parámetro
            CUIT = CUIT.Replace("-", "");
            
            var sum = 0;

            // Paso el CUIT a un array de int con cada dígito como valor
            var digits = CUIT.ToCharArray().Select(x => int.Parse(x.ToString())).ToArray();

            // Dígito verificador
            var verifyingDigit = digits[CUIT.Length - 1];

            // Saco el dígito verificador del array de digitos del CUIT
            digits = digits.Where((val, index) => index != CUIT.Length - 1).ToArray();

            // Consigo la suma del valor por el algóritmo
            for (int i = 0; i < digits.Length; i++) {
                sum += digits[9 - i] * (2 + (i % 6));
            }

            // Si la suma da 0 es inválido ya que significa que todos los digitos son 0
            if (sum == 0)
                return false;

            // Hago el módulo 11 de la suma
            var validVerifyingDigit = 11 - (sum % 11);
            if (validVerifyingDigit == 11)
                validVerifyingDigit = 0;

            return validVerifyingDigit == verifyingDigit;
        }
    }
}
