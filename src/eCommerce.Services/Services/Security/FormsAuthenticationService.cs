﻿using System.Web.Security;
using System.Web;
using eCommerce.Services.Model;

namespace eCommerce.Services.Security
{
    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        public void SignIn(string userName, bool createPersistentCookie)
        {
            if (!createPersistentCookie)
            {
                FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
            }
            else
            {
                var authTicket = new FormsAuthenticationTicket(1, userName, CurrentDate.Now, CurrentDate.Now.AddMinutes(180), true, "");

                string cookieContents = FormsAuthentication.Encrypt(authTicket);
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookieContents)
                {
                    Expires = authTicket.Expiration,
                    Path = FormsAuthentication.FormsCookiePath
                };

                if (HttpContext.Current != null)
                {
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
            }
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}
