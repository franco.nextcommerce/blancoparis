﻿using eCommerce.Services.Model.Enums;
using System.Security.Principal;

namespace eCommerce.Services.Security
{

    public class CustomIdentity : IIdentity
    {
        public string Identifier { get; set; }
        public string FullName { get; set; }
        public int CustomerId { get; set; }
        public int CustomerPriceList { get; set; }

        public CustomIdentity(string identifier, string fullName, int customerId, int customerPriceList)
        {
            Identifier = identifier;
            FullName = fullName;
            CustomerId = customerId;
            CustomerPriceList = customerPriceList;
        }

        public string AuthenticationType
        {
            get { return "Forms"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        /*public string FullName
        {
            get { return FullName; }
        }*/

        public string Name
        {
            get { return Identifier; }
        }
    }
}
