﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eCommerce.Services.Model;

namespace eCommerce.Services.Security
{
    public interface IMembershipService
    {
        int MinPasswordLength { get; }

        bool Validate(string email, string password);

        bool ValidateCustomer(string email, string password);

        CustomerExternalLogin GetFacebookCustomer(string facebookUserId);

        bool ChangePassword(string userName, string oldPassword, string newPassword);

        MembershipCreationStatus CreateUser(USER customer);

        MembershipCreationStatus CreateCustomer(Customer customer);

        MembershipCreationStatus CreateExternalCustomer(Customer customer, string facebookUserId, bool createInternalUser);
    }

}
