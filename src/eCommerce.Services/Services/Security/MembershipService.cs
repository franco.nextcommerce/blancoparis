﻿using System;
using System.Linq;
using eCommerce.Services.Common;
using eCommerce.Services.Model;
using eCommerce.Services.Model.Enums;
using eCommerce.Services.Services;

namespace eCommerce.Services.Security
{
    public class MembershipService : IMembershipService
    {
        DbModelEntities modelEntities = new DbModelEntities();
        String KeyString = "Gv7L3V15jCdb9P5XGKiPnhHZ7JlKcmU=";

        public int MinPasswordLength
        {
            get
            {
                return 8;
            }
        }

        public bool Validate(string username, string uiPassword)
        {
            USER user = modelEntities.USERS.SingleOrDefault(x => x.Username == username && x.Active);

            if (user == null) return false;

            string dbPassword = EncryptionService.Decrypt(user.Password, KeyString);

            return (dbPassword == uiPassword);
        }

        public bool ValidateCustomer(string email, string uiPassword)
        {
            Customer customer = modelEntities.Customers.FirstOrDefault(x => (x.Email.ToLower() == email.ToLower() || x.Username.ToLower() == email.ToLower()) && x.Active && x.Deleted == null);
            if (customer == null) return false;

            if (string.IsNullOrEmpty(customer.Password)) return false;

            string dbPassword = EncryptionService.Decrypt(customer.Password, KeyString);
            return (dbPassword == uiPassword);
        }

        public CustomerExternalLogin GetFacebookCustomer(string facebookUserId)
        {
            return modelEntities.CustomerExternalLogins.FirstOrDefault(c => c.ExternalId == facebookUserId && c.Customer.Active && c.Customer.Deleted == null);
        }

        public bool ChangePassword(string email, string oldPassword, string newPassword)
        {
            ValidationUtil.ValidateRequiredStringValue(email, "email");
            ValidationUtil.ValidateRequiredStringValue(oldPassword, "oldPassword");
            ValidationUtil.ValidateRequiredStringValue(newPassword, "newPassword");

            try
            {
                USER user = modelEntities.USERS.SingleOrDefault(x => x.Email == email && x.Active == false);

                if (user == null) return false;

                var dbPassword = EncryptionService.Decrypt(user.Password, KeyString);

                if (dbPassword != oldPassword) return false;

                user.Password = EncryptionService.Encrypt(newPassword, KeyString);
                modelEntities.SaveChanges();

                /*Mail mail = modelEntities.Mails.SingleOrDefault(a=>a.Code == "CHANGEPWD");

                if (mail != null)
                    MailService.Send(mail.From, email, mail.Subject, String.Format(mail.Body, user.FirstName + " " +user.LastName, email, newPassword));
                */

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public MembershipCreationStatus CreateUser(USER user)
        {
            try
            {
                string decriptedPassword = user.Password;
                USER newuser = modelEntities.USERS.SingleOrDefault(x => x.Email == user.Email && x.Active);

                if (newuser != null)
                    return MembershipCreationStatus.DuplicateEmail;

                user.Password = EncryptionService.Encrypt(user.Password, KeyString);
                user.Active = true;
                user.Created = CurrentDate.Now;

                modelEntities.USERS.Add(user);
                modelEntities.SaveChanges();

                return MembershipCreationStatus.Success;
            }
            catch (Exception)
            {
                return MembershipCreationStatus.Fail;
            }
        }

        public MembershipCreationStatus CreateExternalCustomer(Customer customer, string facebookUserId, bool createInternalUser)
        {
            using (var transaction = modelEntities.Database.BeginTransaction())
            {
                try
                {
                    MembershipCreationStatus result;

                    if(createInternalUser)
                        customer = CreateInternalCustomer(customer).Item2;

                    var newCustomer = modelEntities.CustomerExternalLogins.SingleOrDefault(x => x.ExternalId == facebookUserId && x.Customer.Active && x.Customer.Deleted == null);

                    if (newCustomer != null)
                        return MembershipCreationStatus.Fail;

                    newCustomer = new CustomerExternalLogin();
                    newCustomer.ExternalId = facebookUserId;
                    newCustomer.CustomerId = customer.CustomerId;

                    modelEntities.CustomerExternalLogins.Add(newCustomer);
                    modelEntities.SaveChanges();

                    result = MembershipCreationStatus.Success;
                    transaction.Commit();

                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return MembershipCreationStatus.Fail;
                }
            }
        }

        public MembershipCreationStatus CreateCustomer(Customer customer)
        {
            return CreateInternalCustomer(customer).Item1;
        }

        private Tuple<MembershipCreationStatus, Customer> CreateInternalCustomer(Customer customer)
        {
            MembershipCreationStatus result;
            try
            {
                Customer newCustomer = modelEntities.Customers.FirstOrDefault(x => x.Email == customer.Email && x.Active && !x.Deleted.HasValue);

                if (newCustomer != null)
                    return Tuple.Create(MembershipCreationStatus.DuplicateEmail, new Customer());

                /*if (customer.UserType == (int)UserTypeEnum.Mayorista && modelEntities.Customers.Any(x => !x.Deleted.HasValue && x.CUIT == customer.CUIT ))
                    return Tuple.Create(MembershipCreationStatus.DupiclateCuit, new Customer());

                if (!string.IsNullOrEmpty(customer.CUIT))
                {
                    FiscalService fiscal = new FiscalService();
                    var validCUIT = fiscal.ValidateCUIT(customer.CUIT);

                    if (!validCUIT)
                        return Tuple.Create(MembershipCreationStatus.InvalidCUIT, new Customer());
                }*/

                newCustomer = new Customer();
                newCustomer.Created = CurrentDate.Now;
                newCustomer.Active = true;
                newCustomer.Confirmed = true;
                newCustomer.Enabled = true;
                newCustomer.FullName = customer.FullName;
                newCustomer.Username = customer.Username;
                newCustomer.Email = customer.Email;
                newCustomer.IsSiteUser = customer.IsSiteUser;
                newCustomer.IsFacebookUser = customer.IsFacebookUser;
                newCustomer.ProvinceId = customer.ProvinceId;
                newCustomer.City = customer.City;
                newCustomer.Cellphone = customer.Cellphone;
                newCustomer.PhoneNumber = customer.PhoneNumber;
                newCustomer.Address = customer.Address;
                newCustomer.AddressNumber = customer.AddressNumber;
                newCustomer.ZipCode = customer.ZipCode;
                newCustomer.CityId = customer.CityId;
                newCustomer.IvaConditionId = customer.IvaConditionId;
                newCustomer.DNI = customer.DNI;

                newCustomer.CUIT = customer.CUIT;                    
                newCustomer.CorporateName = customer.CorporateName;
                newCustomer.BillingAddress = customer.BillingAddress;
                newCustomer.UserType = customer.UserType;

                if (customer.UserType == (int)UserTypeEnum.Mayorista)
                    newCustomer.PriceList = (int)PriceListEnum.Mayorista;
                else if (customer.UserType == (int)UserTypeEnum.Minorista)
                    newCustomer.PriceList = (int)PriceListEnum.Minorista;
                
                string decriptedPassword = customer.Password;
                if (!string.IsNullOrWhiteSpace(decriptedPassword))
                    newCustomer.Password = EncryptionService.Encrypt(decriptedPassword, KeyString);
                else
                    newCustomer.Password = null;

                var config = modelEntities.Configurations.FirstOrDefault();

                if (newCustomer.UserType == (int)UserTypeEnum.Minorista && config.FirstOrderDiscountEnabled)
                {
                    newCustomer.FirstOrderDiscountPeriodDays = config.FirstOrderDiscountDurationPeriodDays;
                }

                if (newCustomer.UserType == (int)UserTypeEnum.Mayorista && config.FirstOrderDiscountEnabledWS)
                {
                    newCustomer.FirstOrderDiscountPeriodDays = config.FirstOrderDiscountDurationPeriodDaysWS;
                }

                result = MembershipCreationStatus.Success;

                modelEntities.Customers.Add(newCustomer);
                modelEntities.SaveChanges();

                customer = newCustomer;

                return Tuple.Create(result, customer);
            }
            catch (Exception ex)
            {
                return Tuple.Create(MembershipCreationStatus.Fail, new Customer());
            }
        }
    }

}
