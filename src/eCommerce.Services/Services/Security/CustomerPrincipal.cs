﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace NextCommerce.Services.Security
{
    public class CustomerPrincipal : IPrincipal
    {
        private IIdentity CustomIdentity { get; set; }
        private string Roles { get; set; }

        public CustomerPrincipal(IIdentity customerIdentity)
        {
            CustomIdentity = customerIdentity;
        }

        public IIdentity Identity
        {
            get { return CustomIdentity; }
        }

        public bool IsInRole(string role)
        {
            return Roles.Contains(role);
        }
    }
}
