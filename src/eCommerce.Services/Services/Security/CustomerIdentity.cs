﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace NextCommerce.Services.Security
{
    public class CustomerIdentity : IIdentity
    {
        public string Identifier { get; set; }
        public string FullName { get; set; }
        public int Id { get; set; }
        public int TypeId { get; set; }
        public int StoreTypeId { get; set; }

        public CustomerIdentity(string identifier, int id, string fullName, int typeId, int storeTypeId)
        {
            Identifier = identifier;
            Id = id;
            FullName = fullName;
            TypeId = typeId;
            StoreTypeId = storeTypeId;
        }

        public string AuthenticationType
        {
            get { return "Forms"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public string Name
        {
            get { return Identifier; }
        }
    }
}
