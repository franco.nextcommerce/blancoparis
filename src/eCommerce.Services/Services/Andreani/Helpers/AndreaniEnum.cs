﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Services.Services.Andreani
{
    public enum AndreaniTipoEnvio
    {
        Sucursal = 1,
        DomicilioEstandar = 2,
        DomicilioUrgente = 3
    }

    public enum AndreaniStatus
    {
        Ok = 1,
        Error = 2
    }
}
