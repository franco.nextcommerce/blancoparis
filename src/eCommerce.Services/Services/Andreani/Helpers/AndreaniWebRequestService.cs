﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace eCommerce.Services.Services.Andreani
{
    public class AndreaniWebRequestService
    {
        private const string _sucursalesURL = "https://sucursales.andreani.com/";
        private const string _cotizacionURL = "https://cotizador.andreani.com/";
        //private const string _sucursalesURL = "https://sucursalespreprod.andreani.com/";
        //private const string _cotizacionURL = "https://cotizadorpreprod.andreani.com/";
        private const string _URLAction = "ws";
        private string _codigoCliente;
        private string _username;
        private string _password;

        private HttpClient _sucursalesClient = new HttpClient();
        private HttpClient _cotizacionClient = new HttpClient();

        public AndreaniWebRequestService(string _codigoCliente, string _username, string _password)
        {
            this._sucursalesClient.BaseAddress = new Uri(_sucursalesURL);
            this._cotizacionClient.BaseAddress = new Uri(_cotizacionURL);
            this._codigoCliente = _codigoCliente;
            this._username = _username;
            this._password = _password;
        }

        private HttpRequestMessage LoadRequestMessage(string requestBody)
        {
            var message = new HttpRequestMessage(HttpMethod.Post, _URLAction);
            var content = new StringContent(requestBody, Encoding.UTF8, "application/xml");
            message.Content = content;
            return message;
        }

        public List<AndreaniSucursal> GetSucursales(string cp = "", string localidad = "", string provincia = "")
        {
            var requestBody = GetSucursalesRequestXML(cp, localidad, provincia);
            var request = LoadRequestMessage(requestBody);
            var result = _sucursalesClient.SendAsync(request).Result.Content.ReadAsStringAsync().Result;

            //Cargo el XML del resultado
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(result);

            //Nodos de la respuesta
            var nodes = xmlDocument.GetElementsByTagName("ResultadoConsultarSucursales")[0].ChildNodes;

            //Listado que voy a popular y devolver
            var sucursalesList = new List<AndreaniSucursal>();

            //Traigo todas las propiedades de la clase (que son las mismas y en el mismo órden que la respuesta)
            var props = typeof(AndreaniSucursal).GetProperties();

            for (int i = 0; i < nodes.Count; i++)
            {
                var sucursal = new AndreaniSucursal();

                //Por cada propiedad de la clase le asigno el nodo correspondiente (no alterar órden de propiedades)
                for (int j = 0; j < props.Count(); j++)
                {
                    var propValue = nodes[i].ChildNodes[j].ChildNodes[0]?.InnerText;
                    sucursal.GetType().GetProperty(props[j].Name).SetValue(sucursal, propValue);
                }
                sucursalesList.Add(sucursal);
            }

            return sucursalesList;
        }

        public AndreaniCotizacion CotizarEnvio(string contrato, string cpDestino, string peso, string valorDeclarado, string volumen, string sucursalDestino)
        {
            //El CP es obligatorio aunque el envío sea a sucursal (usar el cp de la sucursal de retiro)
            var requestBody = GetCotizacionRequestXML(contrato, cpDestino, peso, valorDeclarado, volumen, sucursalDestino);
            var request = LoadRequestMessage(requestBody);
            var result = _cotizacionClient.SendAsync(request).Result.Content.ReadAsStringAsync().Result;

            //Cargo el XML del resultado
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(result);

            //Nodos de la respuesta
            var nodes = xmlDocument.GetElementsByTagName("CotizarEnvioResult")[0].ChildNodes;

            //Listado que voy a popular y devolver
            var cotizacion = new AndreaniCotizacion();

            //Traigo todas las propiedades de la clase (que son las mismas y en el mismo órden que la respuesta)
            var props = typeof(AndreaniCotizacion).GetProperties();

            //Por cada propiedad de la clase le asigno el nodo correspondiente (no alterar órden de propiedades)
            var node = nodes;
            for (int j = 0; j < props.Count(); j++)
            {
                var propValue = node[j]?.InnerText;
                cotizacion.GetType().GetProperty(props[j].Name).SetValue(cotizacion, propValue);
            }

            return cotizacion;
        }

        #region Request Bodies

        private string GetSucursalesRequestXML(string cp, string localidad, string provincia)
        {
            return $@"<?xml version='1.0' encoding='UTF-8'?>
                <env:Envelope
                    xmlns:env='http://www.w3.org/2003/05/soap-envelope'
                    xmlns:ns1='urn:ConsultarSucursales'
                    xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
                    xmlns:xsd='http://www.w3.org/2001/XMLSchema'
                    xmlns:ns2='http://xml.apache.org/xml-soap'
                    xmlns:ns3='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'
                    xmlns:enc='http://www.w3.org/2003/05/soap-encoding'>
                    <env:Header>
                        <ns3:Security env:mustUnderstand='true'>
                            <ns3:UsernameToken>
                                <ns3:Username></ns3:Username>
                                <ns3:Password></ns3:Password>
                            </ns3:UsernameToken>
                        </ns3:Security>
                    </env:Header>
                    <env:Body>
                        <ns1:ConsultarSucursales env:encodingStyle='http://www.w3.org/2003/05/soap-encoding'>
                            <Consulta xsi:type='ns2:Map'>
                                <item>
                                    <key xsi:type='xsd:string'>consulta</key>
                                    <value xsi:type='ns2:Map'>
                                        <item>
                                            <key xsi:type='xsd:string'>Localidad</key>
                                            <value xsi:type='xsd:string'>{localidad}</value>
                                        </item>
                                        <item>
                                            <key xsi:type='xsd:string'>CodigoPostal</key>
                                            <value xsi:type='xsd:string'>{cp}</value>
                                        </item>
                                        <item>
                                            <key xsi:type='xsd:string'>Provincia</key>
                                            <value xsi:type='xsd:string'>{provincia}</value>
                                        </item>
                                    </value>
                                </item>
                            </Consulta>
                        </ns1:ConsultarSucursales>
                    </env:Body>
                </env:Envelope>";
        }

        private string GetCotizacionRequestXML(string contrato, string cpDestino, string peso, string valorDeclarado, string volumen, string sucursalDestino)
        {

            return $@"<?xml version='1.0' encoding='UTF-8'?>
            <env:Envelope
                xmlns:env='http://www.w3.org/2003/05/soap-envelope'
                xmlns:ns1='urn:CotizarEnvio'
                xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
                xmlns:xsd='http://www.w3.org/2001/XMLSchema'
                xmlns:ns2='http://xml.apache.org/xml-soap'
                xmlns:ns3='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'
                xmlns:enc='http://www.w3.org/2003/05/soap-encoding'>
                <env:Header>
                    <ns3:Security env:mustUnderstand='true'>
                        <ns3:UsernameToken>
                            <ns3:Username>{ _username }</ns3:Username>
                            <ns3:Password>{ _password }</ns3:Password>
                        </ns3:UsernameToken>
                    </ns3:Security>
                </env:Header>
                <env:Body>
                    <ns1:CotizarEnvio env:encodingStyle='http://www.w3.org/2003/05/soap-encoding'>
                        <cotizacionEnvio xsi:type='ns2:Map'>
                            <item>
                                <key xsi:type='xsd:string'>cotizacionEnvio</key>
                                <value xsi:type='ns2:Map'>
                                    <item>
                                        <key xsi:type='xsd:string'>CPDestino</key>
                                        <value xsi:type='xsd:string'>{ cpDestino }</value>
                                    </item>
                                    <item>
                                        <key xsi:type='xsd:string'>Cliente</key>
                                        <value xsi:type='xsd:string'>{ _codigoCliente }</value>
                                    </item>
                                    <item>
                                        <key xsi:type='xsd:string'>Contrato</key>
                                        <value xsi:type='xsd:string'>{ contrato }</value>
                                    </item>
                                    <item>
                                        <key xsi:type='xsd:string'>Peso</key>
                                        <value xsi:type='xsd:int'>{ peso }</value>
                                    </item>
                                    <item>
                                        <key xsi:type='xsd:string'>SucursalRetiro</key>
                                        <value xsi:type='xsd:string'>{ sucursalDestino }</value>
                                    </item>
                                    <item>
                                        <key xsi:type='xsd:string'>Volumen</key>
                                        <value xsi:type='xsd:int'>{ volumen }</value>
                                    </item>
                                    <item>
                                        <key xsi:type='xsd:string'>ValorDeclarado</key>
                                        <value xsi:type='xsd:int'>{ valorDeclarado }</value>
                                    </item>
                                </value>
                            </item>
                        </cotizacionEnvio>
                    </ns1:CotizarEnvio>
                </env:Body>
            </env:Envelope>";
        }

        #endregion
    }
}
