﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace eCommerce.Services.Services.Andreani
{
    public class AndreaniHelpers
    {
        //protected const string _codigoCliente = "CL0003750";
        //protected const string _contratoEnvioSucursal = "400006711";
        //protected const string _contratoEnvioEstandarDomicilio = "400006709";
        //protected const string _contratoEnvioUrgenteDomicilio = "400006710";
        //protected const string _codigoCliente = "12006587";
        //protected const string _contratoEnvioSucursal = "400013151";
        //protected const string _contratoEnvioEstandarDomicilio = "400013147";
        //protected const string _contratoEnvioUrgenteDomicilio = "400013149";
        protected readonly string _codigoCliente = ConfigurationManager.AppSettings["AndreaniCodigoCliente"];
        protected readonly string _contratoEnvioSucursal = ConfigurationManager.AppSettings["AndreaniContratoSucursal"];
        protected readonly string _contratoEnvioEstandarDomicilio = ConfigurationManager.AppSettings["AndreaniContratoEstandarDomicilio"];
        protected readonly string _contratoEnvioUrgenteDomicilio = ConfigurationManager.AppSettings["AndreaniContratoUrgenteDomicilio"];

        protected readonly string _username = ConfigurationManager.AppSettings["AndreaniUsername"];
        protected readonly string _password = ConfigurationManager.AppSettings["AndreaniPassword"];


        protected string GetContrato(int tipoEnvio)
        {
            switch (tipoEnvio)
            {
                default:
                case (int)AndreaniTipoEnvio.Sucursal:
                    return _contratoEnvioSucursal;
                case (int)AndreaniTipoEnvio.DomicilioEstandar:
                    return _contratoEnvioEstandarDomicilio;
                case (int)AndreaniTipoEnvio.DomicilioUrgente:
                    return _contratoEnvioUrgenteDomicilio;
            }
        }

        protected string CalcularVolumen(string alto, string ancho, string largo, decimal total)
        {
            if (total < 3000)
            {
                alto = "15";
                ancho = "29";
                largo = "23";
            }
            else if (total < 5000)
            {
                alto = "23";
                ancho = "27";
                largo = "46";
            }
            else if (total < 10000)
            {
                alto = "37";
                ancho = "34";
                largo = "45";
            }
            else /*if (total < 20000)*/
            {
                alto = "50";
                ancho = "60";
                largo = "40";
            }

            //Devuelve el valor en cm^3
            var vol = float.Parse(alto) * float.Parse(ancho) * float.Parse(largo);
            return Math.Round(vol, 0, MidpointRounding.AwayFromZero).ToString();
        }

        protected string CalcularPesoAforado(string volumen)
        {
            //Devuelve el valor en gr.
            //Formula sacada del documento de Web Service de Andreani.
            var peso = decimal.Parse(volumen) * decimal.Parse("3,50") / decimal.Parse("10000");

            return (Math.Truncate(100 * peso) / 100).ToString().Replace(",", ".");
            //return Math.Round(peso, 0, MidpointRounding.AwayFromZero).ToString();
        }
    }
}
