﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Services.Services.Andreani
{
    public class AndreaniEnvioViewModel
    {
        public string CP { get; set; }
        public string Provincia { get; set; }
        public string Localidad { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Piso { get; set; }
        public string Departamento { get; set; }

        public string Email { get; set; }
        public string NombreCompleto { get; set; }
        public string Celular { get; set; }
        public string TipoDocumento { get; set; }
        public string Documento { get; set; }
        public string Telefono { get; set; }

        public string Peso { get; set; }
        public string Volumen { get; set; }

        public string SucursalCliente { get; set; }
        public string SucursalDestino { get; set; }

        public int TipoEnvio { get; set; }
        public string ValorDeclarado { get; set; }

        public string Alto { get; set; }
        public string Ancho { get; set; }
        public string Largo { get; set; }

        public int DeliveryOrderId { get; set; }

        public decimal Total { get; set; }

    }
}
