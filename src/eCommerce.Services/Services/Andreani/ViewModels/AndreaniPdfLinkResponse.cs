﻿using eCommerce.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace eCommerce.Services.Services.Andreani
{
    public class AndreaniPdfLinkReponse
    {
        public AndreaniStatus Estado { get; set; }
        public string PdfLink { get; set; }
        public string Error { get; set; }
    }
}
