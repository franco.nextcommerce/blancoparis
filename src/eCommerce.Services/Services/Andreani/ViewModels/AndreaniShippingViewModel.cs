﻿using eCommerce.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace eCommerce.Services.Services.Andreani
{
    public class AndreaniShippingViewModel
    {
        public SelectList Sucursales { get; set; }
        public DeliveryOrder DeliveryOrder { get; set; }
        public Customer Customer { get; set; }

        public decimal ValorDeclarado { get; set; }
    }
}
