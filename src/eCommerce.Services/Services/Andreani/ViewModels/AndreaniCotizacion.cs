﻿using eCommerce.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace eCommerce.Services.Services.Andreani
{
    public class AndreaniCotizacion
    {
        public string CategoriaDistancia { get; set; }
        public string CategoriaDistanciaId { get; set; }
        public string CategoriaPeso { get; set; }
        public string CategoriaPesoId { get; set; }
        public string PesoAforado { get; set; }
        public string Tarifa { get; set; }
    }
}
