﻿using System;
using System.Collections.Generic;

namespace eCommerce.Services.Services.Andreani
{
    public interface IAndreaniService
    {
        List<AndreaniSucursal> GetSucursales(string cp = "", string localidad = "", string provincia = "");
        AndreaniCotizacion CotizarEnvio(int tipoEnvio, string cpDestino, string alto, string ancho, string largo, string valorDeclarado, string sucursalDestino, decimal total);
        AndreaniPdfLinkReponse GetImprimirEtiquetaLink(string numero);
    }
}
