﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.ServiceModel;
using eCommerce.Services.ANDREANI_ImposicionRemotaService;
using eCommerce.Services.ANDREANI_EstadoEnviosService;
using eCommerce.Services.Model;

namespace eCommerce.Services.Services.Andreani
{
    public class AndreaniService : AndreaniHelpers, IAndreaniService
    {
        ImposicionRemotaClient _imposicionRemotaClient;
        ServiceClient _estadoEnviosService;
        AndreaniWebRequestService _webRequestService;

        public AndreaniService()
        {
            this._imposicionRemotaClient = new ImposicionRemotaClient();
            this._estadoEnviosService = new ServiceClient();
            this._webRequestService = new AndreaniWebRequestService(_codigoCliente, _username, _password);

            _imposicionRemotaClient.ClientCredentials.UserName.UserName = _username;
            _imposicionRemotaClient.ClientCredentials.UserName.Password = _password;
            _estadoEnviosService.ClientCredentials.UserName.UserName = _username;
            _estadoEnviosService.ClientCredentials.UserName.Password = _password;
        }

        public List<AndreaniSucursal> GetSucursales(string cp = "", string localidad = "", string provincia = "")
        {
            return _webRequestService.GetSucursales(cp, localidad, provincia);
        }

        public AndreaniSucursal GetSucursal(string numeroSucursal)
        {
            return _webRequestService.GetSucursales().FirstOrDefault(x => x.Sucursal == numeroSucursal);
        }

        public AndreaniCotizacion CotizarEnvio(int tipoEnvio, string cpDestino, string alto, string ancho, string largo, string valorDeclarado, string sucursalDestino, decimal total)
        {
            var volumen = CalcularVolumen(alto, ancho, largo, total); //En cm^3
            var peso = CalcularPesoAforado(volumen); //En gr

            var contrato = GetContrato(tipoEnvio);

            return _webRequestService.CotizarEnvio(contrato, cpDestino, peso, valorDeclarado, volumen, sucursalDestino);
        }

        public AndreaniPdfLinkReponse GetImprimirEtiquetaLink(string numero)
        {
            try
            {
                var etiquetaParamList = new List<ParamImprimirConstancia>();

                etiquetaParamList.Add(new ParamImprimirConstancia()
                {
                    NumeroAndreani = numero
                });

                var result = _imposicionRemotaClient.ImprimirConstancia(etiquetaParamList);
                var response = new AndreaniPdfLinkReponse()
                {
                    Estado = AndreaniStatus.Ok,
                    PdfLink = result.First().PdfLinkFile
                };
                return response;
            }
            catch (FaultException exception)
            {
                var response = new AndreaniPdfLinkReponse()
                {
                    Estado = AndreaniStatus.Error,
                    Error = exception.Message
                };
                return response;
            }
        }

        public AndreaniEnvioResponse GenerarEnvio(AndreaniEnvioViewModel model, AndreaniCotizacion cotizacion)
        {
            try
            {
                var volumen = CalcularVolumen(model.Alto, model.Ancho, model.Largo, model.Total);
                var peso = CalcularPesoAforado(volumen);

                if (string.IsNullOrEmpty(model.Calle)) model.Calle = " ";
                if (string.IsNullOrEmpty(model.Numero)) model.Numero = " ";

                var paramGenerarEnvio = new ParamConfirmarCompra()
                {
                    Contrato = GetContrato(model.TipoEnvio),
                    CodigoPostalDestino = model.CP,
                    Provincia = model.Provincia,
                    Localidad = model.Localidad,
                    Calle = model.Calle,
                    Numero = model.Numero,
                    Piso = model.Piso,
                    Departamento = model.Departamento,

                    Email = model.Email,
                    NombreApellido = model.NombreCompleto,
                    NumeroCelular = model.Celular,
                    TipoDocumento = model.TipoDocumento,
                    NumeroDocumento = model.Documento,
                    NumeroTelefono = model.Telefono,

                    Volumen = volumen,
                    Peso = peso,

                    CategoriaDistancia = cotizacion.CategoriaDistanciaId,
                    CategoriaPeso = cotizacion.CategoriaPesoId,

                    SucursalCliente = model.SucursalCliente,
                    SucursalRetiro = model.SucursalDestino,

                    Tarifa = cotizacion.Tarifa
                };

                var envio = _imposicionRemotaClient.ConfirmarCompra(paramGenerarEnvio);

                var result = new AndreaniEnvioResponse()
                {
                    Estado = AndreaniStatus.Ok,
                    NumeroAndreani = envio.NumeroAndreani
                };

                return result;
            }

            catch (FaultException ex)
            {
                var result = new AndreaniEnvioResponse()
                {
                    Estado = AndreaniStatus.Error,
                    Error = ex.Message
                };

                return result;
            }
        }

        public ResultadoAnularEnvios AnularEnvio(string numero)
        {
            try
            {
                var anularEnviosParam = new List<ParamAnularEnvios>();
                anularEnviosParam.Add(new ParamAnularEnvios()
                {
                    NumeroAndreani = numero
                });

                return _imposicionRemotaClient.AnularEnvios(anularEnviosParam).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        //public EnviosRespuesta GetEstadoEnvio(string numero)
        //{
        //    try
        //    {
        //        var envio = new List<Envio1>();
        //        envio.Add(new Envio1()
        //        {
        //            IdentificadorCliente = _codigoCliente,
        //            NumeroAndreani = numero                    
        //        });
        //        var estadoParam = new EnviosConsultas()
        //        {
        //            CodigoCliente = _codigoCliente,
        //            Envios = envio
        //        };

        //        return _estadoEnviosService.ObtenerEstadoDistribucionCodificado(estadoParam);                
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }

        //    return "";
        //}

    }
}
