﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace eCommerce.Services.Arquitecture
{
    public class AjaxOrChildRequestAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var valid = filterContext.Controller.ControllerContext.RequestContext.RouteData.DataTokens.ContainsKey("ParentActionViewContext")
                || filterContext.Controller.ControllerContext.RequestContext.HttpContext.Request.IsAjaxRequest();

            if (!valid) throw new UnauthorizedAccessException();
        }
    }
}
