﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Services.Domain
{
    public class ShoppigCartValidation
    {
        
        public bool IsApproved {  get; private set; }
        public string Message { get; private set; }

        public void Approve()
        {
            IsApproved = true;
            Message = string.Empty;
        }

        public void Reprove(string reason)
        {
            IsApproved = false;
            Message = reason;
        }

        public List<NoStockItem> NoStockItems { get; set; }
    }

    public class NoStockItem
    {
        public int productId { get; set; }
        public int ColorId { get; set; }
        public int SizeId { get; set; }
    }
}
