﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCommerce.Services.Model;

namespace eCommerce.Services.Domain
{
    public class ShoppingCart
    {
        public ShoppingCart()
        {
            Productos = new List<Product>();
        }

        public IEnumerable<Product> Productos { get; set; }
        public Guid Guid { get; set; }
    }
}
