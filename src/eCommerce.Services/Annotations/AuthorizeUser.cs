﻿using System.Web;
using System.Web.Mvc;

namespace eCommerce.Services.Annotations
{

    public class AuthorizeUserAttribute : AuthorizeAttribute
    {   
        protected override bool AuthorizeCore(HttpContextBase httpContext) 
        {
            if (!httpContext.User.Identity.IsAuthenticated)
                return false;

            return httpContext.User.IsInRole(Roles);
        }
    }
}
