﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eCommerce.Services.Common
{
    public enum ActionType
    {
        Back,
        Home,
        Catalogue,
        ShoppingCart
    }
}
