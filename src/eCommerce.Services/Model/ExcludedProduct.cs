//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eCommerce.Services.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExcludedProduct
    {
        public int ProductExcludedPromotionId { get; set; }
        public int ProductId { get; set; }
        public int PromotionId { get; set; }
    
        public virtual Product Product { get; set; }
        public virtual Promotion Promotion { get; set; }
    }
}
