//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eCommerce.Services.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class OcaShipment
    {
        public int OcaShipmentId { get; set; }
        public int DeliveryOrderId { get; set; }
        public int OriginTypeId { get; set; }
        public int DestionationTypeId { get; set; }
        public string OrdenRetiro { get; set; }
        public string NumeroEnvio { get; set; }
        public System.DateTime Date { get; set; }
        public bool Active { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public string DeletedBy { get; set; }
    
        public virtual DeliveryOrder DeliveryOrder { get; set; }
    }
}
