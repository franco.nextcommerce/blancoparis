//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eCommerce.Services.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class BankInterest
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BankInterest()
        {
            this.Orders = new HashSet<Order>();
        }
    
        public int BankInterestId { get; set; }
        public int BankId { get; set; }
        public string Name { get; set; }
        public int Installments { get; set; }
        public bool Active { get; set; }
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public string DeletedBy { get; set; }
        public Nullable<decimal> Percent { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
        public Nullable<int> StoreType { get; set; }
        public Nullable<int> MinimumAmount { get; set; }
    
        public virtual Bank Bank { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Orders { get; set; }
    }
}
