//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eCommerce.Services.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class OcaShipmentOrigin
    {
        public int OcaShipmentOriginId { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }
        public string Floor { get; set; }
        public string Department { get; set; }
        public string ZipCode { get; set; }
        public int ProvinceId { get; set; }
        public string City { get; set; }
        public string ContactName { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public System.DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public string DeletedBy { get; set; }
        public int OcaCenterId { get; set; }
    
        public virtual Province Province { get; set; }
    }
}
