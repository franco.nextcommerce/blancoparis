//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eCommerce.Services.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Category
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Category()
        {
            this.Category1 = new HashSet<Category>();
            this.CategoryPromotions = new HashSet<CategoryPromotion>();
            this.Products = new HashSet<Product>();
        }
    
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public Nullable<int> ParentCategoryId { get; set; }
        public string Image { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
        public bool ShowInHome { get; set; }
        public bool ShowInHeader { get; set; }
        public string CompleteName { get; set; }
        public bool Active { get; set; }
        public System.DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public string DeletedBy { get; set; }
        public bool Enabled { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public Nullable<decimal> Heigth { get; set; }
        public Nullable<decimal> Long { get; set; }
        public Nullable<decimal> Wide { get; set; }
        public string SeoUrl { get; set; }
        public string LargeDescription { get; set; }
        public string MetaDescription { get; set; }
        public string SeoUrlSelf { get; set; }
        public string Description { get; set; }
        public Nullable<int> SizeTableTypeId { get; set; }
        public bool MenuClickeable { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Category> Category1 { get; set; }
        public virtual Category Category2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CategoryPromotion> CategoryPromotions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Product> Products { get; set; }
    }
}
