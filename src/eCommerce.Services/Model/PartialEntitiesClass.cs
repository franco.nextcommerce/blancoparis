﻿using eCommerce.Services.Model.Enums;
using eCommerce.Services.Utils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Web;
using eCommerce.Services.Security;
using System;

namespace eCommerce.Services.Model
{

    #region Category

    public partial class Category
    {

        public Category ParentCategory
        {
            get
            {
                return this.Category2;
            }
            set
            {
                this.Category2 = value;
            }

        }

        public ICollection<Category> ChildCategories
        {
            get
            {
                return this.Category1;
            }
            set
            {
                this.Category1 = value;
            }

        }

        public string GetCompleteName
        {
            get
            {
                return ObtainCompleteName(this);
            }

        }

        public string SeoUrlEditable
        {
            get
            {
                if (this.ParentCategoryId.HasValue)
                {
                    return this.SeoUrl.Replace(this.ParentCategory.SeoUrl + "-", "");
                }
                else
                {
                    return this.SeoUrl;
                }
            }
        }

        private string ObtainCompleteName(Category category)
        {
            if (category.ParentCategory == null)
            {
                return category.Name;
            }
            else
            {
                return ObtainCompleteName(category.ParentCategory) +
                    " > " + category.Name;
            }
        }

        public string FormattedName
        {
            get
            {
                if (string.IsNullOrEmpty(Name)) return null;

                //HttpContext.Current.Server.UrlEncode(destinationURL);
                return Name.RemoveDiacritics().Replace(' ', '-').ToLower();
            }
        }
    }

    #endregion

    #region Image

    public partial class Image
    {
        public string URL { get; set; }

        public string IdImage
        {
            get
            {
                return this.ImageName.Split('.')[0];
            }

        }

    }

    public partial class StoryImage
    {
        public string URL { get; set; }

        public string IdImage
        {
            get
            {
                return this.ImageName.Split('.')[0];
            }
        }

    }

    #endregion Beneciary

    #region Transaction

    public partial class Transaction
    {
        public string ChecksIds
        {
            get
            {
                return string.Join(",", this.Checks.Where(x => x.Deleted == null).Select(c => c.CheckId));
            }
        }

        public ICollection<Check> PaymentChecks
        {
            get
            {
                return this.Checks1;
            }
        }

        public ICollection<Check> ChargeChecks
        {
            get
            {
                return this.Checks2;
            }
        }
       
    }

    #endregion Transaction

    #region Product

    public partial class Product
    {
        public string DifferenceWholesalePricePercentage
        {
            get
            {
                if (this.OldWholesalePrice.HasValue)
                {
                    var number = (this.OldWholesalePrice.Value - this.WholesalePrice) / this.OldWholesalePrice.Value * 100;
                    var percentage = number.Value;
                    return Math.Round((decimal.ToDouble(percentage)), 2).ToString();
                } else
                {
                    return null;
                }

            }
        }

        public int Quantity { get; set; }

        public string FormattedTitle
        {
            get
            {
                if (string.IsNullOrEmpty(Title)) return null;

                //HttpContext.Current.Server.UrlEncode(destinationURL);
                return Title.RemoveDiacritics().Replace(' ', '-').Replace("#", "").Replace("/", "-").ToLower();
            }
        }

        public string DefaultImageUrl { get; set; }
        public string getDefaultImageUrl
        {
            get
            {
                return (this.Images != null && this.Images.Any(i => i.Default)) ?
                    string.Format("products/{0}/{1}",
                        this.ProductId,
                        this.Images.FirstOrDefault(i => i.Default).ImageName) : "products/DefaultProductImage.jpeg";
            }
        }
        public string getDefaultResizedImageUrl
        {
            get
            {
                return (this.Images != null && this.Images.Any(i => i.Default)) ?
                    string.Format("products/resized/{0}/{1}",
                        this.ProductId,
                        this.Images.FirstOrDefault(i => i.Default).ImageName) : "products/DefaultProductImage.jpeg";
            }
        }

        public string getResizedImageUrlByColor(int colorId)
        {
            return (this.Images != null && this.Images.FirstOrDefault(i => i.ColorId == colorId) != null) ?
                string.Format("products/resized/{0}/{1}",
                    this.ProductId,
                    this.Images.FirstOrDefault(i => i.ColorId == colorId).ImageName) : getDefaultResizedImageUrl;
        }

        public decimal? SpecialPrice { get; set; }

        public decimal? OldSpecialPrice { get; set; }

        public decimal CustomerPrice
        {
            get
            {
                decimal? price;
               
                    price = this.WholesalePrice;


                if (price.HasValue)
                    return price.Value;
                else
                    return 0;
            }
        }

        public decimal? CustomerOldPrice
        {
            get
            {
                decimal? price;
                
                price = this.OldWholesalePrice;

                return price;
            }
        }

        //public decimal? CustomerOldPrice
        //{
        //    get
        //    {
        //        //if (this.OldSpecialPrice.HasValue)
        //        //    return this.OldSpecialPrice.Value;
        //        //else if (this.SpecialPrice.HasValue)
        //        //    return null;

        //        if (!HttpContext.Current.User.Identity.IsAuthenticated)
        //        {
        //            if (this.OldRetailPrice.HasValue)
        //                return this.OldRetailPrice.Value;
        //            else
        //                return null;
        //        }
        //        else
        //        {
        //            decimal? price = this.OldRetailPrice;
        //            if (HttpContext.Current.User.Identity is CustomIdentity)
        //            {
        //                switch ((PriceListEnum)((CustomIdentity)HttpContext.Current.User.Identity).CustomerPriceList)
        //                {
        //                    default:
        //                    case PriceListEnum.Minorista:
        //                        price = this.OldRetailPrice;
        //                        break;

        //                    case PriceListEnum.Mayorista:
        //                        price = this.OldWholesalePrice;
        //                        break;
        //                }
        //            }

        //            if (price.HasValue)
        //                return price.Value;
        //            else
        //                return null;
        //        }

        //    }
        //}
    }

    #endregion Product

    #region NewsletterProduct

    public partial class NewsletterProduct
    {
        public string DifferenceSpecialPricePercentage
        {
            get
            {
                if (this.OldSpecialPrice.HasValue)
                {
                    var number = (this.OldSpecialPrice.Value - this.SpecialPrice) / this.OldSpecialPrice.Value * 100;
                    var percentage = number.Value;
                    return Math.Round((decimal.ToDouble(percentage)), 2).ToString();
                }
                else
                {
                    return null;
                }

            }
        }

    }

    #endregion NewsletterProduct

    #region Purchase

    public partial class Purchase
    {
        public decimal TotalAmountPaid
        {
            get
            {
                return this.Transactions.Where(x => x.TransactionTypeId == (int)TransactionTypeEnum.Egreso && x.AccountId.HasValue && x.Amount > 0 && x.Deleted == null).Sum(x => x.Amount);
            }
        }

        public decimal PendingAmountToPay
        {
            get
            {
                return this.Total - this.TotalAmountPaid;
            }
        }

        public bool HasPendingDeliveries
        {
            get
            {
                return this.PurchaseDetails.Any(x => x.HasPendingDeliveries);
            }

        }

        public bool hasPayments
        {
            get
            {
                return this.Transactions.Any(x => x.TransactionTypeId == (int)TransactionTypeEnum.Egreso && x.AccountId.HasValue && x.Deleted == null);
            }
        }

        public bool hasDeliveries
        {
            get
            {
                return this.DeliverySuppliers.Any(delivery => delivery.Deleted == null);
            }
        }

        public bool IsAllowedToEditOrDeletePurchase
        {
            get
            {
                return !this.hasPayments && !this.hasDeliveries;
            }

        }

        public bool IsTotallyPaid
        {
            get
            {
                return this.PendingAmountToPay <= 0;
            }
        }

        public string PaymentStatus
        {
            get
            {
                if (this.IsTotallyPaid)
                {
                    return "Pagado";

                }

                if (this.hasPayments)
                {
                    return "Pago Parcial";
                }

                return "A Pagar";
            }
        }

        public string DeliveryStatus
        {
            get
            {
                if (!this.HasPendingDeliveries)
                {
                    return "Entregado";
                }

                if (this.hasDeliveries)
                {
                    return "Entrega Parcial";
                }

                return "A Entregar";
            }
        }
    }

    #endregion Purchase

    #region PurchaseDetail

    public partial class PurchaseDetail
    {
        public bool HasPendingDeliveries
        {
            get
            {
                return this.PendingDeliveries > 0;
            }
        }

        public int PendingDeliveries
        {
            get
            {
                return this.Quantity - this.DeliverySuppliersDetails.Where(x => x.DeliverySupplier.Deleted == null).Sum(x => x.Quantity);
            }
        }

        public string CompleteName
        {
            get
            {
                return this.Product.Code + " - " + this.Product.Title;
            }
        }

    }

    #endregion PurchaseDetail

    #region Order

    public partial class Order
    {
        public int PaymentMethodId_Decidir { get; set; }
        public decimal TotalAmountPaid
        {
            get
            {
                return this.Transactions.Where(x => x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso && x.Deleted == null).Sum(x => x.Amount);
            }
        }

        public decimal PendingAmountToPay
        {
            get
            {
                return this.OrderTotal - this.TotalAmountPaid;
            }
        }

        public bool HasPendingDeliveries
        {
            get
            {
                return this.OrderDetails.Any(x => x.HasPendingDeliveries);
            }

        }

        public bool hasPayments
        {
            get
            {
                return this.Transactions.Any(x => x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso && x.Deleted == null);
                //return this.Transactions.Where(x => x.TransactionTypeId == (int)TransactionTypeEnum.Ingreso && x.Deleted == null).Count() != 0;
            }
        }

        public bool hasDeliveries
        {
            get
            {
                return this.DeliveryOrders.Where(delivery => delivery.Deleted == null).Count() != 0;
            }
        }

        public bool IsAllowedToEditOrDeletePurchase
        {
            get
            {
                return !this.hasPayments && !this.hasDeliveries;
            }

        }

        public bool IsTotallyPaid
        {
            get
            {
                return this.PendingAmountToPay <= 0;
            }
        }

        public string PaymentStatus
        {
            get
            {
                if (this.IsTotallyPaid)
                {
                    return "Cobrado";
                }

                if (this.hasPayments)
                {
                    return "Cobro Parcial";
                }

                return "A Cobrar";
            }
        }

        public string DeliveryStatus
        {
            get
            {
                if (!this.HasPendingDeliveries)
                {
                    return "Entregado";
                }

                if (this.hasDeliveries)
                {
                    return "Entrega Parcial";
                }

                return "A Entregar";
            }
        }

        public decimal SubTotal
        {
            get
            {
                var subtotal = this.OrderDetails.Where(x => x.Active).Sum(x => x.Price * x.Quantity * (x.Discount.HasValue ? ((100 - x.Discount.Value) / 100) : 1));
                

                return subtotal;
            }
        }

        public decimal SubTotalWithDiscount
        {
            get
            {
                if (this.Discount_Percent.HasValue && this.Discount.HasValue)
                    return this.SubTotal - this.Discount.Value;
                else
                    return SubTotal;
            }
        }
        

        public decimal OrderTotal
        {
            get
            {
                var total = this.SubTotalWithDiscount;

                if (this.IVA.HasValue)
                    total += this.IVA.Value;
                
                if (this.Surcharge.HasValue && this.SurchargeAmount.HasValue)
                {
                    //total = Math.Round(total / Convert.ToDecimal((100 - this.Surcharge.Value) / 100), 2);
                    total += this.SurchargeAmount.Value;
                }

                return total;
            }
        }

        public decimal OrderTotalWithShippingCost
        {
            get
            {
                var total = this.OrderTotal;

                if (ShippingCost.HasValue)
                    total += ShippingCost.Value;

                return total;
            }
        }
        public bool IsFreeShipping { get; set; }


        public bool IsAvailableForOcaShipment
        {
            get
            {
                return (this.DeliveryMethodId == (int)DeliveryMethodEnum.OcaPaP || this.DeliveryMethodId == (int)DeliveryMethodEnum.OcaPaS);
            }

        }
    }

    #endregion Order

    #region OrderDetail

    public partial class OrderDetail
    {
        public bool HasPendingDeliveries
        {
            get
            {
                return this.PendingDeliveries > 0;
            }
        }

        public int PendingDeliveries
        {
            get
            {
                return this.Quantity - this.DeliveryOrderDetails.Where(x => x.DeliveryOrder.Deleted == null).Sum(x => x.Quantity);
            }
        }

        public string CompleteName
        {
            get
            {
                return this.Product.Code + " - " + this.Product.Title;
            }
        }

        public decimal Price_with_discount
        {
            get
            {
                if (this.Discount.HasValue)
                    return this.Price - this.Price * (this.Discount.Value / 100);
                else
                    return Price;
            }
        }

        public decimal Price_with_order_discount
        {
            get
            {
                if (this.Order.Discount.HasValue)
                    return this.Price_with_discount - this.Price_with_discount * (this.Order.Discount_Percent.Value / 100);
                else
                    return Price_with_discount;
            }
        }

        public decimal Precio_Unitario_Final
        {
            get 
            {
                var final_price = this.Price_with_order_discount;

                if (this.Order.Surcharge.HasValue)
                    final_price += final_price * (this.Order.Surcharge.Value / 100);

                    return final_price;
            }
        }

        public decimal SubTotal
        {
            get
            {
                return this.Price * this.Quantity;
            }
        }

        public decimal SubTotalWithDiscount
        {
            get
            {
                var value = this.SubTotal;

                if (this.Discount.HasValue)
                    return this.SubTotal - this.SubTotal * (this.Discount.Value / 100);
                else
                    return this.SubTotal;
            }
        }

        //public decimal? Iva_amount
        //{
        //    get
        //    {
        //        var value = this.SubTotalWithDiscount;

        //        if (this.Order.Discount.HasValue)
        //            value -= value * (this.Order.Discount_Percent.Value / 100);

        //        if (this.Order.IVA_Percent.HasValue)
        //            return value * (this.Order.IVA_Percent.Value / 100);
                
        //        return null;
        //    }
        //}

        //public decimal? Surcharge_amount
        //{
        //    get
        //    {
        //        var value = this.SubTotalWithDiscount;

        //        if (this.Order.Discount.HasValue)
        //            value -= value * (this.Order.Discount_Percent.Value / 100);

        //        if (this.Order.Surcharge.HasValue)
        //        {
        //            value = this.Iva_amount.HasValue ? value + this.Iva_amount.Value : value;
        //            return value * (this.Order.Surcharge.Value / 100);
        //        }
        //        else
        //            return null;
        //    }
        //}

        public decimal Total_with_concepts
        {
            get
            {
                return Precio_Unitario_Final * this.Quantity;
            }
        }


    }

    #endregion

    #region Banner

    public partial class Banner
    {

        public string BannersFileUploadDirectory
        {

            get
            {
                return "/Content/UploadDirectory/Banners/";
            }
        }

        public string DefaultBannerImage
        {
            get
            {
                return "DefaultBannerImage.jpeg";
            }
        }

        public string ImageUrl
        {
            get
            {
                return this.Image != null ?
                    Path.Combine(this.BannersFileUploadDirectory, this.Image)
                    : "";
            }
        }

        public string ImageMobileUrl
        {
            get
            {
                return this.ImageMobile != null ?
                    Path.Combine(this.BannersFileUploadDirectory, this.ImageMobile)
                    : "";
            }
        }

        public string BackImageUrl
        {
            get
            {
                return this.Image != null ?
                    Path.Combine(Utility.ImgUrl, "Banners", this.Image)
                    : "";
            }
        }
    }

    #endregion Banner

    #region Checks

    [MetadataType(typeof(CheckMetadata))]
    public partial class Check
    {
        public bool Active { get; set; }

        public Transaction PaymentTransaction
        {
            get
            {
                return this.Transaction1;
            }
        }

        public Transaction ChargeTransaction
        {
            get
            {
                return this.Transaction2;
            }
        }

        public string EntregadoA
        {
            get
            {
                if (this.Status == (int)CheckStatus.Entregado)
                {
                    if (this.TransactionId.HasValue)
                        return this.Transaction.TransactionCategory.Name;

                    if (this.PaymentTransactionId.HasValue)
                        return this.PaymentTransaction.Purchase.Supplier.Name;
                }

                return string.Empty;
            }
        }

        public string Dueño
        {
            get
            {
                return this.Own ? "Propio" : "Tercero";
            }
        }

        public string RecibidoDe
        {
            get
            {
                if (this.Own)
                {
                    return "SB";
                }
                else
                {
                    if (this.ChargeTransactionId.HasValue)
                    {
                        if (this.ChargeTransaction.Order != null && this.ChargeTransaction.Order.Customer != null)
                        {
                            return this.ChargeTransaction.Order.Customer.FullName;
                        }
                        else
                            return string.Empty;
                    }

                    if (this.TransactionId.HasValue)
                    {
                        return this.Transaction.TransactionCategory.Name;
                    }

                    return string.Empty;
                }

            }
        }
    }

    public class CheckMetadata
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CheckId { get; set; }
    }




    #endregion

    #region Users / Operators

    public partial class USER
    {
        public string AssignedRolesNames
        {
            get
            {
                var roles = this.ROLES.Select(x => x.RoleName).ToList();
                var roles_names = string.Join(", ", roles);

                return roles_names;
            }

        }
    }

    #endregion

    #region Refund
    public partial class Refund
    {
        public int Quantity
        {
            get
            {
                return this.RefundDetails.Sum(x => x.Quantity);
            }
        }
    }
    #endregion

    #region RefundDetail
    public partial class RefundDetail
    {
        public decimal SubTotal
        {
            get
            {
                //return (this.Price.HasValue ? this.Price.Value * this.Quantity : this.Product.CustomerPrice * this.Quantity);
                return (this.Price.HasValue ? this.Price.Value * this.Quantity : 0);
            }
        }

        public decimal SubTotalWithDiscount
        {
            get
            {
                if (this.Discount.HasValue)
                    return this.SubTotal - (this.SubTotal * (this.Discount.Value / 100));
                else
                    return SubTotal;
            }
        }

        public decimal Price_with_discount
        {
            get
            {
                if (this.Discount.HasValue)
                    return this.Price.Value - this.Price.Value * (this.Discount.Value / 100);
                else
                    return this.Price != null? this.Price.Value : 0;
            }
        }

        public decimal Price_with_order_discount
        {
            get
            {
                if (this.Refund.Discount.HasValue)
                    return this.Price_with_discount - this.Price_with_discount * (this.Refund.Discount_Percent.Value / 100);
                else
                    return this.Price_with_discount;
            }
        }

        public decimal Precio_Unitario_Final
        {
            get
            {
                var final_price = this.Price_with_order_discount;

                if (this.Refund.Surcharge.HasValue)
                    final_price += final_price * (this.Refund.Surcharge.Value / 100);

                return final_price;
            }
        }

    }
    #endregion

    #region Exchange
    public partial class Exchange
    {
        public int ReturnedQuatity
        {
            get
            {
                return this.ExchangeDetails.Where(x => x.ExchangeDetailTypeId == (int)ExchangeDetailTypeEnum.Ingreso)
                    .Select(x => x.Quantity).DefaultIfEmpty(0).Sum();
            }
        }

        public int SoldQuatity
        {
            get
            {
                return this.ExchangeDetails.Where(x => x.ExchangeDetailTypeId == (int)ExchangeDetailTypeEnum.Egreso)
                    .Select(x => x.Quantity).DefaultIfEmpty(0).Sum();
            }
        }
    }

    #endregion

    #region Promotion

    public partial class Promotion
    {
        public bool Valid
        {
            get
            {
                return this.StartDate <= CurrentDate.Now && this.EndDate >= CurrentDate.Now;
            }
        }
    }
    #endregion

    #region Coupons

    public partial class Coupon
    {
        public bool Valid
        {
            get
            {
                return this.FromDate <= CurrentDate.Now && this.ExpirationDate >= CurrentDate.Now;
            }
        }
    }
    #endregion
}
