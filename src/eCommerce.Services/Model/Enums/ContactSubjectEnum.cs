﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum ContactSubjectEnum
    {
        [Display(Name = "Recursos Humanos")]
        Recursos_humanos = 0,
        [Display(Name = "Mayorista")]
        Mayorista = 1,
        [Display(Name = "Servicio post venta")]
        Servicio_post_venta = 2,
        [Display(Name = "Cambios")]
        Cambios = 3,
        [Display(Name = "Consultas generales")]
        Consultas_generales = 4,
    }
}