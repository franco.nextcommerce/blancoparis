﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum UserTypeEnum
    {
        Minorista = 1,
        Mayorista = 2,
        Ambas = 3
    }
}