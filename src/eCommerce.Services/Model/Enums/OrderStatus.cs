﻿namespace eCommerce.Services.Model.Enums
{
    public enum OrderStatusEnum
    {
        EnProceso = 1,
        Carrito = 2,
        Confirmado = 4,
        Cancelada = 8,
        Proforma = 9,
        Consigna = 10
    }
}
