﻿namespace eCommerce.Services.Model.Enums
{
    public enum SizeTableTypeEnum
    {
        Top = 1,
        Bottom = 2,
        Jeans = 3,
        Man = 4,
        Woman = 5
    }
}