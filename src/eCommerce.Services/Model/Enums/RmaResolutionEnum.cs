﻿namespace eCommerce.Services.Model.Enums
{
    public enum RmaResolutionEnum
    {
        Reposicion = 1,
        Reparacion = 2,
        A_Cuenta = 3,
        Rechazado = 4
    }
}
