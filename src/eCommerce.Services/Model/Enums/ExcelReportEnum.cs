﻿namespace eCommerce.Services.Model.Enums
{
    public enum ExcelReportEnum
    {
        Sales = 1,
        Charges = 2,
        Refunds = 3,
        Exchanges = 4
    }
}