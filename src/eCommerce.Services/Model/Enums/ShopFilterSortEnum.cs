﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum ShopFilterSortEnum
    {
        Default = 0,
        LowestPrice = 1,
        HighestPrice = 2
    }
}