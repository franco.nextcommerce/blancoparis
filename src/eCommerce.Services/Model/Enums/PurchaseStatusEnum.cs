﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum PurchaseStatusEnum
    {
        Pagado = 1,
        A_Pagar = 2,
        Vencido = 3
    }
}