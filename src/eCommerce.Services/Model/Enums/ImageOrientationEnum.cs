﻿namespace eCommerce.Services.Model.Enums
{
    public enum ImageOrientationEnum
    {
        Portrait = 1,
        Landscape = 2
    }
}