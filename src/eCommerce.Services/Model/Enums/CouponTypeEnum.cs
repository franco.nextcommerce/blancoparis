﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model.Enums;

namespace eCommerce.Services.Model.Enums
{
    public enum CouponTypeEnum
    {
        Minorista = 1,
        Mayorista = 2
    }
}