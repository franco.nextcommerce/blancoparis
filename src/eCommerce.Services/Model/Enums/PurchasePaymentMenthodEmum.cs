﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum PurchasePaymentMenthodEmum
    {
        Efectivo = 1,
        Cheque = 2,
        Transferencia = 3,
        Deposito = 4,
        Pago_de_Proveedor = 5
    }
}