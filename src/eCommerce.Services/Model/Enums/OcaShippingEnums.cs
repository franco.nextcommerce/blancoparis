﻿namespace eCommerce.Services.Model.Enums
{
    public enum OriginShipmentTypeEnum
    {
        Domicilio = 1,
        Sucursal = 2
    }
    
    public enum DestinationShipmentTypeEnum
    {
        Domicilio = 1,
        Sucursal = 2
    }

    public enum CancelShipmentStatusEnum
    {
        Success = 100,
        InvalidUser = 110,
        WrongUser = 120,
        StatusError = 130
    }

    public enum DeleteShipmentStatusEnum
    {
        Success = 1,
        AlreadyDeleted= 2,
        NotCanceled= 3,
        Unknown = 4
    }
}