﻿
namespace eCommerce.Services.Model.Enums
{
    public enum PriceListEnum
    {
        Minorista = 1,
        Mayorista = 2
    }
}