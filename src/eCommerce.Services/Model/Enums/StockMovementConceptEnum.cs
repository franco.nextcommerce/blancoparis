﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum StockMovementConceptEnum
    {
        Edicion = 1,
        Venta = 2
    }
}