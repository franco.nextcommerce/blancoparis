﻿namespace eCommerce.Services.Model.Enums
{
    public enum OrderChannelEnum
    {
        SitioWeb = 1,
        Telefono = 2,
        WhatsApp = 3
    }
}
