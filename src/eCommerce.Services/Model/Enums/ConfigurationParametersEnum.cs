﻿namespace eCommerce.Services.Model.Enums
{
    public enum ConfigurationParametersEnum
    {
        Logo,

        SiteUrl,
        BackOfficeUrl,

        CompanyName,
        CompanyEmail,
        CompanyEmail_User,
        CompanyEmail_Password,
        CompanyPhoneNumber,
        Whatsapp,

        MinimumOrderRetail,
        MinOrder_Amount_Wholesaler,
        MinOrder_Quantity_Retailer,
        MinOrder_Quantity_Wholesaler,

        //ShippingCost_Free_Retailer,
        FreeShippingRetail,
        ShippingCost_Free_Wholesaler,

        ShippingCost_CABA,
        ShippingCost_GBA,
        ShippingCost_Transport,

        PromotionalText,
        PromotionalTextMobile,

        MercadoPago_ClientId,
        MercadoPago_ClientSecret,

        TodoPago_User,
        TodoPago_Login,

        OCA_UserName,
        OCA_CUIT,
        OCA_PAS,
        OCA_PAP,

        ANDREANI_UserName,
        ANDREANI_Password,

        GoogleAnalitycs_Script,
        Facebook_Pixel,
        GoogleAdwords_Script,

        Instagram_Account,
        Instagram_User,
        Instagram_Password,
        Facebook_Account,
        Twitter_Account,
    }
}