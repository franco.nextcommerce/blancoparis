﻿namespace eCommerce.Services.Model.Enums
{
    public enum CheckStatus
    {
        EnCartera = 1,
        Entregado = 2
    }
}