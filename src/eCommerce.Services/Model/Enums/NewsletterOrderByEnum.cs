﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCommerce.Services.Model.Enums;

namespace eCommerce.Services.Model.Enums
{
    public enum NewsletterOrderByEnum
    {
        Default = 1,
        MenorPrecio = 2,
        MayorPrecio = 3,
        MenorStock = 4,
        MayorStock = 5
    }
}