﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum PaymentMethodEnum
    {
        [Display(Name = "A convenir")]
        Convenir = 10,

        MercadoPago = 17,

        [Display(Name = "Transferencia Bancaria / Deposito")]
        TransferenciaBancaria = 1,

        Efectivo = 2,

        Cheque = 5,
        TodoPago = 6,
        Decidir = 7,
        MercadoPagoQR = 8
    }
}