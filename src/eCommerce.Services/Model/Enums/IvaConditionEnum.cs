﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum IvaConditionEnum
    {
        ConsumidorFinal = 1,
        Monotributista = 2,
        ResponsableInscripto = 3,
        Exento = 4
    }
}