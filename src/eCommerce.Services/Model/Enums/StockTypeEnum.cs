﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum StockTypeEnum
    {
        Virtual = 1,
        Real = 2
    }
}