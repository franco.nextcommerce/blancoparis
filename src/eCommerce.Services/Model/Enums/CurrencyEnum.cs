﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum CurrencyEnum
    {
        Peso = 1,
        Dolar = 2
    }
}