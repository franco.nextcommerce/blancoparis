﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum PromotionTypeEnum
    {
        Escala = 1,
        Dividida = 2,
        Cantidad = 3,
        Articulos = 4,
        Porcentaje = 5
    }
}