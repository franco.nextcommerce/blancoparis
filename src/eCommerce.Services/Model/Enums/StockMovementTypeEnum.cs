﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum StockMovementTypeEnum
    {
        Ingreso = 1,
        Egreso = 2
    }
}