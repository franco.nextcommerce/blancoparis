﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum ProductSKU_Origin
    {
        Purchase = 0,
        Sale = 1
    }

    public enum ProductSKU_LabelType
    {
        Master = 0,
        Product = 1
    }
}