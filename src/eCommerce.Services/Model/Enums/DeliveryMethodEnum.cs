﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum DeliveryMethodEnum
    {
        [Display(Name = "Retira en Centro de Entregas")]
        RetiraEnCentroDeEntrega = 1,

        [Display(Name = "OCA - Envío a domicilio")]
        OcaPaP = 16,

        [Display(Name = "Moto Entrega")]
        MotoEntrega = 3,

        [Display(Name = "OCA - Envío a sucursal")]
        OcaPaS = 4,

        [Display(Name = "Transporte")]
        Transporte = 3,

        [Display(Name = "A convenir")]
        AConvenir = 2,

        [Display(Name = "Andreani - Sucursal")]
        AndreaniS = 8,

        [Display(Name = "Andreani - Domicilio")]
        AndreaniD = 9,


        [Display(Name = "Retira en Local")]
        RetiraLocal = 13,

        [Display(Name = "A domicilio CABA")]
        DomicilioCaba = 11,

        [Display(Name = "A domicilio GBA")]
        DomicilioGba = 12,

        [Display(Name = "A domicilio Resto del país")]
        DomicilioProv = 14,

    }
}