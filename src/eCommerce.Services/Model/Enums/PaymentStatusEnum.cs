﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum PaymentStatusEnum
    {
        Pendiente = 1,
        Cobrada = 2,
        Pagada = 3,
        CobroParcial = 4
    }
}