﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum StoreTypeEnum
    {
        [Display(Name = "No Publicado")]
        NotPublished = 1,
        [Display(Name = "Solo Minorista")]
        RetailerOnly = 2,
        [Display(Name = "Solo Mayorista")]
        WholeSalerOnly = 3,
        [Display(Name = "Minorista y Mayorista")]
        PublishedForAll = 4
    }
}