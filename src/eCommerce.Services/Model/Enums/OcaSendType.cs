﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum OcaSendType
    {
        PAS = 1,
        PAP = 2
    }

    public enum AndreaniSendType
    {
        Sucursal = 1,
        Domicilio = 2
    }
}