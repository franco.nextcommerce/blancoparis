﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum TransactionTypeEnum
    {
        Ingreso = 1,
        Egreso = 2
    }

    public enum ExchangeDetailTypeEnum
    {
        Ingreso = 1,
        Egreso = 2
    }
}