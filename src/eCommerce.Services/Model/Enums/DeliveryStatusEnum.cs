﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum DeliveryStatusEnum
    {
        Pendiente = 1,
        Entregado = 2,
        EntregaParcial = 3,
        Preparado = 4
    }
}