﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum BannerPositionEnum
    {
        Central = 1,
        Categoria = 2,
        CategoriaDos = 3,
        //Categoria3 = 4,
        //LateralIzquierdo = 2,
        //LateralDerecho = 3,
        //FacebookCentral = 4,
        //Categoria = 5,
        Popup = 6,
    }

    public enum BannerScreenTypeEnum
    {
        Desktop = 1,
        Mobile = 2,
    }

    public enum BannerTextColorEnum
    {
        Black = 1,
        White = 2
    }
}