﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Services.Model.Enums
{
    public enum ErrorType
    {
        Exception = 1,
        CategoryNotFound = 2,
        ProductNotFound = 3,
        StoryNotFound = 4,
        UnauthorizedAction = 5
    }
}