﻿namespace eCommerce.Services.Model.Enums
{
    public enum RmaStatusEnum
    {
        Pendiente = 1,
        Resuelto = 2,
        Rechazado = 3
    }
}
