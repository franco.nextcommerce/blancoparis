//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eCommerce.Services.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class NewsletterProduct
    {
        public int NewsletterProductId { get; set; }
        public int NewsletterId { get; set; }
        public int ProductId { get; set; }
        public Nullable<decimal> SpecialPrice { get; set; }
        public string Observation { get; set; }
        public string GUID { get; set; }
        public Nullable<decimal> OldSpecialPrice { get; set; }
        public Nullable<decimal> Discount { get; set; }
    
        public virtual Newsletter Newsletter { get; set; }
        public virtual Newsletter Newsletter1 { get; set; }
        public virtual Product Product { get; set; }
    }
}
