﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class PrintGridViewsHelper
{
        /*public static GridViewSettings CreateExportGridViewSettingsForContacts()
        {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "Contactos";
            settings.KeyFieldName = "ContactId";

            settings.Width = Unit.Percentage(100);
            settings.Columns.Add("Name", "Nombre");
            settings.Columns.Add("Description", "Descripción");
            settings.Columns.Add("EMail");
            settings.Columns.Add("PhoneNumber", "Teléfono");
            var columnAddedDate = settings.Columns.Add("AddedDate", "Fecha");
            columnAddedDate.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            columnAddedDate.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
                       
            settings.SettingsExport.RenderBrick = (sender, e) => {
                if(e.RowType == GridViewRowType.Data && e.VisibleIndex % 2 == 0)
                    e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0xEE, 0xEE, 0xEE);
            };

            return settings;
        }

        public static GridViewSettings CreateExportGridViewSettingsForNewsLetters()
        {
            GridViewSettings settings = new GridViewSettings();
            
            settings.Name = "NewsLetters";
            settings.KeyFieldName = "NewsLetterId";
            
            settings.Columns.Add("Name", "Nombre y Apellido");
            settings.Columns.Add("Email");
            var columnAddedDate = settings.Columns.Add("AddedDate", "Fecha Alta");
            columnAddedDate.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            columnAddedDate.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";

            settings.SettingsExport.RenderBrick = (sender, e) =>
            {
                if (e.RowType == GridViewRowType.Data && e.VisibleIndex % 2 == 0)
                    e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0xEE, 0xEE, 0xEE);
            };

            return settings;
        }

        public static GridViewSettings CreateExportGridViewSettingsForOrders()
        {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "Ordenes";
            settings.KeyFieldName = "OrderId";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("OrderId", "#ID");
            settings.Columns.Add("LastName", "Apellido");
            settings.Columns.Add("FirstName", "Nombre");
            settings.Columns.Add("PhoneNumber", "Teléfono");
            settings.Columns.Add("CellPhone", "Celular");
            settings.Columns.Add("Email", "Email");

            /*
            var columnPrecio = settings.Columns.Add("Discount", "Desc. (%)");
            columnPrecio.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            columnPrecio.PropertiesEdit.DisplayFormatString = "{0}";
            */

            /*settings.Columns.Add("DeliveryAddress", "Dirección Envio");
            
            var columnDeliveryDate = settings.Columns.Add("DeliveryDate", "Fecha Envío");
            columnDeliveryDate.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            columnDeliveryDate.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";

            var columnAddedDate = settings.Columns.Add("AddedDate", "Alta");
            columnAddedDate.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            columnAddedDate.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
            
            settings.Columns.Add("OrderStatus", "Estado");

            settings.SettingsExport.RenderBrick = (sender, e) =>
            {
                if (e.RowType == GridViewRowType.Data && e.VisibleIndex % 2 == 0)
                    e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0xEE, 0xEE, 0xEE);
            };
            
            return settings;
        }

        public static GridViewSettings CreateExportGridViewSettingsForProducts()
        {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "Productos";
            settings.KeyFieldName = "ProductID";
           
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add("Code", "Cod.");
            settings.Columns.Add("Name", "Nombre");
            settings.Columns.Add("Description", "Descripción");
            
            var columnPrecio = settings.Columns.Add("RetailPrice", "Precio Minorista");
            columnPrecio.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            columnPrecio.PropertiesEdit.DisplayFormatString = "c2";

            var columnWholesalePrice = settings.Columns.Add("WholesalePrice", "Precio Mayorista");
            columnWholesalePrice.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            columnWholesalePrice.PropertiesEdit.DisplayFormatString = "c2";
            
            settings.Columns.Add("CategoryDescription", "Categoría");
            settings.Columns.Add("OnSale", "Outlet", MVCxGridViewColumnType.CheckBox);
            settings.Columns.Add("IsPublished", "Publicado", MVCxGridViewColumnType.CheckBox);

            return settings;
        }

        public static GridViewSettings CreateExportGridViewSettingsForStock()
        {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "gvStock";
            settings.KeyFieldName = "StockID";

            settings.Width = Unit.Percentage(100);
            settings.Columns.Add("Code", "Código");
            settings.Columns.Add("Name", "Nombre Producto");
            settings.Columns.Add("Color");
            settings.Columns.Add("Size", "Talle");
            settings.Columns.Add("RealQuantity", "Cantidad Real");
            settings.Columns.Add("VirtualQuantity", "Cantidad Virtual");


            return settings;
        }

        public static GridViewSettings CreateExportGridViewSettingsForCustomers()
        {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "Clientes";
            settings.KeyFieldName = "CustomerId";

            settings.Columns.Add("LastName", "Apellido");
            settings.Columns.Add("FirstName", "Nombre");
            settings.Columns.Add("Email", "EMail");
            settings.Columns.Add("City", "Ciudad");
            settings.Columns.Add("Address", "Dirección");
            settings.Columns.Add("PhoneNumber", "Teléfono");
            settings.Columns.Add("CellPhone", "Celular");
            settings.Columns.Add("IsWholesaler", "Mayorista");

            return settings;
        }

        public static GridViewSettings CreateExportGridViewSettingsForFranchises()
        {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "Franquicias";
            settings.KeyFieldName = "FranchiseId";
            
            settings.Columns.Add("FirstName", "Nombre");
            settings.Columns.Add("LastName", "Apellido");
            settings.Columns.Add("Address", "Dirección");
            settings.Columns.Add("PhoneNumber", "Teléfono");
            settings.Columns.Add("Cellular", "Celular");
            var columnAddedDate = settings.Columns.Add("AddedDate", "Fecha Alta");
            columnAddedDate.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            columnAddedDate.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";

            return settings;
        }

        public static GridViewSettings CreateExportGridViewSettingsForCoupons()
        {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "Cupones";
            settings.KeyFieldName = "CouponId";

            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("Campaign", "Campaña");
            settings.Columns.Add("Code", "Código");
            settings.Columns.Add("Percentage", "Porcentaje");
            settings.Columns.Add("ExpirationDate", "Vencimiento");

            return settings;
        }*/

    }

    
