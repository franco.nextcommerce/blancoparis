/*
   viernes, 8 de enero de 20213:52:28 p. m.
   User: 
   Server: DESKTOP-7U4DUVD\SQLEXPRESS
   Database: Plume
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DeliveryMethod
	DROP CONSTRAINT DF_DeliveryMethod_RequiresData
GO
ALTER TABLE dbo.DeliveryMethod
	DROP CONSTRAINT DF_DeliveryMethod_Active
GO
ALTER TABLE dbo.DeliveryMethod
	DROP CONSTRAINT DF_DeliveryMethod_IsTransport
GO
ALTER TABLE dbo.DeliveryMethod
	DROP CONSTRAINT DF_DeliveryMethod_ShowLocales
GO
ALTER TABLE dbo.DeliveryMethod
	DROP CONSTRAINT DF_DeliveryMethod_IsLocal
GO
ALTER TABLE dbo.DeliveryMethod
	DROP CONSTRAINT DF_DeliveryMethod_IsProvince
GO
ALTER TABLE dbo.DeliveryMethod
	DROP CONSTRAINT DF_DeliveryMethod_Enabled
GO
CREATE TABLE dbo.Tmp_DeliveryMethod
	(
	DeliveryMethodId int NOT NULL IDENTITY (1, 1),
	Name nvarchar(255) NOT NULL,
	Description nvarchar(250) NULL,
	Cost decimal(18, 2) NULL,
	RequiresData bit NOT NULL,
	Active bit NOT NULL,
	ExcludedPaymetnMethods nvarchar(250) NULL,
	IsTransport bit NOT NULL,
	ShowLocales bit NOT NULL,
	IsLocal bit NOT NULL,
	IsProvince bit NOT NULL,
	Enabled bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_DeliveryMethod SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_DeliveryMethod ADD CONSTRAINT
	DF_DeliveryMethod_RequiresData DEFAULT ((0)) FOR RequiresData
GO
ALTER TABLE dbo.Tmp_DeliveryMethod ADD CONSTRAINT
	DF_DeliveryMethod_Active DEFAULT ((0)) FOR Active
GO
ALTER TABLE dbo.Tmp_DeliveryMethod ADD CONSTRAINT
	DF_DeliveryMethod_IsTransport DEFAULT ((0)) FOR IsTransport
GO
ALTER TABLE dbo.Tmp_DeliveryMethod ADD CONSTRAINT
	DF_DeliveryMethod_ShowLocales DEFAULT ((0)) FOR ShowLocales
GO
ALTER TABLE dbo.Tmp_DeliveryMethod ADD CONSTRAINT
	DF_DeliveryMethod_IsLocal DEFAULT ((0)) FOR IsLocal
GO
ALTER TABLE dbo.Tmp_DeliveryMethod ADD CONSTRAINT
	DF_DeliveryMethod_IsProvince DEFAULT ((0)) FOR IsProvince
GO
ALTER TABLE dbo.Tmp_DeliveryMethod ADD CONSTRAINT
	DF_DeliveryMethod_Enabled DEFAULT ((0)) FOR Enabled
GO
SET IDENTITY_INSERT dbo.Tmp_DeliveryMethod ON
GO
IF EXISTS(SELECT * FROM dbo.DeliveryMethod)
	 EXEC('INSERT INTO dbo.Tmp_DeliveryMethod (DeliveryMethodId, Name, Description, Cost, RequiresData, Active, ExcludedPaymetnMethods, IsTransport, ShowLocales, IsLocal, IsProvince, Enabled)
		SELECT DeliveryMethodId, Name, Description, Cost, RequiresData, Active, ExcludedPaymetnMethods, IsTransport, ShowLocales, IsLocal, IsProvince, Enabled FROM dbo.DeliveryMethod WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_DeliveryMethod OFF
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_DeliveryMethod
GO
DROP TABLE dbo.DeliveryMethod
GO
EXECUTE sp_rename N'dbo.Tmp_DeliveryMethod', N'DeliveryMethod', 'OBJECT' 
GO
ALTER TABLE dbo.DeliveryMethod ADD CONSTRAINT
	PK_DeliveryMethod PRIMARY KEY CLUSTERED 
	(
	DeliveryMethodId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_DeliveryMethod FOREIGN KEY
	(
	DeliveryMethodId
	) REFERENCES dbo.DeliveryMethod
	(
	DeliveryMethodId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
