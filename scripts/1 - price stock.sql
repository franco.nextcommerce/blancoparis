/*
   Thursday, March 11, 202111:15:15 AM
   User: 
   Server: (LocalDb)\MSSQLLocalDB
   Database: BlancoParis
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Stock ADD
	Price decimal(18, 2) NULL
GO
ALTER TABLE dbo.Stock SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
